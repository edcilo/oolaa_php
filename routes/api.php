<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Stylist'], function () {
    Route::get('/stylist/search', 'SearchController@stylists');
    Route::get('/services/list', 'SearchController@services');
    Route::get('/service-categories', 'SearchController@serviceCategories');
});

Route::group(['namespace' => 'User'], function () {
    Route::post('/user/profile/{customer}', 'ApiController@customer');
    Route::get('/user/locations', 'ApiController@locations');
    Route::post('/user/credit-cards', 'ApiController@creditCards');
    Route::delete('/user/credit-cards/{credit_card}', 'ApiController@destroyCreditCards');
});

Route::group(['namespace' => 'Site'], function () {
    Route::get('/zone/states', 'ZoneController@states');
    Route::get('/zone/state/{state}/municipalities', 'ZoneController@municipalities');
    Route::get('/zone/municipality/{municipality}/colonies', 'ZoneController@colonies');
});
