<?php

Route::get('/', function () {
    return view('site.welcome');
})->name('welcome');

Route::get('about-us', function () {
    return view('site.about_us');
})->name('about.us');

Route::get('/faqs', function () {
    return view('site.faqs');
})->name('faqs');

Route::get('/terms', function () {
    return view('site.terms');
})->name('terms');

Route::get('/cancellation-policies', function () {
    return view('site.policies');
})->name('policies');

Route::get('/thanks', function () {
    return view('site.thanks');
})->name('thanks');
