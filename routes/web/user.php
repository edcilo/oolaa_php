<?php

Route::group(['namespace' => 'User', 'middleware' => 'auth'], function () {
    Route::get('/profile/{name}/{user}', 'ProfileController@home')->name('profile');
    Route::post('/profile/{user}', 'ProfileController@update')->name('profile.update');
    Route::post('/profile/{user}/update-password', 'ProfileController@updatePassword')->name('profile.update.password');

    // Route::get('/profile/{name}/{user}/payment-methods', 'ProfileController@paymentMethods')->name('payment.methods');
    Route::post('/profile/{name}/{user}/payment-methods', 'ProfileController@addPaymentMethods')->name('payment.methods.add');
    Route::delete('/profile/{name}/{user}/payment-methods', 'ProfileController@destroyPaymentMethods')->name('payment.methods.destroy');

    Route::post('/profile/{name}/{user}/locations', 'ProfileController@addLocation')->name('location.add');
    Route::delete('/profile/{name}/{user}/locations', 'ProfileController@desrtoyLocation')->name('location.destroy');
});
