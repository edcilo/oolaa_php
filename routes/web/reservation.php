<?php

Route::group(['namespace' => 'Reservation'], function () {
    Route::post('/reservation', 'ApiController@store')->middleware('auth.ajax');

    Route::get('/reservation/loading/{reservation}/{stylist}/{token}/{action}', 'ReservationController@getSessionId')
        ->name('reservation.session.id')
        ->middleware('reservation.token');

    Route::post('/reservation/stylist/accept/{reservation}/{stylist}/{token}', 'ReservationController@accept')
        ->name('reservation.stylist.accept')
        ->middleware('reservation.token');

    Route::post('/reservation/stylist/decline/{reservation}/{stylist}/{token}', 'ReservationController@decline')
        ->name('reservation.stylist.decline')
        ->middleware('reservation.token');

    Route::post('/reservation/stylist/re-open/{reservation}/{stylist}/{token}', 'ReservationController@reOpen')
        ->name('reservation.stylist.reopen')
        ->middleware('reservation.token');

    Route::post('/reservation/stylist/auto-stylist/{reservation}/{stylist}/{token}', 'ReservationController@acceptAutoStylist')
        ->name('reservation.stylist.accept.auto')
        ->middleware('reservation.token');

    Route::post('/reservation/customer/{customer}/cancel/{reservation}', 'ReservationController@cancel')
        ->name('reservation.customer.cancel');
});
