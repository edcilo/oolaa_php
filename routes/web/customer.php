<?php

Route::group(['namespace' => 'User'], function () {
    Route::post('/customer/profile', 'ProfileController@apiProfile')->middleware('auth.ajax');

    Route::get('/customer/reservations', 'ReservationsController@index')
        ->name('customer.reservations')
        ->middleware('auth');

    Route::get('/customer/reservations/accepted', 'ReservationsController@accepted')
        ->name('customer.reservations.accepted')
        ->middleware('auth');

    Route::get('/customer/reservations/pending', 'ReservationsController@pending')
        ->name('customer.reservations.pending')
        ->middleware('auth');

    Route::get('/customer/reservations/canceled', 'ReservationsController@canceled')
        ->name('customer.reservations.canceled')
        ->middleware('auth');

    Route::get('/customer/reservations/finished', 'ReservationsController@finished')
        ->name('customer.reservations.finished')
        ->middleware('auth');
});
