<?php

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => ['auth', 'can:dashboard.access']
], function () {
    Route::get('/', function () {
        return view('admin.dashboard.index');
    })->name('dashboard');

    // Zones
    Route::delete('zones/delete', 'ZoneController@destroyMultiple')->name('zones.destroy.multiple');
    Route::post('zones/{zone}/add-colonies', 'ZoneController@addColony')->name('zones.add.colony');
    Route::delete('zones/{zone}/remove-colonies', 'ZoneController@removeColonies')->name('zones.remove.colonies');
    Route::resource('zones', 'ZoneController');

    // States
    Route::delete('states/delete', 'StateController@destroyMultiple')->name('states.destroy.multiple');
    Route::resource('states', 'StateController');

    // Services
    Route::get('services/trash', 'ServiceController@trash')->name('services.trash');
    Route::post('services/restore', 'ServiceController@restore')->name('services.restore');
    Route::get('services/attach/{user}', 'ServiceController@attach')->name('services.attach');
    Route::post('services/attach', 'ServiceController@attachStore')->name('services.attach.store');
    Route::post('services/{service}/add-stylist', 'ServiceController@addStylist')->name('services.add.stylist');
    Route::delete('services/{service}/remove-stylist', 'ServiceController@removeStylists')->name('services.remove.stylists');
    Route::delete('services/delete', 'ServiceController@destroyMultiple')->name('services.destroy.multiple');
    Route::post('services/restore/multiple', 'ServiceController@restoreMultiple')->name('services.restore.multiple');
    Route::resource('services', 'ServiceController');
    Route::resource('service-categories', 'ServiceCategoryController');

    // User routes
    Route::post('users/{user}/create-profile-stylist', 'UserController@createStylistProfile')->name('users.create.profile.stylist');
    Route::post('users/{user}/active', 'UserController@active')->name('users.active');
    Route::post('users/{user}/deactivate', 'UserController@deactivate')->name('users.deactivate');
    Route::get('users/trash', 'UserController@trash')->name('users.trash');
    Route::post('users/restore', 'UserController@restore')->name('users.restore');
    Route::delete('users/delete', 'UserController@destroyMultiple')->name('users.destroy.multiple');
    Route::post('users/restore/multiple', 'UserController@restoreMultiple')->name('users.restore.multiple');
    Route::resource('users', 'UserController');

    Route::delete('oauth/{oauth}', 'UserController@oauthUnlink')->name('oauth.destroy');

    // Credit card routes
    Route::get('credit-cards/create/{user}', 'CreditCardController@create')->name('credit-cards.create');
    Route::resource('credit-cards', 'CreditCardController', ['only' => ['store', 'destroy']]);

    // Address routes
    Route::get('addresses/create/{user_id}', 'AddressController@create')->name('addresses.create');
    Route::resource('addresses', 'AddressController', ['only' => [
        'store', 'edit', 'update', 'destroy'
    ]]);

    // Categories
    Route::get('categories/trash', 'CategoryController@trash')->name('categories.trash');
    Route::post('categories/restore', 'CategoryController@restore')->name('categories.restore');
    Route::delete('categories/delete', 'CategoryController@destroyMultiple')->name('categories.destroy.multiple');
    Route::post('categories/multiple', 'CategoryController@restoreMultiple')->name('categories.restore.multiple');
    Route::post('categories/{category}/services', 'CategoryController@servicesSync')->name('categories.services.sync');
    Route::post('categories/{category}/stylist', 'CategoryController@stylistAttach')->name('categories.stylist.attach');
    Route::delete('categories/{category}/stylist/{stylist}', 'CategoryController@stylistDetach')->name('categories.stylist.detach');
    Route::resource('categories', 'CategoryController');

    // Stylist
    Route::get('stylists/{stylist}/calendar', 'StylistController@calendar')->name('stylists.calendar');
    Route::post('stylists/{stylist}/calendar', 'StylistController@storeEvent')->name('stylists.calendar.store');
    Route::post('stylists/{stylist}/calendar/weekdays', 'StylistController@updateWeekdays')->name('stylists.calendar.weekdays');
    Route::put('stylists/{stylist_id}/change-status', 'StylistController@changeStatus')->name('stylists.change.status');
    Route::delete('stylists/delete', 'StylistController@destroyMultiple')->name('stylists.destroy.multiple');
    Route::resource('stylists', 'StylistController');

    // Curriculum
    Route::get('curriculum/create/{user_id}', 'CurriculumController@create')->name('curriculum.create');
    Route::resource('curriculum', 'CurriculumController', ['only' => [
        'store', 'edit', 'update', 'destroy'
    ]]);

    // Photobook
    Route::put('photobook/{photobook}/publish', 'PhotoBookController@publishToggle')->name('photobook.publish');
    Route::resource('photobook', 'PhotoBookController', ['only' => [
        'store', 'edit', 'update', 'destroy'
    ]]);

    // Reservations
    Route::get('reservations/trash', 'ReservationController@trash')->name('reservations.trash');
    Route::post('reservations/restore', 'ReservationController@restore')->name('reservations.restore');
    Route::delete('reservations/delete', 'ReservationController@destroyMultiple')->name('reservations.destroy.multiple');
    Route::post('reservations/restore/multiple', 'ReservationController@restoreMultiple')->name('reservations.restore.multiple');
    Route::put('reservations/{reservation}/auto-stylist/toggle', 'ReservationController@autoStylistToggle')->name('reservations.auto.stylist.toggle');
    Route::post('reservations/{reservation}/add-service', 'ReservationController@addService')->name('reservations.add.service');
    Route::post('reservations/{reservation}/pay', 'ReservationController@pay')->name('reservation.pay');
    Route::post('reservations/{reservation}/coupon', 'ReservationController@addCoupon')->name('reservations.add.coupon');
    Route::delete('reservations/{reservation}/coupon', 'ReservationController@removeCoupon')->name('reservations.remove.coupon');
    Route::post('reservations/{reservation}/notify', 'ReservationController@notify')->name('reservation.notify');
    Route::post('reservations/{reservation}/reject', 'ReservationController@reject')->name('reservation.reject');
    Route::post('reservations/{reservation}/cancel', 'ReservationController@cancel')->name('reservation.cancel');
    Route::resource('reservations', 'ReservationController');

    // Items
    Route::resource('item', 'ItemController', ['only' => [
        'update', 'destroy'
    ]]);

    // Coupons
    Route::delete('coupons/delete', 'CouponController@destroyMultiple')->name('coupons.destroy.multiple');
    Route::resource('coupons', 'CouponController');

    // CMS
    Route::group(['prefix' => 'cms'], function () {
        Route::get('home/carousel', 'CmsCarouselController@index')->name('cms.home.carousel');
        Route::post('home/carousel', 'CmsCarouselController@store')->name('cms.home.carousel.store');
        Route::delete('home/carousel', 'CmsCarouselController@destroy')->name('cms.home.carousel.destroy');

        Route::get('home/top-stylists', 'CmsTopStylistController@index')->name('cms.home.top.stylist');
        Route::post('home/top-stylists', 'CmsTopStylistController@store')->name('cms.home.top.stylist.store');
        Route::delete('home/top-stylists', 'CmsTopStylistController@destroy')->name('cms.home.top.stylist.destroy');

        Route::get('home/video', 'CmsVideoController@index')->name('cms.home.video');
        Route::post('home/video', 'CmsVideoController@store')->name('cms.home.video.store');
        Route::delete('home/video', 'CmsVideoController@destroy')->name('cms.home.video.destroy');

        Route::get('terms/terms', 'CmsTermsController@index')->name('cms.terms');
        Route::put('terms/terms', 'CmsTermsController@update')->name('cms.terms.update');

        Route::get('terms/policy', 'CmsPolicyController@index')->name('cms.policy');
        Route::put('terms/policy', 'CmsPolicyController@update')->name('cms.policy.update');

        Route::get('faqs', 'CmsFaqsController@index')->name('cms.faqs');
        Route::put('faqs', 'CmsFaqsController@update')->name('cms.faqs.update');

        Route::get('about-us', 'CmsAboutUsController@index')->name('cms.about.us');
        Route::put('about-us', 'CmsAboutUsController@update')->name('cms.about.us.update');

        Route::get('contact', 'CmsContactController@index')->name('cms.contact');
        Route::put('contact', 'CmsContactController@update')->name('cms.contact.update');

        Route::get('about-us', 'CmsAboutUsController@index')->name('cms.about.us');
        Route::put('about-us', 'CmsAboutUsController@update')->name('cms.about.us.update');
    });
});
