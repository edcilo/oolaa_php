<?php

Route::group(['namespace' => 'User'], function () {
    Route::get('/stylist/reservations', 'StylistReservationsController@index')
        ->name('stylist.reservations')
        ->middleware('auth');

    Route::get('/stylist/reservations/accepted', 'StylistReservationsController@accepted')
        ->name('stylist.reservations.accepted')
        ->middleware('auth');

    Route::get('/stylist/reservations/pending', 'StylistReservationsController@pending')
        ->name('stylist.reservations.pending')
        ->middleware('auth');

    Route::get('/stylist/reservations/canceled', 'StylistReservationsController@canceled')
        ->name('stylist.reservations.canceled')
        ->middleware('auth');

    Route::get('/stylist/reservations/finished', 'StylistReservationsController@finished')
        ->name('stylist.reservations.finished')
        ->middleware('auth');
});

Route::group(['namespace' => 'Stylist'], function () {
    Route::get('/stylist/{stylist}/calendar', 'StylistCalendarController@index')
        ->name('stylist.calendar')
        ->middleware('auth');

    Route::post('/stylist/{stylist}/calendar/weekdays', 'StylistCalendarController@weekdays')
        ->name('stylist.calendar.weekdays.update')
        ->middleware('auth');

    Route::post('/stylist/{stylist}/calendar/event', 'StylistCalendarController@eventStore')
        ->name('stylist.calendar.event.store')
        ->middleware('auth');
});
