<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test/mail', 'TestController@emailTest');

Route::get('test/imgs/services', 'TestController@imgsServices');
Route::get('test/imgs/photobook', 'TestController@imgsPhotobook');
Route::get('test/imgs/users', 'TestController@imgsUsers');

require __DIR__ . '/web/statics.php';
require __DIR__ . '/web/reservation.php';

// COUPONS
Route::get('/gift', 'Site\CouponController@gift')->name('gift.coupon');
Route::post('/gift', 'Site\CouponController@store')->name('gift.store');

// AUTHENTICATION
Auth::routes();
Route::get('/verify/token/{token}', 'Auth\VerificationController@verify')->name('auth.verify');
Route::get('/verify/resend', 'Auth\VerificationController@resend')->name('auth.verify.resend');

// OAUTH
Route::get('auth/{third_party}', 'Auth\OAuthController@redirect')->name('auth.oauth');
Route::get('login/facebook', 'Auth\OAuthController@facebookHandler')->name('auth.facebook');
Route::get('login/google', 'Auth\OAuthController@googleHandler')->name('auth.google');

Route::get('stylist/search', 'Stylist\SearchController@search')->name('stylist.search');

require __DIR__ . '/web/customer.php';
require __DIR__ . '/web/stylist.php';

// STYLIST
Route::get('stylist/register', 'Site\RegisterStylistController@register')->name('public.stylist.register');
Route::get('stylist/{slug}/{stylist}', 'Site\HomeController@stylist')->name('stylist');

require __DIR__ . '/web/user.php';

require __DIR__ . '/web/admin.php';
