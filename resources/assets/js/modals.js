const $ = (window.jQuery = require("jquery"));
require("./../../../node_modules/magnific-popup/dist/jquery.magnific-popup.js");

$(function() {
    $(".popup-modal").magnificPopup({
        type: "inline",
        closeOnBgClick: true
    });
});
