export default {
    "card_number length is invalid":
        "La longitud del número de la tarjeta es invalido",
    "card_number length must be between 12 and 19, card_number length is invalid":
        "El número de la tarjeta debe tener entre 12 y 19 dígitos",
    "card_number must contain only digits":
        "El número de la tarjeta solo puede contener dígitos",

    "cvv2 length must be 3 digits": "La longitud del CVC debe ser de 3 dígitos",
    "cvv2 length must be 4 digits": "La longitud del CVC debe ser de 4 dígitos",
    "The card number verification digit is invalid":
        "El número de verificación (CVC) es invalido",

    "The expiration date has already passed":
        "La fecha de vencimiento ya ha pasado",

    "expiration_month as is invalid, valid expirations months are 01 to 12":
        "El mes de vencimiento es invalido, los valores validos son de 01 a 12",
    "expiration_month is required":
        "El mes de vencimiento de la tarjeta es obligatorio",

    "expiration_year as is invalid, valid expirations year are 01 to 99":
        "El año de vencimiento es invalido, los valores validos son desde 01 hasta 99",
    "expiration_year is required":
        "El año de vencimiento de la tarjeta es obligatorio"
};
