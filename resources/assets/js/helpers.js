export default {
    loadScript: function(src, callback) {
        const script = document.createElement("script");
        document.body.appendChild(script);

        if (callback) {
            script.onload = callback;
        }

        script.src = src;
    }
};
