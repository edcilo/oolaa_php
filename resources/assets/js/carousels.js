const $ = (window.jQuery = require("jquery"));
require("./../../../node_modules/slick-carousel/slick/slick.js");

$(document).ready(function() {
    if ($("#welcome").length > 0) {
        $(".hero-carousel").slick({
            arrows: false,
            dots: true,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000
        });

        $(".top-stylists-carousel").slick({
            infinite: true,
            arrows: false,
            slidesToScroll: 1,
            slidesToShow: 5,
            autoplay: true,
            autoplaySpeed: 2000,
            responsive: [
                {
                    breakpoint: 1087,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 878,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 668,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 423,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    }

    if ($("#stylist-profile").length > 0) {
        $(".stylist-photo-book-carousel").slick({
            arrows: true,
            dots: false,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            slidesToShow: 4
        });
    }

    if ($("#search").length > 0) {
        $(".stylist-card-portfolio-carousel").slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            prevArrow:
                '<button type="button" class="slick-prev-oolaa"><i class="fa fa-chevron-left"></i></button>',
            nextArrow:
                '<button type="button" class="slick-next-oolaa"><i class="fa fa-chevron-right"></i></button>'
        });
    }
});
