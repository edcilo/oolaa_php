/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import "./carousels";
import "./modals";
import "./userProfile";
import "./calendar";
import underscore from "vue-underscore";
import Vuelidate from "vuelidate";
import VueResource from "vue-resource";
import VTooltip from "v-tooltip";

const edcNav = require("./../../../node_modules/edc-nav/edc-nav/edc-nav");

require("./bootstrap");
window.Vue = require("vue");

Vue.use(underscore);
Vue.use(Vuelidate);
Vue.use(VueResource);
Vue.use(VTooltip);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component("example", require("./components/Example.vue"));
Vue.component("search", require("./components/Search.vue"));

/*
const app = new Vue({
    el: '#app'
});
*/

if ($('#reservation-app').length > 0) {
    new Vue({
        el: "#reservation-app"
    });
}

$(function() {
    edcNav.init();
});
