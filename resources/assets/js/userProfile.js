const $ = (window.jQuery = require("jquery"));
require("./../../../node_modules/slick-carousel/slick/slick.js");

$(document).ready(function() {
    // eliminate some day
    if ($("#user-profile").length > 0) {
        $("#btn-show-profile-edit").click(function() {
            $("#btn-show-profile-edit").hide();
            $(".user-metadata").hide();

            $("#form-profile-update").show();
        });

        $("#btn-hide-profile-edit").click(function() {
            $("#btn-show-profile-edit").show();
            $(".user-metadata").show();

            $(".form-edit").hide();
        });

        $("#btn-show-update-password").click(function() {
            $("#btn-show-update-password").hide();

            $(".user-update-password-form").show();
        });

        $(".btn-password-close").click(function() {
            $("#btn-show-update-password").show();

            $(".user-update-password-form").hide();
        });

        $("#btn-show-credit-card-form").click(function() {
            $("#btn-show-credit-card-form").hide();

            $("#form-credit-card").show();
        });

        $("#btn-hide-credit-card-form").click(function() {
            console.error("edc");
            $("#btn-show-credit-card-form").show();

            $("#form-credit-card").hide();
        });

        $("#btn-show-location-form").click(function() {
            $("#btn-show-location-form").hide();

            $("#form-location").show();
        });

        $("#btn-hide-location-form").click(function() {
            $("#btn-show-location-form").show();

            $("#form-location").hide();
        });
    }
});
