"use strict";

var checkAll = function(triggerSelector, checkboxesSelector) {
    $(triggerSelector).change(function() {
        $(checkboxesSelector).prop("checked", $(this).is(":checked"));
    });
};

var iCheck_checkAll = function(triggerSelector, checkboxesSelector) {
    $(triggerSelector).on("ifChecked", function() {
        $(checkboxesSelector).iCheck("check");
    });

    $(triggerSelector).on("ifUnchecked", function() {
        $(checkboxesSelector).iCheck("uncheck");
    });
};

var checkBoxes = function(selector) {
    $(selector).iCheck({
        checkboxClass: "icheckbox_square-green"
    });
};

var select2 = function(selector) {
    $(selector).select2();
};

$(function() {
    $(".datepicker").datepicker({
        format: "yyyy-mm-dd",
        clearBtn: true,
        language: "es",
        autoclose: true,
        orientation: "bottom"
    });

    $(".datetimepicker").datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss",
        locale: "es"
    });
});
