import $ from "jquery";
import 'fullcalendar';
import 'eonasdan-bootstrap-datetimepicker';

$(function() {
    const opts = { format: 'YYYY-MM-DD HH:mm:ss' };
    $('#start').datetimepicker(opts);
    $('#end').datetimepicker(opts);

    if ($("#calendar").length === 0) {
        return;
    }

    let eventsList = [];

    for (let i = 0; i < $events.length; i++) {
        eventsList.push($events[i]);
    }

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        buttonText: {
            today: "Hoy",
            month: "Mes",
            week: "Semana",
            day: "Día"
        },
        dayRender: function(date, cell) {
            for (let i = 0; i < $businessDays.length; i++) {
                let d = $businessDays[i];

                if (d.number === date.weekday()) {
                    return;
                }
            }

            $(cell).addClass('disabled');
        },
        events: eventsList,
        eventClick: function (data) {
            console.warn(data);
        }
    })
});