@extends('layouts.app')

@section('page', 'policies')

@section('content')
    <div class="container">
        <div class="panel">
            <div class="panel-body">
                {!! $schema['policy']['text'] !!}
            </div>
        </div>
    </div>
@endsection
