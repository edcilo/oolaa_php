@extends('layouts.app')

@section('page', 'faqs')

@section('content')
    <div class="container">
        <div class="panel">
            <div class="panel-body">
                {!! $schema['faqs']['text'] !!}
            </div>
        </div>
    </div>
@endsection
