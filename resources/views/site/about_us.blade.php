@extends('layouts.app')

@section('page', 'about-us')

@section('content')
    <div class="container">
        <div class="panel">
            <div class="panel-body">
                {!! $schema['about_us']['text'] !!}
            </div>
        </div>
    </div>
@endsection
