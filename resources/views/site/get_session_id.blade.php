@extends('layouts.app')

@section('page', 'search')

@section('content')
    <div style="text-align: center">
        <h2>LOADING...</h2>
        <p>Getting openpay session id </p>

        <form
            id="reservation-form"
            method="POST"
            action="{{ route($route, [
                'reservation' => $reservation,
                'stylist' => $stylist,
                'token' => $token,
                'new_stylist' => $new_stylist,
                'item_id' => $item
            ]) }}">
            {{ csrf_field() }}

            <input type="hidden" name="device_session_id" value="" id="device_session_id">
        </form>
    </div>
@endsection

@push('js')
    <script type="text/javascript" src="https://openpay.s3.amazonaws.com/openpay.v1.min.js"></script>
    <script type='text/javascript' src="https://openpay.s3.amazonaws.com/openpay-data.v1.min.js"></script>

    <script type="text/javascript">
        $(function () {
            OpenPay.setId("{{ config('payment.credentials.id') }}");
            OpenPay.setApiKey("{{ config('payment.credentials.key') }}");
            OpenPay.setSandboxMode({{ config('app.debug') }});

            var deviceSessionId = OpenPay.deviceData.setup("credit-card-form", "device_session_id");
            $('#device_session_id').val(deviceSessionId);

            $('#reservation-form').submit();
        });
    </script>
@endpush
