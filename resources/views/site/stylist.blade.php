@extends('layouts.app')

@section('page', 'stylist-profile')

@section('content')
    <div class="container">
        <div class="stylist-header">
            <div class="stylist-header-avatar">
                <img src="{{ $stylist->user->profile->avatar }}" alt="avatar">
            </div>
            <div class="stylist-header-data">
                <div class="stylist-header-name">
                    <h2>{{ $stylist->user->name }}</h2>
                </div>
                <div class="stylist-header-rate">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>
        </div>

        <div class="stylist-photo-book">
            <div class="stylist-photo-book-carousel">
                @foreach($photoBooks as $photo)
                    <div class="stylist-photo-book-item" style="background-image: url({{ $photo->image }})"></div>
                @endforeach
            </div>
        </div>

        <div class="stylist-photo-content row">
            <div class="col-md-4">
                Calendar
            </div>

            <div class="col-md-8">
                <div class="tabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#services" aria-controls="services" role="tab" data-toggle="tab">
                                {{ ucfirst(trans_choice('dictionary.services', 0)) }}
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#curriculum" aria-controls="curriculum" role="tab" data-toggle="tab">
                                {{ __('text.training_and_experience') }}
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#zones" aria-controls="zones" role="tab" data-toggle="tab">
                                {{ ucfirst(trans_choice('dictionary.zones', 0)) }}
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="services">
                            Services
                        </div>
                        <div role="tabpanel" class="tab-pane" id="curriculum">
                            <div class="curriculum training">
                                <h4>{{ ucfirst(__('dictionary.training')) }}</h4>
                                @foreach($curriculum_e as $item)
                                    {!! $item->data !!}
                                @endforeach
                            </div>

                            <div class="curriculum experience">
                                <h4>{{ ucfirst(__('dictionary.experience')) }}</h4>

                                @foreach($curriculum_w as $item)
                                    {!! $item->data !!}
                                @endforeach
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="zones">
                            @foreach($stylist->zones as $zone)
                                <span class="tag">{{ $zone->name }}</span>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
