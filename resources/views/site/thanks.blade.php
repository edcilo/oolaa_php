@extends('layouts.app')

@section('page', 'message')

@section('content')
    <div class="container">
        <div class="message">
            <h2 class="message-title">¡OOLAA {{ strtoupper(auth()->user()->name) }}!</h2>

            <p>Tu solicitud está siendo procesada</p>

            <p>Se te envió un correo con los datos de tu solicitud. </p>

            <p>Una vez que tu cita sea aceptada, recibirás un correo de confirmación.</p>

            <p>¡Muchas gracias! </p>

            <p>Nota: Verifica en tu bandeja de "Correo no deseado/spam". </p>

            <a href="{{ route('customer.reservations') }}" class="btn btn-success">Ir a mis reservaciones</a>
        </div>
    </div>
@endsection
