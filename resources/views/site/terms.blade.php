@extends('layouts.app')

@section('page', 'terms')

@section('content')
    <div class="container">
        <div class="panel">
            <div class="panel-body">
                {!! $schema['terms']['text'] !!}
            </div>
        </div>
    </div>
@endsection
