@inject('homeController', 'App\Http\Controllers\Site\HomeController')

@extends('layouts.app')

@section('page', 'welcome')

@section('content')
    <div class="hero-carousel">
        @foreach($schema['home']['carousel'] as $item)
            <div class="hero-carousel-cover" style="background-image: url({{ $item['picture'] }})">
                <div class="hero-content">
                    <div class="hero-wrapper">
                        <div class="hero-title">
                            <h2>OOLAA, El salón en tu casa</h2>
                        </div>
                        <div class="hero-links">
                            <a href="{{ route('stylist.search') }}" class="btn btn-success">¡Haz tu cita!</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="home-steps">
                    <h2 class="title">{{ __('text.how_does_it_work') }}</h2>

                    <div class="video">
                        {!! $schema['home']['video'] !!}
                    </div>

                    <div class="steps">
                        @foreach($schema['home']['steps'] as $step)
                            <div class="step">
                                <div class="step-wrapper">
                                    <h4 class="step-title">{{ $step['title'] }}</h4>

                                    <div class="step-icon">
                                        <img src="{{ $step['icon'] }}" alt="icon">
                                    </div>

                                    <div class="step-body">
                                        {{ $step['body'] }}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="our-services">
                    <h2 class="title">{{ __('text.our_services') }}</h2>

                    <div class="service-categories">
                        @foreach($homeController->serviceCategories() as $category)
                            <div class="category">
                                <div class="category-wrapper">
                                    <h4 class="category-title">
                                        {{ $category->name }}
                                    </h4>

                                    <div class="category-icon">
                                        <img src="{{ $category->icon }}" alt="icon">
                                    </div>

                                    <div class="category-body">
                                        <ul class="category-services">
                                            @foreach($category->services as $service)
                                                <li>
                                                    {{ $service->name }}
                                                    <span class="category-price pull-right">
                                                        @currency($service->price)
                                                    </span>
                                                </li>

                                                @if(count($service->children))
                                                    <ul>
                                                        @foreach($service->children as $son)
                                                            <li>
                                                                {{ $son->name }}

                                                                <span class="category-price pull-right">
                                                                    @currency($son->price)
                                                                </span>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="home-top-stylists">
        <h2 class="title">{{ __('text.top_five_stylists') }}</h2>

        <div class="top-stylists-carousel">
            @foreach($schema['home']['top_stylists'] as $stylist)
                <div class="top-stylist-carousel-item">
                    <div class="stylist-portfolio">
                        <img src="{{ $stylist['picture'] }}" alt="{{ $stylist['stylist']['user']['name'] }}">
                    </div>
                    <div class="stylist-profile">
                        <div class="stylist-avatar">
                            <img src="{{ $stylist['stylist']['user']['profile']['avatar'] }}" alt="{{ $stylist['stylist']['user']['name'] }}" width="75px" height="75px">
                        </div>
                        <div class="stylist-name">
                            <strong>{{ __('text.make_by') }}:</strong><br>
                            {{ $stylist['stylist']['user']['name'] }}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
