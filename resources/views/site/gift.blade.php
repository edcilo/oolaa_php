@extends('layouts.app')

@section('page', 'gift-coupon')

@section('content')
    <div class="container">
        <div class="gc-wrapper">
            <form action="{{ route('gift.store') }}" method="POST">
                {{ csrf_field()  }}

                <div id="first-step" class="{{ count($errors->all()) ? 'hidden' : ''  }}">
                    <div class="gc-header">
                        <h1>{{ __('text.select_the_services') }}</h1>
                    </div>
                    <div class="gc-body">
                        <div class="services">
                            @foreach($categories as $category)
                                <div class="services-list">
                                    <div class="services-list-wrapper">
                                        <div class="service-list-header">
                                            <div class="service-list-cover">
                                                <img src="{{ $category->cover }}" alt="cover" />
                                            </div>
                                            <div class="service-list-title">
                                                {{$category->name}}
                                            </div>
                                        </div>

                                        <div class="service-list-body">
                                            @include('layouts.partials.servicesList', ['services' => $category->services])
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="gc-footer">
                        <button class="btn btn-primary" id="fs-btn" type="button">{{ ucfirst(__('dictionary.next')) }}</button>
                    </div>
                </div>

                <div id="second-step" class="{{ count($errors->all()) ? 'show' : ''  }}">
                    <div class="gc-header">
                        <h1>{{ __('text.credit_card_registration_form') }}</h1>
                    </div>
                    <div class="gc-body">
                        <div class="gc-selected">
                            <div class="gc-details">
                                <p>Los servicios incluidos en tu cupón son los siguientes:</p>

                                <ul class="gc-services-list">
                                    <div class="gc-container">
                                        <li class="gc-service-item" id="gc-item">
                                            <span class="gc-service-name">...</span>
                                            <span class="gc-service-price">$ 0.00</span>
                                        </li>
                                    </div>
                                    <li class="gc-service-item gc-service-total">
                                        Total: <span class="gc-service-price gc-service-total-price">$ 0.00</span>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="gc-form-container">
                            <div class="row">
                                <div class="col-md-6">
                                    <p><strong>{{ ucfirst(__('dictionary.from')) }}:</strong></p>

                                    <div class="form-group {{ $errors->has('name_0') ? 'has-error' : '' }}">
                                        <input id="name_0" type="text" class="form-control" name="name_0" value="{{ old('name_0') }}" placeholder="{{ ucfirst(__('dictionary.name')) }}" required>

                                        @if ($errors->has('name_0'))
                                            <span class="help-block"><strong>{{ $errors->first('name_0') }}</strong></span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('email_0') ? 'has-error' : '' }}">
                                        <input id="email_0" type="email" class="form-control" name="email_0" value="{{ old('email_0') }}" placeholder="{{ ucfirst(__('dictionary.email')) }}" required>

                                        @if ($errors->has('email_0'))
                                            <span class="help-block"><strong>{{ $errors->first('email_0') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <p><strong>{{ ucfirst(__('dictionary.to')) }}:</strong></p>

                                    <div class="form-group {{ $errors->has('name_1') ? 'has-error' : '' }}">
                                        <input id="name_1" type="text" class="form-control" name="name_1" value="{{ old('name_1') }}" placeholder="{{ ucfirst(__('dictionary.name')) }}" required>

                                        @if ($errors->has('name_1'))
                                            <span class="help-block"><strong>{{ $errors->first('name_1') }}</strong></span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('email_1') ? 'has-error' : '' }}">
                                        <input id="email_1" type="email" class="form-control" name="email_1" value="{{ old('email_1') }}" placeholder="{{ ucfirst(__('dictionary.email')) }}" required>

                                        @if ($errors->has('email_1'))
                                            <span class="help-block"><strong>{{ $errors->first('email_1') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="line"></div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group {{ $errors->has('card_number') ? 'has-error' : '' }}">
                                        <input id="card_number" type="text" class="form-control" name="card_number" value="{{ old('card_number') }}" placeholder="{{ __('text.card_number') }}" required>

                                        @if ($errors->has('card_number'))
                                            <span class="help-block"><strong>{{ $errors->first('card_number') }}</strong></span>
                                        @endif
                                    </div>

                                    <div class="form-group {{ $errors->has('card_name') ? 'has-error' : '' }}">
                                        <input id="card_name" type="text" class="form-control" name="card_name" value="{{ old('card_name') }}" placeholder="{{ __('text.card_name') }}" required>

                                        @if ($errors->has('card_name'))
                                            <span class="help-block"><strong>{{ $errors->first('card_name') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-8">
                                    <div class="form-group {{ $errors->has('card_date') ? 'has-error' : '' }}">
                                        <input id="card_date" type="text" class="form-control" name="card_date" value="{{ old('card_date') }}" placeholder="{{ __('abbr.date_card') }}" required>

                                        @if ($errors->has('card_date'))
                                            <span class="help-block"><strong>{{ $errors->first('card_date') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group {{ $errors->has('card_cvc') ? 'has-error' : '' }}">
                                        <input id="card_cvc" type="text" class="form-control" name="card_cvc" value="{{ old('card_cvc') }}" placeholder="{{ __('abbr.cvc') }}" required>

                                        @if ($errors->has('card_cvc'))
                                            <span class="help-block"><strong>{{ $errors->first('card_cvc') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="gc-footer">
                        <button class="btn btn-success" type="submit">{{ __('text.buy_coupon') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('js')
    <script type="text/javascript">
        $(function () {
            var e = null;
            var s_l = [];
            var t = 0;
            var t_p = '$ _t';
            var n_i = null;

            var f = $('#first-step');
            var s = $('#second-step');
            var f_btn = $('#fs-btn');
            var c = $('.service-check');
            var st = $('.gc-service-total-price');
            var s_c = $('.gc-container');
            var s_i = $('#gc-item');

            s.hide();

            f_btn.click(function () {
                f.hide();
                s.show();
            });

            c.change(function (e) {
                var cd = $('.service-check:checked');
                t = 0;
                s_l = [];

                for (var i=0; i < cd.length; i++) {
                    e = $(cd[i]);
                    t += parseFloat(e.data('price'));

                    s_l.push({
                        name: e.data('name'),
                        price: t_p.replace('_t', parseFloat(e.data('price')).toFixed(2))
                    });
                }

                s_c.empty();

                for (var i = 0; i < s_l.length; i++) {
                    n_i = s_i.clone();

                    n_i.find('.gc-service-name').text(s_l[i].name)
                    n_i.find('.gc-service-price').text(s_l[i].price)

                    n_i.appendTo(s_c);
                }

                t = t_p.replace('_t', t.toFixed(2));

                st.text(t);
            });
        })
    </script>
@endpush
