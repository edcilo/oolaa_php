@extends('layouts.app')

@section('page', 'stylist-register')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="title">
                                {{ __('text.thanks_for_signing_up_as_a_stylist') }}
                            </div>

                            <form class="form-horizontal" method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="col-md-12">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ ucfirst(__('dictionary.email')) }}" required>

                                        @if ($errors->has('email'))
                                            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <div class="col-md-12">
                                        <input id="password" type="password" class="form-control" name="password" placeholder="{{ ucfirst(__('dictionary.password')) }}" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="{{ __('text.confirm_password') }}" required>
                                    </div>
                                </div>


                                <h3 class="subtitle">{{ __('text.personal_data') }}</h3>

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <div class="col-md-12">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="{{ __('text.full_name') }}" required>

                                        @if ($errors->has('name'))
                                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <div class="col-md-12">
                                        <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="{{ __('dictionary.phone') }}" required>

                                        @if ($errors->has('phone'))
                                            <span class="help-block"><strong>{{ $errors->first('phone') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                    <div class="col-md-12">
                                        <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" placeholder="{{ __('dictionary.address') }}" required>

                                        @if ($errors->has('address'))
                                            <span class="help-block"><strong>{{ $errors->first('address') }}</strong></span>
                                        @endif
                                    </div>
                                </div>


                                {{--
                                <h3 class="subtitle">{{ __('text.register_stylist_services') }}</h3>
                                --}}


                                <h3 class="subtitle">{{ __('text.register_stylist_zones') }}</h3>

                                <div class="zones-list">
                                    @foreach($zones as $zone)
                                        <div class="col-md-3 zones">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">{{ $zone->name }}</h4>
                                                </div>

                                                <ul class="list-group">
                                                    @foreach($zone->colonies as $colony)
                                                        <li class="list-group-item">{{ $colony->name }}</li>
                                                    @endforeach
                                                </ul>

                                                <div class="panel-footer">
                                                    <label class="zone-selector" for="{{ "zones-{$zone->id}" }}">
                                                        <input class="zone-checkbox" type="checkbox" name="zones[]" value="{{ $zone->id }}" id="{{ "zones-{$zone->id}" }}">
                                                        {{ __('text.select_zone') }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>


                                <h3 class="subtitle">{{ __('text.register_stylist_weekdays') }}</h3>

                                <ul class="weekdays list-group">
                                    @foreach($week as $day)
                                        <li class="list-group-item">
                                            <label for="{{ "weekdays-{$day->id}" }}">
                                                <input type="checkbox" name="weekdays[]" value="{{ $day->id }}" id="{{ "weekdays-{$day->id}" }}">
                                                {{ $day->name }}
                                            </label>
                                        </li>
                                    @endforeach
                                </ul>


                                <h3 class="subtitle">{{ __('text.register_stylist_work_experience') }}</h3>

                                @for($i = 0; $i < 3; $i++)
                                    <div class="form-group{{ $errors->has('workExperience') ? ' has-error' : '' }}">
                                        <div class="col-md-12">
                                        <textarea name="workExperience[]"
                                              class="form-control"
                                              cols="30"
                                              rows="2">{{ old("workExperience" . $i) }}</textarea>

                                            @if($errors->has('workExperience'))
                                                <span class="help-block"><strong>{{ $errors->first('workExperience') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                @endfor


                                <h3 class="subtitle">{{ __('text.register_stylist_education') }}</h3>


                                @for($i = 0; $i < 3; $i++)
                                    <div class="form-group{{ $errors->has('education') ? ' has-error' : '' }}">
                                        <div class="col-md-12">
                                        <textarea name="education[]"
                                              class="form-control"
                                              cols="30"
                                              rows="2">{{ old('education' . $i) }}</textarea>

                                            @if($errors->has('education'))
                                                <span class="help-block"><strong>{{ $errors->first('education') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                @endfor


                                <h3 class="subtitle">{{ ucfirst(__('dictionary.portfolio')) }}</h3>

                                <div class="form-group{{ $errors->has('portfolio') ? ' has-error' : '' }}">
                                    <div class="col-md-4">
                                        <input type="file" name="portfolio[]">

                                        @if($errors->has('portfolio'))
                                            <span class="help-block"><strong>{{ $errors->first('portfolio') }}</strong></span>
                                        @endif
                                    </div>

                                    <div class="col-md-4">
                                        <input type="file" name="portfolio[]">

                                        @if($errors->has('portfolio'))
                                            <span class="help-block"><strong>{{ $errors->first('portfolio') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12 {{ $errors->has('terms_and_conditions') ? 'has-error' : '' }}">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="terms_and_conditions" {{ old('accepted') ? 'checked' : '' }}>
                                                <a href="{{ route('terms') }}">{{ __('text.i_accept_terms_and_conditions') }}</a>
                                            </label>
                                        </div>

                                        @if ($errors->has('terms_and_conditions'))
                                            <span class="help-block"><strong>{{ $errors->first('terms_and_conditions') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('text.send_form') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script type="text/javascript">
        $('.zone-selector').click(function () {
            var checkboxes = $('.zone-checkbox');

            for (var i = 0; i < checkboxes.length; i++) {
                if($(checkboxes[i]).is(':checked')) {
                    $(this).parent().addClass('active');
                } else {
                    console.warn('TODO remove active class');
                    // $(this).parent().removeClass('active');
                }
            }
        });
    </script>
@endpush
