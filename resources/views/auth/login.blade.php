@extends('layouts.app')

@section('page', 'login')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                @include('layouts.partials.login')
            </div>
        </div>
    </div>
@endsection
