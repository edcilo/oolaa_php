@extends('layouts.email.basic')

@section('body')
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="p-r">
                <p>{{ $stylist->user->name }}, tienes una nueva solicitud realizada por {{ $user->name }}</p>
            </td>
        </tr>
        <tr>
            <td class="p-r">
                <table class="services-list" border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.day')) }}:</strong>
                        </td>
                        <td>
                            {{ __("abbr.day.{$reservation->start_at->format('l')}") }}
                            {{ $reservation->start_at->format('d') }} de
                            {{ __("dictionary.months.{$reservation->start_at->format('F')}") }} del
                            {{ $reservation->start_at->format('Y') }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.hour')) }}:</strong>
                        </td>
                        <td>
                            {{ $reservation->start_at->format('h:i a') }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.address')) }}:</strong>
                        </td>
                        <td>
                            {{ $reservation->address['line'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(trans_choice('dictionary.services', 0))  }}:</strong>
                        </td>
                        <td>
                            @foreach($services as $item)
                                @if($item->status === 'P' OR $item->status === 'A')
                                    {{ $item->quantity }} {{ $item->service->full_name }} <br>

                                    {{--
                                        @if($item->discount)
                                            (@currency($item->discount) {{ __('text.discount') }})
                                        @endif
                                    --}}
                                @endif
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('text.total_reservation') }}:</strong>
                        </td>
                        <td>
                            @currency($total)
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="p-xl text-center">
                <a href="{{ route('reservation.session.id', ['reservation' => $reservation->id, 'stylist' => $stylist->id, 'token' => $token->token, 'action' => 'A']) }}"
                    class="btn btn-primary">Aceptar</a>

                <a href="{{ route('reservation.session.id', ['reservation' => $reservation->id, 'stylist' => $stylist->id, 'token' => $token->token, 'action' => 'D']) }}"
                    class="btn btn-warning">Rechazar</a>
            </td>
        </tr>
    </table>
@endsection

@push('css')
    <style type="text/css">
        .services-list td {
            padding: 6px 0;
            vertical-align: top;
        }
    </style>
@endpush
