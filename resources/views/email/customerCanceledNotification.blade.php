
@extends('layouts.email.basic')

@push('css')
    <style type="text/css">
        .services-list td {
            padding: 6px 0;
            vertical-align: top;
        }
    </style>
@endpush

@section('body')
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="p-r">
                <p>
                    Hola {{ $reservation->customer->name }}. <br>
                    Tu cita ha sido cancelada.
                </p>
            </td>
        </tr>
        <tr>
            <td class="p-r">
                <table class="services-list" border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.day')) }}:</strong>
                        </td>
                        <td>
                            {{ __("abbr.day.{$reservation->start_at->format('l')}") }}
                            {{ $reservation->start_at->format('d') }} de
                            {{ __("dictionary.months.{$reservation->start_at->format('F')}") }} del
                            {{ $reservation->start_at->format('Y') }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.hour')) }}:</strong>
                        </td>
                        <td>
                            {{ $reservation->start_at->format('h:i a') }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.address')) }}:</strong>
                        </td>
                        <td>
                            {{ $reservation->address['line'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(trans_choice('dictionary.services', 0))  }}:</strong>
                        </td>
                        <td>
                            @foreach($reservation->items as $item)
                                @if($item->status === 'P' OR $item->status === 'A')
                                    {{ $item->quantity }} {{ $item->service->full_name }}<br>

                                    {{--
                                        @if($item->discount)
                                            (@currency($item->discount) {{ __('text.discount') }})
                                        @endif
                                    --}}
                                @endif
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.subtotal')) }}:</strong>
                        </td>
                        <td>@currency($reservation->subtotal)</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('text.total_reservation') }}:</strong>
                        </td>
                        <td>
                            @currency($reservation->total)
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="p-r">
                <p>Se aplicarán las siguientes políticas:</p>

                <ul>
                    <li>Si cancelas de 24 a 6 horas antes de tu cita, se te cobrará el 20% del servicio.</li>
                    <li>Si cancelas de 6 a 3 horas antes de la hora de tu cita, se te cobrará el 50% del servicio.</li>
                    <li>Si cancelas con menos de 3 hora de anticipación o no estás presente a la hora de tu cita, se te cobrará el 100% del servicio.</li>
                    <li>Para citas programadas a las 8 am o antes debes cancelar con 12 horas de anticipación, de no ser así se te cobrará el 50% del servicio.</li>
                    <li>Si cancelas una cita hecha con cupones o paquetes resultará en la pérdida de dicho cupón o paquete.</li>
                </ul>
            </td>
        </tr>
    </table>
@endsection
