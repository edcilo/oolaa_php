@extends('layouts.email.basic')

@section('body')
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="p-r">
                <p>Hola {{ $reservation->customer->name }}.</p>

                <p>
                    Tu cita {{ $reservation->confirmation_code }} no fue reservada ya que tu pago no fue procesado
                    por la siguiente razón '{{ __("text.{$error['description']}") }}'
                </p>

                <p>
                    Por favor vuelve a agendar tu cita con otra tarjeta. En el caso de que el pago haya sido rechazado
                    por tu banco, puedes comunicarte con ellos para desbloquear el cargo y volver a agendar tu cita
                    con la misma tarjeta.
                </p>

                <p>¡Lamentamos este inconveniente!</p>

                <p>Equipo OOLAA. </p>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Datos de la reservación</strong>
            </td>
        </tr>
        <tr>
            <td class="p-r">
                <table class="services-list" border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.day')) }}:</strong>
                        </td>
                        <td>
                            {{ __("abbr.day.{$reservation->start_at->format('l')}") }}
                            {{ $reservation->start_at->format('d') }} de
                            {{ __("dictionary.months.{$reservation->start_at->format('F')}") }} del
                            {{ $reservation->start_at->format('Y') }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.hour')) }}:</strong>
                        </td>
                        <td>
                            {{ $reservation->start_at->format('h:i a') }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.address')) }}:</strong>
                        </td>
                        <td>
                            {{ $reservation->address['line'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(trans_choice('dictionary.services', 0))  }}:</strong>
                        </td>
                        <td>
                            @foreach($reservation->items as $item)
                                @if($item->status === 'P' OR $item->status === 'A')
                                    {{ $item->quantity }} {{ $item->service->full_name }} <br>

                                    {{--
                                        @if($item->discount)
                                            (@currency($item->discount) {{ __('text.discount') }})
                                        @endif
                                    --}}
                                @endif
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.subtotal')) }}:</strong>
                        </td>
                        <td>@currency($reservation->subtotal)</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('text.total_reservation') }}:</strong>
                        </td>
                        <td>
                            @currency($reservation->total)
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="p-r text-sm">
                <p>
                    Si tu tarjeta es American Express puedes haber recibido un correo antifraude en donde puedes
                    aprobar el cargo. Si es así, favor de aprobar el cargo y volver a agendar tu cita.
                </p>
            </td>
        </tr>
    </table>
@endsection
