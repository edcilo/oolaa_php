@extends('layouts.email.basic')

@push('css')
    <style type="text/css">
        .services-list td {
            padding: 6px 0;
            vertical-align: top;
        }
    </style>
@endpush

@section('body')
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="p-r">
                <p>
                    Hola {{ $reservation->customer->name }}.<br>

                    Se ha actualizado la información de tu cita.
                </p>

                <ul>
                    @foreach($changes as $change)
                        <li>
                            @if($change === 'address')
                                Dirección
                            @elseif($change === 'date')
                                Fecha
                            @elseif($change === 'hour')
                                Hora
                            @elseif($change === 'services')
                                Servicios
                            @elseif($change === 'stylists')
                                Estilistas
                            @endif
                        </li>
                    @endforeach
                </ul>
            </td>
        </tr>
        <tr>
            <td class="p-r">
                <table class="services-list" border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.day')) }}:</strong>
                        </td>
                        <td>
                            {{ __("abbr.day.{$reservation->start_at->format('l')}") }}
                            {{ $reservation->start_at->format('d') }} de
                            {{ __("dictionary.months.{$reservation->start_at->format('F')}") }} del
                            {{ $reservation->start_at->format('Y') }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.hour')) }}:</strong>
                        </td>
                        <td>
                            {{ $reservation->start_at->format('h:i a') }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.address')) }}:</strong>
                        </td>
                        <td>
                            {{ $reservation->address['line'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(trans_choice('dictionary.services', 0))  }}:</strong>
                        </td>
                        <td>
                            @foreach($reservation->items as $item)
                                @if($item->status === 'P' OR $item->status === 'A')
                                    {{ $item->quantity }} {{ $item->service->full_name }} ({{ ($item->stylist) ? $item->stylist->name : 'No asignado' }})<br>

                                    {{--
                                        @if($item->discount)
                                            (@currency($item->discount) {{ __('text.discount') }})
                                        @endif
                                    --}}
                                @endif
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.subtotal')) }}:</strong>
                        </td>
                        <td>@currency($reservation->subtotal)</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('text.total_reservation') }}:</strong>
                        </td>
                        <td>
                            @currency($reservation->total)
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        @if(array_search('services', $changes))
            <tr>
                <td class="p-r text-sm">
                    <p>Se te aplicó el reembolso correspondiente.</p>
                </td>
            </tr>
        @endif
    </table>
@endsection
