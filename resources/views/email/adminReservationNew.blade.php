@extends('layouts.email.basic')

@section('body')
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="p-r">
                <p>{{ $user->name }}, ha realizado una nueva cita.</p>
            </td>
        </tr>
        <tr>
            <td class="p-r">
                <table class="services-list" border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td>
                            <strong>{{ __('text.type_of_reservation') }}:</strong>
                        </td>
                        <td>
                            @if ($reservation->auto_stylist)
                                {{ __('text.send_me_the_best') }}.
                            @else
                                {{ __('text.stylist_selection') }}.
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.day')) }}:</strong>
                        </td>
                        <td>
                            {{ __("abbr.day.{$reservation->start_at->format('l')}") }}
                            {{ $reservation->start_at->format('d') }} de
                            {{ __("dictionary.months.{$reservation->start_at->format('F')}") }} del
                            {{ $reservation->start_at->format('Y') }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.hour')) }}:</strong>
                        </td>
                        <td>
                            {{ $reservation->start_at->format('h:i a') }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.address')) }}:</strong>
                        </td>
                        <td>
                            {{ $reservation->address['line'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.phone')) }}:</strong>
                        </td>
                        <td>
                            {{ $reservation->customer->user->profile->phone }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(trans_choice('dictionary.services', 0))  }}:</strong>
                        </td>
                        <td>
                            @foreach($reservation->items as $item)
                                @if($item->status === 'P' OR $item->status === 'A')
                                    {{ $item->quantity }} {{ $item->service->full_name }} <br>

                                    {{--
                                        @if($item->discount)
                                            (@currency($item->discount) {{ __('text.discount') }})
                                        @endif
                                    --}}
                                @endif
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.subtotal')) }}:</strong>
                        </td>
                        <td>@currency($reservation->subtotal)</td>
                    </tr>
                    @if($reservation->has_coupon)
                    <tr>
                        <td>
                            <strong>Cupón</strong>
                        </td>
                        <td>
                            @if($reservation->coupon->type === 'F')
                                @currency($reservation->coupon->amount)
                            @else
                                {{ $reservation->coupon->amount }}%
                            @endif
                        </td>
                    </tr>
                    @endif
                    <tr>
                        <td>
                            <strong>{{ __('text.total_reservation') }}:</strong>
                        </td>
                        <td>
                            @currency($reservation->getTotalAttribute())
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@endsection

@push('css')
    <style type="text/css">
        .services-list td {
            padding: 6px 0;
            vertical-align: top;
        }
    </style>
@endpush
