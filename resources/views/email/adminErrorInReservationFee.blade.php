@extends('layouts.email.basic')

@section('body')
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="p-r">
                <p>Hubo un error en un proceso de cobro.</p>

                <ul>
                    <li>
                        <strong>Reservación:</strong>
                        {{ $reservation->confirmation_code }}
                    </li>
                    <li>
                        <strong>Código de error:</strong>
                        {{ $error['code'] }}
                    </li>
                    <li>
                        <strong>Descripción:</strong>
                        {{ $error['description'] }}
                    </li>
                </ul>
            </td>
        </tr>
    </table>
@endsection
