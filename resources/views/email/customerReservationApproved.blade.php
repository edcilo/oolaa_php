@extends('layouts.email.basic')

@push('css')
    <style type="text/css">
        .services-list td {
            padding: 6px 0;
            vertical-align: top;
        }

        .avatar {
            border-radius: 50%;
            height: 50px;
            width: 50px;
        }

        .stylist-list {
            overflow: hidden;
        }

        .stylist-mini-card {
            display: inline-block;
            height: 104px;
            margin-bottom: 16px;
            text-align: center;
            width: 90px;
        }
    </style>
@endpush

@section('body')
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="p-r">
                <p>Hola {{ $reservation->customer->name }}</p>

                <p>¡Tu cita a sido aprobada!</p>

                @if($reservation->countDeclined === 1)
                    <p>{{ __('text.reservation_denied_by_one') }}</p>
                @elseif($reservation->countDeclined > 1)
                    <p>{{ __('text.reservation_denied_by_two_or_mode') }}</p>
                @endif

                @if($reservation->countDeclined)
                    <ul>
                        @foreach($reservation->items as $item)
                            @if($item->declined_by)
                                <li>
                                    <a href="{{ route('stylist', [$item->stylist->user->slug, $item->stylist->id]) }}">{{ $item->stylist->name }}</a>.
                                    Puedes ver su perfil dando <a href="{{ route('stylist', [$item->stylist->user->slug, $item->stylist->id]) }}">click aqui</a>.
                                </li>
                            @endif
                        @endforeach
                    </ul>
                @endif
            </td>
        </tr>
        <tr>
            <td class="p-r">
                <table class="services-list" border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.day')) }}:</strong>
                        </td>
                        <td>
                            {{ __("abbr.day.{$reservation->start_at->format('l')}") }}
                            {{ $reservation->start_at->format('d') }} de
                            {{ __("dictionary.months.{$reservation->start_at->format('F')}") }} del
                            {{ $reservation->start_at->format('Y') }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.hour')) }}:</strong>
                        </td>
                        <td>
                            {{ $reservation->start_at->format('h:i a') }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.address')) }}:</strong>
                        </td>
                        <td>
                            {{ $reservation->address['line'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(trans_choice('dictionary.services', 0))  }}:</strong>
                        </td>
                        <td>
                            @foreach($reservation->items as $item)
                                @if($item->status === 'P' OR $item->status === 'A')
                                    {{ $item->quantity }} {{ $item->service->full_name }} ({{ $item->stylist->name }})<br>

                                    {{--
                                        @if($item->discount)
                                            (@currency($item->discount) {{ __('text.discount') }})
                                        @endif
                                    --}}
                                @endif
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ ucfirst(__('dictionary.subtotal')) }}:</strong>
                        </td>
                        <td>@currency($reservation->subtotal)</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('text.total_reservation') }}:</strong>
                        </td>
                        <td>
                            @currency($reservation->total)
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="p-r">
                <table class="table-width-100">
                    <tr>
                        <td>
                            <strong>Tus estilistas:</strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="stylist-list text-center">
                            @foreach($stylists as $stylist)
                                <div class="stylist-mini-card">
                                    <img class="avatar" src="{{ $stylist->user->profile->avatar }}" alt="{{ $stylist->name }} avatar">
                                    <br>
                                    {{ strlen($stylist->name) > 16 ? substr($stylist->name,0,16)."..." : $stylist->name }}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="p-r text-sm">
                <p>
                    Se te hizo un cargo a tu tarjeta de crédito, si cancelas 24 horas antes de tu servicio se te
                    devolverá el costo total de tu servicio. Para mayor información ver
                    <a href="{{ route('policies') }}">políticas de cancelación</a>
                </p>
            </td>
        </tr>
        @if($reservation->countDeclined)
            <tr>
                <td class="p-r text-sm">
                    <p>
                        Si tienes algún inconveniente con esta selección o quieres buscar otros horarios en los que la 
                        estilista que elegiste esté disponible por favor comunícate con nosotros a contacto@oolaa.com.mx o
                        escríbenos un whats app al 55 7403 9738.
                    </p>
                </td>
            </tr>
        @endif
    </table>
@endsection
