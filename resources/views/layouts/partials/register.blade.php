<div class="auth-form panel panel-default">
    <div class="panel-heading">
        <h2>{{ __('text.register') }}</h2>
    </div>

    <div class="panel-body">
        <div class="link-register">
            {{ __('text.do_you_already_have_an_account') }}
            <a class="popup-modal" href="#login-modal">{{ __('text.log_in') }}</a>
        </div>

        {{--
        <div class="social">
            <a class="btn btn-block btn-facebook" href="{{ route('auth.oauth', 'facebook') }}">
                <i class="fa fa-facebook"></i>
                {{ __('text.sign_in_with_third_party', ['third_party' => 'Facebook']) }}
            </a>

            <a class="btn btn-block btn-google" href="{{ route('auth.oauth', 'google') }}">
                <i class="fa fa-google"></i>
                {{ __('text.sign_in_with_third_party', ['third_party' => 'Google']) }}
            </a>
        </div>
        --}}

        <div class="line"></div>

        <div class="form-login">
            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}

                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <div class="col-md-12">
                        <input id="name"
                               type="text"
                               class="form-control"
                               name="name"
                               value="{{ old('name') }}"
                               placeholder="{{ ucfirst(__('dictionary.name')) }}"
                               autocomplete="name"
                               required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                    <div class="col-md-12">
                        <input id="email"
                               type="email"
                               class="form-control"
                               name="email"
                               value="{{ old('email') }}"
                               placeholder="{{ ucfirst(__('dictionary.email')) }}"
                               autocomplete="email"
                               required>

                        @if ($errors->has('email'))
                            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                    <div class="col-md-12">
                        <input id="password"
                               type="password"
                               class="form-control"
                               name="password"
                               placeholder="{{ ucfirst(__('dictionary.password')) }}"
                               autocomplete="password"
                               required>

                        @if ($errors->has('password'))
                            <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <input id="password-confirm"
                               type="password"
                               class="form-control"
                               name="password_confirmation"
                               placeholder="{{ ucfirst(__('text.password_confirmation')) }}"
                               autocomplete="new-password"
                               required>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12 {{ $errors->has('terms_and_conditions') ? 'has-error' : '' }}">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="terms_and_conditions" {{ old('accepted') ? 'checked' : '' }}>
                                <a href="{{ route('terms') }}">{{ __('text.i_accept_terms_and_conditions') }}</a>
                            </label>
                        </div>

                        @if ($errors->has('terms_and_conditions'))
                            <span class="help-block"><strong>{{ $errors->first('terms_and_conditions') }}</strong></span>
                        @endif
                    </div>
                </div>

                <button type="submit" class="btn btn-block btn-primary">
                    {{ __('text.create_account') }}
                </button>
            </form>
        </div>
    </div>
</div>
