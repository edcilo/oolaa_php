<div class="auth-form panel panel-default">
    <div class="panel-heading">
        <h2>{{ __('text.login') }}</h2>
    </div>

    <div class="panel-body">
        <div class="link-register">
            {{ __('text.you_do_not_have_an_account') }}
            <a class="popup-modal" href="#register-modal">{{ __('text.sign_up') }}</a>
        </div>

        {{--
        <div class="social">
            <a class="btn btn-block btn-facebook" href="{{ route('auth.oauth', 'facebook') }}">
                <i class="fa fa-facebook"></i>
                {{ __('text.login_with_third_party', ['third_party' => 'Facebook']) }}
            </a>

            <a class="btn btn-block btn-google" href="{{ route('auth.oauth', 'google') }}">
                <i class="fa fa-google"></i>
                {{ __('text.login_with_third_party', ['third_party' => 'Google']) }}
            </a>
        </div>
        --}}

        <div class="line"></div>

        <div class="form-login">
            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                    <div class="col-md-12">
                        <input id="register-email"
                               type="email"
                               class="form-control"
                               name="email"
                               value="{{ old('email') }}"
                               placeholder="{{ ucfirst(__('dictionary.email')) }}"
                               autocomplete="email"
                               required autofocus>

                        @if ($errors->has('email'))
                            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                    <div class="col-md-12">
                        <input id="register-password"
                               type="password"
                               class="form-control"
                               name="password"
                               placeholder="{{ ucfirst(__('dictionary.password')) }}"
                               autocomplete="new-password"
                               required>

                        @if ($errors->has('password'))
                            <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-5">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                {{ __('text.remember_me') }}
                            </label>
                        </div>
                    </div>

                    <div class="col-xs-7">
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('text.forgot_your_password') }}
                        </a>
                    </div>
                </div>

                <button type="submit" class="btn btn-block btn-primary">
                    {{ ucfirst(__('dictionary.enter')) }}
                </button>
            </form>
        </div>
    </div>
</div>
