<ul class="service-list-items">
    @foreach($services as $service)
        <li class="service-list-item">
            <div class="service-list-data">
                <div class="service-list-left">
                    <div class="service-list-control">
                        <input id="{{ "service-{$service->id}" }}"
                               class="service-check"
                               data-name="{{ $service->name }}"
                               data-price="{{ $service->price }}"
                               type="checkbox"
                               name="services[]"
                               value="{{ $service->id }}">
                    </div>

                    <div class="service-list-name">
                        <label for="{{ "service-{$service->id}" }}">{{ $service->name }}</label>
                    </div>
                </div>

                <div class="service-list-right">
                    <div class="service-list-price">
                        <nobr>@currency($service->price)</nobr>
                    </div>
                </div>
            </div>

            @if($service->children->count())
                <div class="service-list-children">
                    @include('layouts.partials.servicesList', ['services' => $service->children])
                </div>
            @endif
        </li>
    @endforeach
</ul>
