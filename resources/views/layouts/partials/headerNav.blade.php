<ul>
    <li><a href="{{ route('stylist.search') }}">{{ __('text.find_stylist') }}</a></li>
    <li><a href="{{route('gift.coupon')}}">{{ __('text.give_coupon') }}</a></li>

    @guest
        <li><a href="{{ route('public.stylist.register') }}">{{ __('text.are_you_stylist') }}</a></li>
        <li><a class="popup-modal" href="#login-modal">{{ __('text.login') }}</a></li>
        <li><a class="popup-modal" href="#register-modal">{{ __('text.register') }}</a></li>
    @else
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu">
                <li>
                    <a href="{{ route('profile', [auth()->user()->slug, auth()->id()]) }}">{{ ucfirst(__('dictionary.profile')) }}</a>
                </li>
                @if(auth()->user()->customer)
                    <li>
                        <a href="{{ route('customer.reservations') }}">{{ ucfirst(trans_choice('dictionary.reservations', 0)) }}</a>
                    </li>
                @endif
                @if(auth()->user()->stylist)
                    <li>
                        <a href="{{ route('stylist.reservations.pending') }}">Mis reservaciones <br> <small>(Estilista)</small></a>
                    </li>
                    <li>
                        <a href="{{ route('stylist.calendar', auth()->user()->stylist) }}">Calendario</a>
                    </li>
                @endif
                @can('dashboard.access')
                    <li>
                        <a href="{{ route('dashboard') }}">{{ ucfirst(__('dictionary.dashboard')) }}</a>
                    </li>
                @endcan
                <li>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('text.logout') }}</a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>
    @endguest
</ul>
