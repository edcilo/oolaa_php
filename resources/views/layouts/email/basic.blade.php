<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <!--[if gte mso 15]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OOLAA | Aviso de nueva reservaci&oacute;n</title>

    @include('layouts.email.basicStyles')

    @stack('css')
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
    <tr>
        <td class="email-header">

            <table class="email-section" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="email-logo">
                        <img alt="logo" src="https://gallery.mailchimp.com/d0d137bf62eae8f8d6d14a813/images/108c5688-718e-4f87-83ed-9465ca6c663a.png" width="132" height="58">
                    </td>
                </tr>
            </table>

        </td>
    </tr>
    <tr>
        <td class="email-body">

            <table class="email-section" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        @yield('body')
                    </td>
                </tr>
            </table>

        </td>
    </tr>
    <tr>
        <td class="email-footer">

            <table class="email-section" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="email-row-md text-center">
                        ¿Tienes una pregunta? Cont&aacute;ctanos
                        <br>
                        <a href="mailto:{{ config('oolaa.mail.contact') }}" target="_blank" class="link">{{ config('oolaa.mail.contact') }}</a>
                        <br>
                        {{ config('oolaa.phone.contact') }}
                    </td>
                </tr>
                <tr>
                    <td class="email-row-md text-center">
                        <a class="unlink icon-social" href="{{ config('oolaa.social.facebook') }}" target="_blank">
                            <img src="https://gallery.mailchimp.com/d0d137bf62eae8f8d6d14a813/images/0eb80317-955c-4eb3-bbf2-9a4be77591df.png" class="" width="50" height="50">
                        </a>

                        <a class="unlink icon-social" href="{{ config('oolaa.social.instagram') }}" target="_blank">
                            <img src="https://gallery.mailchimp.com/d0d137bf62eae8f8d6d14a813/images/6ab3ffd1-7c3b-44c2-8fa1-86e234265661.png" class="" width="50" height="50">
                        </a>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>
</body>
</html>
