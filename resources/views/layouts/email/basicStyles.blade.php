<style type="text/css">
    body {
        color: #626262;
        font-family: "Helvetica Neue", Helvetica, Arial, Verdana, sans-serif;
        font-size: 16px;
        font-weight: 300;
        line-height: 24px;
        margin: 0;
        padding: 0;
    }

    p {
        margin: 10px 0;
        padding: 0;
    }

    /* SECTIONS */
    .email-body {
    }

    .email-section {
        margin: 0 auto;
        max-width: 600px;
        min-width: 380px;
        padding: 9px;
    }

    .email-header {
        background-color: #fafafa;
    }

    .email-footer {
        background-color: #fafafa;
        font-size: 13px;
        line-height: 19px;
    }

    .email-row-md {
        padding: 16px 0;
    }

    .email-logo img {
        display: block;
        float: right;
    }

    /* ELEMENTS */
    .btn {
        background-color: #e4e4e4;
        border: 1px solid #e6e6e6;
        border-radius: 3px;
        color: #000000;
        display: inline-block;
        font-size: 14px;
        font-weight: bold;
        margin: 0 32px;
        padding: 8px 32px;
        text-decoration: none;
        text-transform: uppercase;
    }

    .btn-primary {
        background-color: #29b882;
        border-color: #20885F;
        color: #ffffff;
    }

    .btn-warning {
        background-color: #ff485f;
        border-color: #ee425a;
        color: #ffffff;
    }

    .icon-social {
        margin: 0 16px;
    }

    .link {
        color: #29B882;
        text-decoration: none;
    }

    .p-xl {
        padding: 22px 0;
    }

    .p-lg {
        padding: 19px 0;
    }

    .p-r {
        padding: 16px 0;
    }

    .p-sm {
        padding: 14px 0;
    }

    .p-xs {
        padding: 13px 0;
    }

    .table-width-100 {
        width: 100%
    }

    .text-center {
        text-align: center;
    }

    .text-xl {
        font-size: 22px;
    }

    .text-lg {
        font-size: 19px;
    }

    .text-r {
        font-size: 16px;
    }

    .text-sm {
        font-size: 14px;
    }

    .text-xs {
        font-size: 13px;
    }

    .unlink {
        text-decoration: none;
    }
</style>
