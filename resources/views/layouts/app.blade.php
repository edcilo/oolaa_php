<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>
    <meta name="description" content="OOLAA te permite agendar servicios de belleza a domicilio en México. Ofrecemos maquillaje, peinado, manicure, pedicure, depilación, masajes y faciales. Tu escoges el lugar y la hora y nosotros nos ocupamos de lo demás.">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/static/img/favicon.ico/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="login-status" content="{{ Auth::check() }}">

    <title>{{ config('app.name', 'OOLAA') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    @stack('css')

    <script src="https://maps.googleapis.com/maps/api/js?key={{ config('oolaa.google.maps') }}&libraries=places"></script>
</head>
<body id="@yield('page')">

<header class="edc-nav header">
    <ul class="burger edc-nav edc-nav-btn"><li></li><li></li><li></li></ul>

    <div class="logo">
        <a href="{{ route('welcome') }}">
            <img src="{{ asset('images/logo.svg') }}" alt="logo" class="logo-img">
        </a>
    </div>
</header>

<div class="edc-nav nav">
    @include('layouts.partials.headerNav')
</div>

<div class="edc-nav content">
    @yield('content')

    <div id="login-modal" class="mfp-hide">
        @include('layouts.partials.login')
    </div>

    <div id="register-modal" class="mfp-hide">
        @include('layouts.partials.register')
    </div>

    <footer class="footer">
        <div class="container">
            <div class="footer-section">
                <div class="col">
                    <ul class="footer-nav un-list inline social">
                        <li>
                            <a href="{{ $schema['contact']['instagram'] }}"><i class="fa fa-instagram"></i></a>
                        </li>
                        <li>
                            <a href="{{ $schema['contact']['facebook'] }}"><i class="fa fa-facebook"></i></a>
                        </li>
                    </ul>
                </div>

                <div class="col">
                    <div class="contact">
                        <h3>{{ ucfirst(__('dictionary.contact')) }}</h3>
                        <p>{{ $schema['contact']['email'] }}</p>
                        <p>{{ __('abbr.phone') }} {{ $schema['contact']['phone'] }}</p>
                    </div>
                </div>
            </div>
            <div class="footer-section">
                <ul class="footer-nav un-list list-green">
                    <li><a href="{{ route('faqs') }}">{{ __('text.faqs') }}</a></li>
                    <li><a href="{{ route('about.us') }}">{{ __('text.are_you_stylist') }}</a></li>
                    <li><a href="{{ route('stylist.search') }}">{{ __('text.make_a_reservation') }}</a></li>
                </ul>
            </div>
            <div class="footer-section">
                <ul class="footer-nav un-list">
                    <li>
                        <a href="{{ route('welcome') }}" class="logo">
                            <img src="{{ asset('images/logo.svg') }}" alt="logo oolaa">
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('terms') }}">{{ __('text.terms_and_conditions') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('policies') }}">{{ __('text.cancellation_policies') }}</a>
                    </li>
                    <li>{{ __('text.all_rights_reserved') }}</li>
                </ul>
            </div>
        </div>
    </footer>
</div>

<!-- Scripts -->
@stack('js-data')

<script src="{{ asset('js/app.js') }}"></script>

@if (session('login-error') === true)
<script>
    $(function () {
        $.magnificPopup.close();

        $.magnificPopup.open({
            items: {
                src: $('#login-modal'),
            },
            type: 'inline',
            closeOnBgClick: true
        }, 0);
    });
</script>
@endif

@if (session('register-error') === true)
    <script>
        $(function () {
            $.magnificPopup.close();

            $.magnificPopup.open({
                items: {
                    src: $('#register-modal'),
                },
                type: 'inline',
                closeOnBgClick: true
            }, 0);
        });
    </script>
@endif

@stack('js')

</body>
</html>
