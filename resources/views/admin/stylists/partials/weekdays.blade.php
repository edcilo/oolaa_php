<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ __('text.weekdays') }}</h3>
    </div>

    <form action="{{ route('stylists.calendar.weekdays', $stylist) }}" method="POST">
        {{ csrf_field() }}

        <div class="box-body">
            <div class="{{ $errors->has('weekdays') ? 'has-error' : '' }}">
                @if($errors->has('weekdays'))
                    <span class="help-block">{{ $errors->first('weekdays') }}</span>
                @endif
            </div>

            @foreach($weekdays as $day)
                <div class="form-group">
                    <label for="{{ "day-{$day->id}" }}">
                        <input type="checkbox"
                               class="checkbox"
                               name="weekdays[]"
                               id="{{ "day-{$day->id}" }}"
                               value="{{ $day->id }}"
                               {{ $stylist->weekdays->where('id', $day->id)->count() || (old('weekdays') && array_search($day->id, old('weekdays'))) ? 'checked' : '' }}>
                        {{ $day->name }}
                    </label>
                </div>
            @endforeach
        </div>

        <div class="box-footer text-right">
            <button type="submit" class="btn btn-success">
                <i class="fa fa-save"></i>
                {{ ucfirst(__('dictionary.save')) }}
            </button>
        </div>
    </form>
</div>