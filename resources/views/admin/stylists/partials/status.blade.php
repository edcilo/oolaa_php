<div class="admin-status">
    <form action="{{ route('stylists.change.status', $stylist) }}" method="POST" class="form-inline">
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <input type="hidden" name="stylist_id" value="{{ $stylist->id }}">

        <div class="form-group @if($errors->has('status')) has-error @endif">
            <label for="exampleInputName2">{{ ucfirst(__('dictionary.status')) }}:</label>
            <select class="form-control" name="status" id="status">
                @foreach(__('abbr.stylist') as $key => $status)
                    <option value="{{ $key }}" @if($stylist->abbr_status === $key) selected @endif>{{ ucfirst($status) }}</option>
                @endforeach
            </select>
            @if($errors->has('status'))
                <span class="help-block">{{ $errors->first('status') }}</span>
            @endif
        </div>

        <button type="submit" class="btn btn-success pull-right">
            <i class="fa fa-save"></i>
            {{ __('text.change_status') }}
        </button>
    </form>
</div>