<div class="box box-success">
    <div class="box-header row">
        <div class="col-md-9">
            <h3 class="box-title">{{ ucfirst(trans_choice('dictionary.stylists', 0)) }}</h3>
        </div>

        <div class="col-md-3">
            @include('admin.partials.formSearch', ['route_name' => 'services.show', 'route_params' => $route_param])
        </div>
    </div>

    <div class="box-body">
        @include('admin.services.partials.addStylist')
    </div>

    <div class="box-footer">
        <button type="submit" form="destroy_multiple" class="btn btn-sm btn-danger">
            <i class="ti-trash"></i>
            {{ __('text.delete_selected') }}
        </button>
    </div>

    <div class="box-body table-responsive no-padding">
        <form action="{{ route('services.remove.stylists', $service) }}" id="destroy_multiple" method="POST">
            {{ csrf_field() }}
            {{ method_field('delete') }}

            <table class="table table-striped table-simple">
                <thead>
                <tr>
                    <th class="text-center">
                        <input type="checkbox"
                               name="select_all"
                               class="checkbox-delete-trigger"
                               title="{{ __('text.select_all') }}">
                    </th>
                    <th>{{ ucfirst(__('dictionary.photo')) }}</th>
                    <th>{{ ucfirst(__('dictionary.status')) }}</th>
                    <th>{{ ucfirst(__('dictionary.name')) }}</th>
                    <th>{{ ucfirst(__('dictionary.email')) }}</th>
                    <th>{{ ucfirst(__('dictionary.phone')) }}</th>
                    <th style="width: 40px">
                        <i class="fa fa-gears"></i>
                    </th>
                </tr>
                </thead>

                <tbody>
                @forelse($stylists as $stylist)
                    <tr>
                        <td class="text-center">
                            <input type="checkbox"
                                   name="stylist_ids[]"
                                   value="{{ $stylist->id }}"
                                   class="checkbox-delete">
                        </td>
                        <td class="text-center">
                            <img src="{{ $stylist->user->profile->avatar }}"
                                 alt="{{ $stylist->user->name }}"
                                 class="img-circle">
                        </td>
                        <td>{{ ucfirst($stylist->status_readable) }}</td>
                        <td>{{ $stylist->user->name }}</td>
                        <td>{{ $stylist->user->email }}</td>
                        <td>{{ $stylist->user->profile->phone }}</td>
                        <td>
                            <nobr>
                                @include('admin.partials.btnShow', ['route_name' => 'stylists.show', 'model' => $stylist])
                            </nobr>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7">
                            <div class="text-muted text-center h1">
                                <i class="fa fa-search"></i>
                                <span>{{ __('text.no_results_found') }}</span>
                            </div>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </form>
    </div>

    <div class="box-footer clearfix">
        @include('admin.partials.paginateFooter', [
            'model' => $stylists,
            'route_name' => 'services.show',
            'route_params' => $route_param,
            'model_name_trans' => 'dictionary.stylists'
        ])
    </div>
</div>
