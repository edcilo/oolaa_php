<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ __('text.create_entity', ['entity' => __('dictionary.event')]) }}</h3>
    </div>

    <form action="{{ route('stylists.calendar.store', $stylist) }}" method="POST">
        {{ csrf_field() }}

        <div class="box-body">
            <div class="form-group @if($errors->has('title')) has-error @endif">
                <label for="title">{{ ucfirst(__('dictionary.title')) }}</label>
                <input type="text"
                       name="title"
                       value="@if(old('title')){{ old('title') }}@endif"
                       class="form-control"
                       id="title">
                @if($errors->has('title'))
                    <span class="help-block">{{ $errors->first('title') }}</span>
                @endif
            </div>

            <div class="form-group @if($errors->has('allDay')) has-error @endif">
                <label for="all_day">
                    <input type="checkbox" class="checkbox" name="allDay" id="allDay" value="1" @if(old('allDay')) checked @endif>
                    {{ __('text.all_day') }}
                </label>
                @if($errors->has('allDay'))
                    <span class="help-block">{{ $errors->first('allDay') }}</span>
                @endif
            </div>

            <div class="form-group">
                <label for="start">{{ ucfirst(__('dictionary.start')) }}</label>

                <div class="row">
                    <div class="col-md-6 @if($errors->has('start_date')) has-error @endif">
                        <input type="text"
                               name="start_date"
                               value="@if(old('start_date')){{ old('start_date') }}@endif"
                               class="form-control"
                               placeholder="{{ ucfirst(__('dictionary.date')) }}"
                               id="start_date">
                        @if($errors->has('start_date'))
                            <span class="help-block">{{ $errors->first('start_date') }}</span>
                        @endif
                    </div>

                    <div class="col-md-6 hour-fields @if($errors->has('start_hour')) has-error @endif">
                        <input type="text"
                               name="start_hour"
                               value="@if(old('start_hour')){{ old('start_hour') }}@endif"
                               class="form-control"
                               placeholder="{{ ucfirst(__('dictionary.hour')) }}"
                               id="start_hour">
                        @if($errors->has('start_hour'))
                            <span class="help-block">{{ $errors->first('start_hour') }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="form-group hour-fields">
                <label for="end">{{ ucfirst(__('dictionary.end')) }}</label>

                <div class="row">
                    <div class="col-md-6 @if($errors->has('end_date')) has-error @endif">
                        <input type="text"
                               name="end_date"
                               value="@if(old('end_date')){{ old('end_date') }}@endif"
                               class="form-control"
                               placeholder="{{ ucfirst(__('dictionary.date')) }}"
                               id="end_date">
                        @if($errors->has('end_date'))
                            <span class="help-block">{{ $errors->first('end_date') }}</span>
                        @endif
                    </div>

                    <div class="col-md-6 @if($errors->has('end_hour')) has-error @endif">
                        <input type="text"
                               name="end_hour"
                               value="@if(old('end_hour')){{ old('end_hour') }}@endif"
                               class="form-control"
                               placeholder="{{ ucfirst(__('dictionary.hour')) }}"
                               id="end_hour">
                        @if($errors->has('end_hour'))
                            <span class="help-block">{{ $errors->first('end_hour') }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="form-group @if($errors->has('backgroundColor')) has-error @endif">
                <label>
                    {{ ucfirst(__('dictionary.color')) }}
                </label>

                <div>
                    <label class="color-picker-badge" style="background: #29b882">
                        <input type="radio" name="backgroundColor" value="#29b882" checked>
                    </label>

                    <label class="color-picker-badge" style="background: #00a65a">
                        <input type="radio" name="backgroundColor" value="#00a65a">
                    </label>

                    <label class="color-picker-badge" style="background: #00c0ef">
                        <input type="radio" name="backgroundColor" value="#00c0ef">
                    </label>

                    <label class="color-picker-badge" style="background: #0073b7">
                        <input type="radio" name="backgroundColor" value="#0073b7">
                    </label>

                    <label class="color-picker-badge" style="background: #dd4b39">
                        <input type="radio" name="backgroundColor" value="#dd4b39">
                    </label>

                    <label class="color-picker-badge" style="background: #ff851b">
                        <input type="radio" name="backgroundColor" value="#ff851b">
                    </label>

                    <label class="color-picker-badge" style="background: #f39c12">
                        <input type="radio" name="backgroundColor" value="#f39c12">
                    </label>

                    <label class="color-picker-badge" style="background: #605ca8">
                        <input type="radio" name="backgroundColor" value="#605ca8">
                    </label>

                    <label class="color-picker-badge" style="background: #f012be">
                        <input type="radio" name="backgroundColor" value="#f012be">
                    </label>
                </div>

                @if($errors->has('background_color'))
                    <span class="help-block">{{ $errors->first('background_color') }}</span>
                @endif
            </div>

            <div class="form-group @if($errors->has('description')) has-error @endif">
                <label for="description">{{ ucfirst(__('dictionary.description')) }}</label>
                <textarea name="description"
                          class="form-control"
                          id="description"
                          cols="30"
                          rows="3">@if(old('description')){{ old('description') }}@endif</textarea>
                @if($errors->has('description'))
                    <span class="help-block">{{ $errors->first('description') }}</span>
                @endif
            </div>
        </div>

        <div class="box-footer text-right">
            <button type="submit" class="btn btn-success">
                <i class="fa fa-save"></i>
                {{ ucfirst(__('dictionary.save')) }}
            </button>
        </div>
    </form>
</div>