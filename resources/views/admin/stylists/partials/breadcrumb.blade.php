<ol class="breadcrumb">
    <li class="@if(Route::currentRouteName() == 'stylists.index') active @endif">
        <a href="{{ route('stylists.index') }}">
            <i class="fa fa-paint-brush"></i>
            {{ ucfirst(trans_choice('dictionary.stylists', 0)) }}
        </a>
    </li>

    @if(Route::currentRouteName() == 'stylists.create')
        <li class="active">
            <a href="{{ route('stylists.create') }}">
                {{ ucfirst(trans('dictionary.new')) }}
            </a>
        </li>
    @endif

    @if(Route::currentRouteName() == 'stylists.show' AND isset($stylist))
        <li class="active">
            <a href="{{ route('stylists.show', $stylist) }}">
                {{ $stylist->user->full_name }}
            </a>
        </li>
    @endif

    @if(Route::currentRouteName() == 'stylists.edit' AND isset($stylist))
        <li class="active">
            <a href="{{ route('stylists.edit', $stylist) }}">
                {{ ucfirst(trans('dictionary.edit')) }}
            </a>
        </li>
    @endif
</ol>
