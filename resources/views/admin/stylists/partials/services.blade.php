<a href="{{ route('services.attach', $user) }}" class="btn btn-sm btn-success pull-right" title="{{ __('dictionary.admin') }}">
    <i class="fa fa-wrench"></i> {{ ucfirst(__('dictionary.admin')) }}
</a>

<h3>{{ ucfirst(trans_choice('dictionary.services', 0)) }}</h3>

<table class="table">
    <thead>
    <tr>
        <th class="text-center">{{ ucfirst(__('dictionary.name')) }}</th>
        <th class="text-center">{{ __('text.average_time') }}</th>
        <th class="text-center">{{ ucfirst(__('dictionary.price')) }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($services as $service)
        <tr>
            <td>{{ $service->name }}</td>
            <td class="text-center">@minutesToHours($service->average_time)</td>
            <td class="text-right">@currency($service->price)</td>
        </tr>
    @endforeach
    </tbody>
</table>
