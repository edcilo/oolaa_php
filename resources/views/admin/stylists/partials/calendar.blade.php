<div class="pull-right">
    <a href="{{ route('stylists.calendar', $stylist) }}" class="btn btn-sm btn-success">
        <i class="fa fa-wrench"></i>
        {{ ucfirst(__('dictionary.admin')) }}
    </a>
</div>

<h3>
    {{ ucfirst(__('dictionary.calendar')) }}
</h3>

{{--
@if($stylist->weekdays)
    <ul>
        @foreach($stylist->weekdays as $weekday)
            <li>{{ $weekday->name }}</li>
        @endforeach
    </ul>
@else
    No hay dias
@endif
--}}
