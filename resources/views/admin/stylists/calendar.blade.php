@extends('adminlte::page')

@section('title', ucfirst(__('dictionary.calendar')))

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.stylists', 0)) }} <small>{{ ucfirst(__('dictionary.calendar')) }}</small></h1>

    @include('admin.stylists.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="admin-calendar">
        <div class="row">
            <div class="col-md-4">
                @include('admin.stylists.partials.calendarForm')

                @include('admin.stylists.partials.weekdays')
            </div>

            <div class="col-md-8">
                <div class="box box-success">
                    <div class="box-body table-responsive no-padding">
                        <div id='calendar'></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('css')
    <link rel="stylesheet" href="{{ mix('/css/plugins.css') }}">
    <link rel="stylesheet" href="{{ mix('/css/custom.css') }}">
@endpush

@push('js')
    <script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ mix('/js/script.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            checkBoxes('.checkbox');

            if ($('.checkbox:checked').length > 0) {
                $('.hour-fields').hide();

                $('#end_date').val($('#start_date').val());
                $('#start_date').blur(function () {
                    $('#end_date').val($(this).val());
                })
            }

            $('.checkbox').on('ifChecked', function(e) {
                var start = '00:00:00';
                var end = '23:59:59';

                $('#start_date').blur(function () {
                    $('#end_date').val($(this).val());
                })
                $('#start_hour').val(start);
                $('#end_date').val($('#start_date').val());
                $('#end_hour').val(end);
                $('.hour-fields').hide();
            });

            $('.checkbox').on('ifUnchecked', function(e) {
                $('#start_hour').val('');
                $('#end_date').val('');
                $('#end_hour').val('');
                $('.hour-fields').show();
            });

            $('[name=backgroundColor]:checked').parent().addClass('checked');
            $('[name=backgroundColor]').click(function () {
                $('[name=backgroundColor]').parent().removeClass('checked');

                $('[name=backgroundColor]:checked').parent().addClass('checked');
            });

            var a = {!! $eventsJson !!};
            var events = [];

            for (var i = 0; i < a.length; i++) {
                events.push(a[i]);
            }

            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                buttonText: {
                    today: "{{ ucfirst(__('dictionary.today')) }}",
                    month: "{{ ucfirst(__('dictionary.month')) }}",
                    week: "{{ ucfirst(__('dictionary.week')) }}",
                    day: "{{ ucfirst(__('dictionary.day')) }}"
                },
                events: events,
                eventClick: function (data) {
                    console.warn(data);
                }
            })
        })
    </script>
@endpush
