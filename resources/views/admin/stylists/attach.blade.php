@extends('adminlte::page')

@section('title', __('text.attach_services'))

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.services', 0)) }} <small>{{ __('dictionary.attach') }}</small></h1>

    {{-- @include('admin.services.partials.breadcrumb') --}}
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="admin-services">
        <div class="box box-success color-palette-box">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ __('text.attach_services') }}
                </h3>
            </div>
            <div class="box-body">
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="/css/custom.css">
@endpush
