@extends('adminlte::page')

@section('title', $user->name)

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.stylists', 1)) }} <small>{{ __('dictionary.show') }}</small></h1>

    @include('admin.stylists.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="admin-stylists">
        @include('admin.users.partials.body', ['photoBook' => $photoBook])
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="/css/custom.css">
@endpush
