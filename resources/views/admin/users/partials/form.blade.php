<div class="row">
    <div class="col-md-6">
        <div class="form-group @if($errors->has('email')) has-error @endif">
            <label for="email">{{ ucfirst(__('dictionary.email')) }}</label>
            <input type="email"
                   name="email"
                   value="@if(old('email')){{ old('email') }}@elseif(isset($user)){{ $user->email }}@endif"
                   class="form-control"
                   id="email">
            @if($errors->has('email'))
                <span class="help-block">{{ $errors->first('email') }}</span>
            @endif
        </div>

        <div class="form-group @if($errors->has('password')) has-error @endif">
            <label for="password">{{ ucfirst(__('dictionary.password')) }}</label>
            <input type="password" name="password" class="form-control" id="password">
            @if($errors->has('password'))
                <span class="help-block">{{ $errors->first('password') }}</span>
            @endif
        </div>

        <div class="form-group @if($errors->has('password_confirmation')) has-error @endif">
            <label for="password_confirmation">{{ __('text.password_confirmation') }}</label>
            <input type="password" name="password_confirmation" class="form-control" id="password_confirmation">
            @if($errors->has('password_confirmation'))
                <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
            @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group @if($errors->has('name')) has-error @endif">
            <label for="name">{{ ucfirst(__('dictionary.name')) }}</label>
            <input type="text"
                   name="name"
                   value="@if(old('name')){{ old('name') }}@elseif(isset($user)){{ $user->name }}@endif"
                   class="form-control"
                   id="name">
            @if($errors->has('name'))
                <span class="help-block">{{ $errors->first('name') }}</span>
            @endif
        </div>

        <div class="form-group @if($errors->has('phone')) has-error @endif">
            <label for="phone">{{ ucfirst(__('dictionary.phone')) }}</label>
            <input type="text"
                   name="phone"
                   value="@if(old('phone')){{ old('phone') }}@elseif(isset($user)){{ $user->profile->phone }}@endif"
                   class="form-control"
                   id="phone">
            @if($errors->has('phone'))
                <span class="help-block">{{ $errors->first('phone') }}</span>
            @endif
        </div>
    </div>
</div>
