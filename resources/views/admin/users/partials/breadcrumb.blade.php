<ol class="breadcrumb">
    <li class="@if(Route::currentRouteName() == 'users.index') active @endif">
        <a href="{{ route('users.index') }}">
            <i class="fa fa-paint-brush"></i>
            {{ ucfirst(trans_choice('dictionary.users', 0)) }}
        </a>
    </li>

    @if(Route::currentRouteName() == 'users.create')
        <li class="active">
            <a href="{{ route('users.create') }}">
                {{ ucfirst(trans('dictionary.new')) }}
            </a>
        </li>
    @endif

    @if(Route::currentRouteName() == 'users.show' AND isset($user))
        <li class="active">
            <a href="{{ route('users.show', $user) }}">
                {{ $user->name }}
            </a>
        </li>
    @endif

    @if(Route::currentRouteName() == 'users.edit' AND isset($user))
        <li class="active">
            <a href="{{ route('users.edit', $user) }}">
                {{ ucfirst(trans('dictionary.edit')) }}
            </a>
        </li>
    @endif
</ol>
