<div class="box box-widget widget-user-2">
    <div class="widget-user-header bg-green">
        <div class="widget-user-image">
            <img class="img-circle" src="{{ $user->profile->avatar }}" alt="User Avatar">
        </div>

        <h3 class="widget-user-username">{{ $user->name }}</h3>
        <h5 class="widget-user-desc">{{ $user->email }}</h5>
    </div>

    <div class="box-body no-padding">
        <ul class="nav nav-stacked">
            <li class="p">
                {{ ucfirst(__('dictionary.name')) }}
                <span class="pull-right">{{ $user->name }}</span>
            </li>
            <li class="p">
                {{ ucfirst(__('dictionary.email')) }}
                <span class="pull-right">{{ $user->email }}</span>
            </li>
            <li class="p">
                {{ ucfirst(__('dictionary.phone')) }}
                <span class="pull-right">{{ $user->profile->phone }}</span>
            </li>
            <li class="p">
                {{ ucfirst(__('dictionary.active')) }}
                <span class="pull-right">@check($user->verified)</span>
            </li>
            <li class="p">
                {{ __('text.created_at') }}
                <span class="pull-right">@datetime($user->created_at)</span>
            </li>
            <li class="p">
                {{ __('text.updated_at') }}
                <span class="pull-right">@datetime($user->updated_at)</span>
            </li>
            <li class="p">
                {{ ucfirst(__('dictionary.avatar')) }}
                <span class="pull-right">
                    <button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#user-avatar">
                        <i class="fa fa-photo"></i>
                    </button>
                </span>
            </li>
        </ul>
    </div>

    <div class="box-footer">
        @include('admin.partials.btnEdit', ['route_name' => 'users.edit', 'model' => $user])

        <div class="pull-right">
            @include('admin.users.partials.active')

            @include('admin.users.partials.deactivate')
        </div>
    </div>
</div>

<div class="modal fade" id="user-avatar" tabindex="-1" role="dialog" aria-labelledby="serviceModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">{{ ucfirst(__('dictionary.avatar')) }}</h4>
            </div>
            <div class="modal-body">
                <img src="{{ $user->profile->avatar }}" class="img-responsive" alt="avatar">
            </div>
        </div>
    </div>
</div>
