@if(!$user->verified)
    <form action="{{ route('users.active', $user) }}" method="POST" class="form-inline">
        {{ csrf_field() }}

        <button type="submit" class="btn btn-sm btn-warning">
            <i class="fa fa-check"></i> {{ ucfirst(__('dictionary.active')) }}
        </button>
    </form>
@endif
