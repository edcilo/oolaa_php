<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        @if($user->stylist)
            <li class="active">
                <a href="#stylist-tab" data-toggle="tab" aria-expanded="true">
                    {{ ucfirst(__('dictionary.stylist')) }}
                </a>
            </li>
        @endif
        @if($user->customer)
            <li class="active">
                <a href="#customer-tab" data-toggle="tab" aria-expanded="false">
                    {{ ucfirst(__('dictionary.customer')) }}
                </a>
            </li>
        @endif
        <li>
            <a href="#account" data-toggle="tab" aria-expanded="false">
                {{ ucfirst(__('dictionary.account')) }}
            </a>
        </li>
    </ul>
    <div class="tab-content">
        @if($user->stylist)
            <div class="tab-pane @if($user->stylist) active @endif" id="stylist-tab">
                @include('admin.stylists.partials.status', ['stylist' => $user->stylist])

                <hr>

                @include('admin.stylists.partials.calendar', ['stylist' => $user->stylist])

                <hr>

                @include('admin.stylists.partials.services', ['services' => $user->stylist->services])

                <hr>

                @include('admin.photobook.show', ['photoBook' => $photoBook])

                <hr>

                @include('admin.curriculum.show', ['curriculum' => $user->stylist->curriculum])
            </div>
        @endif

        <div class="tab-pane @if($user->customer) active @endif" id="customer-tab">
            Hello world
        </div>

        <div class="tab-pane" id="account">
            @if(!$user->stylist)
                @include('admin.users.partials.createStylistProfile')

                <hr>
            @endif

            @include('admin.oauth.show', ['oauth' => $user->oauth])
        </div>
    </div>
</div>
