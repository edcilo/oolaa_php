<h4 class="text-muted">
    <i class="fa fa-paint-brush"></i>
</h4>

<form action="{{ route('users.create.profile.stylist', $user) }}" method="POST" class="text-center">
    {{ csrf_field() }}

    <button type="submit" class="btn btn-warning">
        {{ __('text.create_stylist_profile') }}
    </button>
</form>
