@if($user->verified)
    <form action="{{ route('users.deactivate', $user) }}" method="POST" class="form-inline">
        {{ csrf_field() }}

        <button type="submit" class="btn btn-sm btn-danger">
            <i class="fa fa-times"></i> {{ ucfirst(__('dictionary.deactivate')) }}
        </button>
    </form>
@endif
