<div class="row">
    <div class="col-md-5">
        @include('admin.users.partials.card')

        @include('admin.addresses.partials.list', ['addresses' => $addresses, 'route_param' => $user])

        @include('admin.creditCards.partials.list', ['creditCards' => $creditCards, 'route_param' => $user])

        <div class="box box-danger">
            <div class="box-body">
                @include('admin.users.partials.delete')
            </div>
        </div>
    </div>

    <div class="col-md-7">
        @include('admin.partials.errors')

        @include('admin.users.partials.tabs', ['photoBook' => $photoBook])
    </div>
</div>
