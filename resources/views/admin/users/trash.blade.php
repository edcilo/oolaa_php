@extends('adminlte::page')

@section('title', ucfirst(trans_choice('dictionary.users', 0)))

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.users', 0)) }} <small>{{ ucfirst(__('dictionary.all')) }}</small></h1>

    @include('admin.users.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    @include('admin.partials.errors')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget">
                <div class="box-header with-border row">
                    <div class="col-xs-7">
                        <button type="submit" form="restore_multiple" class="btn btn-sm btn-warning">
                            <i class="ti-trash"></i>
                            {{ __('text.restore_selected') }}
                        </button>
                    </div>
                    <div class="col-xs-5">
                        @include('admin.partials.formSearch', ['route_name' => 'users.trash'])
                    </div>
                </div>

                <div class="box-body table-responsive no-padding">
                    <form action="{{ route('users.restore.multiple') }}" id="restore_multiple" method="POST">
                        {{ csrf_field() }}

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">
                                    <input type="checkbox"
                                           name="select_all"
                                           class="checkbox-delete-trigger"
                                           title="{{ __('text.select_all') }}">
                                </th>
                                <th class="text-center">{{ ucfirst(__('dictionary.name')) }}</th>
                                <th class="text-center">{{ ucfirst(__('dictionary.email')) }}</th>
                                <th class="text-center">{{ ucfirst(__('dictionary.phone')) }}</th>
                                <th class="text-center">{{ __('text.created_at') }}</th>
                                <th class="text-center">{{ __('text.deleted_at') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($users as $user)
                                <tr>
                                    <td class="text-center">
                                        <input type="checkbox"
                                               name="user_ids[]"
                                               value="{{ $user->id }}"
                                               class="checkbox-delete">
                                    </td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>@if($user->profile->phone) {{ $user->profile->phone }} @else ... @endif</td>
                                    <td>@date($user->created_at)</td>
                                    <td>@date($user->deleted_at)</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7">
                                        <div class="text-muted text-center h1">
                                            <i class="fa fa-search"></i>
                                            <span>{{ __('text.no_results_found') }}</span>
                                        </div>
                                    </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </form>
                </div>

                <div class="box-footer clearfix">
                    @include('admin.partials.paginateFooter',
                        ['model' => $users, 'route_name' => 'users.trash', 'model_name_trans' => 'dictionary.users'])
                </div>
            </div>
        </div>
    </div>
@stop

@push('css')
    <link rel="stylesheet" href="{{ mix('/css/plugins.css') }}">
@endpush

@push('js')
    <script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ mix('/js/script.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            checkBoxes('input');
            iCheck_checkAll('.checkbox-delete-trigger', '.checkbox-delete');
        })
    </script>
@endpush
