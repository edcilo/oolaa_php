@extends('adminlte::page')

@section('title', ucfirst(trans_choice('dictionary.coupons', 0)))

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.coupons', 0)) }} <small>{{ ucfirst(__('dictionary.all')) }}</small></h1>

    @include('admin.coupons.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    @include('admin.partials.errors')

    {{--
    @if(session('restore'))
        @include('admin.partials.formRestore', [
            'message' => __('text.entity_deleted', ['entity' => __('dictionary.category') . ' ' . session('restore')->name]),
            'btn_text' => __('text.restore_entity', ['entity' => __('dictionary.category')]),
            'route_name' => 'categories.restore',
            'entity' => session('restore'),
            'field_name' => 'category_id',
        ])
    @endif

    @if(session('restoreMultiple'))
        @include('admin.partials.formRestoreMultiple', [
            'message' => __('text.entity_deleted_group', ['quantity' => count(session('restoreMultiple')), 'entity' => trans_choice('dictionary.categories', 0)]),
            'btn_text' => __('text.restore_entity', ['entity' => trans_choice('dictionary.categories', 0)]),
            'route_name' => 'categories.restore.multiple',
            'ids' => session('restoreMultiple'),
            'array_name' => 'category_ids',
        ])
    @endif
    --}}

    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget">
                <div class="box-header with-border row">
                    <div class="col-xs-7">
                        <button type="submit" form="destroy_multiple" class="btn btn-sm btn-danger">
                            <i class="ti-trash"></i>
                            {{ __('text.delete_selected') }}
                        </button>
                    </div>
                    <div class="col-xs-5">
                        @include('admin.partials.formSearch', ['route_name' => 'coupons.index'])
                    </div>
                </div>

                <div class="box-body table-responsive no-padding">
                    <form action="{{ route('coupons.destroy.multiple') }}" id="destroy_multiple" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">
                                    <input type="checkbox"
                                           name="select_all"
                                           class="checkbox-delete-trigger"
                                           title="{{ __('text.select_all') }}">
                                </th>
                                <th class="text-center">{{ ucfirst(__('dictionary.code')) }}</th>
                                <th class="text-center">{{ __('text.start_at') }}</th>
                                <th class="text-center">{{ __('text.end_at') }}</th>
                                <th class="text-center">{{ ucfirst(__('dictionary.type')) }}</th>
                                <th class="text-center">{{ ucfirst(__('dictionary.discount')) }}</th>
                                <th class="text-center">{{ __('text.created_at') }}</th>
                                <th class="text-center">
                                    <i class="fa fa-gears"></i>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($coupons as $coupon)
                                <tr>
                                    <td class="text-center">
                                        <input type="checkbox"
                                               name="coupon_ids[]"
                                               value="{{ $coupon->id }}"
                                               class="checkbox-delete">
                                    </td>
                                    <td>{{ $coupon->code }}</td>
                                    <td>@date($coupon->start)</td>
                                    <td>@date($coupon->end)</td>
                                    <td>{{ $coupon->type }}</td>
                                    <td class="text-right">
                                        @if($coupon->type === 'P')
                                            {{ "{$coupon->amount} %" }}
                                        @elseif ($coupon->type === 'F')
                                            @currency($coupon->amount)
                                        @else
                                            {{ $coupon->amount }}
                                        @endif
                                    </td>
                                    <td>@date($coupon->created_at)</td>
                                    <td class="text-center">
                                        <nobr>
                                            @include('admin.partials.btnShow', ['route_name' => 'coupons.show', 'model' => $coupon])
                                            @include('admin.partials.btnEdit', ['route_name' => 'coupons.edit', 'model' => $coupon])
                                        </nobr>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8">
                                        <div class="text-muted text-center h1">
                                            <i class="fa fa-search"></i>
                                            <span>{{ __('text.no_results_found') }}</span>
                                        </div>
                                    </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </form>
                </div>

                <div class="box-footer clearfix">
                    @include('admin.partials.paginateFooter',
                        ['model' => $coupons, 'route_name' => 'coupons.index', 'model_name_trans' => 'dictionary.coupons'])
                </div>
            </div>
        </div>
    </div>
@stop

@push('css')
    <link rel="stylesheet" href="{{ mix('/css/plugins.css') }}">
@endpush

@push('js')
    <script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ mix('/js/script.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            checkBoxes('input');
            iCheck_checkAll('.checkbox-delete-trigger', '.checkbox-delete');
        })
    </script>
@endpush
