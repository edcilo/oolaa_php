@extends('adminlte::page')

@section('title', ucfirst(__('dictionary.coupon')) . ' - ' . $coupon->code)

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.coupons', 1)) }} <small>{{ __('dictionary.show') }}</small></h1>

    @include('admin.coupons.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="row admin-category">
        <div class="col-md-5">
            @include('admin.coupons.partials.card')

            <div class="box box-danger">
                <div class="box-body">
                    @include('admin.coupons.partials.delete')
                </div>
            </div>
        </div>

        <div class="col-md-7"></div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="/css/custom.css">
@endpush
