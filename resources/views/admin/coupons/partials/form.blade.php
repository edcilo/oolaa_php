<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('code') ? 'has-error' : '' }}">
            <label for="code">{{ ucfirst(__('dictionary.code')) }}</label>
            <input type="text"
                   name="code"
                   value="@if(old('code')){{ old('code') }}@elseif(isset($coupon)){{ $coupon->code }}@endif"
                   class="form-control"
                   id="code">
            @if($errors->has('code'))
                <span class="help-block">{{ $errors->first('code') }}</span>
            @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group {{ $errors->has('redemptions') ? 'has-error' : '' }}">
            <label for="code">{{ ucfirst(__('text.number_of_redemptions')) }}</label>
            <input type="text"
                   name="redemptions"
                   value="@if(old('redemptions')){{ old('redemptions') }}@elseif(isset($coupon)){{ $coupon->redemptions }}@endif"
                   class="form-control"
                   id="redemptions">
            @if($errors->has('redemptions'))
                <span class="help-block">{{ $errors->first('redemptions') }}</span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('start') ? 'has-error' : '' }}">
            <label for="code">{{ ucfirst(__('text.start_at')) }}</label>
            <input type="text"
                   name="start"
                   value="@if(old('start')){{ old('start') }}@elseif(isset($coupon)){{ $coupon->start }}@endif"
                   class="form-control datepicker"
                   id="start">
            @if($errors->has('start'))
                <span class="help-block">{{ $errors->first('start') }}</span>
            @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group {{ $errors->has('end') ? 'has-error' : '' }}">
            <label for="code">{{ ucfirst(__('text.end_at')) }}</label>
            <input type="text"
                   name="end"
                   value="@if(old('end')){{ old('end') }}@elseif(isset($coupon)){{ $coupon->end }}@endif"
                   class="form-control datepicker"
                   id="end">
            @if($errors->has('end'))
                <span class="help-block">{{ $errors->first('end') }}</span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
            <label for="code">{{ ucfirst(__('dictionary.type')) }}</label>
            <select name="type" id="type" class="form-control">
                <option>{{ ucfirst(__('dictionary.select')) }}</option>
                @foreach(__('abbr.coupon') as $key => $type)
                    <option value="{{ $key }}" @if(old('type') === $key OR (isset($coupon) AND $coupon->type === $key)) selected @endif>{{ $type }}</option>
                @endforeach
            </select>
            @if($errors->has('type'))
                <span class="help-block">{{ $errors->first('type') }}</span>
            @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group {{ $errors->has('amount') ? 'has-error' : '' }}">
            <label for="code">{{ ucfirst(__('dictionary.amount')) }}</label>
            <input type="text"
                   name="amount"
                   value="@if(old('amount')){{ old('amount') }}@elseif(isset($coupon)){{ $coupon->amount }}@endif"
                   class="form-control"
                   id="amount">
            @if($errors->has('amount'))
                <span class="help-block">{{ $errors->first('amount') }}</span>
            @endif
        </div>
    </div>
</div>
