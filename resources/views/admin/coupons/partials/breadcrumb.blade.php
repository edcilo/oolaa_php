<ol class="breadcrumb">
    <li class="@if(Route::currentRouteName() == 'coupons.index') active @endif">
        <a href="{{ route('coupons.index') }}">
            <i class="fa fa-ticket"></i>
            {{ ucfirst(trans_choice('dictionary.coupons', 0)) }}
        </a>
    </li>

    @if(Route::currentRouteName() == 'coupons.create')
        <li class="active">
            <a href="{{ route('coupons.create') }}">
                {{ ucfirst(trans('dictionary.new')) }}
            </a>
        </li>
    @endif

    @if(Route::currentRouteName() == 'coupons.show' AND isset($coupon))
        <li class="active">
            <a href="{{ route('coupons.show', $coupon) }}">
                {{ $coupon->code }}
            </a>
        </li>
    @endif

    @if(Route::currentRouteName() == 'coupons.edit' AND isset($coupon))
        <li class="active">
            <a href="{{ route('coupons.edit', $coupon) }}">
                {{ ucfirst(trans('dictionary.edit')) }}
            </a>
        </li>
    @endif
</ol>
