<div class="box box-widget widget-user">
    <div class="widget-user-header bg-green">
        <h3 class="widget-user-username">{{ ucfirst(__('dictionary.coupon')) }} {{ $coupon->code }}</h3>
    </div>

    <div class="box-body no-padding">
        <ul class="nav nav-stacked">
            <li class="p">
                {{ ucfirst(trans_choice('dictionary.redemptions', 0)) }}
                <span class="pull-right">{{ $coupon->redemptions }}</span>
            </li>
            <li class="p">
                {{ ucfirst(__('dictionary.type')) }}
                <span class="pull-right">{{ __("abbr.coupon.{$coupon->type}") }}</span>
            </li>
            <li class="p">
                {{ ucfirst(__('dictionary.discount')) }}
                <span class="pull-right">
                    @if($coupon->type === 'F')
                        @currency($coupon->amount)
                    @elseif($coupon->type === 'P')
                        {{ "{$coupon->amount}%" }}
                    @else
                        {{ $coupon->amount }}
                    @endif
                </span>
            </li>
            <li class="p">
                {{ __('text.start_at') }}
                <span class="pull-right">@datetime($coupon->start)</span>
            </li>
            <li class="p">
                {{ __('text.end_at') }}
                <span class="pull-right">@datetime($coupon->end)</span>
            </li>
            <li class="p">
                {{ __('text.created_at') }}
                <span class="pull-right">@datetime($coupon->created_at)</span>
            </li>
            <li class="p">
                {{ __('text.updated_at') }}
                <span class="pull-right">@datetime($coupon->updated_at)</span>
            </li>
        </ul>
    </div>

    <div class="box-footer">
        @include('admin.partials.btnEdit', ['route_name' => 'coupons.edit', 'model' => $coupon])
    </div>
</div>
