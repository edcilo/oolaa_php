@extends('adminlte::page')

@section('title', ucfirst(trans_choice('dictionary.categories', 0)))

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.categories', 0)) }} <small>{{ ucfirst(__('dictionary.edit')) }}</small></h1>

    @include('admin.serviceCategories.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('text.edit_entity', ['entity' => __('dictionary.category')]) }}</h3>
                </div>

                <form action="{{ route('service-categories.update', $category) }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="box-body">
                        @include('admin.serviceCategories.partials.form')
                    </div>

                    <div class="box-footer clearfix text-right">
                        <button class="btn btn-success" type="submit" name="save" value="0">
                            <i class="fa fa-save"></i>
                            {{ ucfirst(__('dictionary.save')) }}
                        </button>

                        <button class="btn btn-warning" type="submit" name="save" value="1">
                            <i class="fa fa-plus"></i>
                            {{ __('text.save_and_close') }}
                        </button>

                        <a href="{{ route('service-categories.index') }}" class="btn btn-danger">
                            <i class="fa fa-times"></i>
                            {{ ucfirst(__('dictionary.cancel')) }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
