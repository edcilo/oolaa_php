@extends('adminlte::page')

@section('title', ucfirst(trans_choice('dictionary.categories', 0)))

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.categories', 0)) }} <small>{{ ucfirst(__('dictionary.all')) }}</small></h1>

    @include('admin.serviceCategories.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    @include('admin.partials.errors')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget">
                <div class="box-header with-border row">
                    <div class="col-xs-7">
                        <a class="btn btn-sm btn-success" href="{{ route('service-categories.create') }}">
                            <i class="fa fa-plus"></i>
                            {{ ucfirst(__('dictionary.new')) }}
                        </a>
                    </div>
                    <div class="col-xs-5">
                        @include('admin.partials.formSearch', ['route_name' => 'service-categories.index'])
                    </div>
                </div>

                <div class="box-body table-responsive no-padding">

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th class="text-center">{{ ucfirst(__('dictionary.name')) }}</th>
                            <th class="text-center">{{ ucfirst(__('dictionary.position')) }}</th>
                            <th class="text-center">{{ __('text.position_in_the_search') }}</th>
                            <th class="text-center">{{ __('text.created_at') }}</th>
                            <th class="text-center">
                                <i class="fa fa-gears"></i>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($categories as $category)
                            <tr>
                                <td>{{ $category->name }}</td>
                                <td class="text-center">{{ $category->order }}</td>
                                <td class="text-center">{{ $category->shopping_order }}</td>
                                <td>@date($category->created_at)</td>
                                <td class="text-center">
                                    <nobr>
                                        @include('admin.partials.btnShow', ['route_name' => 'service-categories.show', 'model' => $category])
                                        @include('admin.partials.btnEdit', ['route_name' => 'service-categories.edit', 'model' => $category])
                                    </nobr>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">
                                    <div class="text-muted text-center h1">
                                        <i class="fa fa-search"></i>
                                        <span>{{ __('text.no_results_found') }}</span>
                                    </div>
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>

                <div class="box-footer clearfix">
                    @include('admin.partials.paginateFooter',
                        ['model' => $categories, 'route_name' => 'service-categories.index', 'model_name_trans' => 'dictionary.categories'])
                </div>
            </div>
        </div>
    </div>
@stop

@push('css')
    <link rel="stylesheet" href="{{ mix('/css/plugins.css') }}">
@endpush

@push('js')
    <script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ mix('/js/script.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            checkBoxes('input');
            iCheck_checkAll('.checkbox-delete-trigger', '.checkbox-delete');
        })
    </script>
@endpush
