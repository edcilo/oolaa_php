<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title">{{ ucfirst(trans_choice('dictionary.services', 0)) }}</h3>
    </div>

    <div class="box-body table-responsive no-padding">
        <table class="table table-striped">
            <thead>
            <tr>
                <th class="text-center">{{ ucfirst(__('dictionary.name')) }}</th>
                <th class="text-center">{{ ucfirst(__('dictionary.price')) }}</th>
                <th class="text-center">{{ __('text.average_time') }}</th>
                <th class="text-center">{{ __('text.created_at') }}</th>
                <th class="text-center">
                    <i class="fa fa-gears"></i>
                </th>
            </tr>
            </thead>
            <tbody>
            @forelse ($services as $service)
                <tr>
                    <td>{{ $service->full_name }}</td>
                    <td class="text-right">@currency($service->price)</td>
                    <td class="text-right">@minutesToHours($service->average_time)</td>
                    <td class="text-center">@date($service->created_at)</td>
                    <td class="text-center">
                        <nobr>
                            @include('admin.partials.btnShow', ['route_name' => 'services.show', 'model' => $service])
                        </nobr>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5">
                        <div class="text-muted text-center h1">
                            <i class="fa fa-search"></i>
                            <span>{{ __('text.no_results_found') }}</span>
                        </div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>