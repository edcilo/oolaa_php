<div class="row">
    <div class="col-md-6">
        <div class="form-group @if($errors->has('icon')) has-error @endif">
            <label for="icon">{{ ucfirst(__('dictionary.icon')) }}</label>
            <input type="file" class="form-control" name="icon" id="icon">
            {{-- <p class="help-block">Example block-level help text here.</p> --}}
            @if($errors->has('icon'))
                <span class="help-block">{{ $errors->first('icon') }}</span>
            @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group @if($errors->has('cover')) has-error @endif">
            <label for="cover">{{ ucfirst(__('dictionary.cover')) }}</label>
            <input type="file" class="form-control" name="cover" id="cover">
            {{-- <p class="help-block">Example block-level help text here.</p> --}}
            @if($errors->has('cover'))
                <span class="help-block">{{ $errors->first('cover') }}</span>
            @endif
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group @if($errors->has('name')) has-error @endif">
            <label for="name">{{ ucfirst(__('dictionary.name')) }}</label>
            <input type="text"
                   name="name"
                   value="@if(old('name')){{ old('name') }}@elseif(isset($category)){{ $category->name }}@endif"
                   class="form-control"
                   id="name">
            @if($errors->has('name'))
                <span class="help-block">{{ $errors->first('name') }}</span>
            @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group @if($errors->has('order')) has-error @endif">
            <label for="order">{{ ucfirst(__('dictionary.position')) }}</label>
            <input type="text"
                   name="order"
                   value="@if(old('order')){{ old('order') }}@elseif(isset($category)){{ $category->order }}@endif"
                   class="form-control"
                   id="order">
            @if($errors->has('order'))
                <span class="help-block">{{ $errors->first('order') }}</span>
            @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group @if($errors->has('shopping_order')) has-error @endif">
            <label for="shopping_order">{{ __('text.position_in_the_search') }}</label>
            <input type="text"
                   name="shopping_order"
                   value="@if(old('shopping_order')){{ old('shopping_order') }}@elseif(isset($category)){{ $category->shopping_order }}@endif"
                   class="form-control"
                   id="shopping_order">
            @if($errors->has('shopping_order'))
                <span class="help-block">{{ $errors->first('shopping_order') }}</span>
            @endif
        </div>
    </div>
</div>
