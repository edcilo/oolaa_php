<div class="box box-widget widget-user-2">
    <div class="widget-user-header bg-green" style="background: url({{ $category->cover }}) center center;">
        <div class="widget-user-image">
            <img class="img-circle" src="{{ $category->icon }}" alt="User Avatar">
        </div>

        <h3 class="widget-user-username">{{ $category->name }}</h3>
        <h5 class="widget-user-desc">&nbsp;</h5>
    </div>

    <div class="box-body no-padding">
        <ul class="nav nav-stacked">
            <li class="p">
                {{ ucfirst(__('dictionary.name')) }}
                <span class="pull-right">{{ $category->name }}</span>
            </li>
            <li class="p">
                {{ ucfirst(__('dictionary.position')) }}
                <span class="pull-right">{{ $category->order }}</span>
            </li>
            <li class="p">
                {{ __('text.position_in_the_search') }}
                <span class="pull-right">{{ $category->shopping_order }}</span>
            </li>
            <li class="p">
                {{ __('text.created_at') }}
                <span class="pull-right">@datetime($category->created_at)</span>
            </li>
            <li class="p">
                {{ __('text.updated_at') }}
                <span class="pull-right">@datetime($category->updated_at)</span>
            </li>
        </ul>
    </div>

    <div class="box-footer">
        @include('admin.partials.btnEdit', ['route_name' => 'service-categories.edit', 'model' => $category])
    </div>
</div>
