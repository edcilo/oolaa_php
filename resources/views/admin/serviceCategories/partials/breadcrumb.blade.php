<ol class="breadcrumb">
    <li class="@if(Route::currentRouteName() == 'service-categories.index') active @endif">
        <a href="{{ route('service-categories.index') }}">
            <i class="fa fa-paint-brush"></i>
            {{ ucfirst(trans_choice('dictionary.categories', 0)) }}
        </a>
    </li>

    @if(Route::currentRouteName() == 'service-categories.create')
        <li class="active">
            <a href="{{ route('service-categories.create') }}">
                {{ ucfirst(trans('dictionary.new')) }}
            </a>
        </li>
    @endif

    @if(Route::currentRouteName() == 'service-categories.show' AND isset($category))
        <li class="active">
            <a href="{{ route('service-categories.show', $category) }}">
                {{ $category->name }}
            </a>
        </li>
    @endif

    @if(Route::currentRouteName() == 'service-categories.edit' AND isset($category))
        <li class="active">
            <a href="{{ route('service-categories.edit', $category) }}">
                {{ ucfirst(trans('dictionary.edit')) }}
            </a>
        </li>
    @endif
</ol>
