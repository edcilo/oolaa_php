@extends('adminlte::page')

@section('title', $category->name)

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.categories', 1)) }} <small>{{ __('dictionary.show') }}</small></h1>

    @include('admin.serviceCategories.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="row admin-category">
        <div class="col-md-5">
            @include('admin.serviceCategories.partials.card')

            <div class="box box-danger">
                <div class="box-body">
                    @include('admin.serviceCategories.partials.delete')
                </div>
            </div>
        </div>

        <div class="col-md-7">
            @include('admin.serviceCategories.partials.services')
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="/css/custom.css">
    <style>
        .widget-user-image .img-circle {
            background: #ffffff;
        }
    </style>
@endpush
