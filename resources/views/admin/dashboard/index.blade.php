@extends('adminlte::page')

@section('title', ucfirst(trans_choice('dictionary.dashboard', 0)))

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.dashboard', 0)) }}</h1>

    @include('admin.dashboard.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    @include('admin.partials.errors')

    {{--
    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget">
                <div class="box-header with-border row"></div>

                <div class="box-body table-responsive no-padding"></div>
            </div>
        </div>
    </div>
    --}}
@stop

@push('css')
@endpush

@push('js')
@endpush
