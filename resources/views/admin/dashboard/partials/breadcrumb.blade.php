<ol class="breadcrumb">
    <li class="@if(Route::currentRouteName() == 'dashboard') active @endif">
        <a href="{{ route('dashboard') }}">
            <i class="fa fa-dashboard"></i>
            {{ ucfirst(trans_choice('dictionary.dashboard', 0)) }}
        </a>
    </li>
</ol>
