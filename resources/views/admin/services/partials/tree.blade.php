<ul>
    @foreach($services as $parent)
        <li>
            <a href="{{ route('services.show', $parent) }}">
                {{ $parent->full_name }}
            </a>

            @include('admin.services.partials.tree', ['services' => $parent->children])
        </li>
    @endforeach
</ul>