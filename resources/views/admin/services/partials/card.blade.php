<div class="box box-widget widget-user-2">
    <div class="widget-user-header bg-green" style="background: url({{ $service->image }}) center center;">
        <div class="widget-user-image">
            <img class="img-circle" src="{{ $service->icon }}" alt="User Avatar">
        </div>

        <h3 class="widget-user-username">{{ $service->full_name }}</h3>
        <h5 class="widget-user-desc">{{ $service->name }}</h5>
    </div>

    @if($service->note)
        <div class="box-body">
            <strong>{{ ucfirst(__('dictionary.note')) }}:</strong> {{ $service->note }}
        </div>
    @endif

    <div class="box-body no-padding">
        <ul class="nav nav-stacked">
            <li class="p">
                {{ ucfirst(__('dictionary.price')) }}
                <span class="pull-right">@currency($service->price)</span>
            </li>
            <li class="p">
                {{ __('text.average_time')  }}
                <span class="pull-right">@minutesToHours($service->average_time)</span>
            </li>
            <li class="p">
                {{ ucfirst(__('dictionary.category')) }}
                <span class="pull-right">{{ $service->category ? $service->category->name : ($service->parent_category ? $service->parent_category->name : '...') }}</span>
            </li>
            <li class="p">
                {{ ucfirst(__('dictionary.parent')) }}
                <span class="pull-right">{{ $service->parent ? $service->parent->full_name : '...' }}</span>
            </li>
            <li class="p">
                {{ ucfirst(__('dictionary.position')) }}
                <span class="pull-right">{{ $service->order }}</span>
            </li>
            <li class="p">
                {{ __('text.price_is_mutable') }}
                <span class="pull-right">@check($service->price_is_mutable)</span>
            </li>
            <li class="p">
                {{ ucfirst(__('dictionary.active')) }}
                <span class="pull-right">@check($service->active)</span>
            </li>
            <li class="p">
                {{ ucfirst(__('dictionary.image')) }}
                <span class="pull-right">
                    <button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#service-image">
                        <i class="fa fa-photo"></i>
                    </button>
                </span>
            </li>
        </ul>
    </div>

    <div class="box-footer">
        @include('admin.partials.btnEdit', ['route_name' => 'services.edit', 'model' => $service])
    </div>
</div>

<div class="modal fade" id="service-image" tabindex="-1" role="dialog" aria-labelledby="serviceModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">{{ ucfirst(__('dictionary.image')) }}</h4>
            </div>
            <div class="modal-body">
                <img src="{{ $service->image }}" class="img-responsive" alt="service image">
            </div>
        </div>
    </div>
</div>
