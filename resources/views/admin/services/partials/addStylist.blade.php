<div class="row">
    <div class="col-md-8 col-lg-offset-2">
        <form action="{{ route('services.add.stylist', $service) }}" method="POST">
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-8">
                    <div class="form-group {{ $errors->has('stylist_id') ? 'has-error' : '' }}">
                        <label for="name">{{ ucfirst(__('dictionary.stylist')) }}</label>
                        <select name="stylist_id" id="stylist_id" class="form-control select2">
                            <option value="">{{ ucfirst(__('dictionary.select')) }}</option>
                            @foreach($stylist_list as $stylist)
                                <option value="{{ $stylist->stylist->id }}" {{ (old('stylist_id') === $stylist->stylist->id) ? 'active' : '' }}>{{ $stylist->name }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('stylist_id'))
                            <span class="help-block">{{ $errors->first('stylist_id') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-md-4">
                    <label>&nbsp;</label> <br>
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i>
                        {{ ucfirst(__('dictionary.add')) }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
