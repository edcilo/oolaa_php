<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
            <label for="name">{{ ucfirst(__('dictionary.name')) }}</label>
            <input type="text"
                   name="name"
                   value="@if(old('name')){{ old('name') }}@elseif(isset($service)){{ $service->name }}@endif"
                   class="form-control"
                   id="name">
            @if($errors->has('name'))
                <span class="help-block">{{ $errors->first('name') }}</span>
            @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group {{ $errors->has('full_name') ? 'has-error' : '' }}">
            <label for="full_name">{{ __('text.full_name') }}</label>
            <input type="text"
                   name="full_name"
                   value="@if(old('full_name')){{ old('full_name') }}@elseif(isset($service)){{ $service->full_name }}@endif"
                   class="form-control"
                   id="full_name">
            @if($errors->has('full_name'))
                <span class="help-block">{{ $errors->first('full_name') }}</span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
            <label for="category_id">{{ ucfirst(__('dictionary.category')) }}:</label>
            <select name="category_id" id="category_id" class="form-control">
                <option value="">{{ ucfirst(__('dictionary.select')) }}</option>
                @foreach($categories as $item)
                    <option value="{{ $item->id }}" @if(old('category_id') == $item->id OR (isset($service) AND $service->category_id === $item->id)) selected @endif>
                        {{ $item->name }}
                    </option>
                @endforeach
            </select>
            @if($errors->has('category_id'))
                <span class="help-block">{{ $errors->first('category_id') }}</span>
            @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group {{ $errors->has('parent_id') ? 'has-error' : '' }}">
            <label for="parent_id">{{ ucfirst(__('dictionary.parent')) }}:</label>
            <select name="parent_id" id="parent_id" class="form-control">
                <option value="">{{ ucfirst(__('dictionary.select')) }}</option>
                @foreach($services as $item)
                    <option value="{{ $item->id }}" @if(old('parent_id') == $item->id OR (isset($service) AND $service->parent_id === $item->id)) selected @endif>
                        {{ $item->full_name }}
                    </option>
                @endforeach
            </select>
            @if($errors->has('parent_id'))
                <span class="help-block">{{ $errors->first('parent_id') }}</span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
            <label for="image">{{ ucfirst(__('dictionary.image')) }}</label>
            <input type="file" class="form-control" name="image" id="image">
            {{-- <p class="help-block">Example block-level help text here.</p> --}}
            @if($errors->has('image'))
                <span class="help-block">{{ $errors->first('image') }}</span>
            @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group {{ $errors->has('icon') ? 'has-error' : '' }}">
            <label for="image">{{ ucfirst(__('dictionary.icon')) }}</label>
            <input type="file" class="form-control" name="icon" id="icon">
            {{-- <p class="help-block">Example block-level help text here.</p> --}}
            @if($errors->has('icon'))
                <span class="help-block">{{ $errors->first('icon') }}</span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group {{ $errors->has('average_time') ? 'has-error' : '' }}">
            <label for="average_time">{{ __('text.average_time') }} ({{ ucfirst(trans_choice('dictionary.minutes', 0)) }})</label>
            <input type="text"
                   name="average_time"
                   value="@if(old('average_time')){{ old('average_time') }}@elseif(isset($service)){{ $service->average_time }}@endif"
                   class="form-control"
                   id="average_time">
            @if($errors->has('average_time'))
                <span class="help-block">{{ $errors->first('average_time') }}</span>
            @endif
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">
            <label for="price">{{ ucfirst(__('dictionary.price')) }}</label>
            <input type="text"
                   name="price"
                   value="@if(old('price')){{ old('price') }}@elseif(isset($service)){{ $service->price }}@endif"
                   class="form-control"
                   id="price">
            @if($errors->has('price'))
                <span class="help-block">{{ $errors->first('price') }}</span>
            @endif
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group {{ $errors->has('order') ? 'has-error' : '' }}">
            <label for="order">{{ ucfirst(__('dictionary.position')) }}</label>
            <input type="text"
                   name="order"
                   value="@if(old('order')){{ old('order') }}@elseif(isset($service)){{ $service->order }}@endif"
                   class="form-control"
                   id="order">
            @if($errors->has('order'))
                <span class="help-block">{{ $errors->first('order') }}</span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="form-group {{ $errors->has('note') ? 'has-error' : '' }}">
            <label for="note">{{ ucfirst(__('dictionary.note')) }}</label>
            <textarea name="note"
                      id="note"
                      cols="30"
                      rows="6"
                      class="form-control">@if(old('note')){{ old('note') }}@elseif(isset($service)){{ $service->note }}@endif</textarea>
            @if($errors->has('note'))
                <span class="help-block">{{ $errors->first('note') }}</span>
            @endif
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group {{ $errors->has('price_is_mutable') ? 'has-error' : '' }}">
            <input type="checkbox"
                   name="price_is_mutable"
                   value="1"
                   {{ (old('price_is_mutable') OR (isset($service) AND $service->price_is_mutable)) ? 'checked' : '' }}
                   id="price_is_mutable">
            <label for="price_is_mutable">{{ ucfirst(__('text.price_is_mutable')) }}</label>
            @if($errors->has('price_is_mutable'))
                <span class="help-block">{{ $errors->first('price_is_mutable') }}</span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('active') ? 'has-error' : '' }}">
            <input type="checkbox"
                   name="active"
                   value="1"
                   {{ (old('active') OR (isset($service) AND $service->active)) ? 'checked' : '' }}
                   id="active">
            <label for="active">{{ ucfirst(__('dictionary.active')) }}</label>
            @if($errors->has('active'))
                <span class="help-block">{{ $errors->first('active') }}</span>
            @endif
        </div>
    </div>
</div>
