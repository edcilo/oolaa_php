<ol class="breadcrumb">
    <li class="@if(Route::currentRouteName() == 'services.index') active @endif">
        <a href="{{ route('services.index') }}">
            <i class="fa fa-paint-brush"></i>
            {{ ucfirst(trans_choice('dictionary.services', 0)) }}
        </a>
    </li>

    @if(Route::currentRouteName() == 'services.create')
        <li class="active">
            <a href="{{ route('services.create') }}">
                {{ ucfirst(trans('dictionary.new')) }}
            </a>
        </li>
    @endif

    @if(Route::currentRouteName() == 'services.show' AND isset($service))
        <li class="active">
            <a href="{{ route('services.show', $service) }}">
                {{ $service->name }}
            </a>
        </li>
    @endif

    @if(Route::currentRouteName() == 'services.edit' AND isset($service))
        <li class="active">
            <a href="{{ route('services.edit', $service) }}">
                {{ ucfirst(trans('dictionary.edit')) }}
            </a>
        </li>
    @endif
</ol>
