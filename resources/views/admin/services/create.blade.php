@extends('adminlte::page')

@section('title', ucfirst(trans_choice('dictionary.services', 0)))

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.services', 0)) }} <small>{{ ucfirst(__('dictionary.new')) }}</small></h1>

    @include('admin.services.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('text.create_entity', ['entity' => __('dictionary.service')]) }}</h3>
                </div>

                <form action="{{ route('services.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="box-body">
                        @include('admin.services.partials.form')
                    </div>

                    <div class="box-footer clearfix text-right">
                        <button class="btn btn-success" type="submit" name="save" value="0">
                            <i class="fa fa-save"></i>
                            {{ ucfirst(__('dictionary.save')) }}
                        </button>

                        <button class="btn btn-warning" type="submit" name="save" value="1">
                            <i class="fa fa-plus"></i>
                            {{ __('text.save_and_new') }}
                        </button>

                        <a href="{{ route('services.index') }}" class="btn btn-danger">
                            <i class="fa fa-times"></i>
                            {{ ucfirst(__('dictionary.cancel')) }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@push('css')
    <link rel="stylesheet" href="{{ mix('/css/plugins.css') }}">
@endpush

@push('js')
    <script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ mix('/js/script.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            checkBoxes('input');
            select2('select');
        })
    </script>
@endpush
