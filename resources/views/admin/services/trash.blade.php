@extends('adminlte::page')

@section('title', ucfirst(trans_choice('dictionary.services', 0)))

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.services', 0)) }} <small>{{ ucfirst(__('dictionary.trash')) }}</small></h1>

    @include('admin.services.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    @include('admin.partials.errors')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget">
                <div class="box-header with-border row">
                    <div class="col-xs-7">
                        <button type="submit" form="restore_multiple" class="btn btn-sm btn-warning">
                            <i class="ti-trash"></i>
                            {{ __('text.restore_selected') }}
                        </button>
                    </div>
                    <div class="col-xs-5">
                        @include('admin.partials.formSearch', ['route_name' => 'services.trash'])
                    </div>
                </div>

                <div class="box-body table-responsive no-padding">
                    <form action="{{ route('services.restore.multiple') }}" id="restore_multiple" method="POST">
                        {{ csrf_field() }}

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">
                                    <input type="checkbox"
                                           name="select_all"
                                           class="checkbox-delete-trigger"
                                           title="{{ __('text.select_all') }}">
                                </th>
                                <th class="text-center">{{ ucfirst(__('dictionary.name')) }}</th>
                                <th class="text-center">{{ __('text.created_at') }}</th>
                                <th class="text-center">{{ __('text.deleted_at') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($services as $service)
                                <tr>
                                    <td class="text-center">
                                        <input type="checkbox"
                                               name="service_ids[]"
                                               value="{{ $service->id }}"
                                               class="checkbox-delete">
                                    </td>
                                    <td>{{ $service->name }}</td>
                                    <td>@date($service->created_at)</td>
                                    <td>@date($service->deleted_at)</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">
                                        <div class="text-muted text-center h1">
                                            <i class="fa fa-search"></i>
                                            <span>{{ __('text.no_results_found') }}</span>
                                        </div>
                                    </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </form>
                </div>

                <div class="box-footer clearfix">
                    @include('admin.partials.paginateFooter',
                        ['model' => $services, 'route_name' => 'services.trash', 'model_name_trans' => 'dictionary.services'])
                </div>
            </div>
        </div>
    </div>
@stop

@push('css')
    <link rel="stylesheet" href="{{ mix('/css/plugins.css') }}">
@endpush

@push('js')
    <script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ mix('/js/script.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            checkBoxes('input');
            iCheck_checkAll('.checkbox-delete-trigger', '.checkbox-delete');
        })
    </script>
@endpush
