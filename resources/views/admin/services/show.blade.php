@extends('adminlte::page')

@section('title', $service->name)

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.services', 1)) }} <small>{{ __('dictionary.show') }}</small></h1>

    @include('admin.services.partials.breadcrumb')
@stop

@section('content')
    <div class="row admin-services">
        <div class="col-md-4">
            @include('admin.services.partials.card')

            @include('admin.services.partials.delete')
        </div>

        <div class="col-md-8">
            @include('admin.partials.alerts')

            @include('admin.partials.errors')

            @include('admin.stylists.partials.tableSimple', ['route_param' => $service, 'stylists' => $stylists])
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ mix('/css/plugins.css') }}">
    <link rel="stylesheet" href="/css/custom.css">
@endpush

@push('js')
    <script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ mix('/js/script.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            select2('select');
            checkBoxes('input');
            iCheck_checkAll('.checkbox-delete-trigger', '.checkbox-delete');
        })
    </script>
@endpush
