<div class="admin-curriculum">
    <a href="{{ route('curriculum.create', $user) }}" class="btn btn-sm btn-success pull-right" title="{{ __('dictionary.new') }}">
        <i class="fa fa-plus"></i>
    </a>

    <h3>{{ ucfirst(__('dictionary.curriculum')) }}</h3>

    <div class="row">
        <div class="col-md-12">
            @forelse($curriculum as $data)
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            {{ ucfirst(__('dictionary.type')) }}: {{ ucfirst($data->type) }}
                        </h3>
                    </div>

                    <div class="box-body">
                        {!! $data->data !!}
                    </div>

                    <div class="box-footer text-right">
                        @include('admin.partials.btnEdit', ['route_name' => 'curriculum.edit', 'model' => $data])
                        &nbsp;
                        @include('admin.curriculum.partials.delete', ['curriculum' => $data])
                    </div>
                </div>
            @empty
                <div class="text-muted text-center h1">
                    <span>{{ __('text.no_results_found') }}</span>
                </div>
            @endforelse
        </div>
    </div>
</div>
