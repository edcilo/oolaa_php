<form action="{{ route('curriculum.destroy', $curriculum) }}" method="POST" class="pull-right">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}

    <button class="btn btn-sm btn-danger" type="submit" title="{{ __('dictionary.delete') }}">
        <i class="fa fa-trash"></i>
    </button>
</form>