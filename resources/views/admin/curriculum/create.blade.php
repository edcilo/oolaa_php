@extends('adminlte::page')

@section('title', $user->name)

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.users', 1)) }} <small>{{ __('dictionary.show') }}</small></h1>

    @include('admin.users.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="row">
        <div class="col-md-5">
            @include('admin.users.partials.card')

            @include('admin.addresses.partials.list', ['addresses' => $addresses, 'route_param' => $user])
        </div>

        <div class="col-md-7">
            <div class="box box-widget">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('text.create_entity', ['entity' => __('dictionary.experience')]) }}</h3>
                </div>

                <form action="{{ route('curriculum.store') }}" method="POST">
                    {{ csrf_field() }}

                    <input type="hidden" name="stylist_id" value="{{ $user->stylist->id }}">

                    <div class="box-body">
                        <div class="form-group @if($errors->has('data_type')) has-error @endif">
                            <label>{{ ucfirst(__('dictionary.type')) }}</label>
                            <select class="form-control"
                                    name="data_type">
                                <option>{{ ucfirst(__('dictionary.select')) }}</option>
                                <option value="E" @if(old('data_type'))
                                    {{ old('data_type') === 'E' ? 'selected' : '' }}
                                        @elseif(isset($curriculum))
                                    {{ $curriculum->data_type === 'E' ? 'selected' : '' }}
                                        @endif>
                                    {{ ucfirst(__('abbr.curriculum.E')) }}
                                </option>
                                <option value="X" @if(old('data_type'))
                                    {{ old('data_type') === 'X' ? 'selected' : '' }}
                                        @elseif(isset($curriculum))
                                    {{ $curriculum->data_type === 'X' ? 'selected' : '' }}
                                        @endif>
                                    {{ ucfirst(__('abbr.curriculum.X')) }}
                                </option>
                            </select>
                            @if($errors->has('data_type'))
                                <span class="help-block">{{ $errors->first('data_type') }}</span>
                            @endif
                        </div>

                        <div class="form-group @if($errors->has('data')) has-error @endif">
                            <label for="data">{{ ucfirst(__('dictionary.curriculum')) }}</label>
                            <textarea name="data"
                                      class="form-control"
                                      id="data"
                                      cols="30"
                                      rows="10">@if(old('data')){{ old('data') }}@elseif(isset($curriculum)){{ $curriculum->data }}@endif</textarea>
                            @if($errors->has('data'))
                                <span class="help-block">{{ $errors->first('data') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="box-footer clearfix text-right">
                        <button class="btn btn-success" type="submit" name="save" value="0">
                            <i class="fa fa-save"></i>
                            {{ ucfirst(__('dictionary.save')) }}
                        </button>

                        <button class="btn btn-warning" type="submit" name="save" value="1">
                            <i class="fa fa-plus"></i>
                            {{ __('text.save_and_new') }}
                        </button>

                        <a href="{{ route('stylists.show', $user->stylist) }}" class="btn btn-danger">
                            <i class="fa fa-times"></i>
                            {{ ucfirst(__('dictionary.cancel')) }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="/css/custom.css">
    <link rel="stylesheet" href="/css/plugins.css">
@endpush

@push('js')
    <script type="text/javascript" src="/js/script.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#data').wysihtml5();
        })
    </script>
@endpush