@extends('adminlte::page')

@section('title', ucfirst(__('dictionary.video')))

@section('content_header')
    <h1>{{ __('text.video') }}</h1>
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="admin-cms">
        <div class="box box-widget">
            <div class="box-header with-border">
                @include('admin.cms.home.video.partials.delete')

                <h3 class="box-title">
                    {{ ucfirst(__('dictionary.video')) }}
                </h3>
            </div>

            <div class="box-body">
                {!! $video !!}

                @include('admin.cms.home.video.partials.form')
            </div>
        </div>
    </div>
@endsection
