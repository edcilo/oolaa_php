@if($video)
    <form action="{{ route('cms.home.video.destroy') }}" method="POST">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}

        <button type="submit" class="btn btn-danger pull-right">
            <i class="fa fa-trash"></i>
            {{ ucfirst(__('dictionary.delete')) }}
        </button>
    </form>
@endif