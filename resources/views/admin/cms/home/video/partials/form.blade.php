<form action="{{ route('cms.home.video.store') }}" method="POST">
    {{ csrf_field() }}

    <div class="row">
        <div class="col-md-9">
            <div class="form-group {{ $errors->has('video') ? 'has-error' : '' }}">
                <label for="video">{{ ucfirst(__('dictionary.video')) }} (iframe)</label>
                <input type="text"
                       name="video"
                       value="{{ old('video') }}"
                       class="form-control"
                       id="video">
                @if($errors->has('video'))
                    <span class="help-block">{{ $errors->first('video') }}</span>
                @endif
            </div>
        </div>

        <div class="col-md-3">
            <label>&nbsp;</label>
            <br>
            <button type="submit" class="btn btn-success">
                <i class="fa fa-save"></i>
                {{ ucfirst(__('dictionary.save')) }}
            </button>
        </div>
    </div>
</form>