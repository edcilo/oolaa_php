@extends('adminlte::page')

@section('title', ucfirst(__('dictionary.carousel')))

@section('content_header')
    <h1>{{ ucfirst(__('dictionary.carousel')) }}</h1>
    {{--
        @include('admin.users.partials.breadcrumb')
        --}}
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="admin-cms">
        <div class="box box-widget">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ ucfirst(__('dictionary.new')) }}
                </h3>
            </div>

            <div class="box-body table-responsive">
                @include('admin.cms.home.carousel.partials.form')
            </div>
        </div>

        <div class="box box-widget">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ ucfirst(trans_choice('dictionary.images', 0)) }}
                </h3>
            </div>

            <div class="box-body table-responsive no-padding">
                <table class="table">
                    <thead>
                    <tr>
                        <th>{{ ucfirst(__('dictionary.position')) }}</th>
                        <th>{{ ucfirst(__('dictionary.image')) }}</th>
                        <th class="text-center">
                            <i class="fa fa-gears"></i>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($carousel as $item)
                        <tr>
                            <td class="text-center">{{ $item['order'] }}</td>
                            <td>
                                <a href="{{ $item['picture'] }}" target="_blank">
                                    {{ $item['picture'] }}
                                </a>
                            </td>
                            <td class="text-center">
                                @include('admin.cms.home.carousel.partials.show')

                                @include('admin.cms.home.carousel.partials.delete')
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3">
                                <div class="text-muted text-center h1">
                                    <i class="fa fa-search"></i>
                                    <span>{{ __('text.no_results_found') }}</span>
                                </div>
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
