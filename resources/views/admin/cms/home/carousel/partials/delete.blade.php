<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="{{ "#item-delete-{$loop->index}" }}">
    <i class="fa fa-trash"></i>
</button>

<div class="modal fade" id="{{ "item-delete-{$loop->index}" }}" tabindex="-1" role="dialog" aria-labelledby="itemServiceDeleteModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">{{ ucfirst(__('text.delete_entity', ['entity' => __('dictionary.image')])) }}</h4>
            </div>

            <div class="modal-body">
                <form action="{{ route('cms.home.carousel.destroy') }}" method="POST" class="form-inline">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}

                    <input type="hidden" name="picture" value="{{ $item['picture'] }}">

                    <p>{{ __('text.delete_entity_question', ['entity' => __('dictionary.image')]) }}</p>

                    <button type="submit" class="btn btn-danger btn-block">
                        <i class="fa fa-trash"></i>
                        {{ ucfirst(__('dictionary.delete')) }}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
