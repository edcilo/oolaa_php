<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="{{ "#item-{$loop->index}" }}">
    <i class="fa fa-eye"></i>
</button>

<div class="modal fade" id="{{ "item-{$loop->index}" }}" tabindex="-1" role="dialog" aria-labelledby="imageModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">{{ ucfirst(__('dictionary.image')) }}</h4>
            </div>
            <div class="modal-body">
                <img src="{{ $item['picture'] }}" class="img-responsive" alt="service image">
            </div>
        </div>
    </div>
</div>