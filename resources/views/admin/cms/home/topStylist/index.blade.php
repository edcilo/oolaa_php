@extends('adminlte::page')

@section('title', ucfirst(__('dictionary.carousel')))

@section('content_header')
    <h1>{{ __('text.top_stylists') }}</h1>
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="admin-cms">
        <div class="box box-widget">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ ucfirst(__('dictionary.new')) }}
                </h3>
            </div>

            <div class="box-body table-responsive">
                @include('admin.cms.home.topStylist.partials.form')
            </div>
        </div>

        <div class="box box-widget">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ ucfirst(trans_choice('dictionary.images', 0)) }}
                </h3>
            </div>

            <div class="box-body table-responsive no-padding">
                <table class="table">
                    <thead>
                    <tr>
                        <th>{{ ucfirst(__('dictionary.position')) }}</th>
                        <th>{{ ucfirst(__('dictionary.stylist')) }}</th>
                        <th>{{ ucfirst(__('dictionary.image')) }}</th>
                        <th class="text-center">
                            <i class="fa fa-gears"></i>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($top as $stylist)
                        <tr>
                            <td class="text-center">{{ $stylist['order'] }}</td>
                            <td>{{ $stylist['stylist']['user']['name'] }}</td>
                            <td>
                                <a href="{{ $stylist['picture'] }}" target="_blank">
                                    {{ $stylist['picture'] }}
                                </a>
                            </td>
                            <td class="text-center">
                                @include('admin.cms.home.topStylist.partials.show')

                                @include('admin.cms.home.topStylist.partials.delete')
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4">
                                <div class="text-muted text-center h1">
                                    <i class="fa fa-search"></i>
                                    <span>{{ __('text.no_results_found') }}</span>
                                </div>
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
