<form action="{{ route('cms.home.top.stylist.store') }}" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="row">
        <div class="col-md-3">
            <div class="form-group {{ $errors->has('order') ? 'has-error' : '' }}">
                <label for="order">{{ ucfirst(__('dictionary.position')) }}</label>
                <input type="text"
                       name="order"
                       value="{{ old('order') }}"
                       class="form-control"
                       id="order">
                @if($errors->has('order'))
                    <span class="help-block">{{ $errors->first('order') }}</span>
                @endif
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group {{ $errors->has('stylist_id') ? 'has-error' : '' }}">
                <label for="stylist_id">{{ ucfirst(__('dictionary.stylist')) }}</label>

                <select name="stylist_id" id="stylist_id" class="form-control">
                    <option>
                        {{ ucfirst(__('dictionary.select')) }}
                    </option>
                    @foreach($stylists as $stylist)
                        <option value="{{ $stylist->id }}" {{ (old('stylist_id') === $stylist->id) ? 'selected' : '' }}>
                            {{ $stylist->name }}
                        </option>
                    @endforeach
                </select>

                @if($errors->has('stylist_id'))
                    <span class="help-block">{{ $errors->first('stylist_id') }}</span>
                @endif
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group {{ $errors->has('picture') ? 'has-error' : '' }}">
                <label for="picture">{{ ucfirst(__('dictionary.image')) }}</label>
                <input type="file"
                       name="picture"
                       class="form-control"
                       id="picture">
                @if($errors->has('picture'))
                    <span class="help-block">{{ $errors->first('picture') }}</span>
                @endif
            </div>
        </div>

        <div class="col-md-2">
            <label>&nbsp;</label>
            <br>
            <button type="submit" class="btn btn-success">
                <i class="fa fa-save"></i>
                {{ ucfirst(__('dictionary.save')) }}
            </button>
        </div>
    </div>
</form>