@extends('adminlte::page')

@section('title', __('text.contact_information'))

@section('content_header')
    <h1>{{ __('text.contact_information') }}</h1>
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="admin-cms">
        <div class="box box-widget">
            <div class="box-body">
                <form action="{{ route('cms.contact.update') }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                        <label for="phone">{{ ucfirst(__('dictionary.phone')) }}</label>
                        <input type="text"
                               name="phone"
                               value="@if(old('phone')){{ old('phone') }}@elseif(isset($contact)){{ $contact['phone'] }}@endif"
                               class="form-control"
                               id="phone">
                        @if($errors->has('phone'))
                            <span class="help-block">{{ $errors->first('phone') }}</span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label for="email">{{ ucfirst(__('dictionary.email')) }}</label>
                        <input type="text"
                               name="email"
                               value="@if(old('email')){{ old('email') }}@elseif(isset($contact)){{ $contact['email'] }}@endif"
                               class="form-control"
                               id="email">
                        @if($errors->has('email'))
                            <span class="help-block">{{ $errors->first('email') }}</span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('facebook') ? 'has-error' : '' }}">
                        <label for="facebook">{{ ucfirst(__('dictionary.facebook')) }}</label>
                        <input type="text"
                               name="facebook"
                               value="@if(old('facebook')){{ old('facebook') }}@elseif(isset($contact)){{ $contact['facebook'] }}@endif"
                               class="form-control"
                               id="facebook">
                        @if($errors->has('facebook'))
                            <span class="help-block">{{ $errors->first('facebook') }}</span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('instagram') ? 'has-error' : '' }}">
                        <label for="instagram">{{ ucfirst(__('dictionary.instagram')) }}</label>
                        <input type="text"
                               name="instagram"
                               value="@if(old('instagram')){{ old('instagram') }}@elseif(isset($contact)){{ $contact['instagram'] }}@endif"
                               class="form-control"
                               id="instagram">
                        @if($errors->has('instagram'))
                            <span class="help-block">{{ $errors->first('instagram') }}</span>
                        @endif
                    </div>

                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-save"></i>
                        {{ ucfirst(__('dictionary.save')) }}
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
