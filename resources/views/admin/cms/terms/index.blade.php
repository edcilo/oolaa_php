@extends('adminlte::page')

@section('title', __('text.terms_and_conditions'))

@section('content_header')
    <h1>{{ __('text.terms_and_conditions') }}</h1>
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="admin-cms">
        <div class="box box-widget">
            <div class="box-body">
                <form action="{{ route('cms.terms.update') }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="form-group {{ $errors->has('terms') ? 'has-error' : '' }}">
                        <textarea name="terms" id="terms" class="form-control" rows="20">{!! old('terms') ? old('terms') : $terms !!}</textarea>

                        @if($errors->has('terms'))
                            <span class="help-block">{{ $errors->first('terms') }}</span>
                        @endif
                    </div>

                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-save"></i>
                        {{ ucfirst(__('dictionary.save')) }}
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="/css/plugins.css">
@endpush

@push('js')
    <script type="text/javascript" src="/js/script.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#terms').wysihtml5();
        })
    </script>
@endpush
