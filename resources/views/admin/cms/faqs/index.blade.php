@extends('adminlte::page')

@section('title', __('text.faqs'))

@section('content_header')
    <h1>{{ __('text.faqs') }}</h1>
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="admin-cms">
        <div class="box box-widget">
            <div class="box-body">
                <form action="{{ route('cms.faqs.update') }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="form-group {{ $errors->has('faqs') ? 'has-error' : '' }}">
                        <textarea name="faqs" id="faqs" class="form-control" rows="20">{!! old('faqs') ? old('faqs') : $faqs !!}</textarea>

                        @if($errors->has('faqs'))
                            <span class="help-block">{{ $errors->first('faqs') }}</span>
                        @endif
                    </div>

                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-save"></i>
                        {{ ucfirst(__('dictionary.save')) }}
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="/css/plugins.css">
@endpush

@push('js')
    <script type="text/javascript" src="/js/script.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#faqs').wysihtml5();
        })
    </script>
@endpush
