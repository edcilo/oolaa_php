@extends('adminlte::page')

@section('title', __('text.about_us'))

@section('content_header')
    <h1>{{ __('text.about_us') }}</h1>
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="admin-cms">
        <div class="box box-widget">
            <div class="box-body">
                <form action="{{ route('cms.about.us.update') }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="form-group {{ $errors->has('about_us') ? 'has-error' : '' }}">
                        <textarea name="about_us" id="about_us" class="form-control" rows="20">{!! old('about_us') ? old('about_us') : $about_us !!}</textarea>

                        @if($errors->has('about_us'))
                            <span class="help-block">{{ $errors->first('about_us') }}</span>
                        @endif
                    </div>

                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-save"></i>
                        {{ ucfirst(__('dictionary.save')) }}
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="/css/plugins.css">
@endpush

@push('js')
    <script type="text/javascript" src="/js/script.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#about_us').summernote({
                height: 500
            });
        })
    </script>
@endpush
