@extends('adminlte::page')

@section('title', __('text.cancellation_policies'))

@section('content_header')
    <h1>{{ __('text.cancellation_policies') }}</h1>
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="admin-cms">
        <div class="box box-widget">
            <div class="box-body">
                <form action="{{ route('cms.policy.update') }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="form-group {{ $errors->has('policy') ? 'has-error' : '' }}">
                        <textarea name="policy" id="policy" class="form-control" rows="20">{!! old('policy') ? old('policy') : $policy !!}</textarea>

                        @if($errors->has('policy'))
                            <span class="help-block">{{ $errors->first('policy') }}</span>
                        @endif
                    </div>

                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-save"></i>
                        {{ ucfirst(__('dictionary.save')) }}
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="/css/plugins.css">
@endpush

@push('js')
    <script type="text/javascript" src="/js/script.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#policy').wysihtml5();
        })
    </script>
@endpush
