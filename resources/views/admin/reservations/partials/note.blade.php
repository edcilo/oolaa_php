<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title">
            {{ ucfirst(trans_choice('dictionary.notes', 0)) }}
        </h3>
    </div>

    <form action="{{ route('reservations.update', $reservation) }}" method="POST">
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <div class="box-body">
            <div class="form group">
                <textarea name="note" id="note" cols="30" rows="6" class="form-control">{{ $reservation->note }}</textarea>
            </div>
        </div>

        <div class="box-footer clearfix">
            <button type="submit" class="btn btn-warning pull-right">
                <i class="fa fa-pencil"></i>
                {{ ucfirst(__('dictionary.update')) }}
            </button>
        </div>
    </form>
</div>

