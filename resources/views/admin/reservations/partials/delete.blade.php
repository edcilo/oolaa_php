<button type="button" class="btn btn-block btn-danger" data-toggle="modal" data-target="#reservation-delete">
    <i class="fa fa-trash"></i> {{ __('text.delete_entity', ['entity' => $reservation->confirmation_code]) }}
</button>

<div class="modal fade" id="reservation-delete" tabindex="-1" role="dialog" aria-labelledby="reservationDeleteModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h4 class="modal-title" id="myModalLabel">{{ ucfirst(__('text.delete_entity', ['entity' => $reservation->confirmation_code])) }}</h4>
            </div>

            <div class="modal-body">
                <form action="{{ route('reservations.destroy', $reservation) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}

                    <p>{{ __('text.delete_entity_question', ['entity' => $reservation->confirmation_code]) }}</p>

                    <button type="submit" class="btn btn-danger btn-block">
                        <i class="fa fa-trash"></i>
                        {{ ucfirst(__('dictionary.delete')) }}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
