<div class="row">
    <div class="col-md-8">
        <div class="form-group @if($errors->has('customer_id')) has-error @endif">
            <label for="start_at">{{ ucfirst(__('dictionary.customer')) }}</label>
            <select name="customer_id" class="form-control" id="customer_id">
                <option>{{ ucfirst(__('dictionary.select')) }}</option>
                @foreach($customers as $customer)
                    <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                @endforeach
            </select>
            @if($errors->has('customer_id'))
                <span class="help-block">{{ $errors->first('customer_id') }}</span>
            @endif
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group @if($errors->has('start_at')) has-error @endif">
            <label for="start_at">{{ __('text.start_at') }}</label>
            <input type="text"
                   name="start_at"
                   value="@if(old('start_at')){{ old('start_at') }}@elseif(isset($reservation)){{ $reservation->start_at }}@endif"
                   class="form-control datetimepicker"
                   id="start_at">
            @if($errors->has('start_at'))
                <span class="help-block">{{ $errors->first('start_at') }}</span>
            @endif
        </div>
    </div>
</div>
