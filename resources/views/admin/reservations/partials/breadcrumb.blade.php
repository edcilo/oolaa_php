<ol class="breadcrumb">
    <li class="@if(Route::currentRouteName() == 'reservations.index') active @endif">
        <a href="{{ route('reservations.index') }}">
            <i class="fa fa-calendar"></i>
            {{ ucfirst(trans_choice('dictionary.reservations', 0)) }}
        </a>
    </li>

    @if(Route::currentRouteName() == 'reservations.create')
        <li class="active">
            <a href="{{ route('reservations.create') }}">
                {{ ucfirst(trans('dictionary.new')) }}
            </a>
        </li>
    @endif

    @if(Route::currentRouteName() == 'reservations.show' AND isset($reservation))
        <li class="active">
            <a href="{{ route('reservations.show', $reservation) }}">
                {{ $reservation->confirmation_code }}
            </a>
        </li>
    @endif

    @if(Route::currentRouteName() == 'reservations.edit' AND isset($reservation))
        <li class="active">
            <a href="{{ route('reservations.edit', $reservation) }}">
                {{ ucfirst(trans('dictionary.edit')) }}
            </a>
        </li>
    @endif
</ol>
