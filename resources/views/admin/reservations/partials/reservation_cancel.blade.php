<form action="{{ route('reservation.cancel', $reservation) }}" method="POST">
    {{ csrf_field() }}

    <h4>Cancelar</h4>

    <button type="submit" class="btn btn-block btn-danger">
        <i class="fa fa-envelope"></i>
        Cancelar servicio
    </button>
</form>