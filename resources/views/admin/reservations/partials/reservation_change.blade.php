<form action="{{ route('reservation.notify', $reservation) }}" method="POST">
    {{ csrf_field() }}

    <h4>Cambios</h4>

    <ul>
        <li>
            <label for="change-address">
                <input type="checkbox" value="address" id="change-address" name="type[]">
                Dirección
            </label>
        </li>
        <li>
            <label for="change-date">
                <input type="checkbox" value="date" id="change-date" name="type[]">
                Fecha
            </label>
        </li>
        <li>
            <label for="change-hour">
                <input type="checkbox" value="hour" id="change-hour" name="type[]">
                Hora
            </label>
        </li>
        <li>
            <label for="change-services">
                <input type="checkbox" value="services" id="change-services" name="type[]">
                Servicios
            </label>
        </li>
        <li>
            <label for="change-stylists">
                <input type="checkbox" value="stylists" id="change-stylists" name="type[]">
                Estilistas
            </label>
        </li>
    </ul>

    <button type="submit" class="btn btn-block btn-warning">
        <i class="fa fa-envelope"></i>
        Enviar notificación
    </button>
</form>