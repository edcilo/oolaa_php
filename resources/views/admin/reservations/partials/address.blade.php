<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title">
            {{ ucfirst(__('dictionary.address')) }}
        </h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#address-edit">
                <i class="fa fa-pencil"></i>
            </button>
        </div>
    </div>

    <div class="box-body">
        @if(isset($reservation->address['line']))
            {{ $reservation->address['line'] }}
        @endif
    </div>
</div>

<div class="modal fade" id="address-edit" tabindex="-1" role="dialog" aria-labelledby="addressEditModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h4 class="modal-title" id="myModalLabel">{{ ucfirst(__('dictionary.address')) }}</h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('reservations.update', $reservation) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="form-group">
                        <select name="address_id" id="address_id" class="form-control">
                            @foreach($addresses as $address)
                                <option value="{{ $address->id }}" @if(isset($reservation->address['id']) AND $reservation->address['id'] === $address->id) selected @endif>
                                    {{ $address->line }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <button type="submit" class="btn btn-success btn-block">
                        <i class="fa fa-save"></i>
                        {{ ucfirst(__('dictionary.save')) }}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>