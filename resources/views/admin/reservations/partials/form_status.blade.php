<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title">
            {{ ucfirst(__('dictionary.status')) }}
        </h3>
    </div>

    <div class="box-body">
        @include('admin.reservations.partials.reservation_change')

        <hr>

        @include('admin.reservations.partials.reservation_reject')

        <hr>

        @include('admin.reservations.partials.reservation_cancel')
    </div>
</div>

