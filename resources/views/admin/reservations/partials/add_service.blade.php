<form action="{{ route('reservations.add.service', $reservation) }}" method="POST">
    {{ csrf_field() }}

    <div class="row">
        <div class="col-md-3">
            <div class="form-group @if($errors->has('quantity')) has-error @endif">
                <label for="quantity">{{ ucfirst(__('dictionary.quantity')) }}:</label>
                <input type="text"
                       class="form-control text-right"
                       name="quantity"
                       id="quantity"
                       value="{{ old('quantity') ? old('quantity') : 1 }}">
                @if($errors->has('quantity'))
                    <span class="help-block">{{ $errors->first('quantity') }}</span>
                @endif
            </div>
        </div>

        <div class="col-md-7">
            <div class="form-group @if($errors->has('service_id')) has-error @endif">
                <label for="service_id">{{ ucfirst(__('dictionary.service')) }}:</label>
                <select name="service_id" id="service_id" class="form-control">
                    <option>{{ ucfirst(__('dictionary.select')) }}</option>
                    @foreach($services as $service)
                        <option value="{{ $service->id }}" @if(old('service_id') == $service->id) selected @endif>
                            {{ $service->full_name }}
                        </option>
                    @endforeach
                </select>
                @if($errors->has('service_id'))
                    <span class="help-block">{{ $errors->first('service_id') }}</span>
                @endif
            </div>
        </div>

        <div class="col-md-2">
            <label>&nbsp;</label>
            <br>
            <button type="submit" class="btn btn-block btn-success">
                <i class="fa fa-plus"></i>
                {{ ucfirst(__('dictionary.save')) }}
            </button>
        </div>
    </div>
</form>