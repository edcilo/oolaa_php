<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title">
            {{ ucfirst(trans_choice('dictionary.services', 0)) }}
        </h3>

        <div class="pull-right">
            @include('admin.reservations.partials.auto_stylist')
        </div>
    </div>

    <div class="box-body">
        <div class="row">
            <div class="col-md-4">
                <strong>{{ ucfirst(__('dictionary.status')) }}:</strong> {{ $reservation->status }}
            </div>
            <div class="col-md-4">
                <strong>{{ __('text.confirmation_code') }}:</strong> {{ $reservation->confirmation_code }}
            </div>
            <div class="col-md-4">
                <strong>{{ ucfirst(__('dictionary.paid')) }}:</strong> @check($reservation->paid_out)
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <strong>{{ ucfirst(__('dictionary.rating')) }}:</strong>
                <nobr>
                    @for($i = 0; $i < $reservation->rating; $i++)
                        <i class="fa fa-star"></i>
                    @endfor
                </nobr>
            </div>
            <div class="col-md-4">
                <strong>{{ __('text.start_at') }}:</strong> @datetime($reservation->start_at)
            </div>
            <div class="col-md-4">
                <strong>{{ __('text.payment_reference') }}:</strong> {{ $reservation->payment_reference }}
            </div>
        </div>
    </div>

    <div class="box-footer">
        <div>
            @include('admin.reservations.partials.add_service')
        </div>
    </div>

    <div class="box-body no-padding">
        @include('admin.reservations.partials.items')
    </div>

    @if (!$reservation->paid_out)
        <div class="box-footer">
            @include('admin.reservations.partials.form_payment')
        </div>
    @endif
</div>
