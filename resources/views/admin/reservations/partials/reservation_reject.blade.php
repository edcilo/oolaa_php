<form action="{{ route('reservation.reject', $reservation) }}" method="POST">
    {{ csrf_field() }}

    <h4>Rechazar</h4>

    <button type="submit" class="btn btn-block btn-danger">
        <i class="fa fa-envelope"></i>
        Rechazar servicio
    </button>
</form>