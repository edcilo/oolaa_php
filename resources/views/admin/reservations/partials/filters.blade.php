<form action="{{ route('reservations.index') }}">

<div class="box box box-default box-widget {{ (!request()->get('from_date') AND !request()->get('to_date') AND !request()->get('status')) ? 'collapsed-box' : '' }}">
    <div class="box-header with-border">
        <div class="box-title">
            <i class="fa fa-filter"></i> {{ ucfirst(__('dictionary.filter')) }}
        </div>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group {{ $errors->has('from_date') ? 'has-error' : '' }}">
                    <label for="from_date">{{ __('text.per_date') }}</label>
                    <input type="text"
                           name="from_date"
                           value="{{ old('from_date') ? old('from_date') : request()->get('from_date') }}"
                           placeholder="{{ __('text.from_date') }}"
                           class="form-control datepicker"
                           id="from_date">
                    @if($errors->has('from_date'))
                        <span class="help-block">{{ $errors->first('from_date') }}</span>
                    @endif
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group {{ $errors->has('to_date') ? 'has-error' : '' }}">
                    <label for="to_date">{{ __('text.per_date') }}</label>
                    <input type="text"
                           name="to_date"
                           value="{{ old('to_date') ? old('to_date') : request()->get('to_date') }}"
                           placeholder="{{ __('text.to_date') }}"
                           class="form-control datepicker"
                           id="to_date">
                    @if($errors->has('to_date'))
                        <span class="help-block">{{ $errors->first('to_date') }}</span>
                    @endif
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                    <label for="name">{{ ucfirst(__('text.by_status')) }}</label>
                    <div>
                        <select name="status" id="status" class="form-control" style="width: 100%">
                            <option value="">{{ ucfirst(__('dictionary.select')) }}</option>
                            @foreach(__('abbr.reservation') as $key => $value)
                                <option value="{{ $key }}" @if(request()->get('status') === $key) selected @endif>{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                    @if($errors->has('status'))
                        <span class="help-block">{{ $errors->first('name') }}</span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer text-right">
        <button type="submit" class="btn btn-success">
            <i class="fa fa-filter"></i>
            {{ ucfirst(__('dictionary.filter')) }}
        </button>

        <a href="{{ route('reservations.index') }}" class="btn btn-danger">
            <i class="fa fa-times"></i>
            {{ ucfirst(__('dictionary.clear')) }}
        </a>
    </div>
</div>

</form>
