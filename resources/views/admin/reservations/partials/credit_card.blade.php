<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title">
            {{ ucfirst(__('dictionary.credit_card')) }}
        </h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#credit-card-edit">
                <i class="fa fa-pencil"></i>
            </button>
        </div>
    </div>

    <div class="box-body">
        @if($reservation->credit_card)
            <ul class="list-unstyled">
                <li>
                    <strong>{{ ucfirst(__('dictionary.brand')) }}:</strong>
                    {{ __("text.{$reservation->credit_card['brand']}") }}
                </li>
                <li>
                    <strong>{{ ucfirst(__('text.number_card')) }}:</strong>
                    {{ $reservation->credit_card['card'] }}
                </li>
            </ul>
        @endif
    </div>
</div>

<div class="modal fade" id="credit-card-edit" tabindex="-1" role="dialog" aria-labelledby="creditCardEditModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h4 class="modal-title" id="myModalLabel">{{ ucfirst(__('dictionary.credit_card')) }}</h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('reservations.update', $reservation) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="form-group">
                        <select name="credit_card_id" id="credit_card_id" class="form-control">
                            @foreach($creditCards as $creditCard)
                                <option value="{{ $creditCard->id }}" @if(isset($reservation->credit_card['id']) AND $reservation->credit_card['id'] === $creditCard->id) selected @endif>
                                    {{ __("text.{$creditCard->brand}") }} - {{ $creditCard->card }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <button type="submit" class="btn btn-success btn-block">
                        <i class="fa fa-save"></i>
                        {{ ucfirst(__('dictionary.save')) }}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>