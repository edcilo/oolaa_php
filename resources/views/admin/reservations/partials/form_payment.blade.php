<form action="{{ route('reservation.pay', $reservation) }}" method="POST">
    {{ csrf_field() }}

    <input type="hidden" name="device_session_id" value="" id="device_session_id">

    <button class="btn btn-success">{{ __('text.make_payment') }}</button>
</form>

@push('js')
    <script type="text/javascript" src="https://openpay.s3.amazonaws.com/openpay.v1.min.js"></script>
    <script type='text/javascript' src="https://openpay.s3.amazonaws.com/openpay-data.v1.min.js"></script>

    <script type="text/javascript">
        $(function () {
            OpenPay.setId("{{ config('payment.credentials.id') }}");
            OpenPay.setApiKey("{{ config('payment.credentials.key') }}");
            OpenPay.setSandboxMode({{ config('app.debug') }});

            var deviceSessionId = OpenPay.deviceData.setup("credit-card-form", "device_session_id");
            $('#device_session_id').val(deviceSessionId);
        });
    </script>
@endpush
