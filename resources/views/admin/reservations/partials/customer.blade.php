<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title">
            {{ ucfirst(__('dictionary.customer')) }}
        </h3>

        <div class="box-tools pull-right">
            <a href="{{ route('users.show', $customer->user_id) }}" class="btn btn-sm btn-info" title="{{ __('dictionary.show') }}">
                <i class="fa fa-eye"></i>
            </a>
        </div>
    </div>

    <div class="box-body">
        <div class="row">
            <div class="col-md-3">
                <img src="{{ $customer->avatar }}" alt="{{ $customer->name }}" class="img-circle img-responsive">
            </div>

            <div class="col-md-9">
                <ul class="list-unstyled">
                    <li>
                        <strong>{{ ucfirst(__('dictionary.name')) }}:</strong>
                        {{ $customer->name }}
                    </li>
                    <li>
                        <strong>{{ ucfirst(__('dictionary.email')) }}:</strong>
                        {{ $customer->email }}
                    </li>
                    <li>
                        <strong>{{ ucfirst(__('dictionary.phone')) }}:</strong>
                        {{ $customer->phone }}
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

