<form action="{{ route('reservations.auto.stylist.toggle', $reservation) }}" method="POST" class="form-inline">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <div class="form-group">
        <label for="auto_stylist">{{ __('text.auto_stylist') }}:</label>
        <input type="checkbox" id="auto_stylist" name="auto_stylist" value="1" @if($reservation->auto_stylist) checked @endif>
    </div>

    <button type="submit" class="btn btn-success">
        <i class="fa fa-save"></i>
        {{ ucfirst(__('dictionary.save')) }}
    </button>
</form>