<div class="table-responsive">
    <table class="table">
        <thead>
        <tr>
            <th class="text-center">{{ ucfirst(__('dictionary.status')) }}</th>
            <th class="text-center">{{ ucfirst(__('dictionary.stylist')) }}</th>
            <th class="text-center">{{ ucfirst(__('dictionary.service')) }}</th>
            <th class="text-center">{{ ucfirst(__('dictionary.quantity')) }}</th>
            <th class="text-center">{{ ucfirst(__('dictionary.price')) }}</th>
            <th class="text-center">{{ ucfirst(__('dictionary.discount')) }}</th>
            <th class="text-center">{{ ucfirst(__('dictionary.total')) }}</th>
            <th class="text-center">
                <i class="fa fa-gears"></i>
            </th>
        </tr>
        </thead>
        <tbody>
        @forelse($reservation->items as $item)
            <tr>
                <form action="{{ route('item.update', $item) }}" method="POST" id="{{ "update-item-{$item->id}" }}" class="form-inline">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <td>
                        <select name="status" id="status" class="form-control input-sm">
                            @foreach(__('abbr.item') as $key => $value)
                                <option value="{{ $key }}" @if($key === $item->status) selected @endif>
                                    {{ $value }}
                                </option>
                            @endforeach
                            {{-- __("abbr.item.{$item->status}") --}}
                        </select>
                    </td>
                    <td>
                        <select name="stylist_id" id="stylist_id" class="form-control input-sm">
                            <option value="">{{ ucfirst(__('dictionary.select')) }}</option>
                            @foreach($stylists as $stylist)
                                <option value="{{ $stylist->id }}" @if($item->stylist AND $item->stylist->id === $stylist->id) selected @endif>
                                    {{ $stylist->name }}
                                </option>
                            @endforeach
                        </select>

                        @if($item->status !== 'D' AND $item->declined_by)
                            <br>
                            <strong>Rechazado por:</strong> {{ $item->declined_by['user']['name'] }}
                        @endif
                    </td>
                    <td>
                        <nobr>
                            {{ $item->service->full_name }}
                        </nobr>
                    </td>
                    <td class="text-right">
                        <nobr>
                            <input type="text" class="form-control input-sm text-right" name="quantity" value="{{ $item->quantity }}">
                        </nobr>
                    </td>
                    <td class="text-right">
                        <nobr>
                            $ <input type="text" class="form-control input-sm text-right" name="unit_price" value="{{ $item->unit_price }}">
                        </nobr>
                    </td>
                    <td class="text-right">
                        <nobr>
                            $ <input type="text" class="form-control input-sm text-right" name="discount" value="{{ $item->discount }}">
                        </nobr>
                    </td>
                    <td class="text-right">
                        <nobr>
                            @currency($item->total)
                        </nobr>
                    </td>
                </form>
                <td class="text-center">
                    <nobr>
                        <button type="submit" form="{{ "update-item-{$item->id}" }}" class="btn btn-sm btn-warning" title="{{ __('dictionary.save') }}">
                            <i class="fa fa-save"></i>
                        </button>

                        @include('admin.item.partials.delete')
                    </nobr>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="8">
                    <div class="text-muted text-center h1">
                        <span>{{ __('text.no_results_found') }}</span>
                    </div>
                </td>
            </tr>
        @endforelse
        </tbody>
        <tfoot>
        <tr>
            <td colspan="5"></td>
            <td class="text-right"><strong>{{ __('text.sum_of_services') }}</strong></td>
            <td class="text-right"><strong><nobr>@currency($reservation->sumOfServices)</nobr></strong></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="5"></td>
            <td class="text-right"><strong>{{ ucfirst(__('dictionary.discount')) }}</strong></td>
            <td class="text-right"><strong><nobr>@currency($reservation->discount)</nobr></strong></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="5"></td>
            <td class="text-right"><strong>{{ ucfirst(__('dictionary.coupon')) }}</strong></td>
            <td class="text-right"><strong><nobr>@currency($reservation->coupon_discount)</nobr></strong></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="5"></td>
            <td class="text-right"><strong>{{ ucfirst(__('dictionary.subtotal')) }}</strong></td>
            <td class="text-right"><strong><nobr>@currency($reservation->subtotal)</nobr></strong></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="5"></td>
            <td class="text-right">
                <form action="{{ route('reservations.update', $reservation) }}" method="POST" class="form-inline">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <nobr>
                        <label for="iva_percent">{{ __('abbr.iva') }}</label>
                        <input type="text"
                               class="form-control text-right input-sm"
                               name="iva_percent"
                               id="iva_percent"
                               style="width: 40px"
                               value="{{ $reservation->iva_percent }}">
                        <span>%</span>
                        <button type="submit" class="btn btn-sm btn-success" title="{{ __('dictionary.save') }}">
                            <i class="fa fa-save"></i>
                        </button>
                    </nobr>
                </form>
            </td>
            <td class="text-right"><strong><nobr>@currency($reservation->iva)</nobr></strong></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="5"></td>
            <td class="text-right"><strong><nobr>{{ ucfirst(__('dictionary.total')) }}</nobr></strong></td>
            <td class="text-right"><strong><nobr>@currency($reservation->getTotalAttribute())</nobr></strong></td>
            <td></td>
        </tr>
        </tfoot>
    </table>
</div>
