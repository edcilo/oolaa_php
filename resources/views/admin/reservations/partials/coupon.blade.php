<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title">
            {{ ucfirst(__('dictionary.coupon')) }}
        </h3>

        @if($reservation->hasCoupon)
            <div class="box-tools pull-right">
                <form action="{{ route('reservations.remove.coupon', $reservation) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}

                    <button type="submit" class="btn btn-sm btn-danger">
                        <i class="fa fa-trash"></i>
                    </button>
                </form>
            </div>
        @endif
    </div>

    <div class="box-body">
        @if(!$reservation->hasCoupon)
            <form action="{{ route('reservations.add.coupon', $reservation) }}" method="POST">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('coupon_id') ? 'has-error' : '' }}">
                            <select name="coupon_id" id="coupon_id" class="form-control">
                                <option value="">{{ ucfirst(__('dictionary.select')) }}</option>
                                @foreach($coupons as $coupon)
                                    <option value="{{ $coupon->id }}">{{ $coupon->code }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('coupon_id'))
                                <span class="help-block">{{ $errors->first('coupon_id') }}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-success">
                    <i class="fa fa-plus"></i>
                    {{ ucfirst(__('dictionary.add')) }}
                </button>
            </form>
        @else
            <ul class="list-unstyled">
                <li>
                    <strong>{{ ucfirst(__('dictionary.code')) }}:</strong>
                    {{ $reservation->coupon->code }}
                </li>
                <li>
                    <strong>{{ ucfirst(__('dictionary.type')) }}:</strong>
                    {{ ucfirst(__('abbr.coupon.' . $reservation->coupon->type)) }}
                </li>
                <li>
                    <strong>{{ ucfirst(__('dictionary.discount')) }}:</strong>
                    @if($reservation->coupon->type === 'F')
                        @currency($reservation->coupon->amount)
                    @else
                        {{ $reservation->coupon->amount }} %
                    @endif
                </li>
            </ul>
        @endif
    </div>
</div>

