@extends('adminlte::page')

@section('title', ucfirst(trans_choice('dictionary.reservations', 0)))

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.reservations', 0)) }} <small>{{ ucfirst(__('dictionary.all')) }}</small></h1>

    @include('admin.reservations.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    @include('admin.partials.errors')

    @if(session('restore'))
        @include('admin.partials.formRestore', [
            'message' => __('text.entity_deleted', ['entity' => __('dictionary.reservation') . ' ' . session('restore')->confirmation_code]),
            'btn_text' => __('text.restore_entity', ['entity' => __('dictionary.reservation')]),
            'route_name' => 'reservations.restore',
            'entity' => session('restore'),
            'field_name' => 'reservation_id',
        ])
    @endif

    @if(session('restoreMultiple'))
        @include('admin.partials.formRestoreMultiple', [
            'message' => __('text.entity_deleted_group', ['quantity' => count(session('restoreMultiple')), 'entity' => trans_choice('dictionary.reservations', 0)]),
            'btn_text' => __('text.restore_entity', ['entity' => trans_choice('dictionary.reservations', 0)]),
            'route_name' => 'reservations.restore.multiple',
            'ids' => session('restoreMultiple'),
            'array_name' => 'reservation_ids',
        ])
    @endif

    <div class="row">
        <div class="col-md-12">
            @include('admin.reservations.partials.filters')

            <div class="box box-widget">
                <div class="box-header with-border row">
                    <div class="col-md-6">
                        <button type="submit" form="destroy_multiple" class="btn btn-sm btn-danger">
                            <i class="ti-trash"></i>
                            {{ __('text.delete_selected') }}
                        </button>
                    </div>
                    <div class="col-md-6">
                        @include('admin.partials.formSearch', ['route_name' => 'reservations.index'])
                    </div>
                </div>

                <div class="box-body table-responsive no-padding">
                    <form action="{{ route('reservations.destroy.multiple') }}" id="destroy_multiple" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">
                                    <input type="checkbox"
                                           name="select_all"
                                           class="checkbox-delete-trigger"
                                           title="{{ __('text.select_all') }}">
                                </th>
                                <th class="text-center">{{ __('text.confirmation_code') }}</th>
                                <th class="text-center">{{ ucfirst(__('dictionary.status')) }}</th>
                                <th class="text-center">{{ ucfirst(__('dictionary.customer')) }}</th>
                                <th class="text-center">{{ ucfirst(trans_choice('dictionary.stylists', 0)) }}</th>
                                <th class="text-center">{{ ucfirst(__('dictionary.total')) }}</th>
                                <th class="text-center">{{ __('text.created_at') }}</th>
                                <th class="text-center">{{ __('text.start_at') }}</th>
                                <th class="text-center">{{ __('text.auto_stylist') }}</th>
                                <th class="text-center">{{ ucfirst(__('dictionary.rating')) }}</th>
                                <th class="text-center">
                                    <i class="fa fa-gears"></i>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($reservations as $reservation)
                                <tr>
                                    <td class="text-center">
                                        <input type="checkbox"
                                               name="reservation_ids[]"
                                               value="{{ $reservation->id }}"
                                               class="checkbox-delete">
                                    </td>
                                    <td>{{ $reservation->confirmation_code }}</td>
                                    <td>{{ $reservation->status }}</td>
                                    <td>{{ $reservation->customer->name }}</td>
                                    <td>
                                        <ul>
                                            @foreach($reservation->items as $item)
                                                <li>
                                                    <strong>{{ $item->service->name }}</strong>
                                                    @if($item->stylist)
                                                        <ul>
                                                            <li>{{ $item->stylist->name }} ({{ __("abbr.item.{$item->status}") }})</li>
                                                            @if($item->status !== 'D' && $item->declined_by)
                                                                <li>{{ $item->declined_by['user']['name'] }} ({{ __("abbr.item.D") }})</li>
                                                            @endif
                                                        </ul>
                                                    @endif
                                                </li>
                                            @endforeach
                                        </ul>
                                    </td>
                                    <td class="text-right">
                                        <nobr>@currency($reservation->getTotalAttribute())</nobr>
                                    </td>
                                    <td><nobr>@date($reservation->created_at)</nobr></td>
                                    <td>@datetime($reservation->start_at)</td>
                                    <td class="text-center">@check($reservation->auto_stylist)</td>
                                    <td>
                                        <nobr>
                                            @for($i = 0; $i < $reservation->rating; $i++)
                                                <i class="fa fa-star"></i>
                                            @endfor
                                        </nobr>
                                    </td>
                                    <td class="text-center">
                                        <nobr>
                                            @include('admin.partials.btnShow', ['route_name' => 'reservations.show', 'model' => $reservation])
                                            @include('admin.partials.btnEdit', ['route_name' => 'reservations.edit', 'model' => $reservation])
                                        </nobr>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="11">
                                        <div class="text-muted text-center h1">
                                            <i class="fa fa-search"></i>
                                            <span>{{ __('text.no_results_found') }}</span>
                                        </div>
                                    </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </form>
                </div>

                <div class="box-footer clearfix">
                    @include('admin.partials.paginateFooter', [
                        'model' => $reservations,
                        'route_name' => 'reservations.index',
                        'model_name_trans' => 'dictionary.reservations',
                        'pagination_params' => ['status' => request()->get('status')]
                    ])
                </div>
            </div>
        </div>
    </div>
@stop

@push('css')
    <link rel="stylesheet" href="{{ mix('/css/plugins.css') }}">
@endpush

@push('js')
    <script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ mix('/js/script.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            checkBoxes('input');
            select2('select');
            iCheck_checkAll('.checkbox-delete-trigger', '.checkbox-delete');
        })
    </script>
@endpush
