@extends('adminlte::page')

@section('title', ucfirst(trans_choice('dictionary.reservations', 0)))

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.reservations', 0)) }} <small>{{ ucfirst(__('dictionary.trash')) }}</small></h1>

    @include('admin.reservations.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    @include('admin.partials.errors')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget">
                <div class="box-header with-border row">
                    <div class="col-xs-7">
                        <button type="submit" form="restore_multiple" class="btn btn-sm btn-warning">
                            <i class="ti-trash"></i>
                            {{ __('text.restore_selected') }}
                        </button>
                    </div>
                    <div class="col-xs-5">
                        @include('admin.partials.formSearch', ['route_name' => 'reservations.trash'])
                    </div>
                </div>

                <div class="box-body table-responsive no-padding">
                    <form action="{{ route('reservations.restore.multiple') }}" id="restore_multiple" method="POST">
                        {{ csrf_field() }}

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">
                                    <input type="checkbox"
                                           name="select_all"
                                           class="checkbox-delete-trigger"
                                           title="{{ __('text.select_all') }}">
                                </th>
                                <th class="text-center">{{ __('text.confirmation_code') }}</th>
                                <th class="text-center">{{ ucfirst(__('dictionary.status')) }}</th>
                                <th class="text-center">{{ ucfirst(__('dictionary.total')) }}</th>
                                <th class="text-center">{{ __('text.created_at') }}</th>
                                <th class="text-center">{{ __('text.deleted_at') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($reservations as $reservation)
                                <tr>
                                    <td class="text-center">
                                        <input type="checkbox"
                                               name="reservation_ids[]"
                                               value="{{ $reservation->id }}"
                                               class="checkbox-delete">
                                    </td>
                                    <td>{{ $reservation->confirmation_code }}</td>
                                    <td>{{ $reservation->status }}</td>
                                    <td class="text-right">@currency($reservation->getTotalAttribute())</td>
                                    <td>@date($reservation->created_at)</td>
                                    <td>@date($reservation->deleted_at)</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6">
                                        <div class="text-muted text-center h1">
                                            <i class="fa fa-search"></i>
                                            <span>{{ __('text.no_results_found') }}</span>
                                        </div>
                                    </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </form>
                </div>

                <div class="box-footer clearfix">
                    @include('admin.partials.paginateFooter',
                        ['model' => $reservations, 'route_name' => 'reservations.trash', 'model_name_trans' => 'dictionary.reservations'])
                </div>
            </div>
        </div>
    </div>
@stop

@push('css')
    <link rel="stylesheet" href="{{ mix('/css/plugins.css') }}">
@endpush

@push('js')
    <script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ mix('/js/script.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            checkBoxes('input');
            iCheck_checkAll('.checkbox-delete-trigger', '.checkbox-delete');
        })
    </script>
@endpush
