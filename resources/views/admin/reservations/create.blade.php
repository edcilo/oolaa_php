@extends('adminlte::page')

@section('title', ucfirst(trans_choice('dictionary.reservations', 0)))

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.reservations', 0)) }} <small>{{ ucfirst(__('dictionary.new')) }}</small></h1>

    @include('admin.reservations.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('text.create_entity', ['entity' => __('dictionary.reservation')]) }}</h3>
                </div>

                <form action="{{ route('reservations.store') }}" method="POST">
                    {{ csrf_field() }}

                    <div class="box-body">
                        @include('admin.reservations.partials.form')
                    </div>

                    <div class="box-footer clearfix text-right">
                        <button class="btn btn-success" type="submit">
                            <i class="fa fa-save"></i>
                            {{ ucfirst(__('dictionary.save')) }}
                        </button>

                        <a href="{{ route('reservations.index') }}" class="btn btn-danger">
                            <i class="fa fa-times"></i>
                            {{ ucfirst(__('dictionary.cancel')) }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@push('css')
    <link rel="stylesheet" href="{{ mix('/css/plugins.css') }}">
@endpush

@push('js')
    <script src="{{ mix('/js/script.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            select2('select');
        })
    </script>
@endpush
