@extends('adminlte::page')

@section('title', $reservation->confirmation_code)

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.reservations', 1)) }} <small>{{ __('dictionary.show') }}</small></h1>

    @include('admin.reservations.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="admin-reservations">
        <div class="row">
            <div class="col-md-4">
                @include('admin.reservations.partials.customer')
            </div>

            <div class="col-md-4">
                @include('admin.reservations.partials.address')
            </div>

            <div class="col-md-4">
                @include('admin.reservations.partials.credit_card')
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('admin.reservations.partials.services')
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                @include('admin.reservations.partials.note')
            </div>

            <div class="col-md-4">
                @include('admin.reservations.partials.coupon')

                @include('admin.reservations.partials.form_status')
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('admin.reservations.partials.delete')
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="/css/custom.css">
    <link rel="stylesheet" href="/css/plugins.css">
@endpush

@push('js')
    <script type="text/javascript" src="/js/script.js"></script>
    <script type="text/javascript">
        $(function () {
            select2('select');
            $('#note').wysihtml5();
            $("[name='auto_stylist']").bootstrapSwitch();
        })
    </script>
@endpush
