<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>

    {{ $message }}

    <form action="{{ route($route_name) }}" method="POST" class="inline">
        {{ csrf_field() }}

        @foreach($ids as $id)
            <input type="hidden" name="{{ "{$array_name}[]" }}" value="{{ $id }}">
        @endforeach

        <button type="submit" class="btn btn-warning btn-sm">
            {{ $btn_text }}
        </button>
    </form>
</div>
