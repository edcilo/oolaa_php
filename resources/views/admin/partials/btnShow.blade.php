<a href="{{ route($route_name, $model) }}" class="btn btn-sm btn-info" title="{{ __('text.show_details') }}">
    <i class="fa fa-eye"></i>
</a>
