<div class="row">
    <div class="@if(!isset($perPage) || $perPage) col-md-3 @else col-md-6 @endif">
        {{ ucfirst(trans('dictionary.page')) }}
        {{ $model->currentPage() }}
        {{ trans('dictionary.of') }}
        {{ $model->lastPage() }}

        ({{ $model->total() }} {{ trans_choice($model_name_trans, $model->total()) }})
    </div>

    <div class="col-md-6 @if(!isset($perPage) || $perPage) text-center @else text-right @endif">
        {{ $model->appends(array_merge(['s' => request()->get('s'), 'pp' => request()->get('pp')], isset($pagination_params) ? $pagination_params : []))->links() }}
    </div>

    @if(!isset($perPage) || $perPage)
        <div class="col-md-3">
            <form action="{{ route($route_name, isset($route_params) ? $route_params : null) }}">
                <div class="input-group input-group-sm">
                    <input type="hidden" value="{{ request()->get('s') }}" name="s">

                    <input type="text" value="{{ request()->get('pp') ?: 10 }}" name="pp" class="form-control">

                    <span class="input-group-btn">
                    <button type="submit" class="btn btn-success btn-flat" title="{{ __('dictionary.search') }}">
                        {{ __('text.per_page') }}
                    </button>
                </span>
                </div>
            </form>
        </div>
    @endif
</div>
