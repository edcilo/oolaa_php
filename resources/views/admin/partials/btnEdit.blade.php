<a href="{{ route($route_name, $model->id) }}" class="btn btn-sm btn-warning" title="{{ ucfirst(__('dictionary.edit')) }}">
    <i class="fa fa-pencil"></i>
</a>
