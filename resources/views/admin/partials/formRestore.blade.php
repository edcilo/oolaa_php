<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
        <span aria-hidden="true">&times;</span>
    </button>

    {{ $message }}

    <form action="{{ route($route_name) }}" class="inline" method="POST">
        {{ csrf_field() }}

        <input type="hidden" name="{{ $field_name }}" value="{{ $entity->id }}">

        <button type="submit" class="btn btn-warning btn-sm">
            {{ $btn_text }}
        </button>
    </form>
</div>
