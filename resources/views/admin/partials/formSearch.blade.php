<form action="{{ route($route_name, isset($route_params) ? $route_params: null) }}" method="GET" accept-charset="UTF-8">
    <div class="input-group input-group-sm">
        @if(request()->get('pp'))
            <input type="hidden" value="{{ request()->get('pp') }}" name="pp">
        @endif

        <input name="s"
               value="{{ request()->get('s') }}"
               placeholder="{{ ucfirst(__('dictionary.search')) }}"
               type="text"
               class="form-control">

        <span class="input-group-btn">
            <button type="submit" class="btn btn-success btn-flat" title="{{ __('dictionary.search') }}">
                <i class="fa fa-search"></i>
            </button>

            @if(request()->get('s'))
                <a href="{{ route($route_name, isset($route_params) ? $route_params : null) }}"
                   class="btn btn-danger btn-flat"
                   title="{{ __('dictionary.all') }}">
                    <i class="fa fa-list"></i>
                </a>
            @endif
        </span>
    </div>
</form>
