@extends('adminlte::page')

@section('title', $user->name)

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.users', 1)) }} <small>{{ __('dictionary.show') }}</small></h1>

    @include('admin.users.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="row">
        <div class="col-md-5">
            @include('admin.users.partials.card')

            @include('admin.creditCards.partials.list', ['$creditCards' => $creditCards, 'route_param' => $user])
        </div>

        <div class="col-md-7">
            <div class="alert alert-danger" style="display: none" id="form-errors" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p id="form-errors-msg"></p>
            </div>

            <div class="box box-widget">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('text.create_entity', ['entity' => __('dictionary.credit_card')]) }}</h3>
                </div>

                <form action="{{ route('credit-cards.store') }}" method="POST" id="credit-card-form">
                    {{ csrf_field() }}

                    <input type="hidden" name="token_id" value="" id="token_id"/>
                    <input type="hidden" name="device_session_id" value="" id="device_session_id">

                    <div class="box-body">
                        @include('admin.creditCards.partials.form')
                    </div>

                    <div class="box-footer clearfix text-right">
                        <button class="btn btn-success" id="save-button" type="submit" name="save" value="0">
                            <i class="fa fa-save"></i>
                            {{ ucfirst(__('dictionary.save')) }}
                        </button>

                        <a href="{{ route('users.show', $user) }}" class="btn btn-danger">
                            <i class="fa fa-times"></i>
                            {{ ucfirst(__('dictionary.cancel')) }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="/css/custom.css">
@endpush

@push('js')
    <script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <script type="text/javascript" src="https://openpay.s3.amazonaws.com/openpay.v1.min.js"></script>
    <script type='text/javascript' src="https://openpay.s3.amazonaws.com/openpay-data.v1.min.js"></script>

    <script type="text/javascript">
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-green',
            });

            OpenPay.setId("{{ config('payment.credentials.id') }}");
            OpenPay.setApiKey("{{ config('payment.credentials.key') }}");
            OpenPay.setSandboxMode({{ config('app.debug') }});

            var deviceSessionId = OpenPay.deviceData.setup("credit-card-form", "device_session_id");
            $('#device_session_id').val(deviceSessionId);

            $('#save-button').on('click', function(event) {
                event.preventDefault();

                $("#save-button").prop("disabled", true);

                OpenPay.token.extractFormAndCreate('credit-card-form',
                    function(response) {
                        var token_id = response.data.id;
                        $('#token_id').val(token_id);
                        console.warn($('#credit-card-form'));
                        $('#credit-card-form').submit();
                    },
                    function(response) {
                        var desc = response.data.description != undefined ? response.data.description : response.message;

                        $('#form-errors-msg').text("ERROR [" + response.status + "] " + desc);
                        $('#form-errors').show();

                        $("#save-button").prop("disabled", false);
                    });
            });
        });
    </script>
@endpush
