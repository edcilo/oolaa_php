<input type="hidden" name="user_id" value="{{ $user->id }}">

<div class="row">
    <div class="col-md-12">
        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
            <label for="name">{{ ucfirst(__('dictionary.name')) }}</label>
            <input type="text"
                   name="name"
                   data-openpay-card="holder_name"
                   value="@if(old('name')){{ old('name') }}@endif"
                   class="form-control"
                   id="name">
            @if($errors->has('name'))
                <span class="help-block">{{ $errors->first('name') }}</span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group {{ $errors->has('card') ? 'has-error' : '' }}">
            <label for="card">{{ ucfirst(__('dictionary.credit_card')) }}</label>
            <input type="text"
                   name="card"
                   data-openpay-card="card_number"
                   value="@if(old('card')){{ old('card') }}@endif"
                   class="form-control"
                   id="card">
            @if($errors->has('card'))
                <span class="help-block">{{ $errors->first('card') }}</span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group {{ $errors->has('date_month')  ? 'has-error' : '' }}">
            <label for="date_month">{{ ucfirst(__('dictionary.month')) }}</label>
            <input type="text"
                   name="date_month"
                   data-openpay-card="expiration_month"
                   value="@if(old('date_month')){{ old('date_month') }}@endif"
                   class="form-control"
                   id="date_month">
            @if($errors->has('date_month'))
                <span class="help-block">{{ $errors->first('date_month') }}</span>
            @endif
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group {{ $errors->has('date_year')  ? 'has-error' : '' }}">
            <label for="date_year">{{ ucfirst(__('dictionary.year')) }}</label>
            <input type="text"
                   name="date_year"
                   data-openpay-card="expiration_year"
                   value="@if(old('date_year')){{ old('date_year') }}@endif"
                   class="form-control"
                   id="date_year">
            @if($errors->has('date_year'))
                <span class="help-block">{{ $errors->first('date_year') }}</span>
            @endif
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group {{ $errors->has('cvv')  ? 'has-error' : '' }}">
            <label for="cvv">{{ ucfirst(__('abbr.cvc')) }}</label>
            <input type="text"
                   name="cvv"
                   data-openpay-card="cvv2"
                   value="@if(old('cvv')){{ old('cvv') }}@endif"
                   class="form-control"
                   id="cvv">
            @if($errors->has('cvv'))
                <span class="help-block">{{ $errors->first('cvv') }}</span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>
            <input type="checkbox" value="1" name="default" @if(old('default') || (isset($address) AND $address->default)) checked @endif>
            {{ __('text.set_default') }}
        </label>
    </div>
</div>

@push('css')
    <link rel="stylesheet" href="{{ mix('/css/plugins.css') }}">
@endpush

@push('js')
    <script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
@endpush
