<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="{{ "#user-credit-card-{$creditCard->id}" }}">
    <i class="fa fa-eye"></i>
</button>

<div class="modal fade" id="{{ "user-credit-card-{$creditCard->id}" }}" tabindex="-1" role="dialog" aria-labelledby="creditCardModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">{{ ucfirst(__('dictionary.credit_card')) }}</h4>
            </div>
            <div class="modal-body">
                <div class="box box-widget widget-user-2">
                    <div class="box-footer no-padding">
                        <ul class="nav nav-stacked">
                            <li class="p">
                                {{ ucfirst(__('dictionary.brand')) }}
                                <span class="pull-right">{{ __("text.{$creditCard->brand}") }}</span>
                            </li>
                            <li class="p">
                                {{ __('text.number_card') }}
                                <span class="pull-right">{{ $creditCard->card }}</span>
                            </li>
                            <li class="p">
                                {{ ucfirst(__('dictionary.default')) }}
                                <span class="pull-right">@check($creditCard->default)</span>
                            </li>
                            <li class="p">
                                {{ __('text.provider_id') }}
                                <span class="pull-right">{{ $creditCard->provider_id }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
