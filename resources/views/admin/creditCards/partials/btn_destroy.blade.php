<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="{{ "#credit-card-delete-{$creditCard->id}" }}">
    <i class="fa fa-trash"></i>
</button>

<div class="modal fade" id="{{ "credit-card-delete-{$creditCard->id}" }}" tabindex="-1" role="dialog" aria-labelledby="creditCardDeleteModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">{{ $creditCard->card }}</h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('credit-cards.destroy', $creditCard) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}

                    <p>{{ __('text.delete_entity_question', ['entity' => __('text.this_credit_card')]) }}</p>

                    <button type="submit" class="btn btn-danger btn-block">
                        <i class="fa fa-trash"></i>
                        {{ ucfirst(__('dictionary.delete')) }}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
