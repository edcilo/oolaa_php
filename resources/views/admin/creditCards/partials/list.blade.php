<div class="box box-success">
    <div class="box-header with-border row">
        <div class="col-md-8">
            <h3 class="box-title">
                <i class="fa fa-credit-card"></i>
                {{ __('text.credit_cards') }}
            </h3>
        </div>

        <div class="col-md-4 text-right">
            <a href="{{ route('credit-cards.create', $route_param) }}" class="btn btn-sm btn-success">
                {{ __('text.new_model', ['model' => __('dictionary.credit_card')]) }}
            </a>
        </div>
    </div>

    <div class="box-body table-responsive no-padding">
        <table class="table table-striped table-simple">
            <thead>
            <tr>
                <th>{{ ucfirst(__('dictionary.brand')) }}</th>
                <th>{{ __('text.number_card') }}</th>
                <th>{{ ucfirst(__('dictionary.default')) }}</th>
                <th class="text-center" style="width: 40px">
                    <i class="fa fa-gears"></i>
                </th>
            </tr>
            </thead>
            <tbody>
            @forelse($creditCards as $creditCard)
                <tr>
                    <td>{{ __("text.{$creditCard->brand}") }}</td>
                    <td>{{ $creditCard->card }}</td>
                    <td class="text-center">@check($creditCard->default)</td>
                    <td>
                        <nobr>
                            @include('admin.creditCards.partials.btn_show')

                            @include('admin.creditCards.partials.btn_destroy')
                        </nobr>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="4">
                        <div class="text-muted text-center h1">
                            <i class="fa fa-search"></i>
                            <span>{{ __('text.no_results_found') }}</span>
                        </div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>

    <div class="box-footer clearfix">
        @include('admin.partials.paginateFooter', [
            'model' => $creditCards,
            'route_name' => 'users.show',
            'route_params' => $route_param,
            'model_name_trans' => 'dictionary.credit_cards',
            'perPage' => false
        ])
    </div>
</div>
