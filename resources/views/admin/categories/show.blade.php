@extends('adminlte::page')

@section('title', $category->name)

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.categories', 1)) }} <small>{{ __('dictionary.show') }}</small></h1>

    @include('admin.categories.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="row admin-category">
        <div class="col-md-5">
            @include('admin.categories.partials.card')

            <div class="box box-danger">
                <div class="box-body">
                    @include('admin.categories.partials.delete')
                </div>
            </div>

            @include('admin.categories.partials.stylists')
        </div>

        <div class="col-md-7">
            @include('admin.categories.partials.services')
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ mix('/css/plugins.css') }}">
    <link rel="stylesheet" href="/css/custom.css">
@endpush

@push('js')
    <script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ mix('/js/script.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            select2('select');
            checkBoxes('input');
            iCheck_checkAll('.checkbox-services-trigger', '.checkbox-service');
        })
    </script>
@endpush
