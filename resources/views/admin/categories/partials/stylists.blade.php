<div class="box box-widget">
    <div class="box-header with-border">
        <div class="box-title">
            {{ ucfirst(trans_choice('dictionary.stylists', 0)) }}
        </div>

        <div class="pull-right">
            @include('admin.categories.partials.addStylist')
        </div>
    </div>

    <div class="box-body table-responsive no-padding">
        <table class="table table-striped">
            <thead>
            <tr>
                <th class="text-center">{{ ucfirst(__('dictionary.name')) }}</th>
                <th class="text-center">{{ ucfirst(__('dictionary.status')) }}</th>
                <th class="text-center">{{ ucfirst(__('dictionary.email')) }}</th>
                <th class="text-center">{{ ucfirst(__('dictionary.phone')) }}</th>
                <th class="text-center">
                    <i class="fa fa-gears"></i>
                </th>
            </tr>
            </thead>
            <tbody>
            @forelse ($stylists as $stylist)
                <tr>
                    <td>{{ $stylist->user->name }}</td>
                    <td>{{ ucfirst($stylist->status_readable) }}</td>
                    <td>{{ $stylist->user->email }}</td>
                    <td>@if($stylist->user->profile->phone) {{ $stylist->user->profile->phone }} @else ... @endif</td>
                    <td class="text-center">
                        <nobr>
                            @include('admin.categories.partials.stylistDetach')
                        </nobr>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5">
                        <div class="text-muted text-center h1">
                            <i class="fa fa-search"></i>
                            <span>{{ __('text.no_results_found') }}</span>
                        </div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>

    <div class="box-footer clearfix">
        <div class="row">
            <div class="col-md-12 text-center">
                {{ $stylists->links() }}
            </div>
        </div>
    </div>
</div>
