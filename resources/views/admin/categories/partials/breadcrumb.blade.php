<ol class="breadcrumb">
    <li class="@if(Route::currentRouteName() == 'categories.index') active @endif">
        <a href="{{ route('categories.index') }}">
            <i class="fa fa-paint-brush"></i>
            {{ ucfirst(trans_choice('dictionary.categories', 0)) }}
        </a>
    </li>

    @if(Route::currentRouteName() == 'categories.create')
        <li class="active">
            <a href="{{ route('categories.create') }}">
                {{ ucfirst(trans('dictionary.new')) }}
            </a>
        </li>
    @endif

    @if(Route::currentRouteName() == 'categories.show' AND isset($category))
        <li class="active">
            <a href="{{ route('categories.show', $category) }}">
                {{ $category->name }}
            </a>
        </li>
    @endif

    @if(Route::currentRouteName() == 'categories.edit' AND isset($category))
        <li class="active">
            <a href="{{ route('categories.edit', $category) }}">
                {{ ucfirst(trans('dictionary.edit')) }}
            </a>
        </li>
    @endif
</ol>
