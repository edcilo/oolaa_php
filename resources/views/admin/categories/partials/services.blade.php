<div class="box box-widget">
    <div class="box-header with-border">
        <div class="col-xs-5">
            <div class="box-title">
                {{ ucfirst(trans_choice('dictionary.services', 0)) }}
            </div>
        </div>

        <div class="col-xs-7 text-right">
            <button type="submit" form="sync_services" class="btn btn-sm btn-warning">
                <i class="ti-trash"></i>
                {{ __('text.save_changes') }}
            </button>
        </div>
    </div>

    <div class="box-body table-responsive no-padding">
        <form action="{{ route('categories.services.sync', $category) }}" id="sync_services" method="POST">
            {{ csrf_field() }}

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>
                        <input type="checkbox"
                               name="select_all"
                               class="checkbox-services-trigger"
                               title="{{ __('text.select_all') }}">
                    </th>
                    <th class="text-center">{{ ucfirst(__('dictionary.name')) }}</th>
                    <th class="text-center">{{ __('text.average_time') }}</th>
                    <th class="text-center">{{ ucfirst(__('dictionary.price')) }}</th>
                    <th class="text-center">
                        <i class="fa fa-usd"></i>
                    </th>
                </tr>
                </thead>
                <tbody>
                @forelse ($serviceCategories as $serviceCategory)
                    <tr>
                        <td colspan="5">
                            <strong>{{ $serviceCategory->name }}</strong>
                        </td>
                    </tr>

                    @foreach($serviceCategory->services as $service)
                        <tr>
                            <td>
                                <input type="checkbox"
                                       name="service_ids[]"
                                       value="{{ $service->id }}"
                                       {{ (in_array($service->id, $servicesAttached)) ? 'checked' : '' }}
                                       class="checkbox-service">
                            </td>
                            <td>{{ $service->full_name }}</td>
                            <td class="text-right">@minutesToHours($service->average_time)</td>
                            <td class="text-right">@currency($service->price)</td>
                            <td class="text-center">
                                <input type="text"
                                       class="form-control input-sm text-right"
                                       name="price[{{ $service->id }}]" placeholder="{{ ucfirst(__('dictionary.price')) }}"
                                       value="{{ $category->getServicePrice($service->id) }}">
                            </td>
                        </tr>
                    @endforeach
                @empty
                    <tr>
                        <td colspan="5">
                            <div class="text-muted text-center h1">
                                <i class="fa fa-search"></i>
                                <span>{{ __('text.no_results_found') }}</span>
                            </div>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>

        </form>
    </div>

    <div class="box-footer text-right">
        <button type="submit" form="sync_services" class="btn btn-sm btn-warning">
            <i class="ti-trash"></i>
            {{ __('text.save_changes') }}
        </button>
    </div>
</div>

@push('css')
    <link rel="stylesheet" href="{{ mix('/css/plugins.css') }}">
@endpush

@push('js')
    <script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ mix('/js/script.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            checkBoxes('input');
            iCheck_checkAll('.checkbox-services-trigger', '.checkbox-service')

            $('#services-tree').jstree();
        })
    </script>
@endpush