<form action="{{ route('categories.stylist.attach', $category) }}" method="POST">
    {{ csrf_field() }}

    <div class="form-inline">
        <div class="form-group @if($errors->has('stylist_id')) has-error @endif">
            {{-- <label for="stylist_id">{{ ucfirst(__('dictionary.stylist')) }}</label> --}}

            <select name="stylist_id" id="stylist_id" class="form-control">
                <option>{{ ucfirst(__('dictionary.select')) }}</option>
                @foreach($stylistsList as $stylist)
                    <option value="{{ $stylist->id }}" @if(old('stylist_id') === $stylist->id) selected @endif>{{ $stylist->name }}</option>
                @endforeach
            </select>

            @if($errors->has('stylist_id'))
                <span class="help-block">{{ $errors->first('stylist_id') }}</span>
            @endif
        </div>

        <button type="submit" class="btn btn-success">
            <i class="fa fa-plus"></i>
            {{ __('text.add_stylist') }}
        </button>
    </div>
</form>
