<div class="box box-widget widget-user">
    <div class="widget-user-header bg-green">
        <h3 class="widget-user-username">{{ $category->name }}</h3>
    </div>

    <div class="box-body no-padding">
        <ul class="nav nav-stacked">
            <li class="p">
                {{ ucfirst(__('dictionary.name')) }}
                <span class="pull-right">{{ $category->name }}</span>
            </li>
            <li class="p">
                {{ __('text.created_at') }}
                <span class="pull-right">@datetime($category->created_at)</span>
            </li>
            <li class="p">
                {{ __('text.updated_at') }}
                <span class="pull-right">@datetime($category->updated_at)</span>
            </li>
        </ul>
    </div>

    <div class="box-footer">
        @include('admin.partials.btnEdit', ['route_name' => 'categories.edit', 'model' => $category])
    </div>
</div>
