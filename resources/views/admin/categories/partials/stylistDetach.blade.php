<form action="{{ route('categories.stylist.detach', [$category, $stylist]) }}" method="POST" class="form-inline">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}

    <button type="submit" class="btn btn-sm btn-danger">
        <i class="fa fa-times"></i>
    </button>
</form>