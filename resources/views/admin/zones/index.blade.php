@extends('adminlte::page')

@section('title', ucfirst(trans_choice('dictionary.zones', 0)))

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.zones', 0)) }} <small>{{ ucfirst(__('dictionary.all')) }}</small></h1>

    @include('admin.zones.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    @include('admin.partials.errors')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget">
                <div class="box-header with-border row">
                    <div class="col-xs-7"></div>
                    <div class="col-xs-5">
                        @include('admin.partials.formSearch', ['route_name' => 'zones.index'])
                    </div>
                </div>

                <div class="box-body table-responsive no-padding">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">{{ ucfirst(__('dictionary.name')) }}</th>
                                <th class="text-center">{{ __('text.created_at') }}</th>
                                <th class="text-center">{{ __('text.updated_at') }}</th>
                                <th class="text-center">
                                    <i class="fa fa-gears"></i>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($zones as $zone)
                                <tr>
                                    <td>{{ $zone->name }}</td>
                                    <td>@date($zone->created_at)</td>
                                    <td>@date($zone->updated_at)</td>
                                    <td class="text-center">
                                        <nobr>
                                            @include('admin.partials.btnShow', ['route_name' => 'zones.show', 'model' => $zone])
                                            @include('admin.partials.btnEdit', ['route_name' => 'zones.edit', 'model' => $zone])
                                        </nobr>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">
                                        <div class="text-muted text-center h1">
                                            <i class="fa fa-search"></i>
                                            <span>{{ __('text.no_results_found') }}</span>
                                        </div>
                                    </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </form>
                </div>

                <div class="box-footer clearfix">
                    @include('admin.partials.paginateFooter',
                        ['model' => $zones, 'route_name' => 'zones.index', 'model_name_trans' => 'dictionary.zones'])
                </div>
            </div>
        </div>
    </div>
@stop
