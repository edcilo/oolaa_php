@extends('adminlte::page')

@section('title', ucfirst(trans_choice('dictionary.zones', 0)))

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.zones', 0)) }} <small>{{ ucfirst(__('dictionary.new')) }}</small></h1>

    @include('admin.zones.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('text.create_entity', ['entity' => __('dictionary.zone')]) }}</h3>
                </div>

                <form action="{{ route('zones.store') }}" method="POST">
                    {{ csrf_field() }}

                    <div class="box-body">
                        @include('admin.zones.partials.form')
                    </div>

                    <div class="box-footer clearfix text-right">
                        <button class="btn btn-success" type="submit" name="save" value="0">
                            <i class="fa fa-save"></i>
                            {{ ucfirst(__('dictionary.save')) }}
                        </button>

                        <button class="btn btn-warning" type="submit" name="save" value="1">
                            <i class="fa fa-plus"></i>
                            {{ __('text.save_and_new') }}
                        </button>

                        <a href="{{ route('zones.index') }}" class="btn btn-danger">
                            <i class="fa fa-times"></i>
                            {{ ucfirst(__('dictionary.cancel')) }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
