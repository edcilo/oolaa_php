@extends('adminlte::page')

@section('title', $zone->name)

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.zones', 1)) }} <small>{{ __('dictionary.show') }}</small></h1>

    @include('admin.zones.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    @include('admin.partials.errors')

    <div class="admin-zones">
        <div class="row">
            <div class="col-md-4">
                @include('admin.zones.partials.card')

                <div class="box box-danger">
                    <div class="box-body">
                        @include('admin.zones.partials.delete')
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                @include('admin.zones.partials.colonies')
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ mix('/css/plugins.css') }}">
    <link rel="stylesheet" href="/css/custom.css">
@endpush

@push('js')
    <script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ mix('/js/script.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            checkBoxes('input');
            iCheck_checkAll('.checkbox-delete-trigger', '.checkbox-delete');
        })
    </script>
@endpush
