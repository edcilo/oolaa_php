<div class="box box-widget widget-user-2">
    <div class="box-body no-padding">
        <ul class="nav nav-stacked">
            <li class="p">
                {{ ucfirst(__('dictionary.name')) }}
                <span class="pull-right">{{ $zone->name }}</span>
            </li>
            <li class="p">
                {{ __('text.created_at') }}
                <span class="pull-right">@datetime($zone->created_at)</span>
            </li>
            <li class="p">
                {{ __('text.updated_at') }}
                <span class="pull-right">@datetime($zone->updated_at)</span>
            </li>
        </ul>
    </div>

    <div class="box-footer">
        @include('admin.partials.btnEdit', ['route_name' => 'zones.edit', 'model' => $zone])
    </div>
</div>
