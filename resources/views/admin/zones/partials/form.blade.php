<div class="row">
    <div class="col-md-12">
        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
            <label for="name">{{ ucfirst(__('dictionary.name')) }}</label>
            <input type="text"
                   name="name"
                   value="@if(old('name')){{ old('name') }}@elseif(isset($zone)){{ $zone->name }}@endif"
                   class="form-control"
                   id="name">
            @if($errors->has('name'))
                <span class="help-block">{{ $errors->first('name') }}</span>
            @endif
        </div>
    </div>
</div>
