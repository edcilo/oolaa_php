<ol class="breadcrumb">
    <li class="@if(Route::currentRouteName() == 'zones.index') active @endif">
        <a href="{{ route('zones.index') }}">
            <i class="fa fa-map-o"></i>
            {{ ucfirst(trans_choice('dictionary.zones', 0)) }}
        </a>
    </li>

    @if(Route::currentRouteName() == 'zones.create')
        <li class="active">
            <a href="{{ route('zones.create') }}">
                {{ ucfirst(trans('dictionary.new')) }}
            </a>
        </li>
    @endif

    @if(Route::currentRouteName() == 'zones.show' AND isset($zone))
        <li class="active">
            <a href="{{ route('zones.show', $zone) }}">
                {{ $zone->name }}
            </a>
        </li>
    @endif

    @if(Route::currentRouteName() == 'zones.edit' AND isset($zone))
        <li class="active">
            <a href="{{ route('zones.edit', $zone) }}">
                {{ ucfirst(trans('dictionary.edit')) }}
            </a>
        </li>
    @endif
</ol>
