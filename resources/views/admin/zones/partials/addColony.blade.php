<div id="app">
    <form action="{{ route('zones.add.colony', $zone) }}" method="POST">
        {{ csrf_field() }}

        <div class="row">
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('state_id') ? 'has-error' : '' }}">
                    <label for="state_id">{{ ucfirst(__('dictionary.state')) }}</label>
                    <select name="state_id"
                            id="state_id"
                            data-old="{{ old('state_id') }}"
                            class="form-control"
                            v-model="state"
                            @change="updateMunicipalities">
                        <option value="">{{ ucfirst(__('dictionary.select')) }}</option>
                        <option v-for="item in states" :value="item.id">@{{ item.name }}</option>
                    </select>
                    @if($errors->has('state_id'))
                        <span class="help-block">{{ $errors->first('state_id') }}</span>
                    @endif
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group {{ $errors->has('municipality_id') ? 'has-error' : '' }}">
                    <label for="municipality_id">{{ ucfirst(__('dictionary.municipality')) }}</label>
                    <select name="municipality_id"
                            id="municipality_id"
                            data-old="{{ old('municipality_id') }}"
                            class="form-control"
                            v-model="municipality"
                            @change="updateColonies">
                        <option value="">{{ ucfirst(__('dictionary.select')) }}</option>
                        <option v-for="item in municipalities" :value="item.id">@{{ item.name }}</option>
                    </select>
                    @if($errors->has('municipality_id'))
                        <span class="help-block">{{ $errors->first('municipality_id') }}</span>
                    @endif
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group {{ $errors->has('colony_id') ? 'has-error' : '' }}">
                    <label for="colony_id">{{ ucfirst(__('dictionary.colony')) }}</label>
                    <select name="colony_id"
                            id="colony_id"
                            data-old="{{ old('colony_id') }}"
                            class="form-control">
                        <option value="">{{ ucfirst(__('dictionary.select')) }}</option>
                        <option v-for="item in colonies" :value="item.id">@{{ item.name }}</option>
                    </select>
                    @if($errors->has('colony_id'))
                        <span class="help-block">{{ $errors->first('colony_id') }}</span>
                    @endif
                </div>
            </div>

            <div class="col-md-3">
                <label for="">&nbsp;</label> <br>
                <button type="submit" class="btn btn-block btn-success">
                    <i class="fa fa-plus"></i>
                    {{ ucfirst(__('dictionary.add')) }}
                </button>
            </div>
        </div>
    </form>
</div>

@push('js')
    <script src="{{ mix('/js/script.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        new Vue({
            el: '#app',
            data: {
                states: [],
                municipalities: [],
                colonies: [],
                state: '',
                municipality: '',
                colony: '',
            },
            methods: {
                updateColonies: function () {
                    var that = this;
                    axios.get('/api/zone/municipality/{municipality}/colonies'.replace('{municipality}', this.municipality))
                        .then(function (response) {
                            that.colonies = response.data;
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                },
                updateMunicipalities: function () {
                    var that = this;
                    axios.get('/api/zone/state/{state}/municipalities'.replace('{state}', this.state))
                        .then(function (response) {
                            that.municipalities = response.data;
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                },
            },
            created() {
                var that = this;
                axios.get('/api/zone/states')
                    .then(function (response) {
                        that.states = response.data;
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        });
    </script>
@endpush
