<div class="box box-success">
    <div class="box-header row">
        <div class="col-md-9">
            <h3 class="box-title">{{ ucfirst(trans_choice('dictionary.colonies', 0)) }}</h3>
        </div>

        <div class="col-md-3">
            @include('admin.partials.formSearch', ['route_name' => 'zones.show', 'route_params' => $zone->id])
        </div>
    </div>

    <div class="box-body">
        @include('admin.zones.partials.addColony')
    </div>

    <div class="box-footer">
        <button type="submit" form="destroy_multiple" class="btn btn-sm btn-danger">
            <i class="ti-trash"></i>
            {{ __('text.delete_selected') }}
        </button>
    </div>

    <div class="box-body table-responsive no-padding">
        <form action="{{ route('zones.remove.colonies', $zone) }}" id="destroy_multiple" method="POST">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}

            <table class="table table-striped table-simple">
                <thead>
                <tr>
                    <th class="text-center">
                        <input type="checkbox"
                               name="select_all"
                               class="checkbox-delete-trigger"
                               title="{{ __('text.select_all') }}">
                    </th>
                    <th>{{ ucfirst(__('dictionary.name')) }}</th>
                    <th>{{ ucfirst(__('dictionary.municipality')) }}</th>
                    <th>{{ __('text.zip_code') }}</th>
                </tr>
                </thead>

                <tbody>
                @forelse($colonies as $colony)
                    <tr>
                        <td class="text-center">
                            <input type="checkbox"
                                   name="colony_ids[]"
                                   value="{{ $colony->id }}"
                                   class="checkbox-delete">
                        </td>
                        <td>{{ $colony->name }}</td>
                        <td>{{ $colony->municipality->name }}</td>
                        <td class="text-center">{{ $colony->zip_code }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4">
                            <div class="text-muted text-center h1">
                                <i class="fa fa-search"></i>
                                <span>{{ __('text.no_results_found') }}</span>
                            </div>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </form>
    </div>

    <div class="box-footer clearfix">
        @include('admin.partials.paginateFooter', [
            'model' => $colonies,
            'route_name' => 'zones.show',
            'route_params' => $zone,
            'model_name_trans' => 'dictionary.colonies'
        ])
    </div>
</div>
