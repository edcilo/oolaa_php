@extends('adminlte::page')

@section('title', $state->name)

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.states', 1)) }} <small>{{ __('dictionary.show') }}</small></h1>

    @include('admin.states.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="row admin-category">
        <div class="col-md-5">
            @include('admin.states.partials.card')

            <div class="box box-danger">
                <div class="box-body">
                    @include('admin.states.partials.delete')
                </div>
            </div>
        </div>

        <div class="col-md-7"></div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="/css/custom.css">
@endpush
