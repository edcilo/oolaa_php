<ol class="breadcrumb">
    <li class="@if(Route::currentRouteName() == 'states.index') active @endif">
        <a href="{{ route('states.index') }}">
            <i class="fa fa-globe"></i>
            {{ ucfirst(trans_choice('dictionary.states', 0)) }}
        </a>
    </li>

    @if(Route::currentRouteName() == 'states.create')
        <li class="active">
            <a href="{{ route('states.create') }}">
                {{ ucfirst(trans('dictionary.new')) }}
            </a>
        </li>
    @endif

    @if(Route::currentRouteName() == 'states.show' AND isset($state))
        <li class="active">
            <a href="{{ route('states.show', $state) }}">
                {{ $state->name }}
            </a>
        </li>
    @endif

    @if(Route::currentRouteName() == 'states.edit' AND isset($state))
        <li class="active">
            <a href="{{ route('states.edit', $state) }}">
                {{ ucfirst(trans('dictionary.edit')) }}
            </a>
        </li>
    @endif
</ol>
