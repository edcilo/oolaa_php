@extends('adminlte::page')

@section('title', $user->name)

@section('content_header')
    <h1>{{ ucfirst(trans_choice('dictionary.users', 1)) }} <small>{{ __('dictionary.show') }}</small></h1>

    @include('admin.users.partials.breadcrumb')
@stop

@section('content')
    @include('admin.partials.alerts')

    <div class="row admin-address">
        <div class="col-md-5">
            @include('admin.users.partials.card')

            @include('admin.addresses.partials.list', ['addresses' => $addresses, 'route_param' => $user])
        </div>

        <div class="col-md-7">
            <div class="box box-widget">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('text.create_entity', ['entity' => __('dictionary.address')]) }}</h3>
                </div>

                <form action="{{ route('addresses.update', $address) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="box-body">
                        @include('admin.addresses.partials.form')
                    </div>

                    <div class="box-footer clearfix text-right">
                        <button class="btn btn-success" type="submit" name="save" value="0">
                            <i class="fa fa-save"></i>
                            {{ ucfirst(__('dictionary.save')) }}
                        </button>

                        <button class="btn btn-warning" type="submit" name="save" value="1">
                            <i class="fa fa-plus"></i>
                            {{ __('text.save_and_close') }}
                        </button>

                        <a href="{{ route('users.show', $user) }}" class="btn btn-danger">
                            <i class="fa fa-times"></i>
                            {{ ucfirst(__('dictionary.cancel')) }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="/css/custom.css">
@endpush
