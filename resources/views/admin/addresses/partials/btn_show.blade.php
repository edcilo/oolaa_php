<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="{{ "#user-address-{$address->id}" }}">
    <i class="fa fa-eye"></i>
</button>

<div class="modal fade" id="{{ "user-address-{$address->id}" }}" tabindex="-1" role="dialog" aria-labelledby="addressModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">{{ $address->line }}</h4>
            </div>
            <div class="modal-body">
                <div class="box box-widget widget-user-2">
                    <div class="box-footer no-padding">
                        <ul class="nav nav-stacked">
                            <li class="p">
                                {{ ucfirst(__('dictionary.default')) }}
                                <span class="pull-right">@check($address->default)</span>
                            </li>
                            <li class="p">
                                {{ ucfirst(__('dictionary.street')) }}
                                <span class="pull-right">{{ $address->street }}</span>
                            </li>
                            <li class="p">
                                {{ __('text.outdoor_number') }}
                                <span class="pull-right">{{ $address->ext_number }}</span>
                            </li>
                            <li class="p">
                                {{ __('text.interior_number') }}
                                <span class="pull-right">{{ $address->int_number }}</span>
                            </li>
                            <li class="p">
                                {{ ucfirst(__('dictionary.colony')) }}
                                <span class="pull-right">{{ $address->neighborhood }}</span>
                            </li>
                            <li class="p">
                                {{ __('text.zip_code') }}
                                <span class="pull-right">{{ $address->zip_code }}</span>
                            </li>
                            <li class="p">
                                {{ ucfirst(__('dictionary.city')) }}
                                <span class="pull-right">{{ $address->city }}</span>
                            </li>
                            <li class="p">
                                {{ ucfirst(__('dictionary.state')) }}
                                <span class="pull-right">{{ $address->state }}</span>
                            </li>
                            <li class="p">
                                {{ ucfirst(__('dictionary.longitude')) }}
                                <span class="pull-right">{{ $address->longitude }}</span>
                            </li>
                            <li class="p">
                                {{ ucfirst(__('dictionary.latitude')) }}
                                <span class="pull-right">{{ $address->latitude }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
