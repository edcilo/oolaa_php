<div class="box box-success">
    <div class="box-header with-border row">
        <div class="col-md-8">
            <h3 class="box-title">
                <i class="fa fa-map-marker"></i>
                {{ ucfirst(trans_choice('dictionary.locations', 0)) }}
            </h3>
        </div>

        <div class="col-md-4 text-right">
            <a href="{{ route('addresses.create', $route_param) }}" class="btn btn-sm btn-success">
                {{ __('text.new_model', ['model' => __('dictionary.address')]) }}
            </a>
        </div>
    </div>

    <div class="box-body table-responsive no-padding">
        <table class="table table-striped table-simple">
            <thead>
            <tr>
                <th>{{ ucfirst(__('dictionary.address')) }}</th>
                <th>{{ ucfirst(__('dictionary.default')) }}</th>
                <th class="text-center" style="width: 40px">
                    <i class="fa fa-gears"></i>
                </th>
            </tr>
            </thead>
            <tbody>
            @forelse($addresses as $address)
                <tr>
                    <td>{{ $address->line }}</td>
                    <td class="text-center">@check($address->default)</td>
                    <td>
                        <nobr>
                            @include('admin.addresses.partials.btn_show')

                            @include('admin.partials.btnEdit', ['route_name' => 'addresses.edit', 'model' => $address])

                            @include('admin.addresses.partials.btn_destroy')
                        </nobr>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="3">
                        <div class="text-muted text-center h1">
                            <i class="fa fa-search"></i>
                            <span>{{ __('text.no_results_found') }}</span>
                        </div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>

    <div class="box-footer clearfix">
        @include('admin.partials.paginateFooter', [
            'model' => $addresses,
            'route_name' => 'users.show',
            'route_params' => $route_param,
            'model_name_trans' => 'dictionary.locations',
            'perPage' => false
        ])
    </div>
</div>
