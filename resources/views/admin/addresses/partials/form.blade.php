<input type="hidden" name="user_id" value="{{ $user->id }}">

<div class="row">
    <div class="col-md-12">
        <div class="form-group @if($errors->has('line')) has-error @endif">
            <label for="line">{{ ucfirst(__('dictionary.resume')) }}</label>
            <textarea name="line"
                      class="form-control"
                      id="line"
            >@if(old('line')){{ old('line') }}@elseif(isset($address)){{ $address->line }}@endif</textarea>
            @if($errors->has('line'))
                <span class="help-block">{{ $errors->first('line') }}</span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group @if($errors->has('street')) has-error @endif">
            <label for="street">{{ ucfirst(__('dictionary.street')) }}</label>
            <input type="text"
                   name="street"
                   value="@if(old('street')) === 0){{ old('street') }}@elseif(isset($address)){{ $address->street }}@endif"
                   class="form-control"
                   id="street">
            @if($errors->has('street'))
                <span class="help-block">{{ $errors->first('street') }}</span>
            @endif
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group @if($errors->has('ext_number')) has-error @endif">
                    <label for="ext_number">{{ __('text.outdoor_number') }}</label>
                    <input type="text"
                           name="ext_number"
                           value="@if(old('ext_number')){{ old('ext_number') }}@elseif(isset($address)){{ $address->ext_number }}@endif"
                           class="form-control"
                           id="ext_number">
                    @if($errors->has('ext_number'))
                        <span class="help-block">{{ $errors->first('ext_number') }}</span>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group @if($errors->has('int_number')) has-error @endif">
                    <label for="int_number">{{ __('text.interior_number') }}</label>
                    <input type="text"
                           name="int_number"
                           value="@if(old('int_number')){{ old('int_number') }}@elseif(isset($address)){{ $address->int_number }}@endif"
                           class="form-control"
                           id="int_number">
                    @if($errors->has('int_number'))
                        <span class="help-block">{{ $errors->first('int_number') }}</span>
                    @endif
                </div>
            </div>
        </div>

        <div class="form-group @if($errors->has('neighborhood')) has-error @endif">
            <label for="neighborhood">{{ ucfirst(__('dictionary.colony')) }}</label>
            <input type="text"
                   name="neighborhood"
                   value="@if(old('neighborhood')){{ old('neighborhood') }}@elseif(isset($address)){{ $address->neighborhood }}@endif"
                   class="form-control"
                   id="neighborhood">
            @if($errors->has('neighborhood'))
                <span class="help-block">{{ $errors->first('neighborhood') }}</span>
            @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group @if($errors->has('zip_code')) has-error @endif">
            <label for="zip_code">{{ __('text.zip_code') }}</label>
            <input type="text"
                   name="zip_code"
                   value="@if(old('zip_code') OR old('zip_code') === ''){{ old('zip_code') }}@elseif(isset($address)){{ $address->zip_code }}@endif"
                   class="form-control"
                   id="zip_code">
            @if($errors->has('zip_code'))
                <span class="help-block">{{ $errors->first('zip_code') }}</span>
            @endif
        </div>

        <div class="form-group @if($errors->has('city')) has-error @endif">
            <label for="city">{{ ucfirst(__('dictionary.city')) }}</label>
            <input type="text"
                   name="city"
                   value="@if(old('city')){{ old('city') }}@elseif(isset($address)){{ $address->city }}@endif"
                   class="form-control"
                   id="city">
            @if($errors->has('city'))
                <span class="help-block">{{ $errors->first('city') }}</span>
            @endif
        </div>

        <div class="form-group @if($errors->has('state')) has-error @endif">
            <label for="state">{{ ucfirst(__('dictionary.state')) }}</label>
            <input type="text"
                   name="state"
                   value="@if(old('state')){{ old('state') }}@elseif(isset($address)){{ $address->state }}@endif"
                   class="form-control"
                   id="state">
            @if($errors->has('state'))
                <span class="help-block">{{ $errors->first('state') }}</span>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>
            <input type="checkbox" value="1" name="default" @if(old('default') || (isset($address) AND $address->default)) checked @endif>
            {{ __('text.set_default') }}
        </label>
    </div>
</div>

@push('css')
    <link rel="stylesheet" href="{{ mix('/css/plugins.css') }}">
@endpush

@push('js')
    <script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>

    <script type="text/javascript">
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-green',
            });
        })
    </script>
@endpush
