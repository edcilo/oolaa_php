
@if($oauth->count())
    <h3>{{ __('text.oauth_links') }}</h3>

    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th></th>
                <th class="text-center">{{ ucfirst(__('text.third_party')) }}</th>
                <th class="text-center">{{ ucfirst(__('dictionary.id')) }}</th>
                <th class="text-center">{{ __('text.created_at') }}</th>
                <th class="text-center">
                    <i class="fa fa-gears"></i>
                </th>
            </tr>
            </thead>
            <tbody>
            @forelse($oauth as $app)
                <tr>
                    <td class="text-center">
                        <i class="{{ "fa fa-{$app->third_party}" }}"></i>
                    </td>
                    <td>{{ __("abbr.oauth.{$app->third_party}") }}</td>
                    <td>{{ $app->third_party_id }}</td>
                    <td>@datetime($app->created_at)</td>
                    <td class="text-center">
                        @include('admin.oauth.partials.delete', ['oauth' => $app])
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5">
                        <div class="text-muted text-center h1">
                            <i class="fa fa-search"></i>
                            <span>{{ __('text.no_results_found') }}</span>
                        </div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
@else
    <h4 class="text-muted">
        <i class="fa fa-chain"></i>
        {{ __('text.no_linked_application') }}
    </h4>
@endif