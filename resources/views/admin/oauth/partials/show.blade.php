<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="{{ "#oauth-show-{$oauth->id}" }}">
    <i class="fa fa-eye"></i>
</button>

<div class="modal fade" id="{{ "oauth-show-{$oauth->id}" }}" tabindex="-1" role="dialog" aria-labelledby="oauthShowModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h4 class="modal-title" id="myModalLabel">{{ __("abbr.oauth.{$oauth->third_party}") }}</h4>
            </div>

            <div class="modal-body text-left">
                <ul class="nav nav-stacked">
                    <li class="p">
                        {{ ucfirst(__('dictionary.icon')) }}
                        <span class="pull-right">
                            <i class="{{ "fa fa-{$app->third_party}" }}"></i>
                        </span>
                    </li>
                    <li class="p">
                        {{ ucfirst(__('dictionary.name')) }}
                        <span class="pull-right">{{ $oauth->third_party }}</span>
                    </li>
                    <li class="p">
                        {{ ucfirst(__('dictionary.id')) }}
                        <span class="pull-right">{{ $oauth->third_party_id }}</span>
                    </li>
                    <li class="p">
                        {{ ucfirst(__('text.created_at')) }}
                        <span class="pull-right">@datetime($oauth->created_at)</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
