<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="{{ "#oauth-delete-{$oauth->id}" }}">
    <i class="fa fa-trash"></i>
</button>

<div class="modal fade" id="{{ "oauth-delete-{$oauth->id}" }}" tabindex="-1" role="dialog" aria-labelledby="oauthDeleteModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">{{ ucfirst(__('text.delete_entity', ['entity' => $oauth->third_party])) }}</h4>
            </div>

            <div class="modal-body">
                <form action="{{ route('oauth.destroy', $oauth) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}

                    <p>{{ __('text.delete_entity_question', ['entity' => $oauth->third_party]) }}</p>

                    <button type="submit" class="btn btn-danger btn-block">
                        <i class="fa fa-trash"></i>
                        {{ ucfirst(__('dictionary.delete')) }}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
