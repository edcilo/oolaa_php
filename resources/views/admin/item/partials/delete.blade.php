<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="{{ "#item-service-delete-{$item->id}" }}">
    <i class="fa fa-trash"></i>
</button>

<div class="modal fade" id="{{ "item-service-delete-{$item->id}" }}" tabindex="-1" role="dialog" aria-labelledby="itemServiceDeleteModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">{{ ucfirst(__('text.delete_entity', ['entity' => __('dictionary.service') . ' ' . $item->service->full_name])) }}</h4>
            </div>

            <div class="modal-body">
                <form action="{{ route('item.destroy', $item) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}

                    <p>{{ __('text.delete_entity_question', ['entity' => __('dictionary.service') . ' ' . $item->service->full_name]) }}</p>

                    <button type="submit" class="btn btn-danger btn-block">
                        <i class="fa fa-trash"></i>
                        {{ ucfirst(__('dictionary.delete')) }}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
