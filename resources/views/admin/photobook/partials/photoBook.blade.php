<div class="photobook">
    @forelse($photoBook as $photo)
        <div class="photobook-image-wrapper">
            <div class="photobook-options">
                @include('admin.photobook.partials.position', ['photobook' => $photo])
            </div>

            <button type="button" class="btn btn-primary btn-image btn-lg" data-toggle="modal" data-target="{{ "#photobook-{$photo->id}" }}">
                <div class="photobook-image @if(!$photo->is_selected) photo-hidden @endif"
                     style="{{ "background-image: url({$photo->image})" }}">
                </div>
            </button>

            <div class="photobook-options clearfix">
                @include('admin.photobook.partials.publish', ['photobook' => $photo])
                @include('admin.photobook.partials.delete', ['photobook' => $photo])
            </div>

            <div class="modal fade" id="{{ "photobook-{$photo->id}" }}" tabindex="-1" role="dialog" aria-labelledby="photobook image">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <img class="img-responsive" src="{{ $photo->image }}" alt="{{ $photo->name }}">
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <strong>
                                        {{ ucfirst(__('dictionary.path')) }}:
                                    </strong>
                                </div>
                                <div class="col-md-10">
                                    <a href="{{ $photo->image }}" target="_blank">
                                        {{ $photo->image }}
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <strong>
                                        {{ ucfirst(__('dictionary.position')) }}:
                                    </strong>
                                </div>
                                <div class="col-md-10">
                                    {{ $photo->position }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <strong>
                                        {{ ucfirst(__('dictionary.published')) }}:
                                    </strong>
                                </div>
                                <div class="col-md-10">
                                    @check($photo->is_selected)
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @empty
        <div class="text-muted text-center h1">
            <span>{{ __('text.no_results_found') }}</span>
        </div>
    @endforelse
</div>

@push('css')
@endpush