<form action="{{ route('photobook.destroy', $photobook) }}" method="POST" class="form-inline pull-right">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}

    <button class="btn btn-xs btn-danger" type="submit" title="{{ __('dictionary.delete') }}">
        <i class="fa fa-trash"></i>
    </button>
</form>