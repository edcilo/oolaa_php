<form action="{{ route('photobook.publish', $photobook) }}" method="POST" class="form-inline pull-left">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <button class="btn btn-xs @if(!$photo->is_selected) btn-info @else btn-warning @endif"
            type="submit"
            title="{{ __('dictionary.hide') }}">
        @check(!$photo->is_selected)
    </button>
</form>
