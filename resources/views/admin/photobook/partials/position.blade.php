<form action="{{ route('photobook.update', $photobook) }}" method="POST" class="form-inline">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <div class="input-group">
        <input type="text"
               value="{{ $photobook->position }}"
               name="position"
               class="form-control input-sm"
               placeholder="{{ ucfirst(__('dictionary.position')) }}">
        <span class="input-group-btn">
        <button class="btn btn-sm btn-info" type="submit">
            <i class="fa fa-save"></i>
        </button>
      </span>
    </div>
</form>