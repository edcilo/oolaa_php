<div class="box box-success @if(!$errors->any()) collapsed-box @endif">
    <div class="box-header with-border">
        <h3 class="box-title">{{ __('text.upload_new_image') }}</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
        </div>
    </div>

    <div class="box-body">
        <form action="{{ route('photobook.store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}

            <input type="hidden" name="stylist_id" value="{{ $stylist->id }}">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group @if($errors->has('position')) has-error @endif">
                        <label for="position">{{ ucfirst(__('dictionary.position')) }}</label>
                        <input type="text"
                               name="position"
                               value="@if(old('position')){{ old('position') }}@elseif(isset($photobook)){{ $photobook->position }}@endif"
                               class="form-control"
                               id="position">
                        @if($errors->has('position'))
                            <span class="help-block">{{ $errors->first('position') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="form-group @if($errors->has('image')) has-error @endif">
                        <label for="image">{{ ucfirst(__('dictionary.image')) }}</label>
                        <input type="file" class="form-control" name="image" id="image">
                        {{-- <p class="help-block">Example block-level help text here.</p> --}}
                        @if($errors->has('image'))
                            <span class="help-block">{{ $errors->first('image') }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label>
                        <input type="checkbox" value="1" name="is_selected" @if(old('is_selected') || (isset($photobook) AND $photobook->is_selected)) checked @endif>
                        {{ ucfirst(__('dictionary.publish')) }}
                    </label>
                </div>

                <div class="col-md-6 clearfix">
                    <button class="btn btn-success pull-right" type="submit">
                        <i class="fa fa-save"></i>
                        {{ ucfirst(__('dictionary.save')) }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>


@push('css')
    <link rel="stylesheet" href="{{ mix('/css/plugins.css') }}">
@endpush

@push('js')
    <script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>

    <script type="text/javascript">
        $(function () {
            checkBoxes('input');
        })
    </script>
@endpush
