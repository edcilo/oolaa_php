<div class="admin-photobook">
    <h3>{{ ucfirst(__('dictionary.photobook')) }}</h3>

    @include('admin.photobook.partials.form', ['stylist' => $user->stylist])

    <div class="row">
        <div class="col-md-12">
            @include('admin.photobook.partials.photoBook', ['photoBook' => $photoBook])
        </div>
    </div>
</div>
