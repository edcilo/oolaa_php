@extends('user.partials.profile')

@section('profile')
    <div class="user-profile-content-header">
        <h3 class="user-profile-content-title">{{ ucfirst(__('dictionary.profile')) }}</h3>
    </div>

    <div class="user-profile-content-message">
        @include('layouts.partials.alerts')
    </div>

    <div class="user-profile-content-body">
        <form action="{{ route('profile.update', $user) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group {{ $errors->has('avatar') ? 'has-error' : '' }}">
                <label for="avatar">{{ __('text.profile_picture') }}</label>

                <input type="file" name="avatar" id="avatar">

                @if ($errors->has('avatar'))
                    <span class="help-block"><strong>{{ $errors->first('avatar') }}</strong></span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ ucfirst(__('dictionary.name')) }}</label>

                <input type="text"
                       name="name"
                       value="@if(old('name')){{ old('name') }}@elseif(isset($user)){{ $user->name }}@endif"
                       class="form-control"
                       id="name">

                @if ($errors->has('name'))
                    <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                <label for="email">{{ ucfirst(__('dictionary.email')) }}</label>

                <input type="email"
                       name="email"
                       value="@if(old('email')){{ old('email') }}@elseif(isset($user)){{ $user->email }}@endif"
                       class="form-control"
                       id="email">

                @if($errors->has('email'))
                    <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                <label for="phone">{{ ucfirst(__('dictionary.phone')) }}</label>

                <input type="text"
                       name="phone"
                       value="@if(old('phone')){{ old('phone') }}@elseif(isset($user)){{ $user->profile->phone }}@endif"
                       class="form-control"
                       id="phone">

                @if($errors->has('phone'))
                    <span class="help-block">{{ $errors->first('phone') }}</span>
                @endif
            </div>

            <button type="submit" class="btn btn-block btn-primary">
                {{ ucfirst(__('dictionary.save')) }}
            </button>
        </form>
    </div>

    <div class="line"></div>

    <div class="user-profile-content-header">
        <h3 class="user-profile-content-title">{{ ucfirst(__('dictionary.password')) }}</h3>
    </div>

    <div class="user-profile-content-body">
        <form action="{{ route('profile.update.password', $user) }}" method="POST">
            {{ csrf_field() }}

            @if($user->password)
                <div class="form-group {{ $errors->has('current_password') ? 'has-error' : '' }}">
                    <label for="current_password">{{ __('text.current_password') }}</label>

                    <input type="password" name="current_password" class="form-control" id="current_password">

                    @if($errors->has('current_password'))
                        <span class="help-block"><strong>{{ $errors->first('current_password') }}</strong></span>
                    @endif
                </div>
            @endif

            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                <label for="password">{{ __('text.new_password') }}</label>

                <input type="password" name="password" class="form-control" id="password">

                @if($errors->has('password'))
                    <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                <label for="password_confirmation">{{ __('text.password_confirmation') }}</label>

                <input type="password" name="password_confirmation" class="form-control" id="password_confirmation">

                @if($errors->has('password_confirmation'))
                    <span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
                @endif
            </div>

            <button type="submit" class="btn btn-block btn-primary">
                {{ __('text.update_password') }}
            </button>
        </form>
    </div>
@endsection