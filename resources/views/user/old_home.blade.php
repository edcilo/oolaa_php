@extends('user.partials.profile')

@section('profile')
    <div class="user-profile-wrapper">
        <div class="user-profile-content-message">
            @include('layouts.partials.alerts')
        </div>

        <div class="user-profile-content-body form-edit" id="form-profile-update" style="{{ ($errors->has('avatar') OR $errors->has('name') OR $errors->has('email') OR $errors->has('phone')) ? 'display: block' : '' }}">
            <form action="{{ route('profile.update', $user) }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group {{ $errors->has('avatar') ? 'has-error' : '' }}">
                    <label for="avatar">{{ __('text.profile_picture') }}</label>

                    <input type="file" name="avatar" id="avatar">

                    @if ($errors->has('avatar'))
                        <span class="help-block"><strong>{{ $errors->first('avatar') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name">{{ ucfirst(__('dictionary.name')) }}</label>

                    <input type="text"
                           name="name"
                           value="@if(old('name')){{ old('name') }}@elseif(isset($user)){{ $user->name }}@endif"
                           class="form-control"
                           id="name">

                    @if ($errors->has('name'))
                        <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                    <label for="email">{{ ucfirst(__('dictionary.email')) }}</label>

                    <input type="email"
                           name="email"
                           value="@if(old('email')){{ old('email') }}@elseif(isset($user)){{ $user->email }}@endif"
                           class="form-control"
                           id="email">

                    @if($errors->has('email'))
                        <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                    <label for="phone">{{ ucfirst(__('dictionary.phone')) }}</label>

                    <input type="text"
                           name="phone"
                           value="@if(old('phone')){{ old('phone') }}@elseif(isset($user)){{ $user->profile->phone }}@endif"
                           class="form-control"
                           id="phone">

                    @if($errors->has('phone'))
                        <span class="help-block">{{ $errors->first('phone') }}</span>
                    @endif
                </div>

                <div class="buttons">
                    <button type="submit" class="btn btn-primary">
                        {{ ucfirst(__('dictionary.save')) }}
                    </button>

                    <button type="button" class="btn btn-primary btn-close" id="btn-hide-profile-edit">
                        {{ ucfirst(__('dictionary.cancel')) }}
                    </button>
                </div>
            </form>
        </div>

        <div class="user-profile-content-header">
            <div class="user-profile-content-header">
                <h3 class="user-profile-content-title">{{ ucfirst(trans_choice('dictionary.locations', 0)) }}</h3>
            </div>
        </div>

        <div class="user-profile-content-body user-profile-locations">
            <div class="location-list">
                @forelse($user->addresses as $address)
                    <div class="user-location">
                        {{ $address->line }}
                    </div>
                @empty
                    <div class="locations-empty"></div>
                @endforelse
            </div>

            <button class="btn btn-primary" id="btn-show-location-form">{{ __('text.add_address') }}</button>

            <div class="form-add-location" id="form-location" style="{{ ($errors->has('street') OR $errors->has('neighborhood') OR $errors->has('ext_number') OR $errors->has('int_number') OR $errors->has('zip_code') OR $errors->has('city')) ? 'display: block' : '' }}">
                <form action="{{ route('location.add', [$user->slug, $user]) }}" method="POST">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group {{ $errors->has('street') ? 'has-error' : '' }}">
                                <input type="text"
                                       name="street"
                                       value="@if(old('street')){{ old('street') }}@endif"
                                       class="form-control"
                                       placeholder="{{ ucfirst(__('dictionary.street')) }}"
                                       id="street">
                                @if($errors->has('street'))
                                    <span class="help-block">{{ $errors->first('street') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group {{ $errors->has('neighborhood') ? 'has-error' : '' }}">
                                <input type="text"
                                       name="neighborhood"
                                       value="@if(old('neighborhood')){{ old('neighborhood') }}@endif"
                                       class="form-control"
                                       placeholder="{{ ucfirst(__('dictionary.colony')) }}"
                                       id="neighborhood">
                                @if($errors->has('neighborhood'))
                                    <span class="help-block">{{ $errors->first('neighborhood') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group {{ $errors->has('ext_number') ? 'has-error' : '' }}">
                                <input type="text"
                                       name="ext_number"
                                       value="@if(old('ext_number')){{ old('ext_number') }}@endif"
                                       class="form-control"
                                       placeholder="{{ __('text.outdoor_number') }}"
                                       id="ext_number">
                                @if($errors->has('ext_number'))
                                    <span class="help-block">{{ $errors->first('ext_number') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group {{ $errors->has('int_number') ? 'has-error' : '' }}">
                                <input type="text"
                                       name="int_number"
                                       value="@if(old('int_number')){{ old('int_number') }}@endif"
                                       class="form-control"
                                       placeholder="{{ __('text.interior_number') }}"
                                       id="int_number">
                                @if($errors->has('int_number'))
                                    <span class="help-block">{{ $errors->first('int_number') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group {{ $errors->has('zip_code') ? 'has-error' : '' }}">
                                <input type="text"
                                       name="zip_code"
                                       value="@if(old('zip_code') OR old('zip_code') === ''){{ old('zip_code') }}@elseif(isset($address)){{ $address->zip_code }}@endif"
                                       class="form-control"
                                       placeholder="{{ __('text.zip_code') }}"
                                       id="zip_code">
                                @if($errors->has('zip_code'))
                                    <span class="help-block">{{ $errors->first('zip_code') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group {{ $errors->has('city') ? 'has-error' : '' }}">
                                <input type="text"
                                       name="city"
                                       value="@if(old('city')){{ old('city') }}@elseif(isset($address)){{ $address->city }}@endif"
                                       class="form-control"
                                       placeholder="{{ ucfirst(__('dictionary.city')) }}"
                                       id="city">
                                @if($errors->has('city'))
                                    <span class="help-block">{{ $errors->first('city') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="buttons">
                        <button type="submit" class="btn btn-primary">
                            {{ __('text.add_address') }}
                        </button>

                        <button type="button" class="btn btn-primary btn-password-close" id="btn-hide-location-form">
                            {{ ucfirst(__('dictionary.cancel')) }}
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <div class="user-profile-content-header user-update-payment-methods">
            <div class="user-profile-content-header user-update-password">
                <h3 class="user-profile-content-title">{{ __('text.payment_methods') }}</h3>
            </div>
        </div>

        <div class="user-profile-content-body user-profile-payment-methods">
            <div class="credit-card-list">
                @forelse($user->creditCards as $creditCard)
                    <div class="user-credit-card">
                        <div class="user-credit-card-wrapper">
                            <span class="icon">💳</span>
                            <span class="credit-card">
                                <nobr>
                                    {{ __("text.{$creditCard->brand}") }}
                                    {{ $creditCard->card }}
                                </nobr>
                            </span>
                            <div class="form-delete">
                                <form action="{{ route('payment.methods.destroy', [$user->slug, $user]) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <input type="hidden" name="card_id" value="{{ $creditCard->id }}">

                                    <button type="submit" class="btn btn-destroy">✘</button>
                                </form>
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="credit-card-empty"></div>
                @endforelse
            </div>

            <button class="btn-link" id="btn-show-credit-card-form">{{ __('text.add_credit_cards') }}</button>

            <div class="form-add-credit-card" id="form-credit-card" style="{{ ($errors->has('card') OR $errors->has('date') OR $errors->has('cvc') OR $errors->has('card_name')) ? 'display: block' : '' }}">
                <form action="{{ route('payment.methods.add', [$user->slug, $user]) }}" method="POST">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group {{ $errors->has('card') ? 'has-error' : '' }}">
                                <input type="text"
                                       name="card"
                                       value="@if(old('card')){{ old('card') }}@endif"
                                       class="form-control"
                                       placeholder="{{ __('text.number_card') }}"
                                       id="card">

                                @if ($errors->has('card'))
                                    <span class="help-block"><strong>{{ $errors->first('card') }}</strong></span>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group {{ $errors->has('date') ? 'has-error' : '' }}">
                                <input type="text"
                                       name="date"
                                       value="@if(old('date')){{ old('date') }}@endif"
                                       class="form-control"
                                       placeholder="{{ __('abbr.date_card') }}"
                                       id="date">

                                @if ($errors->has('date'))
                                    <span class="help-block"><strong>{{ $errors->first('date') }}</strong></span>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group {{ $errors->has('cvc') ? 'has-error' : '' }}">
                                <input type="text"
                                       name="cvc"
                                       value="@if(old('cvc')){{ old('cvc') }}@endif"
                                       class="form-control"
                                       placeholder="{{ __('abbr.cvc') }}"
                                       id="cvc">

                                @if ($errors->has('cvc'))
                                    <span class="help-block"><strong>{{ $errors->first('cvc') }}</strong></span>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group {{ $errors->has('card_name') ? 'has-error' : '' }}">
                                <input type="text"
                                       name="card_name"
                                       value="@if(old('card_name')){{ old('card_name') }}@endif"
                                       class="form-control"
                                       placeholder="{{ ucfirst(__('dictionary.name')) }}"
                                       id="card_name">

                                @if ($errors->has('card_name'))
                                    <span class="help-block"><strong>{{ $errors->first('card_name') }}</strong></span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="buttons">
                        <button type="submit" class="btn btn-primary">
                            {{ __('text.add_credit_cards') }}
                        </button>

                        <button type="button" class="btn btn-primary btn-password-close" id="btn-hide-credit-card-form">
                            {{ ucfirst(__('dictionary.cancel')) }}
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <div class="user-profile-content-header user-update-password">
            <button type="button" id="btn-show-update-password" class="btn btn-block btn-primary">
                {{ __('text.update_password') }}
            </button>
        </div>

        <div class="user-profile-content-body user-update-password-form form-edit" style="{{ ($errors->has('password') OR $errors->has('current_password')) ? 'display: block' : '' }}">
            <div class="user-profile-content-header user-update-password">
                <h3 class="user-profile-content-title">{{ __('text.update_password') }}</h3>
            </div>

            <form action="{{ route('profile.update.password', $user) }}" method="POST">
                {{ csrf_field() }}

                @if($user->password)
                    <div class="form-group {{ $errors->has('current_password') ? 'has-error' : '' }}">
                        <label for="current_password">{{ __('text.current_password') }}</label>

                        <input type="password" name="current_password" class="form-control" id="current_password">

                        @if($errors->has('current_password'))
                            <span class="help-block"><strong>{{ $errors->first('current_password') }}</strong></span>
                        @endif
                    </div>
                @endif

                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                    <label for="password">{{ __('text.new_password') }}</label>

                    <input type="password" name="password" class="form-control" id="password">

                    @if($errors->has('password'))
                        <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <label for="password_confirmation">{{ __('text.password_confirmation') }}</label>

                    <input type="password" name="password_confirmation" class="form-control" id="password_confirmation">

                    @if($errors->has('password_confirmation'))
                        <span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
                    @endif
                </div>

                <div class="buttons" id="btn-hide-profile-edit">
                    <button type="submit" class="btn btn-primary">
                        {{ __('text.update_password') }}
                    </button>

                    <button type="button" class="btn btn-primary btn-password-close">
                        {{ ucfirst(__('dictionary.cancel')) }}
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection