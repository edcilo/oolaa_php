@extends('layouts.app')

@section('page', 'user-profile')

@section('content')
    <div class="user-hero">
        <div class="container">
            <div class="user-avatar">
                <img src="{{ $user->profile->avatar }}" alt="avatar of {{ $user->name }}">
            </div>

            <div class="user-data">
                <h2 class="user-name">
                    {{ $user->name }}
                    <button class="btn" id="btn-show-profile-edit">
                        ✎
                    </button>
                </h2>
                <ul class="user-metadata">
                    <li>
                        <i class="fa fa-envelope-o"></i> {{ $user->email }}
                    </li>
                    <li>
                        <i class="fa fa-phone"></i> {{ $user->profile->phone }}
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="user-profile">
        <div class="container">
            <div class="user-profile-nav">
                <ul class="nav">
                    <li>
                        <a href="#">{{ __('text.my_reservations') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('profile', [$user->slug, $user]) }}">{{ ucfirst(__('dictionary.profile')) }}</a>
                    </li>
                    <li>
                        <a href="{{-- route('payment.methods', [$user->slug, $user]) --}}">{{ __('text.payment_methods') }}</a>
                    </li>
                </ul>
            </div>

            <div class="user-profile-content">
                @yield('profile')
            </div>
        </div>
    </div>
@endsection