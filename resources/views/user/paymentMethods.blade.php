@extends('user.partials.profile')

@section('profile')
    <div class="user-profile-content-header">
        <div class="pull-right">
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#credit-card-form" aria-expanded="false" aria-controls="collapseExample">
                <i class="fa fa-plus"></i>
            </button>
        </div>

        <h3 class="user-profile-content-title">{{ __('text.payment_methods') }}</h3>
    </div>

    <div class="user-profile-content-message">
        @include('layouts.partials.alerts')
    </div>


    <div class="collapse" id="credit-card-form">
        <div class="user-profile-content-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group {{ $errors->has('card') ? 'has-error' : '' }}">
                        <input type="text"
                               name="card"
                               value="@if(old('card')){{ old('card') }}@endif"
                               class="form-control"
                               placeholder="{{ __('text.number_card') }}"
                               id="name">

                        @if ($errors->has('card'))
                            <span class="help-block"><strong>{{ $errors->first('card') }}</strong></span>
                        @endif
                    </div>
                </div>


                <div class="col-xs-6">
                    <div class="form-group {{ $errors->has('date') ? 'has-error' : '' }}">
                        <input type="text"
                               name="date"
                               value="@if(old('date')){{ old('date') }}@endif"
                               class="form-control"
                               placeholder="{{ __('abbr.date_card') }}"
                               id="name">

                        @if ($errors->has('date'))
                            <span class="help-block"><strong>{{ $errors->first('date') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="col-xs-6">
                    <div class="form-group {{ $errors->has('cvc') ? 'has-error' : '' }}">
                        <input type="text"
                               name="cvc"
                               value="@if(old('cvc')){{ old('cvc') }}@endif"
                               class="form-control"
                               placeholder="{{ __('abbr.cvc') }}"
                               id="cvc">

                        @if ($errors->has('cvc'))
                            <span class="help-block"><strong>{{ $errors->first('cvc') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <input type="text"
                               name="name"
                               value="@if(old('name')){{ old('name') }}@endif"
                               class="form-control"
                               placeholder="{{ ucfirst(__('dictionary.name')) }}"
                               id="name">

                        @if ($errors->has('name'))
                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="line"></div>
    </div>

    <div class="user-profile-content-body">
        @foreach($user->creditCards as $creditCard)
            <div class="credit-card">
                {{ $creditCard->card }}
            </div>
        @endforeach
    </div>
@endsection