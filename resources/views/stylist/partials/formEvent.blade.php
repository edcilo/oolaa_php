<div class="panel panel-default">
    <div class="panel-heading">
        Agregar un evento
    </div>

    <form action="{{route('stylist.calendar.event.store', $stylist)}}" method="POST">
        {{ csrf_field() }}

        <div class="panel-body">
            <div class="form-group @if($errors->has('title')) has-error @endif">
                <label for="title">{{ ucfirst(__('dictionary.title')) }}</label>
                <input type="text"
                       name="title"
                       value="@if(old('title')){{ old('title') }}@endif"
                       class="form-control"
                       id="title">
                @if($errors->has('title'))
                    <span class="help-block">{{ $errors->first('title') }}</span>
                @endif
            </div>

            <div class="checkbox">
                <label for="allDay">
                    <input type="checkbox" class="checkbox" name="allDay" id="allDay" value="1" @if(old('allDay')) checked @endif>
                    {{ __('text.all_day') }}
                </label>
                @if($errors->has('allDay'))
                    <span class="help-block">{{ $errors->first('allDay') }}</span>
                @endif
            </div>

            <div class="form-group @if($errors->has('start')) has-error @endif">
                <label for="start">{{ ucfirst(__('dictionary.start')) }}</label>
                <input type="text"
                       name="start"
                       value="@if(old('start')){{ old('start') }}@endif"
                       class="form-control"
                       placeholder="{{ ucfirst(__('dictionary.date')) }}"
                       id="start">
                @if($errors->has('start'))
                    <span class="help-block">{{ $errors->first('start') }}</span>
                @endif
            </div>

            <div class="form-group hour-fields @if($errors->has('end')) has-error @endif">
                <label for="end">{{ ucfirst(__('dictionary.end')) }}</label>
                <input type="text"
                       name="end"
                       value="@if(old('end')){{ old('end') }}@endif"
                       class="form-control"
                       placeholder="{{ ucfirst(__('dictionary.date')) }}"
                       id="end">
                @if($errors->has('end'))
                    <span class="help-block">{{ $errors->first('end') }}</span>
                @endif
            </div>

            <div class="form-group @if($errors->has('description')) has-error @endif">
                <label for="description">{{ ucfirst(__('dictionary.description')) }}</label>
                <textarea name="description"
                          class="form-control"
                          id="description"
                          cols="30"
                          rows="3">@if(old('description')){{ old('description') }}@endif</textarea>
                @if($errors->has('description'))
                    <span class="help-block">{{ $errors->first('description') }}</span>
                @endif
            </div>
        </div>

        <div class="panel-footer">
            <button type="submit" class="btn btn-sm btn-success">
                Guardar
            </button>
        </div>
    </form>
</div>