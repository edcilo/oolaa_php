<div class="panel panel-default">
    <div class="panel-heading">
        Días laborales
    </div>

    <form action="{{ route('stylist.calendar.weekdays.update', $stylist) }}" method="POST">

        {{ csrf_field() }}

        <div class="panel-body">
            @foreach($weekdays as $day)
                <div class="checkbox">
                    <label for="{{ "day-{$day->id}" }}">
                        <input type="checkbox"
                               class="checkbox"
                               name="weekdays[]"
                               id="{{ "day-{$day->id}" }}"
                               value="{{ $day->id }}"
                                {{ $stylist->weekdays->where('id', $day->id)->count() || (old('weekdays') && array_search($day->id, old('weekdays'))) ? 'checked' : '' }}>
                        {{ $day->name }}
                    </label>
                </div>
            @endforeach
        </div>

        <div class="panel-footer text-right">
            <button type="submit" class="btn btn-sm btn-success">
                <i class="fa fa-save"></i>
                {{ ucfirst(__('dictionary.save')) }}
            </button>
        </div>

    </form>
</div>