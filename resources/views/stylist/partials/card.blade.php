<div class="stylist-card">
    <div class="stylist-card-wrapper">
        <div class="stylist-card-header">
            <div class="stylist-card-avatar">
                <img src="https://oolaa.com.mx/media/profile_images/534/avatar.JPG" alt="stylist avatar">
            </div>
            <div class="stylist-card-data">
                <div class="stylist-card-name">
                    Esperanza Julieta Garnica
                </div>
                <div class="stylist-card-rate">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>
        </div>

        <div class="stylist-card-services">
            <div class="stylist-card-service">
                <div class="stylist-card-counter">
                    <button class="btn btn-less">-</button>

                    <span class="stylist-card-quantity">0</span>

                    <button class="btn btn-less">+</button>
                </div>

                <div class="stylist-card-service-name">
                    Manicure y pedicure
                </div>

                <div class="stylist-card-service-price">
                    $450.00
                </div>
            </div>
        </div>

        <div class="stylist-card-portfolio">
            <div class="stylist-card-portfolio-carousel">
                <div class="stylist-card-portfolio-carousel-item">
                    <div class="stylist-card-portfolio-carousel-item-wrapper"
                         style="background-image: url('https://oolaa.com.mx/media/stylists/30/photobook/afa1fd532fa1404e820ac3fdd9890966.jpg')"></div>
                </div>
                <div class="stylist-card-portfolio-carousel-item">
                    <div class="stylist-card-portfolio-carousel-item-wrapper"
                         style="background-image: url('https://oolaa.com.mx/media/stylists/30/photobook/545108bc9d7f47059fca92b715b1a4ec.jpg')"></div>
                </div>
                <div class="stylist-card-portfolio-carousel-item">
                    <div class="stylist-card-portfolio-carousel-item-wrapper"
                        style="background-image: url('https://oolaa.com.mx/media/stylists/30/photobook/3cc549a75b14449c869c8f492b076e28.jpg')"></div>
                </div>
                <div class="stylist-card-portfolio-carousel-item">
                    <div class="stylist-card-portfolio-carousel-item-wrapper"
                         style="background-image: url('https://oolaa.com.mx/media/stylists/30/photobook/c9d09bc07fa346df9395d23f038147be.jpg')"></div>
                </div>
                <div class="stylist-card-portfolio-carousel-item">
                    <div class="stylist-card-portfolio-carousel-item-wrapper"
                         style="background-image: url('https://oolaa.com.mx/media/stylists/30/photobook/fdeb97fcaa5145629008d7be97629f14.jpg')"></div>
                </div>
            </div>
        </div>
    </div>
</div>