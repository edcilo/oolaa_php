<div class="reservation-card">
    <div class="reservation-header">
        <div class="reservation-name">
            <h4>{{ $reservation->confirmation_code }}</h4>
            <div class="reservation-status {{ $reservation->status === __('abbr.reservation.F') || $reservation->status === __('abbr.reservation.A') ? 'green' : '' }}">{{ $reservation->status }}</div>
        </div>
    </div>

    <div class="reservation-body">
        <div class="reservation-datetime">
            <strong>Fecha:</strong>
            {{ __("abbr.day.{$reservation->start_at->format('l')}") }}
            {{ $reservation->start_at->format('d') }} de
            {{ __("dictionary.months.{$reservation->start_at->format('F')}") }} del
            {{ $reservation->start_at->format('Y H:i A') }}
        </div>

        <div class="reservation-address">
            <strong>Dirección:</strong>
            @if(isset($reservation->address['line']))
                {{ $reservation->address['line'] }}
            @endif
        </div>

        <div class="reservation-payment">
            <span>💳</span> {{ strtoupper($reservation->credit_card['brand']) }} {{ $reservation->credit_card['card'] }}
        </div>

        <div class="reservation-services">
            <table class="table-services">
                <thead>
                <tr>
                    <th>Servicio</th>
                    <th>Duración</th>
                    <th>Inicio</th>
                    <th>Precio</th>
                </tr>
                </thead>
                <tbody>
                @foreach($reservation->items as $item)
                    @if($item->stylist_id === auth()->user()->stylist->id)
                        <tr>
                            <td><span>{{ $item->service->name }}</span></td>
                            <td class="text-right">{{ $item->service->average_time }} min.</td>
                            <td class="text-right">{{ $item->quantity }}</td>
                            <td class="text-right">@currency($item->getTotalAttribute(true))</td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>

            <div class="reservation-total">
                <div class="reservation-label">Precio total:</div>
                <div class="reservation-amount">@currency($reservation->getTotalAttribute(auth()->user()->stylist)) MXN</div>
            </div>

            @if($reservation->status === __('abbr.reservation.P') AND $tokens->where('reservation_id', $reservation->id)->count())
                <a class="btn btn-primary"
                   href="{{ route('reservation.session.id', [
                    'reservation_id' => $reservation->id,
                    'stylist_id' => auth()->user()->stylist->id,
                    'token' => $tokens->where('reservation_id', $reservation->id)->first()->token,
                    'action' => 'A'
                   ]) }}">
                    Aceptar cita
                </a>

                <a class="btn btn-danger"
                   href="{{ route('reservation.session.id', [
                    'reservation_id' => $reservation->id,
                    'stylist_id' => auth()->user()->stylist->id,
                    'token' => $tokens->where('reservation_id', $reservation->id)->first()->token,
                    'action' => 'D'
                   ]) }}">
                    Rechazar cita
                </a>
            @endif
        </div>
    </div>
</div>
