@extends('layouts.app')

@section('page', 'stylist-calendar')

@section('content')

    <div class="container">
        {{-- <h1>Calendario</h1> --}}

        <div class="row">
            <div class="col-md-9">
                <div id="calendar"></div>
            </div>

            <div class="col-md-3">
                <div class="form-weekdays">
                    @include("stylist.partials.formEvent")

                    @include("stylist.partials.formWeekdays")
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js-data')
    <script>
        var $events = {!! $eventsJson !!};
        var $businessDays = {!! $businessDays !!}
    </script>
@endpush
