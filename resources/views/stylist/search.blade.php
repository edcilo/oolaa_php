@extends('layouts.app')

@section('page', 'search')

@section('content')
    <div id="reservation-app">
        <search></search>
    </div>
@endsection

@push('js')
    <script>
        var OOLAA = {
            payGateway: {
                driver: '{{ config('payment.gateway') }}',
                merchantId: '{{ config('payment.credentials.id') }}',
                publicKey: '{{ config('payment.credentials.key') }}',
                sandbox: '{{ config('app.debug') }}'
            }
        };
    </script>
@endpush