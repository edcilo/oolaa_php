@extends('layouts.app')

@section('page', 'customer-reservations')

@section('content')

    <div class="panel-o">
        @include('stylist.partials.nav', ['title' => $title])

        <div class="panel-body">
            @foreach($reservations as $reservation)
                @include('stylist.partials.reservation')
            @endforeach
        </div>
    </div>

@endsection
