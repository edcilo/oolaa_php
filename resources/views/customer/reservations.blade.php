@extends('layouts.app')

@section('page', 'customer-reservations')

@section('content')

    <div class="panel-o">
        @include('customer.partials.nav', ['title' => $title])

        <div class="panel-body">
            @foreach($reservations as $reservation)
                @include('customer.partials.reservation')
            @endforeach
        </div>
    </div>

@endsection
