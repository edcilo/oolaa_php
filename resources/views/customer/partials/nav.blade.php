<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <i class="fa fa-angle-down"></i>
            </button>
            <a class="navbar-brand" href="#">
                {{ $title }}
            </a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="{{ Route::currentRouteName() === 'customer.reservations.finished' ? 'active' : '' }}">
                    <a href="{{ route('customer.reservations.finished') }}">{{ ucfirst(__('dictionary.record')) }}</a>
                </li>
                <li class="{{ Route::currentRouteName() === 'customer.reservations.canceled' ? 'active' : '' }}">
                    <a href="{{ route('customer.reservations.canceled') }}">{{ __('text.canceled_appointments') }}</a>
                </li>
                <li class="{{ Route::currentRouteName() === 'customer.reservations.pending' ? 'active' : '' }}">
                    <a href="{{ route('customer.reservations.pending') }}">{{ __('text.waiting_requests') }}</a>
                </li>
                <li class="{{ Route::currentRouteName() === 'customer.reservations.accepted' ? 'active' : '' }}">
                    <a href="{{ route('customer.reservations.accepted') }}">{{ __('text.accepted_appointments') }}</a>
                </li>
                <li class="{{ Route::currentRouteName() === 'customer.reservations' ? 'active' : '' }}">
                    <a href="{{ route('customer.reservations') }}">{{ ucfirst(__('dictionary.all')) }}</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
