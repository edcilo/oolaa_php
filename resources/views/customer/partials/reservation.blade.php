<div class="reservation-card">
    <div class="reservation-header">
        <div class="reservation-name">
            <h4>{{ $reservation->confirmation_code }}</h4>
            <div class="reservation-status {{ $reservation->status === __('abbr.reservation.F') || $reservation->status === __('abbr.reservation.A') ? 'green' : '' }}">{{ $reservation->status }}</div>
        </div>
    </div>

    <div class="reservation-body">
        <div class="reservation-datetime">
            <strong>Fecha:</strong>
            {{ __("abbr.day.{$reservation->start_at->format('l')}") }}
            {{ $reservation->start_at->format('d') }} de
            {{ __("dictionary.months.{$reservation->start_at->format('F')}") }} del
            {{ $reservation->start_at->format('Y H:i A') }}
        </div>

        <div class="reservation-address">
            <strong>Dirección:</strong> {{ $reservation->address['line'] }}
        </div>

        <div class="reservation-payment">
            <span>💳</span> {{ strtoupper($reservation->credit_card['brand']) }} {{ $reservation->credit_card['card'] }}
        </div>

        <div class="reservation-services">
            <table class="table-services">
                <thead>
                <tr>
                    <th>Servicio</th>
                    <th>Duración</th>
                    <th>Inicio</th>
                    <th>Precio</th>
                </tr>
                </thead>
                <tbody>
                @foreach($reservation->items as $item)
                    <tr>
                        <td><span>{{ $item->service->name }}</span></td>
                        <td class="text-right">{{ $item->service->average_time }} min.</td>
                        <td class="text-right">{{ $item->quantity }}</td>
                        <td class="text-right">@currency($item->total)</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            @if($reservation->has_coupon)
                <table class="table-services">
                    <thead>
                        <tr>
                            <th>Cupón</th>
                            <th>Descuento</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $reservation->coupon->code }}</td>
                            <td>
                                @if($reservation->coupon->type === 'F')
                                    @currency($reservation->coupon->amount)
                                @else
                                    {{ $reservation->coupon->amount }}%
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
            @endif

            <div class="reservation-total">
                <div class="reservation-label">Precio total:</div>
                <div class="reservation-amount">@currency($reservation->getTotalAttribute()) MXN</div>
            </div>

            @if($reservation->status === __('abbr.reservation.P'))
                <form action="{{ route('reservation.customer.cancel', ['customer' => auth()->user()->customer, 'reservation' =>$reservation]) }}" method="POST">
                    {{ csrf_field() }}

                    <button type="submit" class="btn btn-danger">
                        Cancelar reservación
                    </button>
                </form>
            @endif
        </div>
    </div>
</div>
