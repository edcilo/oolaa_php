<?php

return [
    // A
    'active' => 'active',
    'address' => 'address',
    'admin' => 'admin',
    'all' => 'all',
    'approved' => 'approved',
    'avatar' => 'avatar',

    // B
    'blocked' => 'blocked',
    'brand' => 'brand',

    // C
    'cancel' => 'cancel',
    'category' => 'category',
    'categories' => 'category|categories',
    'create' => 'create',
    'city' => 'city',
    'colony' => 'colony',
    'colonies' => 'colony|colonies',
    'credit_card' => 'credit card',
    'credit_cards' => 'credit card|credit cards',
    'customer' => 'customer',
    'customers' => 'customer|customers',

    // D
    'default' => 'default',
    'delete' => 'delete',
    'detail' => 'detail',
    'details' => 'detail|details',

    // E
    'edit' => 'edit',
    'email' => 'email',

    // F
    'facebook' => 'Facebook',
    'first_name' => 'first name',

    // I
    'image' => 'image',
    'instagram' => 'Instagram',

    // L
    'last_name' => 'last name',
    'list' => 'list',
    'lists' => 'list|lists',
    'location' => 'location',
    'locations' => 'location|locations',
    'latitude' => 'latitude',
    'longitude' => 'longitude',

    // N
    'name' => 'name',
    'names' => 'name|names',
    'new' => 'new',
    'note' => 'note',

    // O
    'of' => 'of',
    'phone' => 'phone',
    'photo' => 'photo',

    // P
    'password' => 'password',
    'path' => 'path',
    'page' => 'page',
    'pages' => 'page|pages',
    'parent' => 'parent',
    'pending' => 'pending',
    'position' => 'position',
    'photobook' => 'photobook',
    'price' => 'price',
    'prices' => 'price|prices',
    'publish' => 'publish',
    'published' => 'published',

    // R
    'rejected' => 'rejected',
    'resume' => 'resume',

    // S
    'save' => 'save',
    'search' => 'search',
    'service' => 'service',
    'services' => 'service|services',
    'show' => 'show',
    'state' => 'state',
    'states' => 'state|states',
    'status' => 'status',
    'street' => 'street',
    'stylist' => 'stylist',
    'stylists' => 'stylist|stylists',

    // T
    'tree' => 'tree',
    'trash' => 'trash',

    // U
    'user' => 'user',
    'users' => 'user|users',

    // Y
    'year' => 'year'
];