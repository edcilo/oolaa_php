<?php

return [
    'american_express' => 'American express',
    'average_time' => 'Average time',

    'confirm_password' => 'Confirm Password',
    'create_entity' => 'New :entity',
    'created_at' => 'Created at',
    'credit_cards' => 'Credit cards',

    'delete_entity' => 'Delete to :entity',
    'delete_entity_question' => 'Are you sure you want to eliminate :entity?',
    'delete_selected' => 'Delete selected',
    'deleted_at' => 'Deleted at',

    'edit_entity' => 'Edit :entity',
    'entity_created' => 'The :entity has been created successfully',
    'entity_deleted' => 'The :entity has been deleted successfully',
    'entity_deleted_group' => ':quantity :entity has been deleted',
    'entity_recovered' => ':quantity :entity have recovered',
    'entity_restored' => 'The :entity has been restored successfully',
    'entity_updated' => 'The :entity has been updated successfully',

    'interior_number' => 'Interior number',

    'mastercard' => 'Mastercard',

    'new_model' => 'New :model',
    'no_results_found' => 'No results found',
    'number_card' => 'Number card',

    'outdoor_number' => 'Outdoor number',

    'password_confirmation' => 'Password confirmation',
    'per_page' => 'Per page',
    'price_is_mutable' => 'Price is mutable',
    'provider_id' => 'Provider id',

    'restore_entity' => 'Restore :entity deleted.',
    'restore_selected' => 'Restore selected',

    'save_and_close' => 'Save and close',
    'save_and_new' => 'Save and new',
    'select_all' => 'Select all',
    'send_password_reset_link' => 'Send Password Reset Link',
    'set_default' => 'Set default',
    'show_details' => 'Show details',

    'this_address' => 'this address',
    'this_credit_card' => 'this credit card',

    'updated_at' => 'Updated at',
    'upload_new_image' => 'Upload new image',

    'visa' => 'Visa',

    'zip_code' => 'Zip code',
];