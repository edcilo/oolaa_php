<?php

return [
    'January' => 'January',
    'February' => 'February',
    'March' => 'March',
    'April' => 'April',
    'May' => 'May',
    'June' => 'June',
    'July' => 'July',
    'August' => 'August',
    'September' => 'September',
    'October' => 'October',
    'November' => 'November',
    'December' => 'December',

    'abbr' => [
        'January' => 'Jan.',
        'February' => 'Feb.',
        'March' => 'Mar.',
        'April' => 'Apr.',
        'May' => 'May',
        'June' => 'Jun.',
        'July' => 'Jul.',
        'August' => 'Ags.',
        'September' => 'Sep.',
        'October' => 'Oct.',
        'November' => 'Nov.',
        'December' => 'Dec.',
    ],
];