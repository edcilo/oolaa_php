<?php

return [
    // A
    'account' => 'cuenta',
    'active' => 'activo',
    'add' => 'agregar',
    'address' => 'dirección',
    'admin' => 'administrar',
    'all' => 'todo',
    'amount' => 'cantidad',
    'application' => 'aplicación',
    'approved' => 'aprobado',
    'attach' => 'adjuntar',
    'avatar' => 'avatar',

    // B
    'blocked' => 'bloqueado',
    'brand' => 'marca',

    // C
    'calendar' => 'calendario',
    'cancel' => 'cancelar',
    'carousel' => 'carrucel',
    'category' => 'categoría',
    'categories' => 'categoría|categorías',
    'create' => 'crear',
    'city' => 'ciudad',
    'clear' => 'limpiar',
    'code' => 'código',
    'colony' => 'colonia',
    'colonies' => 'colonia|colonias',
    'color' => 'color',
    'contact' => 'contacto',
    'cover' => 'portada',
    'coupon' => 'cupón',
    'coupons' => 'cupón|cupones',
    'credit_card' => 'tarjeta de crédito',
    'credit_cards' => 'tarjeta de crédito|tarjetas de crédito',
    'curriculum' => 'curriculum',
    'customer' => 'cliente',
    'customers' => 'cliente|clientes',

    // D
    'dashboard' => 'panel de control',
    'date' => 'fecha',
    'day' => 'día',
    'deactivate' => 'desactivar',
    'default' => 'predefinido',
    'delete' => 'eliminar',
    'description' => 'descripción',
    'detail' => 'detalle',
    'details' => 'detalle|detalles',
    'discount' => 'descuento',

    // E
    'edit' => 'editar',
    'email' => 'correo electrónico',
    'end' => 'fin',
    'enter' => 'ingresar',
    'event' => 'evento',
    'events' => 'evento|eventos',
    'experience' => 'experiencia',

    // F
    'filter' => 'filtrar',
    'first_name' => 'nombre',
    'from' => 'de',

    // H
    'hide' => 'ocultar',
    'hour' => 'hora',

    // I
    'icon' => 'icono',
    'id' => 'ID',
    'image' => 'imágen',
    'images' => 'imágen|imágenes',

    // L
    'last_name' => 'apellido',
    'list' => 'lista',
    'lists' => 'lista|listas',
    'location' => 'ubicación',
    'locations' => 'ubicación|ubicaciones',
    'latitude' => 'latitud',
    'longitude' => 'longitud',

    // M
    'minute' => 'minuto',
    'minutes' => 'minuto|minutos',
    'month' => 'mes',
    'months' => [
        'January' => 'Enero',
        'February' => 'Febrero',
        'March' => 'Marzo',
        'April' => 'Abril',
        'May' => 'Mayo',
        'June' => 'Junio',
        'July' => 'Julio',
        'August' => 'Agosto',
        'September' => 'Septiembre',
        'October' => 'Octubre',
        'November' => 'Noviembre',
        'December' => 'Diciembre'
    ],
    'municipality' => 'municipio',

    // N
    'name' => 'nombre',
    'names' => 'nombre|nombres',
    'new' => 'nuevo',
    'next' => 'siguiente',
    'note' => 'nota',
    'notes' => 'nota|notas',

    // O
    'of' => 'de',
    'phone' => 'teléfono',
    'photo' => 'foto',

    // P
    'paid' => 'pagado',
    'password' => 'contraseña',
    'path' => 'ruta',
    'page' => 'página',
    'pages' => 'página|páginas',
    'parent' => 'padre',
    'payment' => 'pago',
    'pending' => 'pendiente',
    'portfolio' => 'portafolio',
    'position' => 'posición',
    'photobook' => 'album',
    'price' => 'precio',
    'prices' => 'precio|precios',
    'profile' => 'perfil',
    'publish' => 'publicar',
    'published' => 'publicado',

    // Q
    'quantity' => 'cantidad',

    // R
    'rating' => 'calificación',
    'record' => 'historial',
    'redemptions' => 'redención|redenciones',
    'rejected' => 'rechazado',
    'reservation' => 'reservación',
    'reservations' => 'reservación|reservaciones',
    'resume' => 'resumen',

    // S
    'save' => 'guardar',
    'search' => 'buscar',
    'select' => 'selecciona',
    'service' => 'servicio',
    'services' => 'servicio|servicios',
    'show' => 'ver',
    'state' => 'estado',
    'states' => 'estado|estados',
    'status' => 'estado',
    'start' => 'inicio',
    'street' => 'calle',
    'stylist' => 'estilista',
    'stylists' => 'estilista|estilistas',
    'subtotal' => 'subtotal',

    // T
    'title' => 'título',
    'to' => 'para',
    'today' => 'hoy',
    'token' => 'token',
    'total' => 'total',
    'training' => 'formación',
    'trash' => 'papelera',
    'tree' => 'árbol',
    'type' => 'tipo',

    // U
    'update' => 'Actualizar',
    'user' => 'usuario',
    'users' => 'usuario|usuarios',

    // V
    'video' => 'video',

    // W
    'week' => 'semana',

    // Y
    'year' => 'año',

    // Z
    'zone' => 'zona',
    'zones' => 'zona|zonas'
];
