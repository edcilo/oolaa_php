<?php

return [
    'coupon' => [
        'F' => 'Fijo',
        'P' => 'Porcentaje',
    ],
    'curriculum' => [
        'E' => 'educación',
        'X' => 'experiencia',
    ],
    'cvc' => 'CVV',

    'day' => [
        'Monday' => 'Lunes',
        'Tuesday' => 'Martes',
        'Wednesday' => 'Miércoles',
        'Thursday' => 'Jueves',
        'Friday' => 'Viernes',
        'Saturday' => 'Sábado',
        'Sunday' => 'Domingo',
    ],
    'date_card' => 'MM/AA',

    'hrs' => 'Hrs.',

    'item' => [
        'P' => 'Pendiente',
        'A' => 'Aceptado',
        'C' => 'Cancelado',
        'D' => 'Rechazado',
    ],

    'iva' => 'I.V.A',

    'months' => [
        'Jan' => 'Ene.',
        'Feb' => 'Feb.',
        'Mar' => 'Mar.',
        'Apr' => 'Abr.',
        'May' => 'May.',
        'Jun' => 'Jun.',
        'Jul' => 'Jul.',
        'Aug' => 'Ago.',
        'Sep' => 'Sept.',
        'Oct' => 'Oct',
        'Nov' => 'Nov',
        'Dec' => 'Dic.'
    ],

    'oauth' => [
        'google' => 'Google',
        'facebook' => 'Facebook',
        'twitter' => 'Twitter',
    ],

    'phone' => 'Tel.',

    'reservation' => [
        'P' => 'Pendiente',
        'A' => 'Aceptado',
        'C' => 'Cancelado',
        'D' => 'Rechazado',
        'F' => 'Completado',
    ],

    'stylist' => [
        'P' => 'pendiente',
        'A' => 'aprobado',
        'R' => 'rechazado',
        'B' => 'bloqueado',
    ],
];
