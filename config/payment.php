<?php

return [
    'gateway' => env('PAY_DRIVER', ''),

    'credentials' => [
        'id' => env('PAY_ID'),
        'secret' => env('PAY_SECRET'),
        'key' => env('PAY_PUBLIC')
    ],
];
