<?php

return [
    'iva' => 16,

    'mail' => [
        'admin' => 'contacto@oolaa.com.mx',
        'cc' => 'no-reply@oolaa.com.mx',
        'contact' => 'contacto@oolaa.com.mx',
        'name' => 'OOLAA',
    ],

    'google' => [
        'maps' => env('GOOGLE_MAPS_KEY', ''),
    ],

    'oauth' => [
        'apps' => [
            'facebook' => true,

            'google' => true,

            'twitter' => false,

            'linkedin' => false,

            'github' => false,

            'bitbucket' => false,
        ]
    ],

    'phone' => [
        'contact' => '(55) 740 397 38'
    ],

    'social' => [
        'facebook' => 'https://www.facebook.com/oolaabeautyroutine/?fref=ts',
        'instagram' => 'https://www.instagram.com/oolaabeautyroutine/',
    ]
];
