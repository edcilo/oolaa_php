<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'OOLAA',

    'title_prefix' => '',

    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => 'OOLAA',

    'logo_mini' => 'OLA',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'green',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => null,

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => '/',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    'register_url' => 'register',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */

    'menu' => [
        'RESERVACIONES',
        [
            'text' => 'Reservaciones',
            'icon' => 'calendar',
            'submenu' => [
                [
                    'text' => 'Todo',
                    'url' => 'admin/reservations',
                    'icon' => 'list'
                ],
                [
                    'text' => 'Nuevo',
                    'url' => 'admin/reservations/create',
                    'icon' => 'plus'
                ],
                [
                    'text' => 'Papelera',
                    'url' => 'admin/reservations/trash',
                    'icon' => 'trash'
                ],
            ]
        ],
        [
            'text' => 'Usuarios',
            'icon' => 'users',
            'submenu' => [
                [
                    'text' => 'Todo',
                    'url' => 'admin/users',
                    'icon' => 'list'
                ],
                [
                    'text' => 'Nuevo usuario',
                    'url' => 'admin/users/create',
                    'icon' => 'plus'
                ],
                [
                    'text' => 'Papelera',
                    'url' => 'admin/users/trash',
                    'icon' => 'trash'
                ],
                [
                    'text' => 'Estilistas',
                    'url' => 'admin/stylists',
                    'icon' => 'paint-brush'
                ],
                [
                    'text' => 'Categorías de estilistas',
                    'icon' => 'tags',
                    'submenu' => [
                        [
                            'text' => 'Todo',
                            'url' => 'admin/categories',
                            'icon' => 'list'
                        ],
                        [
                            'text' => 'Nuevo',
                            'url' => 'admin/categories/create',
                            'icon' => 'plus'
                        ],
                        [
                            'text' => 'Papelera',
                            'url' => 'admin/categories/trash',
                            'icon' => 'trash'
                        ],
                    ]
                ],
            ]
        ],
        [
            'text' => 'Servicios',
            'icon' => 'shopping-bag',
            'submenu' => [
                [
                    'text' => 'Todo',
                    'url' => 'admin/services',
                    'icon' => 'list'
                ],
                [
                    'text' => 'Nuevo',
                    'url' => 'admin/services/create',
                    'icon' => 'plus'
                ],
                [
                    'text' => 'Papelera',
                    'url' => 'admin/services/trash',
                    'icon' => 'trash'
                ],
                [
                    'text' => 'Categorías',
                    'url' => 'admin/service-categories',
                    'icon' => 'list',
                ],
            ],
        ],
        [
            'text' => 'Cupones',
            'icon' => 'ticket',
            'submenu' => [
                [
                    'text' => 'Todo',
                    'url' => 'admin/coupons',
                    'icon' => 'list'
                ],
                [
                    'text' => 'Nuevo',
                    'url' => 'admin/coupons/create',
                    'icon' => 'plus'
                ],
            ]
        ],
        [
            'text' => 'Zonas',
            'icon' => 'map-o',
            'submenu' => [
                [
                    'text' => 'Todo',
                    'url' => 'admin/zones',
                    'icon' => 'list'
                ],
                [
                    'text' => 'Nuevo',
                    'url' => 'admin/zones/create',
                    'icon' => 'plus'
                ],
                /*
                [
                    'text' => 'Estados',
                    'url' => 'admin/states/',
                    'icon' => 'globe'
                ]
                */
            ],
        ],
        'CMS',
        [
            'text' => 'Inicio',
            'icon' => 'home',
            'submenu' => [
                [
                    'text' => 'Carrucel',
                    'url' => 'admin/cms/home/carousel',
                    'icon' => ''
                ],
                [
                    'text' => 'Top de estilistas',
                    'url' => 'admin/cms/home/top-stylists',
                    'icon' => ''
                ],
                [
                    'text' => 'Video',
                    'url' => 'admin/cms/home/video',
                    'icon' => ''
                ]
            ],
        ],
        [
            'text' => 'Datos de contacto',
            'url' => 'admin/cms/contact',
            'icon' => 'share-alt'
        ],
        [
            'text' => 'Términos y políticas',
            'icon' => 'institution',
            'submenu' => [
                [
                    'text' => 'Términos y condiciones',
                    'url' => 'admin/cms/terms/terms',
                    'icon' => ''
                ],
                [
                    'text' => 'Políticas de cancelación',
                    'url' => 'admin/cms/terms/policy',
                    'icon' => ''
                ]
            ]
        ],
        [
            'text' => 'Preguntas frecuentes',
            'url' => 'admin/cms/faqs',
            'icon' => 'question'
        ],
        [
            'text' => '¿Quienes somos?',
            'url' => 'admin/cms/about-us',
            'icon' => 'question'
        ],
        /*
        'ACCOUNT SETTINGS',
        [
            'text'    => 'Multilevel',
            'icon'    => 'share',
            'submenu' => [
                [
                    'text' => 'Level One',
                    'url'  => '#',
                ],
                [
                    'text'    => 'Level One',
                    'url'     => '#',
                    'submenu' => [
                        [
                            'text' => 'Level Two',
                            'url'  => '#',
                        ],
                        [
                            'text'    => 'Level Two',
                            'url'     => '#',
                            'submenu' => [
                                [
                                    'text' => 'Level Three',
                                    'url'  => '#',
                                ],
                                [
                                    'text' => 'Level Three',
                                    'url'  => '#',
                                ],
                            ],
                        ],
                    ],
                ],
                [
                    'text' => 'Level One',
                    'url'  => '#',
                ],
            ],
        ],
        [
            'text'       => 'Important',
            'icon_color' => 'aqua',
        ],
        */
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Choose which JavaScript plugins should be included. At this moment,
    | only DataTables is supported as a plugin. Set the value to true
    | to include the JavaScript file from a CDN via a script tag.
    |
    */

    'plugins' => [
        'datatables' => false,
        'select2'    => true,
    ],
];
