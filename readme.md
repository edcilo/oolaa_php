# OOLAA

### Requerimientos

* node/npm
* docker

### Instalación

1. Clonar el repositorio
```
git clone git@bitbucket.org:edcilo/oolaa_php.git
```

2. Entrar al directorio del proyecto
```
cd oolaa_php
```

3. Construir las imagenes docker
```
docker-compose build
```

4. Crear el archivo de entorno
```
cp .env.example .env
```

Modificar el puerto a usar para la app y los datos de la base de datos
```
...
APP_PORT=80

...
DB_PORT=3306
DB_DATABASE=oolaa
DB_USERNAME=oolaa
DB_PASSWORD=secret
...
```

5. Iniciar docker compose
```
docker-compose up -d
```

6. Acceder al contenedor de PHP
```
docker-compose exec php sh
```

7. Instalar dependencias de Laravel
```
composer install
```

8. Crear el APP_KEY
```
php artisan key:generate
```

9. Crear tablas
```
php artisan migrate --seed
```

Para hacer un refresh de la base de datos
```
php artisan migrate:fresh --seed
```

10. Crear link simbolico del storage
```
php artisan storage:link
```

11. Instalar dependencias js
```
npm install
```

12. Compilar archivos js
```
npm run dev
```

o

```
npm run watch
```

13. Usuarios de pruebas

```
usuario administrador
email: admin@oolaa.com.mx
password: secret

usuario comun
email: c@oolaa.com.mx
password: secret
```
