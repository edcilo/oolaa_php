<?php

namespace App\Listeners;

use App\Mail\SendVerificationToken;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendVerificationEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object $event
     * @return void
     */
    public function handle($event)
    {
        if (!$event->user->hasVerifiedEmail()) {
            Mail::to($event->user)->queue(new SendVerificationToken($event->user->verificationToken));
        }
    }
}
