<?php

namespace App\Services;

use App\Entities\Customer;
use App\Entities\Reservation;
use App\Entities\User;
use Openpay;

class OpenPayService
{
    protected $instance;

    public function __construct()
    {
        Openpay::setProductionMode(!config('app.debug'));

        $this->instance = Openpay::getInstance(
            config('payment.credentials.id'),
            config('payment.credentials.secret')
        );
    }

    public function addCard(string $token_id, string $session_id)
    {
        return $this->instance->cards->add(
            [
            'token_id' => $token_id,
            'device_session_id' => $session_id,
            ]
        );
    }

    public function addCustomer(User $user)
    {
        return $this->instance->customers->add(
            [
            'name' => $user->name,
            'email' => $user->email,
            'requires_account' => false
            ]
        );
    }

    public function createCharge(array $gateway, User $user, float $amount, string $description)
    {
        return $this->instance->charges->create(
            [
            'method' => 'card',
            'source_id' => $gateway['id'],
            'amount' => $amount,
            'description' => $description,
            'device_session_id' => $gateway['session_id'],
            'customer' => [
                'name' => $user->name,
                // 'last_name' => '',
                'phone_number' => $user->profile->phone,
                'email' => $user->email,
            ]
            ]
        );
    }

    public function customerCharge(string $customer_id, Reservation $reservation, string $session_id)
    {
        $customer = $this->getCustomer($customer_id);

        $chargeRequest = [
            'source_id' => $reservation->credit_card['provider_id'],
            'device_session_id' => $session_id,
            'method' => 'card',
            'amount' => $reservation->getTotalAttribute(),
            'currency' => 'MXN',
            'description' => __('text.charge_for_service') . ' ' . $reservation->confirmation_code,
            'order_id' => $reservation->confirmation_code,
        ];

        try {
            $charge = $customer->charges->create($chargeRequest);
        } catch (\OpenpayApiTransactionError $e) {
            return [
                'success' => false,
                'data' => $e
            ];
        } catch (\OpenpayApiRequestError $e) {
            return [
                'success' => false,
                'data' => $e
            ];
        }

        return [
            'success' => true,
            'data' => $charge
        ];
    }

    public function getCustomer(string $customer_id)
    {
        return $this->instance->customers->get($customer_id);
    }

    public function storeCard(Customer $customer, string $token_id, string $session_id)
    {
        if ($customer->openpay) {
            $op_customer = $this->getCustomer($customer->openpay);
        } else {
            $op_customer = $this->addCustomer($customer->user);
            $customer->update(['openpay' => $op_customer->id]);
        }

        try {
            $card = $op_customer->cards->add(['token_id' => $token_id, 'device_session_id' => $session_id]);
        } catch (\OpenpayApiTransactionError $e) {
            return [
                'error' => true,
                'data' => $e
            ];
        }

        return $card;
    }
}
