<?php

namespace App\Services;

use App\Entities\Item;
use App\Entities\Reservation;
use App\Entities\ReservationToken;
use App\Entities\Stylist;
use App\Entities\User;
use App\Mail\AdminCanceledNotification;
use App\Mail\AdminErrorInReservationFee;
use App\Mail\AdminItemDeclinedNotification;
use App\Mail\AdminReservationNew;
use App\Mail\AdminReservationNotAnswered;
use App\Mail\CustomerCanceledNotification;
use App\Mail\CustomerChangeNotification;
use App\Mail\CustomerDeclinedNotification;
use App\Mail\CustomerErrorInReservationFee;
use App\Mail\CustomerReservationApproved;
use App\Mail\CustomerReservationNew;
use App\Mail\StylistCancelNotificartion;
use App\Mail\StylistChangeNotification;
use App\Mail\StylistErrorInReservationFee;
use App\Mail\StylistItemDeclined;
use App\Mail\StylistNewReservationAutoStylist;
use App\Mail\StylistReservationApproved;
use App\Mail\StylistReservationNew;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;

class MailService
{
    public function newReservation(User $user, Reservation $reservation)
    {
        Mail::to(config('oolaa.mail.admin'))->queue(new AdminReservationNew($reservation, $user));
        Mail::to($user->email)->queue(new CustomerReservationNew($reservation, $user));

        if (!$reservation->auto_stylist) {
            $stylists = $reservation->stylists->unique();

            foreach ($stylists as $stylist) {
                $services = Item::where('stylist_id', $stylist->id)->where('reservation_id', $reservation->id)->get();
                $token = ReservationToken::where('reservation_id', $reservation->id)->where('stylist_id', $stylist->id)->first();

                Mail::to($stylist->email)->queue(new StylistReservationNew($reservation, $user, $stylist, $services, $token));
            }
        }
    }

    public function newAutoStylist(Reservation $reservation, Collection $stylists, Item $item)
    {
        foreach ($stylists as $stylist) {
            $hasService = $stylist->services()->where('id', $item->service_id)->count();
            $isBusy = $stylist->calendar()
                ->where('start', '<=', $item->reservation->start_at)
                ->where('end', '>=', $item->reservation->start_at)
                ->count();

            if (!$hasService || $isBusy) {
                continue;
            }

            Mail::to($stylist->user->email)->queue(new StylistNewReservationAutoStylist($reservation, $stylist, $item));
        }
    }

    public function successInReservationFee(Reservation $reservation)
    {
        $stylists = $reservation->stylists->unique();

        Mail::to($reservation->customer->email)->queue(new CustomerReservationApproved($reservation, $stylists));

        foreach ($stylists as $stylist) {
            Mail::to($stylist->email)->queue(new StylistReservationApproved($reservation, $stylist));
        }
    }

    public function errorInReservationFee($errorCode, $reservation, $error)
    {
        Mail::to(config('oolaa.mail.admin'))->queue(new AdminErrorInReservationFee($reservation, $error));
        Mail::to($reservation->customer->email)->queue(new CustomerErrorInReservationFee($reservation, $error));

        $stylists = $reservation->stylists->unique();

        foreach ($stylists as $stylist) {
            Mail::to($stylist->email)->queue(new StylistErrorInReservationFee($reservation, $stylist, $error));
        }
    }

    public function notifyChangeInReservation(Reservation $reservation, $changes)
    {
        $stylists = $reservation->stylists->unique();

        Mail::to($reservation->customer->email)->queue(new CustomerChangeNotification($reservation, $changes));

        foreach ($stylists as $stylist) {
            Mail::to($stylist->email)->queue(new StylistChangeNotification($reservation, $stylist, $changes));
        }
    }

    public function notifyReservationCanceled(Reservation $reservation)
    {
        $stylists = $reservation->stylists->unique();

        Mail::to(config('oolaa.mail.admin'))->queue(new AdminCanceledNotification($reservation));
        Mail::to($reservation->customer->email)->queue(new CustomerCanceledNotification($reservation));

        foreach ($stylists as $stylist) {
            Mail::to($stylist->email)->queue(new StylistCancelNotificartion($reservation, $stylist));
        }
    }

    public function notifyReservationDeclined(Reservation $reservation)
    {
        Mail::to($reservation->customer->email)->queue(new CustomerDeclinedNotification($reservation));
    }

    public function notifyDeclinedItem(Reservation $reservation, Collection $items, Collection $stylists, Stylist $old_stylist)
    {
        foreach ($stylists as $stylist) {
            if ($stylist->id === $old_stylist->id) {
                continue;
            }

            $token = ReservationToken::where('reservation_id', $reservation->id)->where('stylist_id', $old_stylist->id)->first();

            Mail::to($stylist->email)->queue(new StylistItemDeclined($reservation, $stylist, $old_stylist, $items, $token));
        }
    }

    public function notifyDeclinedItemToAdmin(Reservation $reservation, Collection $items)
    {
        Mail::to(config('oolaa.mail.admin'))->queue(new AdminItemDeclinedNotification($reservation, $items));
    }

    public function notifyDeclinedAutoStylistToAdmin(Reservation $reservation)
    {
        Mail::to(config('oolaa.mail.admin'))->queue(new AdminReservationNotAnswered($reservation));
    }
}
