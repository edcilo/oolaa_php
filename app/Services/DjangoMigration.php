<?php

namespace App\Services;

use App\Entities\User;

class DjangoMigration
{
    private $supportedHash = ['sha256'];

    public function migrate(string $username, string $password, string $field)
    {
        $user = User::where($field, $username)->first();

        if (!$user) {
            return false;
        }

        $old_password = $hashedValue = $user->getAuthPassword();

        if (!$old_password) {
            return false;
        }

        $validate = $this->validatePassword($password, $old_password);

        if (!$validate) {
            return false;
        }

        $this->updatePassword($user, $password);

        return true;
    }

    private function validatePassword(string $password, string $oldPassword)
    {
        $exploded = explode('$', $oldPassword);

        if (count($exploded) !== 4 || $exploded[0] !== 'pbkdf2_sha256') {
            return false;
        }

        list($method, $rounds, $salt, $trueHash) = $exploded;

        if (!$this->isSupported($method)) {
            return false;
        }

        return hash_equals(
            $trueHash,
            base64_encode(hash_pbkdf2($this->extractHash($method), $password, $salt, $rounds, strlen(base64_decode($trueHash)), true))
        );
    }

    private function isSupported(string $method)
    {
        $exploded = explode('_', $method);

        if (count($exploded) !== 2 || $exploded[0] !== 'pbkdf2' || !in_array($exploded[1], $this->supportedHash)) {
            return false;
        }

        return true;
    }

    private function extractHash(string $method)
    {
        return explode('_', $method)[1];
    }

    private function updatePassword(User $user, string $password)
    {
        $password = bcrypt($password);

        $user->update([
            'password' => $password
        ]);
    }
}