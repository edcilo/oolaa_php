<?php

namespace App\Mail;

use App\Entities\Reservation;
use App\Entities\Stylist;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class StylistReservationApproved extends Mailable
{
    use Queueable, SerializesModels;

    public $reservation;
    public $stylist;
    public $total;

    /**
     * Create a new message instance.
     *
     * @param \App\Entities\Reservation $reservation
     * @param \App\Entities\Stylist $stylist
     */
    public function __construct(Reservation $reservation, Stylist $stylist)
    {
        $this->reservation = $reservation;
        $this->stylist = $stylist;

        foreach ($reservation->items as $service) {
            if ($service->stylist_id === $stylist->id) {
                $this->total += ($service->quantity * $service->unit_price) * 0.8;
            }
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "¡{$this->stylist->name}, tu cita {$this->reservation->confirmation_code} ha sido aprobada.";

        return $this->from(
            [
                'address' => config('oolaa.mail.cc'),
                'name' => config('oolaa.mail.name')
            ]
        )->cc(config('oolaa.mail.cc'))
            ->subject($subject)
            ->view('email.stylistReservationApproved');
    }
}
