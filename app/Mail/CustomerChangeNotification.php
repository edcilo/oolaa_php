<?php

namespace App\Mail;

use App\Entities\Reservation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerChangeNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $reservation;
    public $changes;

    /**
     * Create a new message instance.
     *
     * @param \App\Entities\Reservation $reservation
     * @param array $changes
     * @return void
     */
    public function __construct(Reservation $reservation, array $changes)
    {
        $this->reservation = $reservation;
        $this->changes = $changes;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "{$this->reservation->customer->name}, hubo un cambio en tu reservación {$this->reservation->confirmation_code}.";

        return $this->from(
            [
                'address' => config('oolaa.mail.cc'),
                'name' => config('oolaa.mail.name')
            ]
        )->cc(config('oolaa.mail.cc'))
            ->subject($subject)
            ->view('email.customerChangeNotification');
    }
}
