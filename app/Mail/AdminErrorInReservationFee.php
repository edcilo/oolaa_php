<?php

namespace App\Mail;

use App\Entities\Reservation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminErrorInReservationFee extends Mailable
{
    use Queueable, SerializesModels;

    public $reservation;
    public $error;

    /**
     * Create a new message instance.
     *
     * @param \App\Entities\Reservation $reservation
     * @param array                     $error
     */
    public function __construct(Reservation $reservation, array $error)
    {
        $this->reservation = $reservation;
        $this->error = $error;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "Error en el cobro de la reservación {$this->reservation->confirmation_code}";

        return $this->from(
            [
            'address' => config('oolaa.mail.cc'),
            'name' => config('oolaa.mail.name')
            ]
        )->cc(config('oolaa.mail.cc'))
            ->subject($subject)
            ->view('email.adminErrorInReservationFee');
    }
}
