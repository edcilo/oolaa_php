<?php

namespace App\Mail;

use App\Entities\Reservation;
use App\Entities\Stylist;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class StylistChangeNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $reservation;
    public $stylist;
    public $total;
    public $changes;

    /**
     * Create a new message instance.
     *
     * @param \App\Entities\Reservation $reservation
     * @param \App\Entities\Stylist $stylist
     * @param array $changes
     * @return void
     */
    public function __construct(Reservation $reservation, Stylist $stylist, array $changes)
    {
        $this->reservation = $reservation;
        $this->stylist = $stylist;
        $this->changes = $changes;

        foreach ($reservation->items as $service) {
            if ($service->stylist_id === $stylist->id) {
                $this->total += ($service->quantity * $service->unit_price) * 0.8;
            }
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "{$this->stylist->name}, hubo un cambio en tu reservación {$this->reservation->confirmation_code}.";

        return $this->from(
            [
                'address' => config('oolaa.mail.cc'),
                'name' => config('oolaa.mail.name')
            ]
        )->cc(config('oolaa.mail.cc'))
            ->subject($subject)
            ->view('email.stylistChangeNotification');
    }
}
