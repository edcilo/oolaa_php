<?php

namespace App\Mail;

use App\Entities\Reservation;
use App\Entities\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerReservationNew extends Mailable
{
    use Queueable, SerializesModels;

    public $reservation;
    public $user;

    /**
     * Create a new message instance.
     *
     * @param \App\Entities\Reservation $reservation
     * @param \App\Entities\User        $user
     *
     * @return void
     */
    public function __construct(Reservation $reservation, User $user)
    {
        $this->reservation = $reservation;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "¡{$this->user->name}, tu cita está siendo procesada! Código de reserva {$this->reservation->confirmation_code}.";

        return $this->from(
            [
                'address' => config('oolaa.mail.cc'),
                'name' => config('oolaa.mail.name')
            ]
        )->cc(config('oolaa.mail.cc'))
            ->subject($subject)
            ->view('email.customerReservationNew');
    }
}
