<?php

namespace App\Mail;

use App\Entities\Reservation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerCanceledNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $reservation;

    /**
     * Create a new message instance.
     *
     * @param \App\Entities\Reservation $reservation
     * @return void
     */
    public function __construct(Reservation $reservation)
    {
        $this->reservation = $reservation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "{$this->reservation->customer->name}, la reservación {$this->reservation->confirmation_code} fue cancelada.";

        return $this->from(
            [
                'address' => config('oolaa.mail.cc'),
                'name' => config('oolaa.mail.name')
            ]
        )->cc(config('oolaa.mail.cc'))
            ->subject($subject)
            ->view('email.customerCanceledNotification');
    }
}
