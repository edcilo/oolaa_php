<?php

namespace App\Mail;

use App\Entities\Item;
use App\Entities\Reservation;
use App\Entities\Stylist;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class StylistNewReservationAutoStylist extends Mailable
{
    use Queueable, SerializesModels;

    public $reservation;
    public $stylist;
    public $item;
    public $total;

    /**
     * Create a new message instance.
     *
     * @param \App\Entities\Reservation $reservation
     * @param \App\Entities\Stylist $stylist
     * @param \App\Entities\Item $item
     *
     * @return void
     */
    public function __construct(Reservation $reservation, Stylist $stylist, Item $item)
    {
        $this->reservation = $reservation;
        $this->stylist = $stylist;
        $this->item = $item;

        $this->total += ($item->quantity * $item->unit_price) * 0.8;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "Hay una nueva reservación. Código de reserva {$this->reservation->confirmation_code}";

        return $this->from(
            [
                'address' => config('oolaa.mail.cc'),
                'name' => config('oolaa.mail.name')
            ]
        )->cc(config('oolaa.mail.cc'))
            ->subject($subject)
            ->view('email.stylistNewReservationAutoStylist');
    }
}
