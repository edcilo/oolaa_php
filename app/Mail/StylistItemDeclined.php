<?php

namespace App\Mail;

use App\Entities\Item;
use App\Entities\Reservation;
use App\Entities\ReservationToken;
use App\Entities\Stylist;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class StylistItemDeclined extends Mailable
{
    use Queueable, SerializesModels;

    public $reservation;
    public $services;
    public $stylist;
    public $old_stylist;
    public $total;
    public $token;

    /**
     * Create a new message instance.
     *
     * @param \App\Entities\Reservation $reservation
     * @param \App\Entities\Stylist $stylist
     * @param \App\Entities\Stylist $old_stylist
     * @param \Illuminate\Database\Eloquent\Collection $services
     * @param \App\Entities\ReservationToken $token
     *
     * @return void
     */
    public function __construct(Reservation $reservation, Stylist $stylist, Stylist $old_stylist, Collection $services, ReservationToken $token)
    {
        $this->reservation = $reservation;
        $this->stylist = $stylist;
        $this->old_stylist = $old_stylist;
        $this->services = $services;
        $this->token = $token;

        foreach ($services as $service) {
            $this->total += ($service->quantity * $service->unit_price) * 0.8;
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "Hay una nueva reservación que necesita tu aprobación. Código de reserva {$this->reservation->confirmation_code}";

        return $this->from(
            [
                'address' => config('oolaa.mail.cc'),
                'name' => config('oolaa.mail.name')
            ]
        )->cc(config('oolaa.mail.cc'))
            ->subject($subject)
            ->view('email.stylistItemDeclined');
    }
}
