<?php

namespace App\Mail;

use App\Entities\Reservation;
use App\Entities\ReservationToken;
use App\Entities\Stylist;
use App\Entities\User;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class StylistReservationNew extends Mailable
{
    use Queueable, SerializesModels;

    public $reservation;
    public $user;
    public $stylist;
    public $services;
    public $token;
    public $total = 0;

    /**
     * Create a new message instance.
     *
     * @param \App\Entities\Reservation                $reservation
     * @param \App\Entities\User                       $user
     * @param \App\Entities\Stylist                    $stylist
     * @param \Illuminate\Database\Eloquent\Collection $services
     * @param \App\Entities\ReservationToken           $token
     *
     * @return void
     */
    public function __construct(Reservation $reservation, User $user, Stylist $stylist, Collection $services, ReservationToken $token)
    {
        $this->reservation = $reservation;
        $this->user = $user;
        $this->stylist = $stylist;
        $this->services = $services;
        $this->token = $token;

        foreach ($services as $service) {
            $this->total += ($service->quantity * $service->unit_price) * 0.8;
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "Hay una nueva reservación que necesita tu aprobación. Código de reserva {$this->reservation->confirmation_code}";

        return $this->from(
            [
            'address' => config('oolaa.mail.cc'),
            'name' => config('oolaa.mail.name')
            ]
        )->cc(config('oolaa.mail.cc'))
            ->subject($subject)
            ->view('email.stylistReservationNew');
    }
}
