<?php

namespace App\Mail;

use App\Entities\Reservation;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminItemDeclinedNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $reservation;
    public $services;

    /**
     * Create a new message instance.
     *
     * @param \App\Entities\Reservation $reservation
     * @param \Illuminate\Database\Eloquent\Collection $services
     * @return void
     */
    public function __construct(Reservation $reservation, Collection $services)
    {
        $this->reservation = $reservation;
        $this->services = $services;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "[{$this->reservation->confirmation_code}] Servicio rechazado por estilista";

        return $this->from(
            [
                'address' => config('oolaa.mail.cc'),
                'name' => config('oolaa.mail.name')
            ]
        )->cc(config('oolaa.mail.cc'))
            ->subject($subject)
            ->view('email.adminItemDeclinedNotification');
    }
}
