<?php

namespace App\Mail;

use App\Entities\Reservation;
use App\Entities\Stylist;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class StylistErrorInReservationFee extends Mailable
{
    use Queueable, SerializesModels;

    public $reservation;
    public $stylist;
    public $error;

    /**
     * Create a new message instance.
     *
     * @param \App\Entities\Reservation $reservation
     * @param \App\Entities\Stylist     $stylist
     * @param array                     $error
     */
    public function __construct(Reservation $reservation, Stylist $stylist, array $error)
    {
        $this->reservation = $reservation;
        $this->stylist = $stylist;
        $this->error = $error;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "{$this->stylist->name} no paso el pago de una de tus citas";

        return $this->from(
            [
            'address' => config('oolaa.mail.cc'),
            'name' => config('oolaa.mail.name')
            ]
        )->cc(config('oolaa.mail.cc'))
            ->subject($subject)
            ->view('email.stylistErrorInReservationFee');
    }
}
