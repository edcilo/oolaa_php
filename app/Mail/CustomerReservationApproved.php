<?php

namespace App\Mail;

use App\Entities\Reservation;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerReservationApproved extends Mailable
{
    use Queueable, SerializesModels;

    public $reservation;
    public $stylists;

    /**
     * Create a new message instance.
     *
     * @param \App\Entities\Reservation $reservation
     * @param $stylists
     */
    public function __construct(Reservation $reservation, $stylists)
    {
        $this->reservation = $reservation;
        $this->stylists = $stylists;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "¡{$this->reservation->customer->name}, tu cita {$this->reservation->confirmation_code} ha sido confirmada.";

        return $this->from(
            [
                'address' => config('oolaa.mail.cc'),
                'name' => config('oolaa.mail.name')
            ]
        )->cc(config('oolaa.mail.cc'))
            ->subject($subject)
            ->view('email.customerReservationApproved');
    }
}
