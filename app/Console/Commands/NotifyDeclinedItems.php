<?php

namespace App\Console\Commands;

use App\Entities\Reservation;
use App\Entities\Stylist;
use App\Repositories\ItemRepository;
use App\Repositories\StylistRepository;
use App\Services\MailService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class NotifyDeclinedItems extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:notify-declined-items';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email notification for declined items';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param \App\Repositories\ItemRepository $itemRepo
     * @param \App\Repositories\StylistRepository $stylistRepo
     * @param \App\Services\MailService        $mail
     * @return mixed
     */
    public function handle(ItemRepository $itemRepo, StylistRepository $stylistRepo, MailService $mail)
    {
        $from = Carbon::now()->subHours(24);
        $items = $itemRepo->declined($from);
        $reservations = $items->groupBy([
            'reservation_id',
            function ($item) {
                return $item['stylist_id'];
            }
        ], $preserveKeys = true);

        $s0 = $stylistRepo->byRatingInRange(4.5, 5.0);
        $s1 = $stylistRepo->byRatingInRange(4.0, 4.5);
        $s2 = $stylistRepo->byRatingInRange(3.5, 4.0);
        $s3 = $stylistRepo->byRatingInRange(3.0, 3.5);
        $s4 = $stylistRepo->byRatingInRange(0, 3.0);

        foreach ($reservations as $reservation_id => $stylists) {
            $reservation = Reservation::find($reservation_id);

            if (!$reservation) {
                continue;
            }

            foreach ($stylists as $stylist_id => $items) {
                $stylist = Stylist::find($stylist_id);

                if (!$stylist) {
                    continue;
                }

                $min = Carbon::now()->diffInMinutes($reservation->updated_at);

                if ($min < 10) {
                    $mail->notifyDeclinedItem($reservation, $items, $s0, $stylist);
                } else if ($min < 20) {
                    $mail->notifyDeclinedItem($reservation, $items, $s1, $stylist);
                } else if ($min < 30) {
                    $mail->notifyDeclinedItem($reservation, $items, $s2, $stylist);
                } else if ($min < 40) {
                    $mail->notifyDeclinedItem($reservation, $items, $s3, $stylist);
                } else if ($min < 50) {
                    $mail->notifyDeclinedItem($reservation, $items, $s4, $stylist);
                } else {
                    $mail->notifyDeclinedItemToAdmin($reservation, $items);
                }
            }
        }
    }
}
