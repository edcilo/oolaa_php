<?php

namespace App\Console\Commands;

use App\Entities\Reservation;
use App\Entities\Stylist;
use App\Mail\StylistReservationNew;
use App\Repositories\StylistRepository;
use App\Services\MailService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendReservationEmailsToStylists extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:new-reservation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email notification for new reservations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param \App\Repositories\StylistRepository $stylistRepo
     * @param \App\Services\MailService        $mail
     *
     * @return mixed
     */
    public function handle(StylistRepository $stylistRepo, MailService $mail)
    {
        $from = Carbon::now()->subHours(24);
        $reservations = Reservation::with('items', 'customer')
            ->where('status', 'P')
            ->where('auto_stylist', true)
            ->where('updated_at', '>=', $from)
            ->get();

        $s0 = $stylistRepo->byRatingInRange(4.5, 5.0);
        $s1 = $stylistRepo->byRatingInRange(4.0, 4.5);
        $s2 = $stylistRepo->byRatingInRange(3.5, 4.0);
        $s3 = $stylistRepo->byRatingInRange(3.0, 3.5);
        $s4 = $stylistRepo->byRatingInRange(0, 3.0);

        foreach ($reservations as $reservation) {
            $min = Carbon::now()->diffInMinutes($reservation->updated_at);

            foreach ($reservation->items as $item) {
                if ($item->status !== 'P') {
                    continue;
                }

                if ($min < 10) {
                    $mail->newAutoStylist($reservation, $s0, $item);
                } else if ($min < 20) {
                    $mail->newAutoStylist($reservation, $s1, $item);
                } else if ($min < 30) {
                    $mail->newAutoStylist($reservation, $s2, $item);
                } else if ($min < 40) {
                    $mail->newAutoStylist($reservation, $s3, $item);
                } else if ($min < 50) {
                    $mail->newAutoStylist($reservation, $s4, $item);
                }
            }

            if ($min > 60) {
                $mail->notifyDeclinedAutoStylistToAdmin($reservation);
            }
        }
    }
}
