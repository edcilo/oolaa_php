<?php

namespace App\Http\Controllers\admin;

use App\Entities\Colony;
use App\Entities\State;
use App\Entities\Zone;
use App\Http\Requests\AdminZoneAddColony;
use App\Http\Requests\AdminZoneRemoveColonies;
use App\Http\Requests\AdminZoneStoreRequest;
use App\Http\Requests\AdminZoneUpdateRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class ZoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request): View
    {
        $zones = $this->paginateOrSearch(
            Zone::class,
            $request->get('s'),
            $request->get('pp'),
            'created_at',
            'desc'
        );

        return view('admin.zones.index', compact('zones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(): View
    {
        return view('admin.zones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AdminZoneStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminZoneStoreRequest $request): RedirectResponse
    {
        $zone = Zone::create($request->all());

        $message = __('text.entity_created', ['entity' => __('dictionary.zone') . ' ' . $zone->name]);
        session()->flash('success', $message);

        return  ($request->get('save'))
            ? redirect()->back()
            : redirect()->route('zones.show', $zone->id);
    }

    /**
     * @param \App\Entities\Zone                    $zone
     * @param \App\Http\Requests\AdminZoneAddColony $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addColony(Zone $zone, AdminZoneAddColony $request): RedirectResponse
    {
        $colony = Colony::findOrFail($request->get('colony_id'));
        $zone->colonies()->attach($colony->id);

        session()->flash('success', __('text.colony_added', ['name' => $colony->name]));

        return redirect()->back();
    }

    /**
     * @param \App\Entities\Zone                         $zone
     * @param \App\Http\Requests\AdminZoneRemoveColonies $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeColonies(Zone $zone, AdminZoneRemoveColonies $request): RedirectResponse
    {
        $zone->colonies()->detach($request->get('colony_ids'));

        $message = __(
            'text.entity_deleted_group', [
            'quantity' => count($request->get('colony_ids')),
            'entity' => trans_choice('dictionary.colonies', count($request->get('colony_ids')))
            ]
        );
        session()->flash('success', $message);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\Zone       $zone
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\View\View
     */
    public function show(Zone $zone, Request $request): View
    {
        $search = $request->get('s');
        $perPage = $request->get('pp') ? $request->get('pp') : 10;

        $colonies = ($search)
            ? $zone->colonies()->search($search)->paginate($perPage)
            : $zone->colonies()->paginate($perPage);

        return view('admin.zones.show', compact('zone', 'colonies'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entities\Zone $zone
     * @return \Illuminate\View\View
     */
    public function edit(Zone $zone): View
    {
        return view('admin.zones.edit', compact('zone'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\AdminZoneUpdateRequest $request
     * @param  \App\Entities\Zone                        $zone
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminZoneUpdateRequest $request, Zone $zone): RedirectResponse
    {
        $zone->update($request->all());

        $message = __('text.entity_updated', ['entity' => __('dictionary.zone') . ' ' . $zone->name]);
        session()->flash('success', $message);

        return  ($request->get('save'))
            ? redirect()->route('zones.show', $zone->id)
            : redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entities\Zone $zone
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Zone $zone): RedirectResponse
    {
        Zone::destroy($zone->id);

        $message = __('text.entity_deleted', ['entity' => __('dictionary.zone') . ' ' . $zone->name]);
        session()->flash('warning', $message);

        return redirect()->route('zones.index');
    }
}
