<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Cms;
use App\Http\Requests\AdminCmsVideoRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class CmsVideoController extends Controller
{
    public function index(): View
    {
        $cms = Cms::first();
        $video = $cms->schema['home']['video'];

        return view('admin.cms.home.video.index', compact('video'));
    }

    public function store(AdminCmsVideoRequest $request): RedirectResponse
    {
        $video = $request->get('video');

        $cms = Cms::first();
        $schema = $cms->schema;

        $schema['home']['video'] = $video;
        $cms->schema = $schema;
        $cms->save();

        return redirect()->back();
    }

    public function destroy(): RedirectResponse
    {
        $cms = Cms::first();
        $schema = $cms->schema;

        $schema['home']['video'] = '';
        $cms->schema = $schema;
        $cms->save();

        return redirect()->back();
    }
}
