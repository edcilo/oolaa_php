<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Cms;
use App\Http\Requests\AdminCmsPolicyUpdateRequest;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class CmsPolicyController extends Controller
{
    public function index(): View
    {
        $cms = Cms::first();
        $policy = $cms->schema['policy']['text'];

        return view('admin.cms.policy.index', compact('policy'));
    }

    public function update(AdminCmsPolicyUpdateRequest $request): RedirectResponse
    {
        $policy = $request->get('policy');

        $cms = Cms::first();
        $schema = $cms->schema;

        $schema['policy']['text'] = $policy;
        $cms->schema = $schema;
        $cms->save();

        return redirect()->back();
    }
}
