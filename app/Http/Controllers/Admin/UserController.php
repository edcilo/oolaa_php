<?php

namespace App\Http\Controllers\Admin;

use App\Entities\OAuth;
use App\Entities\Stylist;
use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminUserRestore;
use App\Http\Requests\AdminUsersDelete;
use App\Http\Requests\AdminUsersRestore;
use App\Http\Requests\AdminUserStore;
use App\Http\Requests\AdminUserUpdate;
use App\Repositories\UserRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class UserController extends Controller
{
    protected $userRepo;

    /**
     * UserController constructor.
     *
     * @param \App\Repositories\UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepo = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request): View
    {
        $users = $this->paginateOrSearch(
            User::class,
            $request->get('s'),
            $request->get('pp'),
            'created_at',
            'desc',
            ['profile']
        );

        return view('admin.users.index', compact('users'));
    }

    /**
     * Display a listing of the resources soft deleted
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\View\View
     */
    public function trash(Request $request): View
    {
        $users = $this->onlyTrashedPaginateOrSearch(
            User::class,
            $request->get('s'),
            $request->get('pp'),
            'created_at',
            'desc'
        );

        return view('admin.users.trash', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(): View
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AdminUserStore $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminUserStore $request): RedirectResponse
    {
        $data = $request->all();
        $data['password'] = bcrypt($request->get('password'));

        $user = User::create($data);
        $user->profile()->create($data);
        $user->customer()->create([]);

        $message = __('text.entity_created', ['entity' => __('dictionary.user') . ' ' . $user->name]);
        session()->flash('success', $message);

        return  ($request->get('save'))
            ? redirect()->back()
            : redirect()->route('users.show', $user->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\User $user
     * @return \Illuminate\View\View
     */
    public function show(User $user): View
    {
        $addresses = $this->userRepo->getAddresses($user, 6);
        $creditCards = $this->userRepo->getCreditCards($user, 6);
        $photoBook = $this->userRepo->getPhotoBook($user);

        return view('admin.users.show', compact('user', 'addresses', 'creditCards', 'photoBook'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entities\User $user
     * @return \Illuminate\View\View
     */
    public function edit(User $user): View
    {
        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\AdminUserUpdate $request
     * @param  \App\Entities\User                 $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminUserUpdate $request, User $user): RedirectResponse
    {
        $data = $request->all();
        if ($request->get('password')) {
            $data['password'] = bcrypt($request->get('password'));
        }

        $user->update($data);
        $user->profile->update($data);

        $message = __('text.entity_updated', ['entity' => __('dictionary.user') . ' ' . $user->name]);
        session()->flash('success', $message);

        return  ($request->get('save'))
            ? redirect()->route('users.show', $user->id)
            : redirect()->back();
    }

    /**
     * @param User $user
     * @return RedirectResponse
     */
    public function active(User $user): RedirectResponse
    {
        $user->update(['verified' => true]);

        session()->flash('warning', __('text.user_activated'));

        return redirect()->back();
    }

    /**
     * @param User $user
     * @return RedirectResponse
     */
    public function deactivate(User $user): RedirectResponse
    {
        $user->update(['verified' => false]);

        session()->flash('danger', __('text.user_deactivated'));

        return redirect()->back();
    }

    /**
     * @param  \App\Entities\OAuth $oauth
     * @return \Illuminate\Http\RedirectResponse
     */
    public function oauthUnlink(OAuth $oauth): RedirectResponse
    {
        OAuth::destroy($oauth->id);

        $message = __('text.entity_deleted', ['entity' => __('dictionary.application') . ' ' . $oauth->third_party]);
        session()->flash('warning', $message);

        return redirect()->route('users.show', $oauth->user);
    }

    /**
     * @param  \App\Entities\User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createStylistProfile(User $user)
    {
        if (!$user->stylists) {
            $user->stylist()->create([]);
        }

        session()->flash('success', __('text.create_stylist_profile'));

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entities\User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(User $user): RedirectResponse
    {
        User::destroy($user->id);
        Stylist::where('user_id', $user->id)->delete();

        session()->flash('restore', $user);

        return redirect()->route('users.index');
    }

    /**
     * Restore a user soft deleted
     *
     * @param  \App\Http\Requests\AdminUserRestore $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore(AdminUserRestore $request): RedirectResponse
    {
        $user_id = $request->get('user_id');

        $user = User::withTrashed()->find($user_id);
        $user->restore();

        Stylist::withTrashed()->where('user_id', $user_id)->restore();

        $message = __('text.entity_restored', ['entity' => __('dictionary.user') . ' ' . $user->name]);
        session()->flash('success', $message);

        return redirect()->back();
    }

    /**
     * Soft delete for multiple users
     *
     * @param  \App\Http\Requests\AdminUsersDelete $request
     * @return RedirectResponse
     */
    public function destroyMultiple(AdminUsersDelete $request): RedirectResponse
    {
        $ids = $request->get('user_ids');

        User::destroy($ids);

        $stylists = Stylist::whereIn('user_id', $ids)->pluck('id')->toArray();
        Stylist::destroy($stylists);

        session()->flash('restoreMultiple', $ids);

        return redirect()->back();
    }

    /**
     * Restore multiple users
     *
     * @param  \App\Http\Requests\AdminUsersRestore $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restoreMultiple(AdminUsersRestore $request): RedirectResponse
    {
        $ids = $request->get('user_ids');

        User::whereIn('id', $ids)->restore();
        Stylist::whereIn('user_id', $ids)->restore();

        $message = __(
            'text.entity_recovered', [
            'quantity' => count($ids), 'entity' => trans_choice('dictionary.users', count($ids))
            ]
        );
        session()->flash('warning', $message);

        return redirect()->back();
    }
}
