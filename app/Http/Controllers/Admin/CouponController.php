<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Coupon;
use App\Http\Requests\AdminCouponsDelete;
use App\Http\Requests\AdminCouponStore;
use App\Http\Requests\AdminCouponUpdate;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request): View
    {
        $coupons = $this->paginateOrSearch(
            Coupon::class,
            $request->get('s'),
            $request->get('pp'),
            'created_at',
            'desc'
        );

        return view('admin.coupons.index', compact('coupons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(): View
    {
        return view('admin.coupons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AdminCouponStore $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminCouponStore $request): RedirectResponse
    {
        $data = $request->all();
        $data['start'] = Carbon::createFromFormat('Y-m-d', $data['start']);
        $data['end'] = Carbon::createFromFormat('Y-m-d', $data['end']);

        $coupon = Coupon::create($data);

        $message = __('text.entity_created', ['entity' => __('dictionary.coupon') . ' ' . $coupon->code]);
        session()->flash('success', $message);

        return  ($request->get('save'))
            ? redirect()->back()
            : redirect()->route('coupons.show', $coupon->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\Coupon $coupon
     * @return \Illuminate\View\View
     */
    public function show(Coupon $coupon): View
    {
        return view('admin.coupons.show', compact('coupon'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entities\Coupon $coupon
     * @return \Illuminate\View\View
     */
    public function edit(Coupon $coupon): View
    {
        return view('admin.coupons.edit', compact('coupon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\AdminCouponUpdate $request
     * @param  \App\Entities\Coupon                 $coupon
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminCouponUpdate $request, Coupon $coupon): RedirectResponse
    {
        $coupon->update($request->all());

        $message = __('text.entity_updated', ['entity' => __('dictionary.coupon') . ' ' . $coupon->code]);
        session()->flash('success', $message);

        return  ($request->get('save'))
            ? redirect()->route('coupons.show', $coupon->id)
            : redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entities\Coupon $coupon
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Coupon $coupon): RedirectResponse
    {
        Coupon::destroy($coupon->id);

        session()->flash('restore', $coupon);

        return redirect()->route('coupons.index');
    }

    /**
     * Soft delete for multiple categories
     *
     * @param  \App\Http\Requests\AdminCouponsDelete $request
     * @return RedirectResponse
     */
    public function destroyMultiple(AdminCouponsDelete $request): RedirectResponse
    {
        $ids = $request->get('coupon_ids');

        Coupon::destroy($ids);

        session()->flash('restoreMultiple', $ids);

        return redirect()->back();
    }
}
