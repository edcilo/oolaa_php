<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Cms;
use App\Http\Requests\AdminCmsCarouselDeleteRequest;
use App\Http\Requests\AdminCmsCarouselRequest;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class CmsCarouselController extends Controller
{
    public function index(): View
    {
        $cms = Cms::first();
        $carousel = $cms->schema['home']['carousel'];

        return view('admin.cms.home.carousel.index', compact('carousel'));
    }

    public function store(AdminCmsCarouselRequest $request): RedirectResponse
    {
        $data = $request->only('order', 'picture');

        if ($request->hasFile('picture')) {
            $path = Storage::disk('local')->put('public/cms/home/carousel', $request->picture);
            $data['picture'] = asset(str_replace('public/', 'storage/', $path));
        }

        $cms = Cms::first();
        $schema = $cms->schema;

        $res = array_slice($schema['home']['carousel'], 0, $request->get('order'), true)
            + array($request->get('order') => $data)
            + array_slice($schema['home']['carousel'], $request->get('order'), count($schema['home']['carousel'])-$request->get('order'), true);

        $schema['home']['carousel'] = $res;

        $cms->schema = $schema;
        $cms->save();

        $message = __('text.entity_created', ['entity' => __('dictionary.image')]);
        session()->flash('success', $message);

        return redirect()->back();
    }

    public function destroy(AdminCmsCarouselDeleteRequest $request): RedirectResponse
    {
        $picture = $request->get('picture');

        $cms = Cms::where('schema->home->carousel', 'like', "%{$picture}%")->first();

        if ($cms) {
            $schema = $cms->schema;
            $carousel = $schema['home']['carousel'];

            foreach ($carousel as $index => $item) {
                if ($item['picture'] === $picture) {
                    array_splice($carousel, $index, 1);
                }
            }

            $schema['home']['carousel'] = $carousel;

            $cms->schema = $schema;
            $cms->save();
        }

        return redirect()->back();
    }
}
