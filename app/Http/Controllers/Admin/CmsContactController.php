<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Cms;
use App\Http\Requests\AdminCmsContactUpdateRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class CmsContactController extends Controller
{
    public function index(): View
    {
        $cms = Cms::first();
        $contact = $cms->schema['contact'];

        return view('admin.cms.contact.index', compact('contact'));
    }

    public function update(AdminCmsContactUpdateRequest $request): RedirectResponse
    {
        $contact = $request->only('phone', 'email', 'facebook', 'instagram');

        $cms = Cms::first();
        $schema = $cms->schema;

        $schema['contact'] = $contact;
        $cms->schema = $schema;
        $cms->save();

        return redirect()->back();
    }
}
