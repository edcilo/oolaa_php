<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Service;
use App\Entities\ServiceCategory;
use App\Entities\Stylist;
use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminServiceAddStylist;
use App\Http\Requests\AdminServiceRemoveStylists;
use App\Http\Requests\AdminServiceRestore;
use App\Http\Requests\AdminServicesDelete;
use App\Http\Requests\AdminServicesRestore;
use App\Http\Requests\AdminServiceStore;
use App\Http\Requests\AdminServiceUpdate;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request): View
    {
        $services = $this->paginateOrSearch(
            Service::class,
            $request->get('s'),
            $request->get('pp'),
            'created_at',
            'desc'
        );

        $tree = Service::with('children')->whereNull('parent_id')->orderBy('order', 'asc')->get();

        return view('admin.services.index', compact('services', 'tree'));
    }

    /**
     * Display a listing of the resources soft deleted
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\View\View
     */
    public function trash(Request $request): View
    {
        $services = $this->onlyTrashedPaginateOrSearch(
            Service::class,
            $request->get('s'),
            $request->get('pp'),
            'created_at',
            'desc'
        );

        return view('admin.services.trash', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(): View
    {
        $categories = ServiceCategory::orderBy('name', 'asc')->get();
        $services = Service::where('active', true)->orderBy('name', 'asc')->get();

        return view('admin.services.create', compact('categories', 'services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AdminServiceStore $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminServiceStore $request): RedirectResponse
    {
        $data = $request->all();

        if ($request->hasFile('image')) {
            $path = Storage::disk('local')->put('public/images/services', $request->image);
            $data['image'] = asset(str_replace('public/', 'storage/', $path));
        }

        if ($request->hasFile('icon')) {
            $path = Storage::disk('local')->put('public/images/services', $request->icon);
            $data['icon'] = asset(str_replace('public/', 'storage/', $path));
        }

        $service = Service::create($data);

        $message = __('text.entity_created', ['entity' => __('dictionary.service')]);
        session()->flash('success', $message);

        return  ($request->get('save'))
            ? redirect()->back()
            : redirect()->route('services.show', $service->id);
    }

    /**
     * @param  \App\Entities\User $user
     * @return \Illuminate\View\View
     */
    public function attach(User $user): View
    {
        return view('admin.stylists.attach');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\Service
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\View\View
     */
    public function show(Service $service, Request $request): View
    {
        $perPage =  $request->get('pp') ?: 10;

        $stylists = ($request->get('s'))
            ? $service->stylists()->with('user.profile')->search($request->get('s'))->paginate($perPage)
            : $service->stylists()->with('user.profile')->paginate($perPage);

        $ids = $service->stylists()->pluck('id')->toArray();

        $stylist_list = User::with(['profile', 'stylist'])
            ->whereHas(
                'stylist', function ($q) use ($ids) {
                    $q->where('status', 'A')->whereNotIn('id', $ids);
                }
            )
            ->orderBy('name', 'asc')
            ->get();

        return view('admin.services.show', compact('service', 'stylists', 'stylist_list'));
    }

    /**
     * @param \App\Entities\Service                     $service
     * @param \App\Http\Requests\AdminServiceAddStylist $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addStylist(Service $service, AdminServiceAddStylist $request): RedirectResponse
    {
        $stylist = Stylist::findOrFail($request->get('stylist_id'));

        if ($stylist->status === 'A') {
            $service->stylists()->attach($stylist->id);

            session()->flash('success', __('text.stylist_added', ['name' => $stylist->name]));
        }

        return redirect()->back();
    }

    public function removeStylists(Service $service, AdminServiceRemoveStylists $request): RedirectResponse
    {
        $service->stylists()->detach($request->get('stylist_ids'));

        $message = __(
            'text.entity_deleted_group', [
            'quantity' => count($request->get('stylist_ids')),
            'entity' => trans_choice('dictionary.stylists', count($request->get('stylist_ids')))
            ]
        );
        session()->flash('success', $message);

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entities\Service $service
     * @return \Illuminate\View\View
     */
    public function edit(Service $service): View
    {
        $categories = ServiceCategory::orderBy('name', 'asc')->get();
        $services = Service::where('active', true)->orderBy('name', 'asc')->get();

        return view('admin.services.edit', compact('service', 'categories', 'services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\AdminServiceUpdate $request
     * @param  \App\Entities\Service                 $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminServiceUpdate $request, Service $service): RedirectResponse
    {
        $data = $request->all();

        if ($request->hasFile('image')) {
            $path = Storage::disk('local')->put('public/images/services', $request->image);
            $data['image'] = asset(str_replace('public/', 'storage/', $path));
        }

        if ($request->hasFile('icon')) {
            $path = Storage::disk('local')->put('public/images/services', $request->icon);
            $data['icon'] = asset(str_replace('public/', 'storage/', $path));
        }

        $data['active'] = isset($data['active']) ? $data['active'] : false;
        $data['price_is_mutable'] = isset($data['price_is_mutable']) ? $data['price_is_mutable'] : false;

        $service->update($data);

        $message = __('text.entity_created', ['entity' => __('dictionary.service')]);
        session()->flash('success', $message);

        $message = __('text.entity_updated', ['entity' => __('dictionary.service') . ' ' . $service->name]);
        session()->flash('success', $message);

        return  ($request->get('save'))
            ? redirect()->route('services.show', $service->id)
            : redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entities\Service $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Service $service): RedirectResponse
    {
        Service::destroy($service->id);

        session()->flash('restore', $service);

        return redirect()->route('services.index');
    }

    /**
     * Restore a category soft deleted
     *
     * @param  \App\Http\Requests\AdminServiceRestore $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore(AdminServiceRestore $request): RedirectResponse
    {
        $service = Service::withTrashed()->find($request->get('service_id'));
        $service->restore();

        $message = __('text.entity_restored', ['entity' => __('dictionary.service') . ' ' . $service->name]);
        session()->flash('success', $message);

        return redirect()->back();
    }

    /**
     * Soft delete for multiple categories
     *
     * @param  \App\Http\Requests\AdminServicesDelete $request
     * @return RedirectResponse
     */
    public function destroyMultiple(AdminServicesDelete $request): RedirectResponse
    {
        $ids = $request->get('service_ids');

        Service::destroy($ids);

        session()->flash('restoreMultiple', $ids);

        return redirect()->back();
    }

    /**
     * Restore multiple services
     *
     * @param  \App\Http\Requests\AdminServicesRestore $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restoreMultiple(AdminServicesRestore $request): RedirectResponse
    {
        $ids = $request->get('service_ids');

        Service::whereIn('id', $ids)->restore();

        $message = __(
            'text.entity_recovered', [
            'quantity' => count($ids), 'entity' => trans_choice('dictionary.services', count($ids))
            ]
        );
        session()->flash('warning', $message);

        return redirect()->back();
    }
}
