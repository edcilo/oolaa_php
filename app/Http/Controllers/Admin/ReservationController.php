<?php

namespace App\Http\Controllers\admin;

use App\Entities\Address;
use App\Entities\Coupon;
use App\Entities\CreditCard;
use App\Entities\Customer;
use App\Entities\Item;
use App\Entities\Reservation;
use App\Entities\Service;
use App\Entities\Stylist;
use App\Http\Requests\AdminReservationAddCoupon;
use App\Http\Requests\AdminReservationAddServiceRequest;
use App\Http\Requests\AdminReservationIndex;
use App\Http\Requests\AdminReservationRestoreRequest;
use App\Http\Requests\AdminReservationsDeleteRequest;
use App\Http\Requests\AdminReservationSendNotification;
use App\Http\Requests\AdminReservationsRestoreRequest;
use App\Http\Requests\AdminReservationStoreRequest;
use App\Http\Requests\AdminReservationUpdateRequest;
use App\Repositories\ItemRepository;
use App\Repositories\ReservationRepository;
use App\Services\MailService;
use App\Services\OpenPayService;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;
use Openpay;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Http\Requests\AdminReservationIndex $request
     * @return \Illuminate\View\View
     */
    public function index(AdminReservationIndex $request): View
    {
        $query = Reservation::with(['items.service', 'items.stylist.user', 'customer.user.profile', 'stylists.user'])
            ->orderBy('id', 'desc');

        if ($request->get('s')) {
            $query->search($request->get('s'));
        }

        if ($request->get('status')) {
            $query->where('status', $request->get('status'));
        }

        if ($request->get('from_date')) {
            $query->where('start_at', '>=', $request->get('from_date') . ' 00:00:00');
        }

        if ($request->get('to_date')) {
            $query->where('start_at', '<=', $request->get('to_date') . ' 23:59:59');
        }

        $reservations = $query->paginate($request->get('pp') ?: 10);

        return view('admin.reservations.index', compact('reservations'));
    }

    public function trash(Request $request): View
    {
        $reservations = $this->onlyTrashedPaginateOrSearch(
            Reservation::class,
            $request->get('s'),
            $request->get('pp'),
            'created_at',
            'desc'
        );

        return view('admin.reservations.trash', compact('reservations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(): View
    {
        $customers = Customer::with('user.profile')->whereHas(
            'user', function ($query) {
                $query->where('verified', true);
            }
        )->get();

        return view('admin.reservations.create', compact('customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AdminReservationStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminReservationStoreRequest $request): RedirectResponse
    {
        $customer = Customer::find($request->get('customer_id'));
        $address = $customer->user->addresses->where('default', true)->first();
        // TODO asignar zona
        $creditCard = $customer->user->creditCards->where('default', true)->first();

        $data = $request->all();
        $data['address'] = $address;
        $data['credit_card'] = $creditCard;
        $data['iva_percent'] = config('oolaa.iva');
        $data['email_send'] = [
            'n0' => false,
            'n1' => false,
            'n2' => false,
            'n3' => false,
            'n4' => false
        ];

        $reservation = Reservation::create($data);

        return redirect()->route('reservations.show', $reservation->id);
    }

    public function addService(AdminReservationAddServiceRequest $request, Reservation $reservation): RedirectResponse
    {
        $service = Service::find($request->get('service_id'));

        $data = [
            'quantity' => $request->get('quantity'),
            'unit_price' => $service->price,
            'service_id' => $service->id,
            'reservation_id' => $reservation->id
        ];

        Item::create($data);

        $reservation->update(['status' => 'P']);

        $message = __('text.service_added');
        session()->flash('success', $message);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\Reservation $reservation
     * @return \Illuminate\View\View
     */
    public function show(Reservation $reservation): View
    {
        $customer = $reservation->customer;
        $addresses = $customer->user->addresses()->orderBy('default', 'desc')->orderBy('created_at', 'desc')->get();
        $creditCards = $customer->user->creditCards()->orderBy('default', 'desc')->orderBy('created_at', 'desc')->get();
        $services = Service::where('active', true)->orderBy('order')->get();
        $stylists = Stylist::with('user')->where('status', 'A')->get();

        $coupons = Coupon::withCount('reservations')->where('start', '>', Carbon::now())->get();
        $coupons = $coupons->filter(
            function ($r) {
                return $r->reservations_count < $r->redemptions;
            }
        );

        return view('admin.reservations.show', compact('reservation', 'customer', 'addresses', 'creditCards', 'services', 'stylists', 'coupons'));
    }

    public function pay(
        Reservation $reservation,
        Request $request,
        ItemRepository $itemRepo,
        ReservationRepository $reservationRepo
    ): RedirectResponse
    {
        $sessionId = $request->get('device_session_id');
        $itemsEmpties = $reservation->items()->whereNull('stylist_id')->count();

        if ($itemsEmpties) {
            session()->flash('danger', __('No es posible efectuar el cobro porque hay servicios que aún no tienen estilista asignado.'));

            return redirect()->back();
        }

        $itemRepo->accept($reservation->items);
        $reservationRepo->updateStatus($reservation);

        $reservationRepo->createCharge($reservation, $sessionId);

        session()->flash('success', __('text.charge_was_made_correctly'));

        return redirect()->back();
    }

    /**
     * @param \App\Entities\Reservation                    $reservation
     * @param \App\Http\Requests\AdminReservationAddCoupon $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addCoupon(Reservation $reservation, AdminReservationAddCoupon $request)
    {
        $reservation->update($request->only('coupon_id'));

        session()->flash('success', __('text.coupon_successfully_added'));

        return redirect()->back();
    }

    /**
     * @param \App\Entities\Reservation $reservation
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeCoupon(Reservation $reservation)
    {
        $reservation->update(['coupon_id' => null]);

        session()->flash('danger', __('text.coupon_successfully_removed'));

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\AdminReservationUpdateRequest $request
     * @param \App\Entities\Reservation                        $reservation
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminReservationUpdateRequest $request, Reservation $reservation): RedirectResponse
    {
        // TODO update zone
        if ($reservation->paid_out) {
            session()->flash('warning', 'No es posible modificar una reservación cobrada');

            return redirect()->back();
        }

        $data = [];

        if ($request->get('credit_card_id')) {
            $data['credit_card'] = CreditCard::find($request->get('credit_card_id'));
        }

        if ($request->get('address_id')) {
            $data['address'] = Address::find($request->get('address_id'));
        }

        if ($request->get('note')) {
            $data['note'] = $request->get('note');
        }

        if ($request->get('iva_percent') !== null && $request->get('iva_percent') >= 0) {
            $data['iva_percent'] = $request->get('iva_percent');
        }

        if (count($data) > 0) {
            $reservation->update($data);
        }

        $message = __('text.entity_updated', ['entity' => __('dictionary.reservation')]);
        session()->flash('success', $message);

        return redirect()->route('reservations.show', $reservation->id);
    }

    public function autoStylistToggle(Request $request, Reservation $reservation): RedirectResponse
    {
        $data['auto_stylist'] = ($request->get('auto_stylist')) ? true : false;
        $reservation->update($data);

        $message = __('text.entity_updated', ['entity' => __('dictionary.reservation')]);
        session()->flash('success', $message);

        return redirect()->route('reservations.show', $reservation->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entities\Reservation $reservation
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Reservation $reservation): RedirectResponse
    {
        Reservation::destroy($reservation->id);

        session()->flash('restore', $reservation);

        return redirect()->route('reservations.index');
    }

    /**
     * Restore a user soft deleted
     *
     * @param  \App\Http\Requests\AdminReservationRestoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore(AdminReservationRestoreRequest $request): RedirectResponse
    {
        $reservation = Reservation::withTrashed()->find($request->get('reservation_id'));
        $reservation->restore();

        $message = __('text.entity_restored', ['entity' => __('dictionary.reservation') . ' ' . $reservation->confirmation_code]);
        session()->flash('success', $message);

        return redirect()->back();
    }

    /**
     * Soft delete for multiple users
     *
     * @param  \App\Http\Requests\AdminReservationsDeleteRequest $request
     * @return RedirectResponse
     */
    public function destroyMultiple(AdminReservationsDeleteRequest $request): RedirectResponse
    {
        $ids = $request->get('reservation_ids');

        Reservation::destroy($ids);

        session()->flash('restoreMultiple', $ids);

        return redirect()->back();
    }

    /**
     * Restore multiple users
     *
     * @param  \App\Http\Requests\AdminReservationsRestoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restoreMultiple(AdminReservationsRestoreRequest $request): RedirectResponse
    {
        $ids = $request->get('reservation_ids');

        Reservation::whereIn('id', $ids)->restore();

        $message = __(
            'text.entity_recovered', [
            'quantity' => count($ids), 'entity' => trans_choice('dictionary.reservations', count($ids))
            ]
        );
        session()->flash('warning', $message);

        return redirect()->back();
    }

    public function notify(AdminReservationSendNotification $request, Reservation $reservation, MailService $mail)
    {
        $changes = $request->get('type');

        $mail->notifyChangeInReservation($reservation, $changes);

        session()->flash('success', 'La notificación se envió correctamente.');
        return redirect()->back();
    }

    public function cancel(Reservation $reservation, MailService $mail)
    {
        $reservation->update(['status' => 'C']);

        $mail->notifyReservationCanceled($reservation);

        session()->flash('warning', 'La reservación fue cancelada correctamente.');
        return redirect()->back();
    }

    public function reject(Reservation $reservation, MailService $mail)
    {
        $reservation->update(['status' => 'D']);

        $mail->notifyReservationDeclined($reservation);

        session()->flash('warning', 'La reservación fue rechazada correctamente.');
        return redirect()->back();
    }
}
