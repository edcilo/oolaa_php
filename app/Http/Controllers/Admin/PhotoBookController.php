<?php

namespace App\Http\Controllers\admin;

use App\Entities\PhotoBook;
use App\Http\Requests\AdminPhotoBookStoreRequest;
use App\Http\Requests\AdminPhotoBookUpdateRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PhotoBookController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AdminPhotoBookStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminPhotoBookStoreRequest $request): RedirectResponse
    {
        $data = $request->all();

        if ($request->hasFile('image')) {
            $path = Storage::disk('local')->put('public/images/photobook', $request->image);
            $data['image'] = asset(str_replace('public/', 'storage/', $path));
        }

        PhotoBook::create($data);

        $message = __('text.entity_created', ['entity' => __('dictionary.image')]);
        session()->flash('success', $message);

        return redirect()->back();
    }

    public function publishToggle(int $id): RedirectResponse
    {
        $photoBook = PhotoBook::findOrFail($id);

        $photoBook->is_selected = !$photoBook->is_selected;
        $photoBook->save();

        if ($photoBook->is_selected) {
            session()->flash('success', __('text.photo_book_published'));
        } else {
            session()->flash('warning', __('text.photo_book_hidden'));
        }

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\AdminPhotoBookUpdateRequest $request
     * @param  int                                            $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminPhotoBookUpdateRequest $request, int $id): RedirectResponse
    {
        $photoBook = PhotoBook::findOrFail($id);
        $photoBook->position = $request->get('position');
        $photoBook->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        $photoBook = PhotoBook::findOrFail($id);
        $photoBook->delete();

        $message = __('text.entity_deleted', ['entity' => __('dictionary.image')]);
        session()->flash('warning', $message);

        return redirect()->back();
    }
}
