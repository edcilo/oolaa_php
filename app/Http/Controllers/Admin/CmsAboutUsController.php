<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Cms;
use App\Http\Requests\AdminCmsAboutUsUpdateRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class CmsAboutUsController extends Controller
{
    public function index(): View
    {
        $cms = Cms::first();
        $about_us = $cms->schema['about_us']['text'];

        return view('admin.cms.about_us.index', compact('about_us'));
    }

    public function update(AdminCmsAboutUsUpdateRequest $request): RedirectResponse
    {
        $about_us = $request->get('about_us');

        $cms = Cms::first();
        $schema = $cms->schema;

        $schema['about_us']['text'] = $about_us;
        $cms->schema = $schema;
        $cms->save();

        return redirect()->back();
    }
}
