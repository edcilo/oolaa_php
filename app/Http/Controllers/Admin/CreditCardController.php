<?php

namespace App\Http\Controllers\Admin;

use App\Entities\CreditCard;
use App\Entities\User;
use App\Http\Requests\AdminCreditCardStore;
use App\Repositories\UserRepository;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\View\View;
use Openpay;

class CreditCardController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Entities\User               $user
     * @param  \App\Repositories\UserRepository $userRepository
     * @return \Illuminate\View\View
     */
    public function create(User $user, UserRepository $userRepository): View
    {
        $creditCards = $userRepository->getCreditCards($user, 6);

        return view('admin.creditCards.create', compact('user', 'creditCards'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AdminCreditCardStore $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminCreditCardStore $request): RedirectResponse
    {
        $user = User::findOrFail($request->get('user_id'));

        $openpay = Openpay::getInstance(config('payment.credentials.id'), config('payment.credentials.secret'));

        if (!$user->customer->openpay) {
            $customer = $openpay->customers->add(
                [
                'name' => $user->name,
                'email' => $user->email,
                'requires_account' => false
                ]
            );

            $user->customer->openpay = $customer->id;
            $user->customer->save();
        } else {
            $customer = $openpay->customers->get($user->customer->openpay);
        }

        $card = $customer->cards->add(
            [
            'token_id' => $request->get('token_id'),
            'device_session_id' => $request->get('device_session_id'),
            ]
        );

        $data = [
            'type' => $card->type,
            'brand' => $card->brand,
            'bank_name' => $card->bank_name,
            'card' => $card->serializableData['card_number'],
            'holder_name' => $card->serializableData['holder_name'],
            'expiration_year' => $card->serializableData['expiration_year'],
            'expiration_month' => $card->serializableData['expiration_month'],
            'default' => $request->get('default') ? $request->get('default') : false,
            'status' => 'A',
            'device_id' => $request->get('device_session_id'),
            'provider_id' => $card->id,
            'user_id' => $user->id,
        ];

        $creditCard = CreditCard::create($data);

        if ($request->get('default')) {
            CreditCard::where('user_id', $creditCard->user_id)
                ->where('default', true)
                ->where('id', '<>', $creditCard->id)
                ->update(['default' => false]);
        }

        $message = __('text.entity_created', ['entity' => __('dictionary.credit_card')]);
        session()->flash('success', $message);

        return redirect()->route('users.show', $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entities\CreditCard $creditCard
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(CreditCard $creditCard): RedirectResponse
    {
        CreditCard::destroy($creditCard->id);

        $message = __('text.entity_deleted', ['entity' => __('dictionary.credit_card') . ' ' . $creditCard->card]);
        session()->flash('warning', $message);

        return redirect()->back();
    }
}
