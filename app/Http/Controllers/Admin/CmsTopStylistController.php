<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Cms;
use App\Entities\Stylist;
use App\Http\Requests\AdminCmsTopStylistDeleteRequest;
use App\Http\Requests\AdminCmsTopStylistsRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class CmsTopStylistController extends Controller
{
    public function index(): View
    {
        $stylists = Stylist::where('status', 'A')->get();

        $cms = Cms::first();
        $top = $cms->schema['home']['top_stylists'];

        return view('admin.cms.home.topStylist.index', compact('top', 'stylists'));
    }

    public function store(AdminCmsTopStylistsRequest $request): RedirectResponse
    {
        $data = $request->only('order', 'stylist_id', 'picture');

        if ($request->hasFile('picture')) {
            $path = Storage::disk('local')->put('public/cms/home/top-stylists', $request->picture);
            $data['picture'] = asset(str_replace('public/', 'storage/', $path));
        }

        $cms = Cms::first();
        $schema = $cms->schema;

        $stylist = Stylist::with('user', 'user.profile')->find($data['stylist_id'])->toArray();
        $data['stylist'] = $stylist;

        $res = array_slice($schema['home']['top_stylists'], 0, $request->get('order'), true)
            + array($request->get('order') => $data)
            + array_slice($schema['home']['top_stylists'], $request->get('order'), count($schema['home']['top_stylists']) - $request->get('order'), true);

        $schema['home']['top_stylists'] = $res;

        $cms->schema = $schema;
        $cms->save();

        $message = __('text.entity_created', ['entity' => __('dictionary.stylist')]);
        session()->flash('success', $message);

        return redirect()->back();
    }

    public function destroy(AdminCmsTopStylistDeleteRequest $request): RedirectResponse
    {
        $picture = $request->get('picture');

        $cms = Cms::where('schema->home->top_stylists', 'like', "%{$picture}%")->first();

        if ($cms) {
            $schema = $cms->schema;
            $top = $schema['home']['top_stylists'];

            foreach ($top as $index => $item) {
                if ($item['picture'] === $picture) {
                    array_splice($top, $index, 1);
                }
            }

            $schema['home']['top_stylists'] = $top;

            $cms->schema = $schema;
            $cms->save();
        }

        return redirect()->back();
    }
}
