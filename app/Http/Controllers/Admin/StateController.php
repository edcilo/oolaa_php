<?php

namespace App\Http\Controllers\Admin;

use App\Entities\State;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminStatesDelete;
use App\Http\Requests\AdminStateStore;
use App\Http\Requests\AdminStateUpdate;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request): View
    {
        $states = $this->paginateOrSearch(
            State::class,
            $request->get('s'),
            $request->get('pp'),
            'name',
            'asc'
        );

        return view('admin.states.index', compact('states'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(): View
    {
        return view('admin.states.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AdminStateStore $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminStateStore $request): RedirectResponse
    {
        $state = State::create($request->all());

        $message = __('text.entity_created', ['entity' => __('dictionary.state') . ' ' . $state->name]);
        session()->flash('success', $message);

        return  ($request->get('save'))
            ? redirect()->back()
            : redirect()->route('states.show', $state->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\State $state
     * @return \Illuminate\View\View
     */
    public function show(State $state): View
    {
        return view('admin.states.show', compact('state'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entities\State $state
     * @return \Illuminate\Http\Response
     */
    public function edit(State $state)
    {
        return view('admin.states.edit', compact('state'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\AdminStateUpdate $request
     * @param  \App\Entities\State                 $state
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminStateUpdate $request, State $state): RedirectResponse
    {
        $state->update($request->all());

        $message = __('text.entity_updated', ['entity' => __('dictionary.state') . ' ' . $state->name]);
        session()->flash('success', $message);

        return  ($request->get('save'))
            ? redirect()->route('states.show', $state->id)
            : redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entities\State $state
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(State $state): RedirectResponse
    {
        State::destroy($state->id);

        session()->flash('restore', $state);

        return redirect()->route('states.index');
    }

    /**
     * Soft delete for multiple categories
     *
     * @param  \App\Http\Requests\AdminStatesDelete $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroyMultiple(AdminStatesDelete $request): RedirectResponse
    {
        $ids = $request->get('state_ids');

        State::destroy($ids);

        // session()->flash('restoreMultiple', $ids);

        return redirect()->back();
    }
}
