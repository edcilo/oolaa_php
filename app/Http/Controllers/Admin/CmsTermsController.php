<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Cms;
use App\Http\Requests\AdminCmsTermsUpdateRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class CmsTermsController extends Controller
{
    public function index():View
    {
        $cms = Cms::first();
        $terms = $cms->schema['terms']['text'];

        return view('admin.cms.terms.index', compact('terms'));
    }

    public function update(AdminCmsTermsUpdateRequest $request): RedirectResponse
    {
        $terms = $request->get('terms');

        $cms = Cms::first();
        $schema = $cms->schema;

        $schema['terms']['text'] = $terms;
        $cms->schema = $schema;
        $cms->save();

        return redirect()->back();
    }
}
