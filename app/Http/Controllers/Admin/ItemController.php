<?php

namespace App\Http\Controllers\admin;

use App\Entities\Calendar;
use App\Entities\Item;
use App\Http\Requests\AdminItemUpdateRequest;
use App\Repositories\CalendarRepository;
use App\Repositories\ReservationRepository;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItemController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\AdminItemUpdateRequest $request
     * @param  \App\Entities\Item                        $item
     * @param  \App\Repositories\ReservationRepository   $reservationRepo
     * @param \App\Repositories\CalendarRepository $calendarRepo
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminItemUpdateRequest $request, Item $item, ReservationRepository $reservationRepo, CalendarRepository $calendarRepo): RedirectResponse
    {
        $reservation = $item->reservation;

        if ($reservation->paid_out) {
            session()->flash('warning', __('No es posible modificar un servicio de una reservación pagada'));

            return redirect()->back();
        }

        $item->update($request->only('status', 'quantity', 'unit_price', 'discount', 'notes', 'stylist_id'));

        if ($item->status === 'D') {
            $stylist = $item->stylist()->with('user')->first();

            $item->update(['declined_by' => $stylist]);
        }

        $calendarRepo->updateOolaaEvent($request->get('stylist_id'), $item, $reservation);
        $reservationRepo->updateStatus($reservation);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entities\Item $item
     * @param  \App\Repositories\ReservationRepository   $reservationRepo
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Item $item, ReservationRepository $reservationRepo): RedirectResponse
    {
        $reservation = $item->reservation;

        if ($reservation->paid_out) {
            session()->flash('warning', __('No es posible eliminar un servicio de una reservación pagada'));

            return redirect()->back();
        }

        Item::destroy($item->id);

        $reservationRepo->updateStatus($item->reservation);

        return redirect()->back();
    }
}
