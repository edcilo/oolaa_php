<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Cms;
use App\Http\Requests\AdminCmsFaqsUpdateRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class CmsFaqsController extends Controller
{
    public function index(): View
    {
        $cms = Cms::first();
        $faqs = $cms->schema['faqs']['text'];

        return view('admin.cms.faqs.index', compact('faqs'));
    }

    public function update(AdminCmsFaqsUpdateRequest $request): RedirectResponse
    {
        $faqs = $request->get('faqs');

        $cms = Cms::first();
        $schema = $cms->schema;

        $schema['faqs']['text'] = $faqs;
        $cms->schema = $schema;
        $cms->save();

        return redirect()->back();
    }
}
