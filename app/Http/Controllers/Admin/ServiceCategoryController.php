<?php

namespace App\Http\Controllers\admin;

use App\Entities\ServiceCategory;
use App\Http\Requests\ServiceCategoryStoreRequest;
use App\Http\Requests\ServiceCategoryUpdateRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class ServiceCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request): View
    {
        $categories = $this->paginateOrSearch(
            ServiceCategory::class,
            $request->get('s'),
            $request->get('pp'),
            'created_at',
            'desc'
        );

        return view('admin.serviceCategories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(): View
    {
        return view('admin.serviceCategories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ServiceCategoryStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ServiceCategoryStoreRequest $request): RedirectResponse
    {
        $data = $request->all();

        if ($request->hasFile('icon')) {
            $path = Storage::disk('local')->put('public/images/service-categories', $request->icon);
            $data['icon'] = asset(str_replace('public/', 'storage/', $path));
        }

        if ($request->hasFile('cover')) {
            $path = Storage::disk('local')->put('public/images/service-categories', $request->cover);
            $data['cover'] = asset(str_replace('public/', 'storage/', $path));
        }

        $category = ServiceCategory::create($data);

        $message = __('text.entity_created', ['entity' => __('dictionary.category') . ' ' . $category->name]);
        session()->flash('success', $message);

        return  ($request->get('save'))
            ? redirect()->back()
            : redirect()->route('service-categories.show', $category->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\ServiceCategory $serviceCategory
     * @return \Illuminate\View\View
     */
    public function show(ServiceCategory $serviceCategory): View
    {
        $category = $serviceCategory;
        $services = $category->services()->orderBy('order')->get();

        return view('admin.serviceCategories.show', compact('category', 'services'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entities\ServiceCategory $serviceCategory
     * @return \Illuminate\View\View
     */
    public function edit(ServiceCategory $serviceCategory): View
    {
        $category = $serviceCategory;

        return view('admin.serviceCategories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ServiceCategoryUpdateRequest $request
     * @param  \App\Entities\ServiceCategory                   $serviceCategory
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceCategoryUpdateRequest $request, ServiceCategory $serviceCategory)
    {
        $data = $request->all();
        $category = $serviceCategory;

        if ($request->hasFile('icon')) {
            $path = Storage::disk('local')->put('public/images/service-categories', $request->icon);
            $data['icon'] = asset(str_replace('public/', 'storage/', $path));
        }

        if ($request->hasFile('cover')) {
            $path = Storage::disk('local')->put('public/images/service-categories', $request->cover);
            $data['cover'] = asset(str_replace('public/', 'storage/', $path));
        }

        $category->update($data);

        $message = __('text.entity_updated', ['entity' => __('dictionary.category') . ' ' . $category->name]);
        session()->flash('success', $message);

        return  ($request->get('save'))
            ? redirect()->route('service-categories.show', $category->id)
            : redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entities\ServiceCategory $serviceCategory
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(ServiceCategory $serviceCategory): RedirectResponse
    {
        ServiceCategory::destroy($serviceCategory->id);

        return redirect()->route('service-categories.index');
    }
}
