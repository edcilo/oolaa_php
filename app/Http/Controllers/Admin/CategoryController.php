<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Category;
use App\Entities\Service;
use App\Entities\ServiceCategory;
use App\Entities\Stylist;
use App\Http\Requests\AdminCategoriesDelete;
use App\Http\Requests\AdminCategoriesRestore;
use App\Http\Requests\AdminCategoryRestore;
use App\Http\Requests\AdminCategoryStore;
use App\Http\Requests\AdminCategoryUpdate;
use App\Http\Requests\CategoryStylistAttachRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request): View
    {
        $categories = $this->paginateOrSearch(
            Category::class,
            $request->get('s'),
            $request->get('pp'),
            'created_at',
            'desc'
        );

        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Display a listing of the resources soft deleted
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\View\View
     */
    public function trash(Request $request): View
    {
        $categories = $this->onlyTrashedPaginateOrSearch(
            Category::class,
            $request->get('s'),
            $request->get('pp'),
            'created_at',
            'desc'
        );

        return view('admin.categories.trash', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(): View
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AdminCategoryStore $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminCategoryStore $request): RedirectResponse
    {
        $category = Category::create($request->all());

        $message = __('text.entity_created', ['entity' => __('dictionary.category') . ' ' . $category->name]);
        session()->flash('success', $message);

        return  ($request->get('save'))
            ? redirect()->back()
            : redirect()->route('categories.show', $category->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\Category $category
     * @return \Illuminate\View\View
     */
    public function show(Category $category): View
    {
        $ids = $category->stylists()->pluck('id')->toArray();
        $servicesAttached = $category->services()->pluck('id')->toArray();

        $stylistsList = Stylist::with('user')->where('status', 'A')->whereNotIn('id', $ids)->get();
        $stylists = $category->stylists()->with('user.profile')->paginate();

        $serviceCategories = ServiceCategory::with('services')->get();

        return view('admin.categories.show', compact('category', 'stylists', 'stylistsList', 'serviceCategories', 'servicesAttached'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entities\Category $category
     * @return \Illuminate\View\View
     */
    public function edit(Category $category): View
    {
        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\AdminCategoryUpdate $request
     * @param  \App\Entities\Category                 $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminCategoryUpdate $request, Category $category): RedirectResponse
    {
        $category->update($request->all());

        $message = __('text.entity_updated', ['entity' => __('dictionary.category') . ' ' . $category->name]);
        session()->flash('success', $message);

        return  ($request->get('save'))
            ? redirect()->route('categories.show', $category->id)
            : redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entities\Category $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Category $category): RedirectResponse
    {
        Category::destroy($category->id);

        session()->flash('restore', $category);

        return redirect()->route('categories.index');
    }

    /**
     * Restore a category soft deleted
     *
     * @param  \App\Http\Requests\AdminCategoryRestore $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore(AdminCategoryRestore $request): RedirectResponse
    {
        $category = Category::withTrashed()->find($request->get('category_id'));
        $category->restore();

        $message = __('text.entity_restored', ['entity' => __('dictionary.category') . ' ' . $category->name]);
        session()->flash('success', $message);

        return redirect()->back();
    }

    /**
     * Soft delete for multiple categories
     *
     * @param  \App\Http\Requests\AdminCategoriesDelete $request
     * @return RedirectResponse
     */
    public function destroyMultiple(AdminCategoriesDelete $request): RedirectResponse
    {
        $ids = $request->get('category_ids');

        Category::destroy($ids);

        session()->flash('restoreMultiple', $ids);

        return redirect()->back();
    }

    /**
     * Restore multiple categories
     *
     * @param  \App\Http\Requests\AdminCategoriesRestore $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restoreMultiple(AdminCategoriesRestore $request): RedirectResponse
    {
        $ids = $request->get('category_ids');

        Category::whereIn('id', $ids)->restore();

        $message = __(
            'text.entity_recovered', [
            'quantity' => count($ids), 'entity' => trans_choice('dictionary.categories', count($ids))
            ]
        );
        session()->flash('warning', $message);

        return redirect()->back();
    }

    // TODO ATTACH/DETACH services
    public function servicesSync(Category $category, Request $request)
    {
        $services = $request->get('service_ids');
        $prices = $request->get('price');
        $data = [];

        for ($i = 0; $i < count($services); $i++) {
            $service_id = $services[$i];
            $price = isset($prices[$service_id]) ? $prices[$service_id] : 0;

            $data[$service_id] = [
                'price' => ($price !== null) ? $price : 0
            ];
        }

        $category->services()->sync($data);

        return redirect()->back();
    }

    public function stylistAttach(Category $category, CategoryStylistAttachRequest $request)
    {
        // TODO validate that stylist is activate
        $category->stylists()->attach($request->get('stylist_id'));

        return redirect()->back();
    }

    public function stylistDetach(Category $category, Stylist $stylist)
    {
        $category->stylists()->detach($stylist->id);

        return redirect()->back();
    }
}
