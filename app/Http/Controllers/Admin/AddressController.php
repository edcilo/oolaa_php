<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Address;
use App\Entities\User;
use App\Http\Requests\AdminAddressStore;
use App\Http\Requests\AdminAddressUpdate;
use App\Repositories\UserRepository;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class AddressController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @param  int                              $user_id
     * @param  \App\Repositories\UserRepository $userRepository
     * @return \Illuminate\View\View
     */
    public function create(int $user_id, UserRepository $userRepository): View
    {
        $user = User::find($user_id);
        $addresses = $userRepository->getAddresses($user, 6);

        return view('admin.addresses.create', compact('user', 'addresses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AdminAddressStore $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminAddressStore $request): RedirectResponse
    {
        $address = Address::create($request->all());

        if ($request->get('default')) {
            Address::where('user_id', $address->user->id)
                ->where('default', true)
                ->where('id', '<>', $address->id)
                ->update(['default' => false]);
        }

        $message = __('text.entity_created', ['entity' => __('dictionary.address') . ' ' . $address->line]);
        session()->flash('success', $message);

        return  ($request->get('save'))
            ? redirect()->back()
            : redirect()->route('users.show', $address->user->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entities\Address            $address
     * @param  \App\Repositories\UserRepository $userRepository
     * @return \Illuminate\View\View
     */
    public function edit(Address $address, UserRepository $userRepository): View
    {
        $user = $address->user;
        $addresses = $userRepository->getAddresses($user, 6);

        return view('admin.addresses.edit', compact('address', 'user', 'addresses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\AdminAddressUpdate $request
     * @param  \App\Entities\Address                 $address
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminAddressUpdate $request, Address $address): RedirectResponse
    {
        $data = $request->all();
        $data['default'] = isset($data['default']) && $data['default'] ? $data['default'] : false;

        $address->update($data);

        $message = __('text.entity_updated', ['entity' => __('dictionary.address') . ' ' . $address->line]);
        session()->flash('success', $message);

        return  ($request->get('save'))
            ? redirect()->route('users.show', $address->user->id)
            : redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entities\Address $address
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Address $address): RedirectResponse
    {
        Address::destroy($address->id);

        $message = __('text.entity_deleted', ['entity' => __('dictionary.address') . ' ' . $address->line]);
        session()->flash('warning', $message);

        return redirect()->route('users.show', $address->user->id);
    }
}
