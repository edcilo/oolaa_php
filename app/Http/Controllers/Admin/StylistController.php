<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Stylist;
use App\Entities\Weekday;
use App\Http\Requests\AdminCalendarEventStore;
use App\Http\Requests\AdminStylistChangeStatusRequest;
use App\Http\Requests\AdminStylistWeekdays;
use App\Repositories\UserRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class StylistController extends Controller
{
    protected $userRepo;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepo = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request): View
    {
        $stylists = $this->paginateOrSearch(
            Stylist::class,
            $request->get('s'),
            $request->get('pp'),
            'created_at',
            'desc',
            ['user.profile']
        );

        return view('admin.stylists.index', compact('stylists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\Stylist $stylist
     * @return \Illuminate\View\View
     */
    public function show(Stylist $stylist): View
    {
        $user = $stylist->user;
        $addresses = $this->userRepo->getAddresses($stylist->user, 6);
        $creditCards = $this->userRepo->getCreditCards($stylist->user, 6);
        $photoBook = $this->userRepo->getPhotoBook($stylist->user);

        return view('admin.stylists.show', compact('user', 'addresses', 'creditCards', 'photoBook'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function calendar(Stylist $stylist): View
    {
        $stylist = Stylist::with('weekdays')->findOrFail($stylist->id);
        $weekdays = Weekday::all();
        $events = $stylist->calendar;
        $eventsJson = $events->toJson();

        return view('admin.stylists.calendar', compact('stylist', 'events', 'eventsJson', 'weekdays'));
    }

    public function storeEvent(AdminCalendarEventStore $request, Stylist $stylist): RedirectResponse
    {
        $data = $request->all();
        $data['start'] = "{$data['start_date']} {$data['start_hour']}";
        $data['end'] = "{$data['end_date']} {$data['end_hour']}";
        $data['borderColor'] = '#ffffff';
        $data['className'] = 'oolaa-event';

        $stylist->calendar()->create($data);

        session()->flash('success', __('text.entity_created', ['entity' => __('dictionary.event')]));

        return redirect()->back();
    }

    public function updateWeekdays(AdminStylistWeekdays $request, Stylist $stylist)
    {
        $weekdays = $request->get('weekdays');
        $stylist->weekdays()->sync($weekdays);

        return redirect()->back();
    }

    public function changeStatus(AdminStylistChangeStatusRequest $request): RedirectResponse
    {
        $stylist = Stylist::findOrFail($request->get('stylist_id'));
        $stylist->status = $request->get('status');
        $stylist->save();

        session()->flash('warning', __('text.stylist_status_changed'));

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
