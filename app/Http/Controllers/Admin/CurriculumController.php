<?php

namespace App\Http\Controllers\admin;

use App\Entities\Curriculum;
use App\Entities\User;
use App\Http\Requests\AdminCurriculumStoreRequest;
use App\Http\Requests\AdminCurriculumUpdateRequest;
use App\Repositories\UserRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class CurriculumController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @param  int                              $user_id
     * @param  \App\Repositories\UserRepository $userRepository
     * @return \Illuminate\View\View
     */
    public function create(int $user_id, UserRepository $userRepository): View
    {
        $user = User::find($user_id);
        $addresses = $userRepository->getAddresses($user, 6);

        return view('admin.curriculum.create', compact('user', 'addresses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AdminCurriculumStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminCurriculumStoreRequest $request): RedirectResponse
    {
        $curriculum = Curriculum::create($request->all());

        $message = __('text.entity_created', ['entity' => __('dictionary.experience')]);
        session()->flash('success', $message);

        return  ($request->get('save'))
            ? redirect()->back()
            : redirect()->route('stylists.show', $curriculum->stylist->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entities\Curriculum         $curriculum
     * @param  \App\Repositories\UserRepository $userRepository
     * @return \Illuminate\View\View
     */
    public function edit(Curriculum $curriculum, UserRepository $userRepository): View
    {
        $user = $curriculum->stylist->user;
        $addresses = $userRepository->getAddresses($user, 6);

        return view('admin.curriculum.edit', compact('curriculum', 'user', 'addresses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\AdminCurriculumUpdateRequest $request
     * @param  \App\Entities\Curriculum                        $curriculum
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminCurriculumUpdateRequest $request, Curriculum $curriculum): RedirectResponse
    {
        $curriculum->update($request->all());

        $message = __('text.entity_updated', ['entity' => __('dictionary.experience')]);
        session()->flash('warning', $message);

        return  ($request->get('save'))
            ? redirect()->back()
            : redirect()->route('stylists.show', $curriculum->stylist->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entities\Curriculum $curriculum
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Curriculum $curriculum): RedirectResponse
    {
        Curriculum::destroy($curriculum->id);

        $message = __('text.entity_deleted', ['entity' => __('dictionary.experience')]);
        session()->flash('warning', $message);

        return redirect()->back();
    }
}
