<?php

namespace App\Http\Controllers\Stylist;

use App\Entities\Stylist;
use App\Entities\Weekday;
use App\Http\Requests\StylistEventStore;
use App\Http\Requests\StylistWeekdays;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StylistCalendarController extends Controller
{
    public function index(Stylist $stylist)
    {
        $stylist = Stylist::with('weekdays')->findOrFail($stylist->id);
        $weekdays = Weekday::orderby('number', 'asc')->get();
        $events = $stylist->calendar;

        $businessDays = $stylist->weekdays->toJson();
        $eventsJson = $events->toJson();

        return view('stylist.calendar', compact('stylist', 'weekdays', 'events', 'eventsJson', 'businessDays'));
    }

    public function weekdays(Stylist $stylist, StylistWeekdays $request) {
        $weekdays = $request->get('weekdays');
        $stylist->weekdays()->sync($weekdays);

        return redirect()->back();
    }

    public function eventStore(Stylist $stylist, StylistEventStore $request)
    {
        $data = $request->all();

        if (isset($data['allDay'])) {
            $start = Carbon::createFromFormat('Y-m-d H:i:s', $data['start'])->setTime(0,0);
            $end = Carbon::createFromFormat('Y-m-d H:i:s', $data['start'])->setTime(23,59);
            $data['start'] = $start;
            $data['end'] = $end;
        }

        $data['borderColor'] = '#ffffff';
        $data['backgroundColor'] = "#ff485f";
        $data['className'] = 'stylist-event';

        $stylist->calendar()->create($data);

        session()->flash('success', __('text.entity_created', ['entity' => __('dictionary.event')]));

        return redirect()->back();
    }
}
