<?php

namespace App\Http\Controllers\Stylist;

use App\Entities\ServiceCategory;
use App\Http\Controllers\Controller;
use App\Http\Resources\ServiceCategoryCollection;
use App\Http\Resources\StylistCollection;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SearchController extends Controller
{
    public function search(): View
    {
        return view('stylist.search');
    }

    public function stylists(Request $request)
    {
        $category_id = $request->get('service_id');
        $search = $request->get('search');
        $zip_code = $request->get('zip_code');
        $price_order = $request->get('price');
        $rating_order = $request->get('rating');

        $date = $request->get('date');
        $time = $request->get('time');
        $datetime = null;

        if ($date || $time) {
            if (!$date) {
                $date = Carbon::now()->format('Y-m-d');
            }
            if (!$time) {
                $time = Carbon::now()->format('H:i');
            }

            $datetime = Carbon::createFromFormat('Y-m-d H:i', "$date $time");
        }

        // TODO add price order
        $categories = ServiceCategory::with(
            [
            'services' => function ($q) use ($search, $zip_code, $rating_order, $datetime) {
                $q->with(
                    [
                    'stylists' => function ($q) use ($search, $zip_code, $rating_order, $datetime) {
                        $q->with('user.profile', 'photoBooks', 'services.category', 'types', 'zones', 'calendar')
                            ->where('status', 'A')
                            ->whereHas(
                                'zones.colonies', function ($q) use ($zip_code) {
                                    if ($zip_code) {
                                        $q->where('zip_code', $zip_code);
                                    }
                                }
                            )
                            ->search($search)
                            ->orderBy('rating', $rating_order);
                    }
                    ]
                )->orderBy('order', 'asc');
            }
            ]
        )
            ->where(
                function ($q) use ($category_id) {
                    if ($category_id) {
                        $q->where('id', $category_id);
                    }
                }
            )
            ->orderBy('shopping_order', 'asc')
            ->get();

        $responseServices = new Collection();
        foreach ($categories as $category) {
            $responseServices->push($category->services);
        }
        $services = $responseServices->collapse();

        $responseStylists = new Collection();
        foreach ($services as $service) {
            $responseStylists->push($service->stylists);
        }
        $stylists = $responseStylists->collapse()->unique('id');

        $stylistCollection = new Collection();
        foreach ($stylists as $stylist) {
            if ($datetime && $stylist->calendar->where('start', '<=', $datetime)->where('end', '>=', $datetime)->count()) {
                continue;
            }

            $stylistCollection->push($stylist);
        }

        return new StylistCollection($stylistCollection);
    }

    public function services()
    {
        $categories = ServiceCategory::with('services')->orderBy('order', 'asc')->get();

        return new ServiceCategoryCollection($categories);
    }

    public function serviceCategories()
    {
        $categories = ServiceCategory::all();

        return response()->json($categories);
    }
}
