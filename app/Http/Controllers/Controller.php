<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param string      $entity
     * @param string|null $search
     * @param int|null    $perPage
     * @param string      $orderColumn
     * @param string      $order
     * @param array       $with
     * @return mixed
     */
    protected function paginateOrSearch(
        string $entity,
        string $search = null,
        int $perPage = null,
        string $orderColumn = 'id',
        string $order = 'ASC',
        array $with = []
    ) {
        $entity = new $entity;
        $perPage = $perPage ?: 10;

        return ($search)
            ? $entity->with($with)->search($search)->orderBy($orderColumn, $order)->paginate($perPage)
            : $entity->with($with)->orderBy($orderColumn, $order)->paginate($perPage);
    }

    protected function onlyTrashedPaginateOrSearch(
        string $entity,
        string $search = null,
        int $perPage = null,
        string $orderColumn = 'id',
        string $order = 'ASC'
    ) {
        $entity = new $entity;
        $perPage = $perPage ?: 10;

        return ($search)
            ? $entity->onlyTrashed()->search($search)->orderBy($orderColumn, $order)->paginate($perPage)
            : $entity->onlyTrashed()->orderBy($orderColumn, $order)->paginate($perPage);
    }
}
