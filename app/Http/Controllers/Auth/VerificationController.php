<?php

namespace App\Http\Controllers\Auth;

use App\Entities\User;
use App\Entities\VerificationToken;
use App\Events\UserRequestedVerificationEmail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class VerificationController extends Controller
{
    public function verify(VerificationToken $token)
    {
        $user = $token->user;
        $user->verified = true;
        $user->save();

        VerificationToken::destroy($token->id);

        Auth::login($token->user);

        return redirect('/home')->withInfo('Email verification succesful. Please login again');
    }

    public function resend(Request $request)
    {
        $user = User::byEmail($request->email)->firstOrFail();

        if ($user->hasVerifiedEmail()) {
            return redirect('/home')->withInfo('Your email has already been verified');
        }

        event(new UserRequestedVerificationEmail($user));

        return redirect('/login')->withInfo('Verification email resent. Please check your inbox');
    }
}
