<?php

namespace App\Http\Controllers\Auth;

use App\Entities\OAuth;
use App\Entities\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class OAuthController extends Controller
{
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/stylist/search';

    protected $third_party;
    protected $url;

    public function redirect($third_party)
    {
        $third_parties = config('oolaa.oauth.apps');

        if (!isset($third_parties[$third_party]) || !$third_parties[$third_party]) {
            abort(404);
        }

        return Socialite::driver($third_party)
            ->redirect();
    }

    public function facebookHandler()
    {
        $this->third_party = 'facebook';
        $this->url = 'https://www.facebook.com/';

        if (auth()->check()) {
            $this->handlerLink();

            return redirect()->intended();
        }

        $user = $this->handler();

        if (is_null($user)) {
            return redirect()->route('register')->with('warning', __('text.auth_user_deleted'));
        }

        Auth::login($user);

        return redirect()->to($this->redirectTo);
    }

    public function googleHandler()
    {
        $this->third_party = 'google';
        $this->url = 'https://www.google.com/';

        if (auth()->check()) {
            $this->handlerLink();

            return redirect()->intended();
        }

        $user = $this->handler();

        if (is_null($user)) {
            return redirect()->route('register')->with('warning', __('text.auth_user_deleted'));
        }

        Auth::login($user);

        return redirect()->intended($this->redirectTo);
    }

    protected function handlerLink()
    {
        $response = Socialite::driver($this->third_party)->user();
        $oauth = Oauth::where('third_party', $this->third_party)->where('third_party_id', $response->id)->first();

        if (is_null($oauth)) {
            $oauth_data = [
                'link' => $this->getUrl($response),
                'token' => $response->token,
                'third_party' => $this->third_party,
                'third_party_id' => $response->id,
            ];

            auth()->user()->oauth()->create($oauth_data);
        } else {
            session()->flash('alert', 'La cuenta ya fue linqueada a otra cuenta');
        }
    }

    protected function handler()
    {
        $response = Socialite::driver($this->third_party)->user();
        $oauth = OAuth::where('third_party', $this->third_party)->where('third_party_id', $response->id)->first();

        if (is_null($oauth)) {
            $user = (!is_null($response->email)) ? $user = User::where('email', $response->email)->first() : null;

            $oauth_data = [
                'link' => $this->getUrl($response),
                'token' => $response->token,
                'third_party' => $this->third_party,
                'third_party_id' => $response->id,
            ];

            if (!is_null($user)) {
                $user->oauth()->create($oauth_data);

                if (!$user->verified) {
                    $user->update(['verified' => true]);
                }
            } else {
                $name = '';

                if (!is_null($response->name)) {
                    $name = $response->name;
                } elseif (!is_null($response->email)) {
                    $email = explode('@', $response->email);
                    $name = $email[0];
                }

                $data = [
                    'name' => $name,
                    'password' => '',
                    'email' => $response->email,
                    'verified' => true,
                ];

                $user = User::create($data);
                $user->oauth()->create($oauth_data);
                $user->profile()->create(['avatar' => $response->avatar_original]);
                $user->customer()->create([]);
            }
        } else {
            $user = $oauth->user;
        }

        return $user;
    }

    protected function getUrl($response)
    {
        switch ($this->third_party) {
        case 'facebook':
            $link = $response->profileUrl;
            break;
        case 'github':
            $link = $response->user['html_url'];
            break;
        case 'linkedin':
            $link = $response->user['publicProfileUrl'];
            break;
        default:
            $link = $this->url . $response->nickname;
        }

        return $link;
    }
}
