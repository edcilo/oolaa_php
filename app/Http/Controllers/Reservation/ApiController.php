<?php

namespace App\Http\Controllers\Reservation;

use App\Entities\Coupon;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReservationStore;
use App\Http\Resources\Reservation;
use App\Mail\AdminReservationNew;
use App\Mail\CustomerReservationNew;
use App\Mail\StylistReservationNew;
use App\Repositories\AddressRepository;
use App\Repositories\CouponRepository;
use App\Repositories\CreditCardRepository;
use App\Repositories\ItemRepository;
use App\Repositories\ReservationRepository;
use App\Repositories\ReservationTokenRepository;
use App\Repositories\ZoneRepository;
use App\Services\MailService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ApiController extends Controller
{
    public function store(
        ReservationStore $request,
        AddressRepository $addressRepo,
        CreditCardRepository $creditCardRepo,
        ItemRepository $itemRepo,
        ReservationRepository $reservationRepo,
        ReservationTokenRepository $tokenRepo,
        ZoneRepository $zoneRepo,
        CouponRepository $couponRepo,
        MailService $mail
    ) {
        $user           = auth()->user();
        $customer       = $user->customer;
        $data           = $request->all();
        $location       = $data['location'];
        $res            = $data['reservation'];
        $cred           = $data['credit_card'];
        $date           = Carbon::createFromFormat('Y-m-d H:i', "{$res['date']} {$res['time']}");
        $couponCode     = $data['coupon'];
        $coupon         = null;

        $zip_code       = $location['zip_code'];
        $zone           = $zoneRepo->findByZipCode($zip_code);

        if ($couponCode) {
            $coupon = $couponRepo->getCouponForReservation($customer, $couponCode);

            if (!$coupon) {
                return response()->json(
                    [
                        'data' => [
                            'error' => 401,
                            'message' => "El cupón es invalido",
                            'large_description' => "Se agotarón el número de redenciones del cupón o ya has usado el cupón.",
                            'data' => []
                        ]
                    ], 401
                );
            }
        }

        if (!$zone) {
            return response()->json(
                [
                'data' => [
                    'error' => 401,
                    'message' => 'Fuera de zona',
                    'large_description' => "¿No llegamos a tu zona? Escribe a contacto@oolaaa.com.mx para que muy pronto podamos hacerlo.",
                    'data' => []
                ]
                ], 401
            );
        }

        $address        = $addressRepo->findOrCreate($user, $location);
        $credit_card    = $creditCardRepo->findOrCreate($user, $cred, isset($data['gateway']) ? $data['gateway'] : []);

        if (is_array($credit_card) && isset($credit_card['error'])) {
            $error = $credit_card['data'];

            return response()->json(
                [
                'data' => [
                    'error' => $error->getErrorCode(),
                    'message' => __("text.{$error->getDescription()}"),
                    'data' => []
                ]
                ], 401
            );
        }

        $reservation    = $reservationRepo->create($customer, $date, $address, $zone, $credit_card, !$data['fromStylists'], $coupon);
        $itemRepo->save($reservation, $data['services']);

        $tokenRepo->newReservation($reservation);

        if (!$user->email) {
            $user->update(['email', $data['email']]);
        }

        if ($user->profile->phone !== $data['phone']) {
            $user->profile->update(['phone' => $data['phone']]);
        }

        $mail->newReservation($user, $reservation);

        return new Reservation($reservation);
    }
}
