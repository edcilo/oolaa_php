<?php

namespace App\Http\Controllers\Reservation;

use App\Entities\Customer;
use App\Entities\Item;
use App\Entities\Reservation;
use App\Entities\Stylist;
use App\Http\Controllers\Controller;
use App\Repositories\CalendarRepository;
use App\Repositories\ItemRepository;
use App\Repositories\ReservationRepository;
use App\Services\MailService;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    public function getSessionId(Request $request, Reservation $reservation, Stylist $stylist, $token, $action)
    {
        $new_stylist = $request->get('new_stylist');
        $item = $request->get('item');
        $route = null;

        if ($action === 'A') {
            $route = 'reservation.stylist.accept';
        } else if ($action === 'D') {
            $route = 'reservation.stylist.decline';
        } else if ($action === 'RO') {
            $route = 'reservation.stylist.reopen';
        } else if ($action === 'AUS') {
            $route = 'reservation.stylist.accept.auto';
        }

        if (!$route) {
            abort(404);
        }

        return view('site.get_session_id', compact('route', 'reservation', 'stylist', 'token', 'action', 'new_stylist', 'item'));
    }

    public function accept(
        Request $request,
        Reservation $reservation,
        Stylist $stylist,
        $token,
        ItemRepository $itemRepo,
        ReservationRepository $reservationRepo,
        CalendarRepository $calendarRepo
    ) {
        $sessionId = $request->get('device_session_id');

        if (!$sessionId) {
            abort(404);
        }

        $items = Item::where('reservation_id', $reservation->id)
            ->where('stylist_id', $stylist->id)
            ->get();

        foreach ($items as $item) {
            $calendarRepo->createOolaaEvent($stylist->id, $item, $reservation);
        }

        $itemRepo->accept($items);
        $reservationRepo->updateStatus($reservation);

        $reservationRepo->createCharge($reservation, $sessionId);

        auth()->login($stylist->user);

        return redirect()->route('stylist.reservations');
    }

    public function acceptAutoStylist(
        Request $request,
        Reservation $reservation,
        Stylist $stylist,
        $token,
        ItemRepository $itemRepo,
        ReservationRepository $reservationRepo,
        CalendarRepository $calendarRepo
    )
    {
        $sessionId = $request->get('device_session_id');
        $itemId = $request->get('item_id');

        if (!$sessionId OR !$itemId) {
            abort(404);
        }

        $item = Item::findOrFail($itemId);

        $itemRepo->acceptAutoStylist($item, $stylist);
        $calendarRepo->createOolaaEvent($stylist->id, $item, $reservation);
        $reservationRepo->updateStatus($reservation);

        $reservationRepo->createCharge($reservation, $sessionId);

        auth()->login($stylist->user);

        return redirect()->route('stylist.reservations');
    }

    public function decline(Reservation $reservation, Stylist $stylist, $token, ItemRepository $itemRepo)
    {
        $items = $reservation->items()->where('stylist_id', $stylist->id)->get();
        $itemRepo->decline($items);

        auth()->login($stylist->user);

        return redirect()->route('stylist.reservations');
    }

    public function reOpen(
        Request $request,
        Reservation $reservation,
        Stylist $stylist,
        $token,
        ItemRepository $itemRepo,
        ReservationRepository $reservationRepo,
        CalendarRepository $calendarRepo
    )
    {
        $sessionId = $request->get('device_session_id');
        $new_stylist = Stylist::findOrFail($request->get('new_stylist'));

        if (!$sessionId) {
            abort(404);
        }

        $items = Item::where('reservation_id', $reservation->id)
            ->where('stylist_id', $stylist->id)
            ->get();

        foreach ($items as $item) {
            $calendarRepo->updateOolaaEvent($stylist->id, $item, $reservation);
        }

        $itemRepo->acceptWithOtherStylist($items, $new_stylist);
        $reservationRepo->updateStatus($reservation);

        $reservationRepo->createCharge($reservation, $sessionId);

        auth()->login($new_stylist->user);

        return redirect()->route('stylist.reservations');
    }

    public function cancel(Customer $customer, Reservation $reservation, MailService $mail)
    {
        $reservation->update(['status' => 'C']);

        $mail->notifyReservationCanceled($reservation);

        return redirect()->back();
    }
}
