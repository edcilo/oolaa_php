<?php

namespace App\Http\Controllers;

use App\Entities\PhotoBook;
use App\Entities\Profile;
use App\Entities\Service;
use App\Entities\ServiceCategory;
use App\Http\Resources\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class TestController extends Controller
{
    public function emailTest()
    {
        $data = [];

        Mail::send('email.test', $data, function($message) {
            $message->from('edcilo@hotmail.com', 'Eduardo C.');
            $message->subject('email de prueba desde oolaa');
            $message->to('edcilop@gmail.com', 'edcilop');
        });

        return redirect()->route('welcome');
    }

    public function imgsServices()
    {
        $services = Service::all();
        $this->downloadImages($services, 'image', 'services');

        $categories = ServiceCategory::all();
        $this->downloadImages($categories, 'icon', 'service-categories');
        $this->downloadImages($categories, 'cover', 'service-categories');

        dd('...');
    }

    public function imgsPhotobook()
    {
        $photobook = PhotoBook::all();
        $this->downloadImages($photobook, 'image', 'photobook');

        dd($photobook);
    }

    public function imgsUsers()
    {
        $profiles = Profile::all();
        foreach ($profiles as $p) {
            $p->avatar = '/images/profile-avatar.png';
            $p->save();
        }
        //$this->downloadImages($profiles, 'avatar', 'users');

        dd('...');
    }

    public function downloadImages($collection, $field, $folder)
    {
        $pathStorage = storage_path() . '/app/public/images/' . $folder . '/';

        foreach ($collection as $i) {
            $url = $i->$field;
            $fileName = explode('/', $url);
            $index = count($fileName);

            $fileName = $fileName[$index - 1];

            if ($fileName !== "") {
                $ch = curl_init($url);
                $fp = fopen($pathStorage . $fileName, 'wb');

                curl_setopt($ch, CURLOPT_FILE, $fp);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_exec($ch);
                curl_close($ch);
                fclose($fp);

                $i->$field = '/storage/images/' . $folder . '/' . $fileName;
                $i->save();
            }
        }
    }
}
