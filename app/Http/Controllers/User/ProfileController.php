<?php

namespace App\Http\Controllers\User;

use App\Entities\User;
use App\Entities\Customer;
use App\Http\Requests\UserAddCreditCardRequest;
use App\Http\Requests\UserAddLocation;
use App\Http\Requests\UserDestroyCreditCardRequest;
use App\Http\Requests\UserPasswordUpdateRequest;
use App\Http\Requests\UserProfileUpdateRequest;
use App\Http\Resources\Customer as CustomerResource;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class ProfileController extends Controller
{
    public function home(string $slug, User $user): View
    {
        return view('user.old_home', compact('user'));
    }

    public function apiProfile(Request $request)
    {
        $user = auth()->user();

        $customer = Customer::with(
            ['user' => function ($q) {
                $q->with('profile', 'addresses', 'creditCards');
            }]
        )->where('user_id', $user->id)->first();

        return new CustomerResource($customer);
    }

    public function update(User $user, UserProfileUpdateRequest $request): RedirectResponse
    {
        $user->update($request->only(['name', 'email']));

        $profileData = $request->only('phone');
        if ($request->hasFile('avatar')) {
            $path = Storage::disk('local')->put('public/images/users', $request->avatar);
            $profileData['avatar'] = asset(str_replace('public/', 'storage/', $path));
        }

        $user->profile->update($profileData);

        session()->flash('success', __('text.profile_updated'));

        return redirect()->back();
    }

    public function updatePassword(User $user, UserPasswordUpdateRequest $request): RedirectResponse
    {
        $password = bcrypt($request->get('password'));
        $type = 'success';
        $message = __('text.password_updated_by_user');

        if ($user->password && Hash::check($request->get('current_password'), $user->password)) {
            $user->update(compact('password'));
        } elseif (!$user->password) {
            $user->update(compact('password'));
        } else {
            $type = 'danger';
            $message = __('text.current_password_failed');
        }

        session()->flash($type, $message);

        return redirect()->back();
    }

    public function paymentMethods(string $slug, User $user): View
    {
        return view('user.paymentMethods', compact('user'));
    }

    public function addPaymentMethods(string $slug, User $user, UserAddCreditCardRequest $request): RedirectResponse
    {
        $data = $request->only('card');

        if ($user->creditCards()->count() === 0) {
            $data['default'] = true;
        }

        // TODO use real data
        $data['provider_id'] = '0123456789aeiou';
        $data['brand'] = 'visa';
        $data['device_id'] = 'aeiou9876543210';
        $data['status'] = 'A';

        $user->creditCards()->create($data);

        session()->flash('success', __('text.credit_card_added'));

        return redirect()->back();
    }

    public function destroyPaymentMethods(string $slug, User $user, UserDestroyCreditCardRequest $request): RedirectResponse
    {
        $user->creditCards()->where('id', $request->get('card_id'))->delete();

        return redirect()->back();
    }

    /**
     * @param string                             $slug
     * @param \App\Entities\User                 $user
     * @param \App\Http\Requests\UserAddLocation $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addLocation(string $slug, User $user, UserAddLocation $request): RedirectResponse
    {
        $data = $request->all();
        $data['line'] = "{$data['street']} {$data['int_number']} {$data['ext_number']}, {$data['neighborhood']}, {$data['zip_code']} {$data['city']}";

        $address = $user->addresses()->create($data);

        return redirect()->back();
    }
}
