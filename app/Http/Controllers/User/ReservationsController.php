<?php

namespace App\Http\Controllers\User;

use App\Entities\Reservation;
use App\Repositories\ReservationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReservationsController extends Controller
{
    public function index()
    {
        $customer = auth()->user()->customer;
        $title = ucfirst(__('dictionary.all'));

        $reservations = Reservation::with('items.service', 'coupon')
            ->where('customer_id', $customer->id)
            ->orderBy('created_at', 'DESC')
            ->get();

        return view('customer.reservations', compact('title', 'reservations'));
    }

    public function accepted(ReservationRepository $reservationRepo)
    {
        $customer = auth()->user()->customer;
        $title = __('text.accepted_appointments');

        $reservations = $reservationRepo->getCustomerReservation($customer, 'A');

        return view('customer.reservations', compact('title', 'reservations'));
    }

    public function pending(ReservationRepository $reservationRepo)
    {
        $customer = auth()->user()->customer;
        $title = __('text.waiting_requests');

        $reservations = $reservationRepo->getCustomerReservation($customer, 'P');

        return view('customer.reservations', compact('title', 'reservations'));
    }

    public function canceled(ReservationRepository $reservationRepo)
    {
        $customer = auth()->user()->customer;
        $title = __('text.canceled_appointments');

        $reservations = $reservationRepo->getCustomerReservation($customer, 'C');

        return view('customer.reservations', compact('title', 'reservations'));
    }

    public function finished(ReservationRepository $reservationRepo)
    {
        $customer = auth()->user()->customer;
        $title = ucfirst(__('dictionary.record'));

        $reservations = $reservationRepo->getCustomerReservation($customer, 'F');

        return view('customer.reservations', compact('title', 'reservations'));
    }
}
