<?php

namespace App\Http\Controllers\User;

use App\Entities\Address;
use App\Entities\CreditCard;
use App\Entities\Customer;
use App\Entities\User;
use App\Http\Resources\AddressCollection;
use App\Http\Resources\Customer as CustomnerResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    public function customer($user_id)
    {
        $customer = Customer::with(
            ['user' => function ($q) {
                $q->with('profile', 'addresses', 'creditCards');
            }]
        )->where('user_id', $user_id)->first();

        return new CustomnerResource($customer);
    }

    public function locations()
    {
        // TODO get user authenticated
        $user = User::with('addresses')->find(2054);

        return new AddressCollection($user->addresses);
    }

    public function creditCards()
    {
        // TODO get user credit cards
        $user = User::find(52);
        $creditCards = $user->creditCards;

        return response()->json($creditCards, 200, ['Gateway' => config('payment.gateway')]);
    }

    public function destroyCreditCards(CreditCard $creditCard)
    {
        CreditCard::destroy($creditCard->id);

        return response()->json(
            [
            'message' => __('text.entity_deleted', ['entity' => __('dictionary.credit_card') . ' ' . $creditCard->card])
            ]
        );
    }
}
