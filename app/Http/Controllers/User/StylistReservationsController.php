<?php

namespace App\Http\Controllers\User;

use App\Entities\ReservationToken;
use App\Repositories\ReservationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StylistReservationsController extends Controller
{
    public function index()
    {
        $stylist = auth()->user()->stylist;
        $title = ucfirst(__('dictionary.all'));
        $tokens = ReservationToken::where('stylist_id', $stylist->id)->get();

        $reservations = $stylist->reservations()
            ->with('items.service', 'coupon')
            ->orderBy('created_at', 'DESC')
            ->get()
            ->unique();

        return view('stylist.reservations', compact('title', 'reservations', 'tokens'));
    }

    public function accepted(ReservationRepository $reservationRepo)
    {
        $stylist = auth()->user()->stylist;
        $title = __('text.accepted_appointments');
        $tokens = ReservationToken::where('stylist_id', $stylist->id)->get();

        $reservations = $reservationRepo->getStylistReservation($stylist, 'A');

        return view('stylist.reservations', compact('title', 'reservations', 'tokens'));
    }

    public function pending(ReservationRepository $reservationRepo)
    {
        $stylist = auth()->user()->stylist;
        $title = __('text.waiting_requests');
        $tokens = ReservationToken::where('stylist_id', $stylist->id)->get();

        $reservations = $reservationRepo->getStylistReservation($stylist, 'P');

        return view('stylist.reservations', compact('title', 'reservations', 'tokens'));
    }

    public function canceled(ReservationRepository $reservationRepo)
    {
        $stylist = auth()->user()->stylist;
        $title = __('text.canceled_appointments');
        $tokens = ReservationToken::where('stylist_id', $stylist->id)->get();

        $reservations = $reservationRepo->getStylistReservation($stylist, 'C');

        return view('stylist.reservations', compact('title', 'reservations', 'tokens'));
    }

    public function finished(ReservationRepository $reservationRepo)
    {
        $stylist = auth()->user()->stylist;
        $title = ucfirst(__('dictionary.record'));
        $tokens = ReservationToken::where('stylist_id', $stylist->id)->get();

        $reservations = $reservationRepo->getStylistReservation($stylist, 'F');

        return view('stylist.reservations', compact('title', 'reservations', 'tokens'));
    }
}
