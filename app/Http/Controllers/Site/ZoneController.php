<?php

namespace App\Http\Controllers\Site;

use App\Entities\Municipality;
use App\Entities\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ZoneController extends Controller
{
    public function states()
    {
        $states = State::select('id', 'name')
            ->orderBy('name', 'asc')
            ->get();

        return response()->json($states);
    }

    public function municipalities(State $state)
    {
        $municipalities = $state->municipalities()
            ->select('id', 'name')
            ->orderBy('name', 'asc')
            ->get();

        return response()->json($municipalities);
    }

    public function colonies(Municipality $municipality)
    {
        $colonies = $municipality->colonies()
            ->select('id', 'name', 'zip_code')
            ->orderBy('name', 'asc')
            ->get();

        return response()->json($colonies);
    }
}
