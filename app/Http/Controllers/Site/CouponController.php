<?php

namespace App\Http\Controllers\Site;

use App\Entities\Coupon;
use App\Entities\Service;
use App\Entities\ServiceCategory;
use App\Http\Requests\GiftCouponStoreRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CouponController extends Controller
{
    public function gift()
    {
        $categories = ServiceCategory::with(
            ['services' => function ($q) {
                $q->with(
                    ['children' => function ($q2) {
                        $q2->where('active', true)->orderBy('order', 'asc');
                    }]
                )->where('active', true)->whereNull('parent_id')->orderBy('order', 'asc');
            }]
        )->orderBy('order', 'asc')->get();

        return view('site.gift', compact('categories'));
    }

    public function store(GiftCouponStoreRequest $request)
    {
        // TODO use openpay
        // TODO show errors when services is invalid
        // TODO send email
        $amount = Service::whereIn('id', $request->get('services'))->sum('price');

        $coupon = Coupon::create(
            [
            'code' => Coupon::generateCode(),
            'start' => Carbon::now(),
            'end' => Carbon::now()->addYear(),
            'redemptions' => 1,
            'type' => 'F',
            'amount' => $amount,
            ]
        );

        return redirect()->route('thanks');
    }
}
