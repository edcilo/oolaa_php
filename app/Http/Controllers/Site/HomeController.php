<?php

namespace App\Http\Controllers\Site;

use App\Entities\Cms;
use App\Entities\ServiceCategory;
use App\Entities\Stylist;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function schema(): array
    {
        return Cms::first()->schema;
    }

    public function stylist(string $slug, Stylist $stylist)
    {
        $photoBooks = $stylist->photoBooks()->orderBy('position')->get();
        $curriculum_e = $stylist->curriculum()->where('data_type', 'E')->get();
        $curriculum_w = $stylist->curriculum()->where('data_type', 'X')->get();

        return view('site.stylist', compact('stylist', 'photoBooks', 'curriculum_e', 'curriculum_w'));
    }

    public function serviceCategories()
    {
        return ServiceCategory::select('id', 'name', 'icon')
            ->with(
                ['services' => function ($q0) {
                    $q0->select('id', 'name', 'price', 'category_id')
                        ->with(
                            ['children' => function ($q1) {
                                $q1->select('id', 'name', 'price', 'parent_id')
                                    ->where('active', true)
                                    ->orderBy('order', 'asc');
                            }]
                        )
                    ->where('active', true)
                    ->whereNull('parent_id')
                    ->orderBy('order', 'asc');
                }]
            )
            ->orderBy('order', 'asc')
            ->get();
    }
}
