<?php

namespace App\Http\Controllers\Site;

use App\Entities\Weekday;
use App\Entities\Zone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class RegisterStylistController extends Controller
{
    public function register(): View
    {
        $zones = Zone::all();
        $week = Weekday::all();

        return view('auth.stylistRegister', compact('zones', 'week'));
    }
}
