<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReservationStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = $this->request->all();

        // USER
        $rules = [
            'email' => 'required|email|max:255',
            'phone' => 'required|max:255',
        ];

        // RESERVATION
        $rules['reservation.time'] = 'required|date_format:H:i';
        $rules['reservation.date'] = 'required|date_format:Y-m-d';

        // CREDIT CARD
        if (isset($data['gateway'])) {
            $rules['gateway.id'] = 'required|max:255';
            $rules['gateway.card.card_number'] = 'required|max:255';
            $rules['gateway.card.brand'] = 'required|max:255';
            $rules['gateway.card.holder_name'] = 'required|max:255';
            $rules['gateway.card.expiration_year'] = 'required|size:2';
            $rules['gateway.card.expiration_month'] = 'required|size:2';
        } else {
            $rules['credit_card.id'] = 'required|exists:credit_cards,id';
        }

        // LOCATION
        if (isset($data['location']) && $data['location']['id'] !== null) {
            $rules['location.id'] = 'exists:addresses,id';
        } else {
            $rules['location.zip_code'] = 'required|size:5|exists:colonies,zip_code';
            $rules['location.line'] = 'required|max:255';
            $rules['location.int_number'] = 'max:255';
            $rules['location.comments'] = 'max:1024';
        }

        // SERVICES
        $rules['services.*.selected.*.quantity'] = 'required|integer';
        $rules['services.*.selected.*.service_id'] = 'required|exists:services,id';
        if ($data['fromStylists']) {
            $rules['services.*.selected.*.stylist_id'] = 'required|exists:stylists,id';
        }

        // COUPON
        if (isset($data['coupon'])) {
            $rules['coupon'] = "exists:coupons,code";
        }

        return $rules;
    }
}
