<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserAddLocation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'street' => 'required|max:255',
            'neighborhood' => 'required|max:255',
            'ext_number' => 'required|max:255',
            'int_number' => 'required|max:255',
            'zip_code' => 'required|max:5',
            'city' => 'required|max:255'
        ];
    }
}
