<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminCreditCardStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'token_id' => 'required|max:255',
            'device_session_id' => 'required|max:255',
            'name' => 'required|max:255',
            'card' => 'required|max:20',
            'date_month' => 'required|date_format:m',
            'date_year' => 'required|date_format:y',
            'cvv' => 'required|max:4',
            'default' => 'boolean'
        ];
    }
}
