<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminPhotoBookStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'stylist_id' => 'required|exists:stylists,id',
            'position' => 'required|integer|min:0',
            'image' => 'required|image',
            'is_selected' => 'boolean'
        ];
    }
}
