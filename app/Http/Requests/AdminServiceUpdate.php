<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminServiceUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'full_name' => 'required|max:255',
            'average_time' => 'required|integer',
            'price' => 'required|numeric',
            'price_is_mutable' => 'boolean',
            'active' => 'boolean',
            'note' => 'max:1024',
            'order' => 'required|integer',
            'image' => 'image',
            'icon' => 'image',
            'parent_id' => 'nullable|required_without:category_id|exists:services,id',
            'category_id' => 'nullable|required_without:parent_id|exists:service_categories,id'
        ];
    }
}
