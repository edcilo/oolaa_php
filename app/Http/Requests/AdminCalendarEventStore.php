<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminCalendarEventStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'allDay' => 'boolean',
            'start_date' => 'required|date_format:Y-m-d',
            'start_hour' => 'required|date_format:H:i:s',
            'end_date' => 'required|date_format:Y-m-d',
            'end_hour' => 'required|date_format:H:i:s',
            'backgroundColor' => 'required|regex:/^#[a-zA-Z0-9]{6}$/',
            'description' => 'max:1024',
        ];
    }
}
