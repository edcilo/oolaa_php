<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AdminCouponUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $coupon = $this->route('coupon');

        return [
            'code' => [
                'required',
                'max:255',
                Rule::unique('coupons')->ignore($coupon->id),
            ],
            'start' => 'required|date',
            'end' => 'required|date',
            'redemptions' => 'required|integer',
            'type' => 'required|in:F,P',
            'amount' => 'required|numeric|min:0'
        ];
    }
}
