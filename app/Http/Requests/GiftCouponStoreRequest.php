<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GiftCouponStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'services' => 'required|exists:services,id',
            'name_0' => 'required|max:255',
            'email_0' => 'required|email|max:255',
            'name_1' => 'required|max:255',
            'email_1' => 'required|email|max:255',
            'card_number' => 'required|max:16',
            'card_name' => 'required|max:255',
            'card_date' => 'required|date_format:m/y',
            'card_cvc' => 'required|min:3|max:4',
        ];
    }
}
