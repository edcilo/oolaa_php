<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminAddressStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'line' => 'required|max:520',
            'street' => 'max:255',
            'ext_number' => 'max:255',
            'int_number' => 'max:255',
            'neighborhood' => 'max:255',
            'zip_code' => 'required|size:5',
            'city' => 'max:255',
            'state' => 'max:255',
            'default' => 'boolean',
        ];
    }
}
