<?php

namespace App\Http\Resources;

use App\Entities\Colony;
use Illuminate\Http\Resources\Json\Resource;

class Stylist extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $url = route('stylist', [$this->user->slug, $this->id]);
        $type = ($this->types->count()) ? $this->types->first() : null;
        $photoBookCollection = new PhotoBookCollection($this->photoBooks);

        $categories = $this->services->groupBy('category_id');

        $arrayOfCategories = [];
        foreach ($categories as $services) {
            array_push(
                $arrayOfCategories, [
                'name' => $services->first()->category->name,
                'services' => new ServiceCollection($services)]
            );
        }

        /*
        $zonesIds = $this->zones()->pluck('id')->toArray();
        $colonies = Colony::whereHas('zone', function($q) use ($zonesIds) {
            $q->whereIn('id', $zonesIds);
        })->get();
        $colonies = new ColonyCollection($colonies);
        */

        return [
            'id' => $this->id,
            'name' => $this->user->name,
            'avatar' => $this->user->profile->avatar,
            'type' => optional($type)->name,
            'rating' => $this->rating,
            'url' => $url,
            'photo_books' => $photoBookCollection,
            'services' => $arrayOfCategories,
            // 'colonies' => $colonies
        ];
    }
}
