<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CreditCard extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'default' => $this->default,
            'status' => $this->status,
            'brand' => $this->brand,
            'card' => $this->card,
            'name' => $this->holder_name,
            'date' => "{$this->expiration_month}/{$this->expiration_year}",
            'cvc' => "..."
        ];
    }
}
