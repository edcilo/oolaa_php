<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Address extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'int_number' => $this->int_number,
            'zip_code' => $this->zip_code,
            'line' => $this->line,
            'comments' => $this->comments,
            'default' => $this->default
        ];
    }
}
