<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Reservation extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->confirmation_code,
            'status' => $this->status
        ];
    }
}
