<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Customer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->user->name,
            'email' => $this->user->email,
            'avatar' => $this->user->profile->avatar,
            'phone' => $this->user->profile->phone,
            'addresses' => new AddressCollection($this->user->addresses),
            'credit_cards' => new CreditCardCollection($this->user->creditCards),
        ];
    }
}
