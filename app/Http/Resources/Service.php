<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;

class Service extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $stylists = 1;

        if ($request->get('date') && $request->get('time')) {
            $date_time_raw = "{$request->get('date')} {$request->get('time')}";
            $date_time = Carbon::createFromFormat('Y-m-d H:i', $date_time_raw);
            $date_es = __("abbr.day.{$date_time->format('l')}");

            $stylists = $this->stylists()
                ->whereHas('weekdays', function($q) use ($date_es) {
                    $q->where('name', $date_es);
                })
                ->whereDoesntHave('calendar', function($q) use ($date_time) {
                    $q->where('start', '<>', $date_time)
                        ->where('end', '<>', $date_time->addHour());
                })
                ->count();
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'full_name' => $this->full_name,
            'average_time' => $this->average_time,
            'price' => '$ ' . number_format(($this->price), 2, '.', ','),
            'value' => number_format(($this->price), 2, '.', ','),
            'note' => $this->note,
            'enable' => $stylists > 0
        ];
    }
}
