<?php

namespace App\Http\Middleware;

use App\Entities\Reservation;
use App\Entities\ReservationToken as Token;

use Closure;

class ReservationToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $reservation_id = is_object($request->route('reservation')) ? $request->route('reservation')->id : $request->route('reservation');
        $stylist_id = is_object($request->route('stylist')) ? $request->route('stylist')->id : $request->route('stylist');

        $exists = Token::where('reservation_id', $reservation_id)
            ->where('stylist_id', $stylist_id)
            ->where('token', $request->route('token'))
            ->count();

        if ($exists) {
            return $next($request);
        }

        $reservation = Reservation::find($reservation_id);
        if ($reservation->auto_stylist) {
            return $next($request);
        }

        abort(404);
    }
}
