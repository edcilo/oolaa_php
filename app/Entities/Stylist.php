<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Simexis\Searchable\SearchableTrait;

class Stylist extends Model
{
    use SearchableTrait, SoftDeletes;

    protected $fillable = [
        'address',
        'status',
        'user_id'
    ];

    protected $statusArray = [
        'P' => 'pending',
        'A' => 'approved',
        'R' => 'rejected',
        'B' => 'blocked'
    ];

    protected $searchable = [
        'columns' => [
            'users.email' => 10,
            'users.name' => 8,
        ],
        'joins' => [
            'users' => ['stylists.user_id','users.id'],
        ],
    ];

    public function getNameAttribute()
    {
        return $this->user->name;
    }

    public function getEmailAttribute()
    {
        return $this->user->email;
    }

    public function getStatusReadableAttribute()
    {
        return __("dictionary.{$this->statusArray[$this->attributes['status']]}");
    }

    public function getAbbrStatusAttribute()
    {
        return $this->attributes['status'];
    }

    public function curriculum()
    {
        return $this->hasMany(Curriculum::class);
    }

    public function photoBooks()
    {
        return $this->hasMany(PhotoBook::class);
    }

    public function calendar()
    {
        return $this->hasMany(Calendar::class);
    }

    public function reservations()
    {
        return $this->belongsToMany(Reservation::class, 'items');
    }

    public function services()
    {
        return $this->belongsToMany(Service::class)->withPivot('active');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function types()
    {
        return $this->belongsToMany(Category::class);
    }

    public function weekdays()
    {
        return $this->belongsToMany(Weekday::class);
    }

    public function zones()
    {
        return $this->belongsToMany(Zone::class);
    }
}
