<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'status',
        'quantity',
        'unit_price',
        'extra_price',
        'discount',
        'notes',
        'declined_by',
        'reservation_id',
        'service_id',
        'stylist_id',
    ];

    protected $casts = [
        'declined_by' => 'array',
    ];

    protected $total = 0.8;

    protected $status = [
        'P' => 'pending',
        'A' => 'accepted',
        'C' => 'canceled',
        'D' => 'declined',
    ];

    public function getSubtotalAttribute($forStylist = false)
    {
        $sub = $this->attributes['quantity'] * $this->attributes['unit_price'];

        if ($forStylist) {
            $sub = $sub * $this->total;
        }

        return $sub;
    }

    public function getTotalAttribute($forStylist = false)
    {
        $sub = $this->getSubtotalAttribute($forStylist);

        return $sub - $this->attributes['discount'];
    }

    public function reservation()
    {
        return $this->belongsTo(Reservation::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function stylist()
    {
        return $this->belongsTo(Stylist::class);
    }

    public function calendar()
    {
        return $this->hasOne(Calendar::class);
    }
}
