<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Simexis\Searchable\SearchableTrait;

class Service extends Model
{
    use SoftDeletes, SearchableTrait;

    protected $fillable = [
        'name',
        'full_name',
        'average_time',
        'price',
        'price_is_mutable',
        'active',
        'note',
        'order',
        'image',
        'icon',
        'parent_id',
        'category_id',
    ];

    protected $searchable = [
        'columns' => [
            'services.name' => 9,
            'services.full_name' => 10,
            'services.note' => 6
        ]
    ];

    protected $dates = ['deleted_at'];

    public function getParentCategoryAttribute()
    {
        return $this->parent ? $this->parent->category : null;
    }

    public function children()
    {
        return $this->hasMany(Service::class, 'parent_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(ServiceCategory::class, 'category_id');
    }

    public function parent()
    {
        return $this->belongsTo(Service::class, 'parent_id', 'id');
    }

    public function stylists()
    {
        return $this->belongsToMany(Stylist::class)->withPivot('active');
    }

    public function types()
    {
        return $this->belongsToMany(Category::class);
    }
}
