<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class CreditCard extends Model
{
    protected $fillable = [
        'type',
        'brand',
        'bank_name',
        'card',
        'holder_name',
        'expiration_year',
        'expiration_month',
        'default',
        'status',
        'device_id',
        'provider_id',
        'user_id',
    ];

    protected $status = [
        'A' => 'active',
        'I' => 'inactive'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
