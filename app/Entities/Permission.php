<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Simexis\Searchable\SearchableTrait;

class Permission extends Model
{
    use SearchableTrait;

    protected $fillable = [
        'name', 'slug'
    ];

    protected $searchable = [
        'columns' => [
            'permissions.name' => 10,
            'permissions.slug' => 9,
        ],
    ];

    public function setNameAttribute(string $name)
    {
        $this->attributes['name'] = $name;
        $this->attributes['slug'] = str_slug(str_replace(' ', '', $name));
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class)->withPivot('value');
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
