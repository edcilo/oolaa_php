<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Simexis\Searchable\SearchableTrait;

class Zone extends Model
{
    use SearchableTrait;

    protected $fillable = ['name'];

    protected $searchable = [
        'columns' => [
            'zones.name' => 10
        ]
    ];

    public function colonies()
    {
        return $this->belongsToMany(Colony::class);
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }

    public function stylists()
    {
        return $this->belongsToMany(Stylist::class);
    }
}
