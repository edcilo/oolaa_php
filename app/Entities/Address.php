<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'street',
        'ext_number',
        'int_number',
        'state',
        'city',
        'neighborhood',
        'zip_code',
        'line',
        'comments',
        'longitude',
        'latitude',
        'default',
        'user_id'
    ];

    protected $casts = [
        'default' => 'boolean',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
