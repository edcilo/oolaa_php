<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Simexis\Searchable\SearchableTrait;

class State extends Model
{
    use SearchableTrait;

    protected $fillable = ['name'];

    protected $searchable = [
        'columns' => [
            'states.name' => 10,
        ],
    ];

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;
        $this->attributes['slug'] = str_slug($name);
    }

    public function area()
    {
        return $this->hasMany(Area::class);
    }

    public function municipalities()
    {
        return $this->hasMany(Municipality::class);
    }

    public function colonies()
    {
        return $this->hasManyThrough(Colony::class, Municipality::class);
    }
}
