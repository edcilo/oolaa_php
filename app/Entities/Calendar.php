<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    protected $fillable = [
        'title',
        'allDay',
        'start',
        'end',
        'description',
        'backgroundColor',
        'borderColor',
        'className',
        'stylist_id',
        'item_id',
    ];

    protected $dates = [
        'start', 'end'
    ];

    public function stylist()
    {
        return $this->belongsTo(Stylist::class);
    }

    public function item()
    {
        return $this->belongsTo(Item::class);
    }
}
