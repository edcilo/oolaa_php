<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\Integer;
use Simexis\Searchable\SearchableTrait;

class Coupon extends Model
{
    use SearchableTrait;

    protected $table = 'coupons';

    protected $fillable = [
        'code',
        'start',
        'end',
        'redemptions',
        'type',
        'amount'
    ];

    protected $searchable = [
        'columns' => [
            'coupons.code' => 10,
            'coupons.type' => 9,
        ],
    ];

    protected $types = [
        'F' => 'fixed',
        'P' => 'percentage'
    ];

    protected $dates = [
        'start', 'end'
    ];

    public static function generateCode(int $length = 6)
    {
        do {
            $code = Str::random($length);

            $reps = Coupon::where('code', $code)->count();
        } while ($reps !== 0);

        return $code;
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }
}
