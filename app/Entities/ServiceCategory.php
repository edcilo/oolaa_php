<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Simexis\Searchable\SearchableTrait;

class ServiceCategory extends Model
{
    use SearchableTrait;

    protected $table = 'service_categories';

    protected $fillable = [
        'name',
        'icon',
        'cover',
        'order',
        'shopping_order',
    ];

    protected $searchable = [
        'columns' => [
            'service_categories.name' => 10,
        ],
    ];

    public function services()
    {
        return $this->hasMany(Service::class, 'category_id');
    }
}
