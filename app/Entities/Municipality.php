<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Municipality extends Model
{
    protected $fillable = [
        'name',
        'geojson',
        'state_id'
    ];

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;
        $this->attributes['slug'] = str_slug($name);
    }

    public function area()
    {
        return $this->hasMany(Area::class);
    }

    public function colonies()
    {
        return $this->hasMany(Colony::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }
}
