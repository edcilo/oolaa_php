<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $fillable = [
        'name',
        'colony_id',
        'municipality_id',
        'state_id',
        'zone_id'
    ];

    public function colony()
    {
        return $this->belongsTo(Colony::class);
    }

    public function municipality()
    {
        return $this->belongsTo(Municipality::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function zone()
    {
        return $this->belongsTo(Zone::class);
    }
}
