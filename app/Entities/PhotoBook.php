<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class PhotoBook extends Model
{
    protected $table = "photo_books";

    protected $fillable = [
        'image',
        'name',
        'position',
        'is_selected',
        'stylist_id'
    ];

    public function stylist()
    {
        return $this->belongsTo(Stylist::class);
    }
}
