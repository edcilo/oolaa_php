<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ReservationToken extends Model
{
    protected $table = 'reservation_tokens';

    protected $fillable = [
        'token',
        'reservation_id',
        'stylist_id'
    ];

    public function reservation()
    {
        return $this->belongsTo(Reservation::class);
    }

    public function stylist()
    {
        return $this->belongsTo(Stylist::class);
    }
}
