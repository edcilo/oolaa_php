<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Weekday extends Model
{
    protected $fillable = ['name', 'number'];

    public function stylists()
    {
        return $this->belongsToMany(Stylist::class);
    }
}
