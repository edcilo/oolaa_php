<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class OAuth extends Model
{
    protected $table = 'oauth';

    protected $fillable = [
        'link',
        'token',
        'third_party',
        'third_party_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
