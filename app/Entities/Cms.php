<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Cms extends Model
{
    protected $table = 'cms';

    protected $fillable = ['schema'];

    protected $casts = [
        'schema' => 'array',
    ];
}

/*
    Schema:

    {
        home: {
            carousel: [{
                order: integer
                picture: string
            }, {...}, {...}],
            top_stylists: [{
                order: integer,
                picture: string
            }, {...}, {...}],
            steps: [{
                title: string,
                icon: string
                body: string
            }, {...}, {...}],
            video: '',
        },
        terms: {
            text: string
        },
        policy: {
            text: string
        },
        faqs: {
            text: string
        },
        about_us: {
            text: string
        },
        contact: {
            phone: string,
            email: string,
            facebook: string,
            instagram: string
        }
    }
*/
