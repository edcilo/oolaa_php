<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{
    protected $fillable = [
        'name',
        'slug'
    ];

    public function setNameAttribute(string $name)
    {
        $this->attributes['name'] = $name;
        $this->attributes['slug'] = str_slug($name);
    }
}
