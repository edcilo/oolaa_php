<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Simexis\Searchable\SearchableTrait;

class Reservation extends Model
{
    use SoftDeletes, SearchableTrait;

    protected $fillable = [
        'status',
        'start_at',
        'subtotal',
        'iva_percent',
        'iva_total',
        'total',
        'address',
        'credit_card',
        'confirmation_code',
        'payment_reference',
        'auto_stylist',
        'rating',
        'comment',
        'email_send',
        'note',
        'paid_out',
        'customer_id',
        'zone_id',
        'coupon_id',
    ];

    protected $searchable = [
        'columns' => [
            'reservations.confirmation_code' => 10,
            'reservations.payment_reference' => 10,
        ]
    ];

    protected $casts = [
        'address' => 'array',
        'credit_card' => 'array',
        'email_send' => 'array',
    ];

    public static $email_send_default = [
        'n0' => false,
        'n1' => false,
        'n2' => false,
        'n3' => false,
        'n4' => false
    ];

    protected $dates = ['start_at', 'deleted_at'];

    protected $status = [
        'P' => 'pending',
        'A' => 'accepted',
        'C' => 'canceled',
        'D' => 'declined',
        'F' => 'finished'
    ];

    public function getStatusAttribute()
    {
        return __("abbr.reservation.{$this->attributes['status']}");
    }

    public function getCountDeclinedAttribute()
    {
        $counter = 0;

        foreach ($this->items as $item) {
            if ($item->declined_by) {
                $counter += 1;
            }
        }

        return $counter;
    }

    public function setCustomerIdAttribute($customer_id)
    {
        $this->attributes['customer_id'] = $customer_id;
        $this->attributes['confirmation_code'] = strtoupper(str_random(8));
    }

    public function getConfirmationCodeAttribute()
    {
        return "{$this->attributes['id']}-{$this->attributes['confirmation_code']}";
    }

    public function getSumOfServicesAttribute($stylist = null)
    {
        $total = 0;
        $forStylist = $stylist !== null;

        foreach ($this->items as $item) {
            if (gettype($stylist) !== 'object') {
                $total += $item->subtotal;
            } else {
                if ($item->stylist_id === $stylist->id && $item->status !== 'D' && $item->status !== 'C') {
                    $total += $item->getSubtotalAttribute(true);
                } else {
                    continue;
                }
            }
        }

        return $total;
    }

    public function getDiscountAttribute($stylist = null)
    {
        $discount = 0;

        foreach ($this->items as $item) {
            if (gettype($stylist) !== 'object') {
                $discount += $item->discount;
            } else {
                if ($item->stylist_id === $stylist->id && $item->status !== 'D' && $item->status !== 'C') {
                    // $discount += $item->discount;
                    continue;
                } else {
                    continue;
                }
            }
        }

        return $discount;
    }

    public function getCouponDiscountAttribute()
    {
        $discount = 0;

        $subtotal = $this->getSumOfServicesAttribute() - $this->getDiscountAttribute();

        if ($this->coupon) {
            $discount = $this->coupon->type === 'F'
                ? $this->coupon->amount
                : ($subtotal / 100) * $this->coupon->amount;
        }

        return $discount;
    }

    public function getSubtotalAttribute($stylist = null)
    {
        $total = $this->getSumOfServicesAttribute($stylist) - $this->getDiscountAttribute($stylist);

        if ($stylist !== null) {
            return $total;
        }

        return $total - $this->getCouponDiscountAttribute();
    }

    public function getIvaAttribute()
    {
        $iva = $this->iva_percent ? $this->iva_percent : 0;

        return ($this->getSubtotalAttribute() / 100) * $iva;
    }

    public function getTotalAttribute($stylist = null)
    {
        return $this->getSubtotalAttribute($stylist) + $this->getIvaAttribute();
    }

    public function getHasCouponAttribute()
    {
        return $this->coupon()->count() > 0;
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function stylists()
    {
        return $this->belongsToMany(Stylist::class, 'items');
    }

    public function token()
    {
        return $this->hasOne(ReservationToken::class);
    }

    public function zone()
    {
        return $this->belongsTo(Zone::class);
    }
}
