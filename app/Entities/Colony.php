<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Simexis\Searchable\SearchableTrait;

class Colony extends Model
{
    use SearchableTrait;

    protected $fillable = [
        'name',
        'zip_code',
        'municipality_id'
    ];

    protected $searchable = [
        'columns' => [
            'colonies.name' => 10,
        ]
    ];

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;
        $this->attributes['slug'] = str_slug($name);
    }

    public function area()
    {
        return $this->hasMany(Area::class);
    }

    public function municipality()
    {
        return $this->belongsTo(Municipality::class);
    }

    public function zone()
    {
        return $this->belongsToMany(Zone::class);
    }
}
