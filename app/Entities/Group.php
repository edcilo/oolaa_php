<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Simexis\Searchable\SearchableTrait;

class Group extends Model
{
    use SearchableTrait;

    public $fillable = [
        'name',
        'slug',
    ];

    protected $searchable = [
        'columns' => [
            'groups.name' => 10,
            'groups.slug' => 9,
        ],
    ];

    public function setNameAttribute(string $name)
    {
        $this->attributes['name'] = $name;
        $this->attributes['slug'] = str_slug(str_replace(' ', '', $name));
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class)
            ->orderBy('name', 'asc')
            ->withPivot('value');
    }

    public function users()
    {
        $this->belongsToMany(User::class);
    }
}
