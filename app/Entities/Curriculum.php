<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Curriculum extends Model
{
    protected $fillable = [
        'data',
        'data_type',
        'stylist_id'
    ];

    protected $dataTypes = [
        'X' => 'experience',
        'E' => 'education'
    ];

    public function getTypeAttribute()
    {
        return __("abbr.{$this->data_type}");
    }

    public function stylist()
    {
        return $this->belongsTo(Stylist::class);
    }
}
