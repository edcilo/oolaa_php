<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Simexis\Searchable\SearchableTrait;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes, SearchableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'verified', 'slug'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'last_login', 'deleted_at'
    ];

    protected $searchable = [
        'columns' => [
            'users.name' => 10,
            'users.email' => 10,
            'profiles.phone' => 10
        ],
        'joins' => [
            'profiles' => ['users.id','profiles.user_id'],
        ],
    ];

    public function getIsStylistAttribute()
    {
        return $this->stylist !== null;
    }

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;
        $this->attributes['slug'] = str_slug($name);
    }

    public function hasVerifiedEmail()
    {
        return $this->verified;
    }

    public function isRoot()
    {
        return $this->groups()->where('name', 'root')->count() > 0;
    }

    public static function byEmail($email)
    {
        return static::where('email', $email);
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class);
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class)
            ->orderBy('name')
            ->withPivot('value');
    }

    public function verificationToken()
    {
        return $this->hasOne(VerificationToken::class);
    }

    public function oauth()
    {
        return $this->hasMany(OAuth::class);
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function creditCards()
    {
        return $this->hasMany(CreditCard::class);
    }

    public function customer()
    {
        return $this->hasOne(Customer::class);
    }

    public function stylist()
    {
        return $this->hasOne(Stylist::class);
    }
}
