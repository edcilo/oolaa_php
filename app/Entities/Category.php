<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Simexis\Searchable\SearchableTrait;

class Category extends Model
{
    use SoftDeletes, SearchableTrait;

    protected $fillable = [
        'name',
        'order'
    ];

    protected $searchable = [
        'columns' => [
            'categories.name' => 10,
        ],
    ];

    protected $dates = ['deleted_at'];

    public function getServicePrice($service_id)
    {
        $service = $this->services()->where('id', $service_id)->first();

        if ($service) {
            return $service->pivot->price;
        }

        return '';
    }

    public function stylists()
    {
        return $this->belongsToMany(Stylist::class);
    }

    public function services()
    {
        return $this->belongsToMany(Service::class)->withPivot('price');
    }
}
