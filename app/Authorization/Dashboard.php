<?php

namespace App\Authorization;

use App\Entities\User;

class Dashboard
{
    public function access(User $user)
    {
        return $user->isRoot();
    }
}
