<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeDirectiveProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive(
            'date', function ($expression) {
                return "<?php
                echo ($expression)
                    ? __('months.'.($expression)->format('F')) . ' ' . ($expression)->format('d, Y')
                    : '';
            ?>";
            }
        );

        Blade::directive(
            'datetime', function ($expression) {
                return "<?php 
                echo ($expression) 
                    ? __('months.'.($expression)->format('F')) . ' ' . ($expression)->format('d, Y, h:i:s a')
                    : '' 
                ?>";
            }
        );

        Blade::directive(
            'monthAndYear', function ($expression) {
                return "<?php
                echo ($expression)
                    ? __('months.abbr.'.($expression)->format('F')) . ' ' . ($expression)->format('Y')
                    : '';
                ?>";
            }
        );

        Blade::directive(
            'minutesToHours', function ($minutes) {
                return "<?php echo (floor(($minutes)/60) < 10 ? '0' . floor(($minutes)/60) : floor(($minutes)/60)) 
                . ':' . (($minutes) % 60 < 10 ? '0' . (($minutes) % 60) : (($minutes) % 60)) 
                . ' ' . __('abbr.hrs') ?>";
            }
        );

        Blade::directive(
            'currency', function ($value) {
                return "<?php echo '$ ' . number_format(($value), 2, '.', ',') ?>";
            }
        );

        Blade::directive(
            'iconHide', function ($hide) {
                return "<?php echo $hide ? '<i class=\'fa fa-eye-slash\'></i>' : '<i class=\'fa fa-eye\'></i>' ?>";
            }
        );

        Blade::directive(
            'check', function ($check) {
                return "<?php echo ($check) ? '<i class=\"fa fa-check\"></i>' : '<i class=\"fa fa-times\"></i>' ?>";
            }
        );

        Blade::directive(
            'iconSex', function ($sex) {
                return "<?php echo (($sex) == 'male') ? '<i class=\"fa fa-mars\"></i>' : '<i class=\"fa fa-venus\"></i>' ?>";
            }
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
