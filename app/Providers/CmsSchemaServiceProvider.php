<?php

namespace App\Providers;

use App\Http\Controllers\Site\HomeController;
use Illuminate\Support\ServiceProvider;

class CmsSchemaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(
            ['site.*', 'auth/*', 'stylist/*', 'user/*', 'customer/*'], function ($view) {
                if (!$view->offsetExists('schema')) {
                    $homeControlle = new HomeController();
                    $schema = $homeControlle->schema();

                    $view->with('schema', $schema);
                }
            }
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
