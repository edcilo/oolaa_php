<?php

namespace App\Providers;

use App\Entities\User;
use App\Events\UserRegistered;
use Illuminate\Support\ServiceProvider;

class EloquentEventServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        User::created(
            function ($user) {
                $user->verificationToken()->create(
                    [
                    'token' => bin2hex(random_bytes(64))
                    ]
                );

                event(new UserRegistered($user));
            }
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
