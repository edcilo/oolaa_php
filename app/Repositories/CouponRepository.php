<?php

namespace App\Repositories;


use App\Entities\Coupon;
use Carbon\Carbon;

class CouponRepository extends Repository
{
    public function getModel()
    {
        return new Coupon();
    }

    public function getCouponForReservation($customer, $code)
    {
        $coupon = $this->model
            ->with('reservations')
            ->where('code', $code)
            ->where('start', '<=', Carbon::now())
            ->where('end', '>=', Carbon::now())
            ->first();

        if ($coupon) {
            $valid = $coupon->reservations->count() < $coupon->redemptions;
            $used = $coupon->reservations()->where('customer_id', $customer->id)->count();

            if ($valid && !$used) {
                return $coupon;
            }
        }

        return null;
    }
}
