<?php

namespace App\Repositories;

use App\Entities\Address;
use App\Entities\User;

class AddressRepository extends Repository
{
    public function getModel()
    {
        return new Address();
    }

    public function findOrCreate(User $user, array $data): Address
    {
        $address = ($data['id'] !== null)
            ? $this->model->find($data['id'])
            : $this->model->create(
                [
                'int_number' => $data['int_number'],
                'zip_code' => $data['zip_code'],
                'line' => $data['line'],
                'comments' => $data['comments'],
                'user_id' => $user->id
                ]
            );

        $this->setDefault($address);

        return $address;
    }

    public function setDefault(Address $address)
    {
        if (!$address->default) {
            $user = $address->user;

            $user->addresses()->update(['default' => false]);

            $address->update(['default' => true]);
        }
    }
}
