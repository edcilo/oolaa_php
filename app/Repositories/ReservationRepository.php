<?php

namespace App\Repositories;

use App\Entities\Customer;
use App\Entities\Reservation;
use App\Entities\Stylist;
use App\Services\MailService;
use App\Services\OpenPayService;

class ReservationRepository extends Repository
{
    public function getModel()
    {
        return new Reservation();
    }

    public function getCustomerReservation(Customer $customer, string $status)
    {
        return $this->model->with('items.service', 'coupon')
            ->where('customer_id', $customer->id)
            ->where('status', $status)
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    public function getStylistReservation(Stylist $stylist, string $status)
    {
        return $this->model->with('items.service', 'coupon')
            ->where('status', $status)
            ->whereHas('items', function($q) use ($stylist) {
                $q->where('stylist_id', $stylist->id);
            })
            ->orderBy('created_at', 'DESC')
            ->get()
            ->unique();
    }

    public function updateStatus(Reservation $reservation)
    {
        $items = $reservation->items;

        if ($items->count() === 0) {
            $reservation->update(['status' => 'P']);
            return $reservation;
        }

        foreach ($items as $item) {
            if ($item->status === 'P') {
                return $reservation;
            }
        }

        $reservation->update(['status' => 'A']);

        return $reservation;
    }

    public function create($customer, $date, $address, $zone, $creditCard, bool $auto, $coupon): Reservation
    {
        return $this->model->create(
            [
            'status' => 'P',
            'start_at' => $date,
            // 'subtotal',
            // 'iva_percent',
            // 'iva_total',
            // 'total',
            'address' => $address,
            'credit_card' => $creditCard,
            'auto_stylist' => $auto,
            'email_send' => Reservation::$email_send_default,
            // 'comment',
            // 'note',
            'customer_id' => $customer->id,
            'zone_id' => $zone->id,
            'coupon_id' => ($coupon) ? $coupon->id : null,
            ]
        );
    }

    public function createCharge(Reservation $reservation, string $sessionId)
    {
        $openPayService = new OpenPayService();
        $mail = new MailService();

        if ($reservation && $reservation->status === __("abbr.reservation.A") && !$reservation->paid_out) {
            $charge = $openPayService->customerCharge($reservation->customer->openpay, $reservation, $sessionId);

            if ($charge['success']) {
                $reservation->update(
                    [
                        'paid_out' => true,
                        'payment_reference' => $charge['data']->id
                    ]
                );

                $mail->successInReservationFee($reservation);
            } else {
                $error = [
                    'code' => $charge['data']->getErrorCode(),
                    'description' => $charge['data']->getDescription()
                ];

                $mail->errorInReservationFee($charge['data']->getErrorCode(), $reservation, $error);

                $reservation->update(['status' => 'C']);
            }
        }
    }
}
