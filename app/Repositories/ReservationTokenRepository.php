<?php

namespace App\Repositories;

use App\Entities\Reservation;
use App\Entities\ReservationToken;
use Carbon\Carbon;

class ReservationTokenRepository extends Repository
{
    public function getModel()
    {
        return new ReservationToken();
    }

    public function newReservation(Reservation $reservation)
    {
        $stylists = $reservation->stylists->unique();

        foreach ($stylists as $stylist) {
            $this->model->create(
                [
                'token' => md5(Carbon::now()) . '-' . $reservation->id . $stylist->id,
                'reservation_id' => $reservation->id,
                'stylist_id' => $stylist->id,
                ]
            );
        }
    }
}
