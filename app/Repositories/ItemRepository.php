<?php

namespace App\Repositories;

use App\Entities\Item;
use App\Entities\Reservation;
use App\Entities\Service;
use App\Entities\Stylist;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class ItemRepository extends Repository
{
    public function getModel()
    {
        return new Item();
    }

    public function accept(Collection $items)
    {
        $this->changeStatus($items, 'A');
    }

    public function acceptAutoStylist(Item $item, Stylist $stylist)
    {
        if ($item->status === 'P') {
            $item->update([
                'status' => 'A',
                'stylist_id' => $stylist->id
            ]);
        }
    }

    public function acceptWithOtherStylist(Collection $items, Stylist $stylist)
    {
        foreach ($items as $item) {
            if ($item->status === 'D') {
                $item->update([
                    'stylist_id' => $stylist->id,
                    'status' => 'A'
                ]);
            }
        }
    }

    public function byStatus($status, $from = null)
    {
        $query = $this->model->where('status', $status);

        if ($from) {
            $query->where('updated_at', '>=', $from);
        }

        return $query->get();
    }

    public function changeStatus(Collection $items, string $status)
    {
        foreach ($items as $item) {
            if ($item->status === 'P') {
                $item->update(['status' => $status]);
            }
        }
    }

    public function decline(Collection $items)
    {
        foreach ($items as $item) {
            $stylist = $item->stylist()->with('user')->first();
            $item->update(['declined_by' => $stylist]);
        }

        return $this->changeStatus($items, 'D');
    }

    public function declined($from = null)
    {
        $query = $this->model->whereHas('reservation', function($q) {
            $q->where('auto_stylist', false);
        })->where('status', 'D');

        if ($from) {
            $query->where('updated_at', '>=', $from);
        }

        return $query->get();
    }

    public function save(Reservation $reservation, array $items)
    {
        $services = [];

        foreach ($items as $stylist) {
            foreach ($stylist['selected'] as $service) {
                $serviceEntity = Service::find($service['service_id']);

                $data = [
                    'quantity' => $service['quantity'],
                    'unit_price' => $serviceEntity->price,
                    // 'extra_price',
                    // 'discount',
                    // 'notes',
                    'reservation_id' => $reservation->id,
                    'service_id' => $serviceEntity->id,
                    'stylist_id' => $service['stylist_id'] === 0 ? null : $service['stylist_id'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];

                array_push($services, $data);
            }
        }

        return $this->model->insert($services);
    }
}
