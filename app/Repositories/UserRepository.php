<?php

namespace App\Repositories;

use App\Entities\User;

class UserRepository extends Repository
{
    public function getModel()
    {
        return new User;
    }

    public function getAddresses(User $user, int $perPage = 10)
    {
        return $user->addresses()
            ->orderBy('default', 'desc')
            ->orderBy('updated_at', 'desc')
            ->paginate($perPage);
    }

    public function getCreditCards(User $user, int $perPage = 10)
    {
        return $user->creditCards()
            ->orderBy('default', 'desc')
            ->orderBy('updated_at', 'desc')
            ->paginate($perPage);
    }

    public function getPhotoBook(User $user)
    {
        if ($user->stylist) {
            return $user->stylist->photoBooks()
                ->orderBy('position', 'asc')
                ->get();
        }

        return null;
    }
}
