<?php

namespace App\Repositories;

use App\Entities\Calendar;
use App\Entities\Item;
use App\Entities\Reservation;

class CalendarRepository extends Repository
{
    public function getModel()
    {
        return new Calendar();
    }

    public function createOolaaEvent($stylist_id, Item $item, Reservation $reservation)
    {
        $service = $item->service;
        $data = [
            'backgroundColor' => "#29b882",
            'borderColor' => "#ffffff",
            'className' => "oolaa-event",
            'item_id' => $item->id,
            'title' => "Reservación: {$reservation->confirmation_code}",
            'description' => "Cliente: {$reservation->customer->name} \nDirección: {$reservation->address['line']} \nService: {$item->service->name} \nTiempo: {$item->service->average_time} min.",
            'start' => $reservation->start_at,
            'end' => $reservation->start_at->addMinutes($service->average_time),
            'stylist_id' => $stylist_id,
        ];

        if ($stylist_id) {
            return $this->model->create($data);
        }

        return null;
    }

    public function updateOolaaEvent($stylist_id, Item $item, Reservation $reservation)
    {
        if ($item->calendar) {
            $service = $item->service;
            $data = [
                'item_id' => $item->id,
                'title' => "Reservación: {$reservation->confirmation_code}",
                'description' => "Cliente: {$reservation->customer->name} \nDirección: {$reservation->address['line']} \nService: {$item->service->name} \nTiempo: {$item->service->average_time} min.",
                'start' => $reservation->start_at,
                'end' => $reservation->start_at->addMinutes($service->average_time),
                'stylist_id' => $stylist_id,
            ];

            return $item->calendar()->update($data);
        }

        $this->createOolaaEvent($stylist_id, $item, $reservation);
    }
}