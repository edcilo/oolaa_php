<?php

namespace App\Repositories;

use App\Entities\CreditCard;
use App\Entities\User;
use App\Services\OpenPayService;

class CreditCardRepository extends Repository
{
    public function getModel()
    {
        return new CreditCard();
    }

    public function create(User $user, array $gateway)
    {
        $op = new OpenPayService();
        $card = $op->storeCard($user->customer, $gateway['id'], $gateway['session_id']);

        if (is_array($card) && isset($card['error'])) {
            return $card;
        }

        $data = [
            'type' => $card->type,
            'brand' => $card->brand,
            'bank_name' => $card->bank_name,
            'card' => $card->serializableData['card_number'],
            'holder_name' => $card->serializableData['holder_name'],
            'expiration_year' => $card->serializableData['expiration_year'],
            'expiration_month' => $card->serializableData['expiration_month'],
            'status' => 'A',
            'device_id' => $gateway['session_id'],
            'provider_id' => $card->id,
            'user_id' => $user->id,
        ];

        return $this->model->create($data);
    }

    public function findOrCreate(User $user, array $data, array $gateway)
    {
        $creditCard = (isset($data['id']))
            ? $this->model->find($data['id'])
            : $this->create($user, $gateway);

        if (!is_array($creditCard) && !isset($creditCard['error'])) {
            $this->setDefault($creditCard);
        }

        return $creditCard;
    }

    public function setDefault(CreditCard $creditCard)
    {
        if (!$creditCard->default) {
            $user = $creditCard->user;

            $user->creditCards()->where('default', true)->update(['default' => false]);

            $creditCard->update(['default' => true]);
        }
    }
}
