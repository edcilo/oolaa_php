<?php

namespace App\Repositories;

use App\Entities\Zone;

class ZoneRepository extends Repository
{
    public function getModel()
    {
        return new Zone();
    }

    public function findByZipCode(string $zipCode)
    {
        $zone = $this->model->whereHas(
            'colonies', function ($query) use ($zipCode) {
                $query->where('zip_code', $zipCode);
            }
        )->first();

        return $zone;
    }
}
