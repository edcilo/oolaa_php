<?php

namespace App\Repositories;

abstract class Repository
{
    protected $model;

    public function __construct()
    {
        $this->model = $this->getModel();
    }

    abstract public function getModel();

    public function newQuery()
    {
        return $this->model->newQuery();
    }

    public function newEntity(array $data = array())
    {
        return $this->model->newInstance($data);
    }
}
