<?php

namespace App\Repositories;

use App\Entities\Stylist;

class StylistRepository extends Repository
{
    public function getModel()
    {
        return new Stylist();
    }

    public function byRating(float $value, string $condition = '=', string $status = 'A') {
        return $this->model
            ->where('status', $status)
            ->where('rating', $condition, $value)
            ->get();
    }

    public function byRatingInRange(float $from, float $to, string $status = 'A') {
        return $this->model
            ->where('status', $status)
            ->where('rating', '>', $from)
            ->where('rating', '<=', $to)
            ->get();
    }
}