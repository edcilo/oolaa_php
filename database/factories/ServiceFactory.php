<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Service::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'full_name' => $faker->firstName . ' ' . $faker->lastName,
        'average_time' => $faker->numberBetween(30, 360),
        'price' => random_int(50, 1500),
        'price_is_mutable' => $faker->randomElement([true, false, true]),
        'active' => $faker->randomElement([true, true, true, false]),
        'note' => $faker->paragraph(),
        'order' => random_int(0, 9),
        'image' => $faker->imageUrl(),
        'icon' => $faker->imageUrl(100, 100)
    ];
});
