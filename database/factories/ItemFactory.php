<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Item::class, function (Faker $faker) {
    return [
        'status' => $faker->randomElement(['P', 'A', 'C', 'D']),
        'quantity' => $faker->numberBetween(1, 6),
        'unit_price' => $faker->numberBetween(100, 500),
        'extra_price' => $faker->randomElement([null, $faker->numberBetween(10, 200)]),
        'discount' => $faker->randomElement([null, $faker->numberBetween(10, 200), null]),
        'notes' => $faker->randomElement([null, null, null, $faker->paragraph()]),
    ];
});
