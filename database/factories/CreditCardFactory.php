<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\CreditCard::class, function (Faker $faker) {
    return [
        'type' => 'debit',
        'brand' => $faker->randomElement(['visa', 'mastercard', 'american_express']),
        'bank_name' => 'bancomer',
        'card' => $faker->creditCardNumber,
        'holder_name' => $faker->name,
        'expiration_year' => $faker->randomElement(['21', '22', '23', '24', '25', '26', '27', '28']),
        'expiration_month' => $faker->randomElement(['01', '02', '03', '04', '05', '06', '07', '08']),
        'default' => $faker->randomElement([false, false, true]),
        'status' => $faker->randomElement(['A', 'I']),
        'provider_id' => md5($faker->word),
        'device_id' => md5($faker->word),
    ];
});
