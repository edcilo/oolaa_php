<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Customer::class, function (Faker $faker) {
    return [
        'openpay' => null
    ];
});
