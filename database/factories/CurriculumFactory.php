<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Curriculum::class, function (Faker $faker) {
    return [
        'data' => $faker->paragraph(),
        'data_type' => $faker->randomElement(['X', 'E']),
    ];
});
