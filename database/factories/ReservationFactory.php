<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Reservation::class, function (Faker $faker) {
    $subtotal = $faker->numberBetween(500, 5000);

    return [
        'status' => $faker->randomElement(['P', 'A', 'C', 'D', 'F']),
        'start_at' => $faker->dateTimeBetween('-6 years', 'now'),
        'subtotal' => $subtotal,
        'iva_percent' => '16',
        'iva_total' => $subtotal * 0.16,
        'total' => $subtotal * 1.16,
        'address' => [
            'address' => $faker->address,
            'latitude' => $faker->latitude,
            'longitude' => $faker->longitude
        ],
        'credit_card' => [
            'brand' => $faker->randomElement(['visa', 'mastercard', 'american_express']),
            'card' => $faker->creditCardNumber,
            'default' => $faker->randomElement([false, false, true]),
            'status' => $faker->randomElement(['A', 'I']),
            'provider_id' => md5($faker->word),
            'device_id' => md5($faker->word),
        ],
        'confirmation_code' => md5(random_int(0, 9)),
        'payment_reference' => md5(random_int(0, 9)),
        'auto_stylist' => $faker->randomElement([true, false, true]),
        'rating' => $faker->numberBetween(1, 5),
        'comment' => $faker->paragraph(),
        'note' => $faker->paragraph(),
        'email_send' => $faker->randomElement([true, false, true]),
    ];
});
