<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Stylist::class, function (Faker $faker) {
    return [
        'address' => $faker->address,
        'status' => $faker->randomElement(['P', 'R', 'A', 'B'])
    ];
});
