<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\ServiceCategory::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'icon' => $faker->imageUrl(100, 100),
        'cover' => $faker->imageUrl(900, 300),
        'order' => $faker->numberBetween(0, 9),
        'shopping_order' => $faker->numberBetween(0, 9),
    ];
});
