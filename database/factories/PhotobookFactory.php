<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\PhotoBook::class, function (Faker $faker) {
    return [
        'image' => $faker->imageUrl(),
        'name' => $faker->name,
        'position' => $faker->numberBetween(0, 9),
        'is_selected' => $faker->randomElement([true, false, true]),
    ];
});
