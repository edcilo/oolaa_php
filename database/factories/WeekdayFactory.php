<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Weekday::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'number' => $faker->randomNumber(1)
    ];
});
