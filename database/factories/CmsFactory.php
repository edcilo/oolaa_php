<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\Cms::class, function (Faker $faker) {
    return [
        'schema' => [
            'home' => [
                'carousel' => [],
                'top_stylists' => [],
                'steps' => [
                    [
                        'title' => 'Donde y cuando',
                        'icon' => 'https://oolaa.com.mx/media/cms/steps/service.PNG',
                        'body' => 'Escoge donde y cuando quieres recibir a tu profesional OOLAA',
                    ],
                    [
                        'title' => 'Selecciona',
                        'icon' => 'https://oolaa.com.mx/media/cms/steps/service_4fjQd2i.PNG',
                        'body' => 'Selecciona que servicio quieres',
                    ],
                    [
                        'title' => 'Elige a tu estilista',
                        'icon' => 'https://oolaa.com.mx/media/cms/steps/service_8w9taqJ.PNG',
                        'body' => 'Elige al profesional que quieres que vaya a tu casa, puedes ver fotos de su trabajo y calificaciones'
                    ],
                    [
                        'title' => 'Déjate consentir',
                        'icon' => 'https://oolaa.com.mx/media/cms/steps/service_TywNbas.png',
                        'body' => 'Paga con tarjeta y espéranos el día de tu cita. ¡Relájate y disfruta!'
                    ],
                ],
                'video' => '<iframe width="560" height="315" src="https://www.youtube.com/embed/RffJSkmDBCc?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>'
            ],
            'terms' => ['text' => '<h1>Términos y Condiciones</h1>'],
            'policy' => ['text' => '<h1>Políticas de cancelación</h1>'],
            'faqs' => ['text' => '<h1>Preguntas freceuntes</h1>'],
            'about_us' => ['text' => '<h1>¿Quiénes somos?</h1>'],
            'contact' => [
                'phone' => '(55) 74 03 97 38',
                'email' => 'contacto@oolaa.com.mx',
                'facebook' => 'https://www.facebook.com/oolaamx/',
                'instagram' => 'https://www.instagram.com/oolaa_mx/?hl=es%20instagram.com',
            ]
        ]
    ];
});
