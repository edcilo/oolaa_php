<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Zone::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});
