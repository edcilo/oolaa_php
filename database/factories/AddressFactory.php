<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Address::class, function (Faker $faker) {
    return [
        'street' => $faker->streetName,
        'ext_number' => $faker->randomNumber(),
        'int_number' => $faker->randomElement([null, $faker->randomNumber()]),
        'state' => $faker->randomElement([null, $faker->streetName()]),
        'city' => $faker->city,
        'neighborhood' => $faker->randomElement([null, $faker->streetName()]),
        'zip_code' => $faker->postcode,
        'line' => $faker->address,
        'longitude' => $faker->longitude,
        'latitude' => $faker->latitude,
    ];
});
