<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\State::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
