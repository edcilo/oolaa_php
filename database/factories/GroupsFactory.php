<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\Group::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});

$factory->define(\App\Entities\Permission::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});
