<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Area::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});
