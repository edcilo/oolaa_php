<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Profile::class, function (Faker $faker) {
    return [
        'avatar' => $faker->imageUrl(100, 100),
        'phone' => $faker->phoneNumber,
    ];
});
