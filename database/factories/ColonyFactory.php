<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Colony::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'zip_code' => $faker->postcode
    ];
});
