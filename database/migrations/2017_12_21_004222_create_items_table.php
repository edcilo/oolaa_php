<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status')->default('P');
            $table->integer('quantity');
            $table->float('unit_price');
            $table->float('extra_price')->nullable()->default(0);
            $table->float('discount')->nullable()->default(0);
            $table->integer('rating')->default(0);
            $table->text('notes')->nullable();
            $table->json('declined_by')->nullable();
            $table->integer('reservation_id')->unsigned();
            $table->integer('service_id')->unsigned();
            $table->integer('stylist_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('reservation_id')
                ->references('id')->on('reservations')
                ->onDelete('cascade');

            $table->foreign('service_id')
                ->references('id')->on('services')
                ->onDelete('cascade');

            $table->foreign('stylist_id')
                ->references('id')->on('stylists')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
