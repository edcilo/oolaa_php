<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('full_name');
            $table->integer('average_time');
            $table->float('price');
            $table->boolean('price_is_mutable')->default(false);
            $table->boolean('active')->default(false);
            $table->text('note');
            $table->integer('order');
            $table->string('image');
            $table->string('icon');
            // $table->integer('left');
            // $table->integer('right');
            // $table->integer('tree_id');
            // $table->integer('level');
            $table->integer('parent_id')->unsigned()->nullable();
            // $table->integer('order_search');
            $table->integer('category_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('parent_id')
                ->references('id')->on('services')
                ->onDelete('cascade');

            $table->foreign('category_id')
                ->references('id')->on('service_categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
