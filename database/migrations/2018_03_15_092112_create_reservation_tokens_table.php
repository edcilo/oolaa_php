<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('token')->unique();
            $table->integer('stylist_id')->unsigned();
            $table->integer('reservation_id')->unsigned();
            $table->timestamps();

            $table->foreign('stylist_id')
                ->references('id')->on('stylists')
                ->onDelete('cascade');

            $table->foreign('reservation_id')
                ->references('id')->on('reservations')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservation_tokens');
    }
}
