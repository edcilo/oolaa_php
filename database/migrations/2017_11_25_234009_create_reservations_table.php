<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status')->default('P');
            $table->dateTime('start_at');
            $table->float('subtotal')->default(0);
            $table->float('iva_percent')->default(0);
            $table->float('iva_total')->default(0);
            $table->float('total')->default(0);
            $table->json('address')->nullable();
            $table->json('credit_card')->nullable();
            $table->string('confirmation_code');
            $table->string('payment_reference')->nullable();
            $table->boolean('auto_stylist')->default(true);
            $table->boolean('paid_out')->default(false);
            $table->float('rating')->default(0);
            $table->text('comment')->nullable();
            $table->text('note')->nullable();
            $table->json('email_send');

            // $table->integer('stylist_id')->unsigned();
            $table->integer('customer_id')->unsigned();
            $table->integer('zone_id')->unsigned()->nullable();
            $table->integer('coupon_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            /*
            $table->foreign('stylist_id')
                ->references('id')->on('stylists')
                ->onDelete('cascade');
            */

            $table->foreign('customer_id')
                ->references('id')->on('customers')
                ->onDelete('cascade');

            $table->foreign('zone_id')
                ->references('id')->on('zones')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
