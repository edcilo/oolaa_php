<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colony_zone', function (Blueprint $table) {
            $table->integer('colony_id')->unsigned();
            $table->integer('zone_id')->unsigned();

            $table->foreign('colony_id')
                ->references('id')->on('colonies')
                ->onDelete('cascade');

            $table->foreign('zone_id')
                ->references('id')->on('zones')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colony_zone');
    }
}
