<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStylistWeekdayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stylist_weekday', function (Blueprint $table) {
            $table->integer('stylist_id')->unsigned();
            $table->integer('weekday_id')->unsigned();

            $table->foreign('stylist_id')
                ->references('id')->on('stylists')
                ->onDelete('cascade');

            $table->foreign('weekday_id')
                ->references('id')->on('weekdays')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stylist_weekday');
    }
}
