<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->boolean('allDay')->default(false);
            $table->timestamp('start');
            $table->timestamp('end')->nullable();
            $table->text('description')->nullable();
            $table->string('backgroundColor');
            $table->string('borderColor')->nullable();
            $table->string('className')->nullable();
            $table->unsignedInteger('stylist_id');
            $table->unsignedInteger('item_id')->nullable();
            $table->timestamps();

            $table->foreign('stylist_id')
                ->references('id')->on('stylists')
                ->onDelete('cascade');

            $table->foreign('item_id')
                ->references('id')->on('items')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendars');
    }
}
