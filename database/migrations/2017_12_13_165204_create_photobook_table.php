<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotobookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photo_books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image');
            $table->string('name')->default('')->nullable();
            $table->integer('position')->default(0);
            $table->boolean('is_selected');
            $table->integer('stylist_id')->unsigned();
            $table->timestamps();

            $table->foreign('stylist_id')
                ->references('id')->on('stylists')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photo_books');
    }
}
