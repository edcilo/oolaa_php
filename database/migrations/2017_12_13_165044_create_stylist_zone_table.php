<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStylistZoneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stylist_zone', function (Blueprint $table) {
            $table->integer('stylist_id')->unsigned();
            $table->integer('zone_id')->unsigned();

            $table->foreign('stylist_id')
                ->references('id')->on('stylists')
                ->onDelete('cascade');

            $table->foreign('zone_id')
                ->references('id')->on('zones')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stylist_zone');
    }
}
