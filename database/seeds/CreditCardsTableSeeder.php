<?php

use Illuminate\Database\Seeder;

class CreditCardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('credit_cards')->delete();

        $users = \App\Entities\User::all();

        foreach ($users as $user) {
            $total = random_int(1, 3);

            factory(\App\Entities\CreditCard::class, $total)->create(['user_id' => $user->id]);
        }
    }
}
