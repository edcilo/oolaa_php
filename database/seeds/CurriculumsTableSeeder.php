<?php

use Illuminate\Database\Seeder;

class CurriculumsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('curricula')->delete();

        $stylists = \App\Entities\Stylist::all();

        foreach ($stylists as $stylist) {
            factory(\App\Entities\Curriculum::class)->create(['stylist_id' => $stylist->id]);
        }
    }
}
