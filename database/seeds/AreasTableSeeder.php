<?php

use Illuminate\Database\Seeder;
use App\Entities\Zone;
use App\Entities\Colony;
use App\Entities\Municipality;
use App\Entities\State;

class AreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('areas')->delete();

        $z = Zone::first();
        $c = Colony::first();
        $m = Municipality::first();
        $s = State::first();

        factory(App\Entities\Area::class)->create([
            'name' => 'Chapultepec Sección I',
            'zone_id' => $z->id,
            'colony_id' => $c->id,
            'municipality_id' => $m->id,
            'state_id' => $s->id
        ]);
    }
}
