<?php

use Illuminate\Database\Seeder;
use App\Entities\Municipality;

class ColoniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('colonies')->delete();

        $municipality = new Municipality();

        $m = $municipality->where('slug', 'acambay-de-ruiz-castaneda')->first();

        factory(App\Entities\Colony::class)
            ->create(['name' => 'Centro de Azcapotzalco', 'zip_code' => '02000', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Delegación Política Azcapotzalco', 'zip_code' => '02008', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Nuevo Barrio San Rafael', 'zip_code' => '02010', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Los Reyes', 'zip_code' => '02010', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Rafael', 'zip_code' => '02010', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Santo Tomás', 'zip_code' => '02020', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Marcos', 'zip_code' => '02020', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Sebastián', 'zip_code' => '02040', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Del Maestro', 'zip_code' => '02040', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Libertad', 'zip_code' => '02050', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Wake', 'zip_code' => '02050', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Santa María Malinalco', 'zip_code' => '02050', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Sindicato Mexicano de Electricistas', 'zip_code' => '02060', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Un Hogar Para Cada Trabajador', 'zip_code' => '02060', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Nextengo', 'zip_code' => '02070', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Azcapotzalco', 'zip_code' => '02070', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Del Recreo', 'zip_code' => '02070', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Palestina', 'zip_code' => '02070', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Clavería', 'zip_code' => '02080', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Sector Naval', 'zip_code' => '02080', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Álvaro', 'zip_code' => '02090', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Ángel Zimbrón', 'zip_code' => '02099', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'El Rosario', 'zip_code' => '02100', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Los Oyameles', 'zip_code' => '02100', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Manuel Rivera Anaya CROC 1', 'zip_code' => '02109', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'CROC 6 Olimpia', 'zip_code' => '02109', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Ecológica Novedades Impacto', 'zip_code' => '02110', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Pablo Xalpa', 'zip_code' => '02110', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Martín Xochinahuac', 'zip_code' => '02120', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Martín Xochinahuac', 'zip_code' => '02125', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Nueva El Rosario', 'zip_code' => '02128', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Nueva España', 'zip_code' => '02129', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Tierra Nueva', 'zip_code' => '02130', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Santa Inés', 'zip_code' => '02140', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Pasteros', 'zip_code' => '02150', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Azcapotzalco Pasteros', 'zip_code' => '02150', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Santo Domingo', 'zip_code' => '02160', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Villas de Azcapotzalco', 'zip_code' => '02169', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Reynosa Tamaulipas', 'zip_code' => '02200', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Santa Bárbara', 'zip_code' => '02230', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Ticoman', 'zip_code' => '02240', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Andrés', 'zip_code' => '02240', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Andrés Fase II', 'zip_code' => '02240', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Andrés', 'zip_code' => '02240', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Santa Catarina', 'zip_code' => '02250', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Pitolaca', 'zip_code' => '02260', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Industrial Vallejo', 'zip_code' => '02300', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Ferrería', 'zip_code' => '02310', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Andrés de las Salinas', 'zip_code' => '02320', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Huautla de las Salinas', 'zip_code' => '02330', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Santa Cruz de las Salinas', 'zip_code' => '02340', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Jardines de Ceylán', 'zip_code' => '02350', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Las Salinas', 'zip_code' => '02360', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Rinconada el Olivar', 'zip_code' => '02400', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Juan Tlihuaca', 'zip_code' => '02400', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Juan Tlihuaca', 'zip_code' => '02400', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Rosendo Salazar', 'zip_code' => '02409', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Prados del Rosario', 'zip_code' => '02410', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Trabajadores de PEMEX', 'zip_code' => '02419', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Francisco Villa', 'zip_code' => '02420', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Ex-Hacienda El Rosario', 'zip_code' => '02420', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Presidente Madero', 'zip_code' => '02430', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Providencia', 'zip_code' => '02440', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Miguel Hidalgo', 'zip_code' => '02450', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Las Trancas', 'zip_code' => '02450', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Tezozomoc', 'zip_code' => '02459', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Axolahua', 'zip_code' => '02460', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'La Preciosa', 'zip_code' => '02460', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Ampliación Petrolera', 'zip_code' => '02470', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Lázaro Cárdenas', 'zip_code' => '02479', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Petrolera', 'zip_code' => '02480', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Kervin', 'zip_code' => '02490', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Mateo', 'zip_code' => '02490', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Unidad Cuitlahuac', 'zip_code' => '02500', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Pantaco', 'zip_code' => '02510', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'El Jagüey', 'zip_code' => '02510', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'El Jagüey', 'zip_code' => '02519', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Estación Pantaco', 'zip_code' => '02520', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Granjas Pantaco Campamento Ferrocarrilero', 'zip_code' => '02525', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Jardín Azpeitia', 'zip_code' => '02530', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Hogares Ferrocarrileros', 'zip_code' => '02540', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Pro-Hogar', 'zip_code' => '02600', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Coltongo', 'zip_code' => '02630', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Coltongo', 'zip_code' => '02630', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Monte Alto', 'zip_code' => '02640', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Trabajadores de Hierro', 'zip_code' => '02650', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Euzkadi', 'zip_code' => '02660', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Cosmopolita', 'zip_code' => '02670', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Potrero del Llano', 'zip_code' => '02680', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Miguel Amantla', 'zip_code' => '02700', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Pedro Xalpa', 'zip_code' => '02710', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Isidro', 'zip_code' => '02710', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Las Armas', 'zip_code' => '02718', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Ampliación San Pedro Xalpa', 'zip_code' => '02719', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Bartolo Cahualtongo', 'zip_code' => '02720', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Antonio', 'zip_code' => '02720', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Campo Encantado', 'zip_code' => '02729', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Francisco Tetecala', 'zip_code' => '02730', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Miguel Lerdo de Tejada', 'zip_code' => '02739', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'La Escuadra', 'zip_code' => '02750', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'La Providencia', 'zip_code' => '02750', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Santiago Ahuizotla', 'zip_code' => '02750', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Industrial San Antonio', 'zip_code' => '02760', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Santa Lucía', 'zip_code' => '02760', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Santa Cruz Acayucan', 'zip_code' => '02770', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Plenitud', 'zip_code' => '02780', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Santa Apolonia', 'zip_code' => '02790', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Nueva Santa María', 'zip_code' => '02800', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Victoria de las Democracias', 'zip_code' => '02810', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Ignacio Allende', 'zip_code' => '02810', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Bernabé', 'zip_code' => '02830', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Toronja', 'zip_code' => '02830', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Cobre de México', 'zip_code' => '02840', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Obrero Popular', 'zip_code' => '02840', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Tlatilco', 'zip_code' => '02850', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Tlatilco', 'zip_code' => '02860', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Salvador Xochimanca', 'zip_code' => '02870', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Aguilera', 'zip_code' => '02900', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Aldana', 'zip_code' => '02910', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Ampliación Cosmopolita', 'zip_code' => '02920', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Liberación', 'zip_code' => '02930', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Porvenir', 'zip_code' => '02940', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Del Gas', 'zip_code' => '02950', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'San Francisco Xocotitla', 'zip_code' => '02960', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Ampliación Del Gas', 'zip_code' => '02970', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Arenal', 'zip_code' => '02980', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'Patrimonio Familiar', 'zip_code' => '02980', 'municipality_id' => $m->id]);
        factory(App\Entities\Colony::class)
            ->create(['name' => 'La Raza', 'zip_code' => '02990', 'municipality_id' => $m->id]);
    }
}
