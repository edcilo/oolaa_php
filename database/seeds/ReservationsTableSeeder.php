<?php

use Illuminate\Database\Seeder;

class ReservationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reservations')->delete();

        for ($i = 0; $i < 50; $i++) {
            $customer = \App\Entities\Customer::inRandomOrder()->first();
            $zone = \App\Entities\Zone::inRandomOrder()->first();

            factory(\App\Entities\Reservation::class)->create([
                'customer_id' => $customer->id,
                'zone_id' => $zone->id
            ]);
        }
    }
}
