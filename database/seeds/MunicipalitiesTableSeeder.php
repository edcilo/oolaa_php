<?php

use Illuminate\Database\Seeder;
use App\Entities\State;

class MunicipalitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('municipalities')->delete();

        $state = new State;

        $aguascalientes = $state->where('slug', 'aguascalientes')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Aguascalientes', 'state_id' => $aguascalientes->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Asientos', 'state_id' => $aguascalientes->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Calvillo', 'state_id' => $aguascalientes->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cosío', 'state_id' => $aguascalientes->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jesús María', 'state_id' => $aguascalientes->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pabellón de Arteaga', 'state_id' => $aguascalientes->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Rincón de Romos', 'state_id' => $aguascalientes->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San José de Gracia', 'state_id' => $aguascalientes->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepezalá', 'state_id' => $aguascalientes->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Llano', 'state_id' => $aguascalientes->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco de los Romo', 'state_id' => $aguascalientes->id]);

        $bajaCalifornia = $state->where('slug', 'baja-california')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ensenada', 'state_id' => $bajaCalifornia->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mexicali', 'state_id' => $bajaCalifornia->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tecate', 'state_id' => $bajaCalifornia->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tijuana', 'state_id' => $bajaCalifornia->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Playas de Rosarito', 'state_id' => $bajaCalifornia->id]);

        $bajaCaliforniaSur = $state->where('slug', 'baja-california-sur')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Comondú', 'state_id' => $bajaCaliforniaSur->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mulegé', 'state_id' => $bajaCaliforniaSur->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Paz', 'state_id' => $bajaCaliforniaSur->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Los Cab', 'state_id' => $bajaCaliforniaSur->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Loreto', 'state_id' => $bajaCaliforniaSur->id]);

        $campeche = $state->where('slug', 'campeche')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Calkiní', 'state_id' => $campeche->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Campeche', 'state_id' => $campeche->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Carmen', 'state_id' => $campeche->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Champotón', 'state_id' => $campeche->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Hecelchakán', 'state_id' => $campeche->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Hopelchén', 'state_id' => $campeche->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Palizada', 'state_id' => $campeche->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tenabo', 'state_id' => $campeche->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Escárcega', 'state_id' => $campeche->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Calakmul', 'state_id' => $campeche->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Candelaria', 'state_id' => $campeche->id]);

        $coahuila = $state->where('slug', 'coahuila')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Abasolo', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acuña', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Allende', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Arteaga', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Candela', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Castaños', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuatro Ciénegas', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Escobedo', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Francisco I. Madero', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Frontera', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'General Cepeda', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Guerrero', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Hidalgo', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jiménez', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Juárez', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Lamadrid', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Matamoros', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Monclova', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Morelos', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Múzquiz', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nadadores', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nava', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ocampo', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Parras', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Piedras Negras', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Progreso', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ramos Arizpe', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sabinas', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sacramento', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Saltillo', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Buenaventura', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan de Sabinas', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sierra Mojada', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Torreón', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Viesca', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa Unión', 'state_id' => $coahuila->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zaragoza', 'state_id' => $coahuila->id]);

        $colima = $state->where('slug', 'colima')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Armería', 'state_id' => $colima->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Colima', 'state_id' => $colima->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Comala', 'state_id' => $colima->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coquimatlán', 'state_id' => $colima->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuauhtémoc', 'state_id' => $colima->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixtlahuacán', 'state_id' => $colima->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Manzanillo', 'state_id' => $colima->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Minatitlán', 'state_id' => $colima->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tecomán', 'state_id' => $colima->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa de Álvarez', 'state_id' => $colima->id]);

        $chiapas = $state->where('slug', 'chiapas')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acacoyagua', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acala', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acapetahua', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Altamirano', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Amatán', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Amatenango de la Frontera', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Amatenango del Valle', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Angel Albino Corzo', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Arriaga', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Bejucal de Ocampo', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Bella Vista', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Berriozábal', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Bochil', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Bosque', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cacahoatán', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Catazajá', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cintalapa', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coapilla', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Comitán de Domínguez', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Concordia', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Copainalá', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chalchihuitán', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chamula', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chanal', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chapultenango', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chenalhó', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chiapa de Corzo', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chiapilla', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chicoasén', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chicomuselo', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chilón', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Escuintla', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Francisco León', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Frontera Comalapa', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Frontera Hidalgo', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Grandeza', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huehuetán', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huixtán', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huitiupán', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huixtla', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Independencia', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixhuatán', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixtacomitán', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixtapa', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixtapangajoya', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jiquipilas', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jitotol', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Juárez', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Larráinzar', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Libertad', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mapastepec', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Las Margaritas', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mazapa de Madero', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mazatán', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Metapa', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mitontic', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Motozintla', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nicolás Ruíz', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ocosingo', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ocotepec', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ocozocoautla de Espinosa', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ostuacán', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Osumacinta', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Oxchuc', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Palenque', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pantelhó', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pantepec', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pichucalco', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pijijiapan', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Porvenir', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa Comaltitlán', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pueblo Nuevo Solistahuacán', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Rayón', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Reforma', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Las Rosas', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sabanilla', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Salto de Agua', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Cristóbal de las Casas', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Fernando', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Siltepec', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Simojovel', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sitalá', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Socoltenango', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Solosuchiapa', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Soyaló', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Suchiapa', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Suchiate', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sunuapa', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tapachula', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tapalapa', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tapilula', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tecpatán', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tenejapa', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Teopisca', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tila', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tonalá', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Totolapa', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Trinitaria', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tumbalá', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tuxtla Gutiérrez', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tuxtla Chico', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tuzantán', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tzimol', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Unión Juárez', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Venustiano Carranza', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa Corzo', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villaflores', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Yajalón', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Lucas', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zinacantán', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Cancuc', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Aldama', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Benemérito de las Américas', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Maravilla Tenejapa', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Marqués de Comillas', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Montecristo de Guerrero', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Andrés Duraznal', 'state_id' => $chiapas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago el Pinar', 'state_id' => $chiapas->id]);

        $chihuahua = $state->where('slug', 'chihuahua')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ahumada', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Aldama', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Allende', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Aquiles Serdán', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ascensión', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Bachíniva', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Balleza', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Batopilas', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Bocoyna', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Buenaventura', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Camargo', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Carichí', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Casas Grandes', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coronado', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coyame del Sotol', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Cruz', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuauhtémoc', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cusihuiriachi', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chihuahua', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chínipas', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Delicias', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Dr. Belisario Domínguez', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Galeana', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Isabel', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Gómez Farías', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Gran Morelos', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Guachochi', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Guadalupe', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Guadalupe y Calvo', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Guazapares', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Guerrero', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Hidalgo del Parral', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huejotitán', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ignacio Zaragoza', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Janos', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jiménez', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Juárez', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Julimes', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'López', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Madera', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Maguarichi', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Manuel Benavides', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Matachí', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Matamoros', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Meoqui', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Morelos', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Moris', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Namiquipa', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nonoava', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nuevo Casas Grandes', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ocampo', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ojinaga', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Praxedis G. Guerrero', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Riva Palacio', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Rosales', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Rosario', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco de Borja', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco de Conchos', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco del Oro', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Bárbara', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Satevó', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Saucillo', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Temósachic', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Tule', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Urique', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Uruachi', 'state_id' => $chihuahua->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Valle de Zaragoza', 'state_id' => $chihuahua->id]);

        $ciudadDeMexico = $state->where('slug', 'ciudad-de-mexico')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Azcapotzalco', 'state_id' => $ciudadDeMexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coyoacán', 'state_id' => $ciudadDeMexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuajimalpa de Morelos', 'state_id' => $ciudadDeMexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Gustavo A. Madero', 'state_id' => $ciudadDeMexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Iztacalco', 'state_id' => $ciudadDeMexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Iztapalapa', 'state_id' => $ciudadDeMexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Magdalena Contreras', 'state_id' => $ciudadDeMexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Milpa Alta', 'state_id' => $ciudadDeMexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Álvaro Obregón', 'state_id' => $ciudadDeMexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tláhuac', 'state_id' => $ciudadDeMexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlalpan', 'state_id' => $ciudadDeMexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Xochimilco', 'state_id' => $ciudadDeMexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Benito Juárez', 'state_id' => $ciudadDeMexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuauhtémoc', 'state_id' => $ciudadDeMexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Miguel Hidalgo', 'state_id' => $ciudadDeMexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Venustiano Carranza', 'state_id' => $ciudadDeMexico->id]);

        $durango = $state->where('slug', 'durango')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Canatlán', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Canelas', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coneto de Comonfort', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuencamé', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Durango', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'General Simón Bolívar', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Gómez Palacio', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Guadalupe Victoria', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Guanaceví', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Hidalgo', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Indé', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Lerdo', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mapimí', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mezquital', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nazas', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nombre de Dios', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ocampo', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Oro', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Otáez', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pánuco de Coronado', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Peñón Blanco', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Poanas', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pueblo Nuevo', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Rodeo', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Bernardo', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Dimas', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan de Guadalupe', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan del Río', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Luis del Cordero', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro del Gallo', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Clara', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Papasquiaro', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Súchil', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tamazula', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepehuanes', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlahualilo', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Topia', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Vicente Guerrero', 'state_id' => $durango->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nuevo Ideal', 'state_id' => $durango->id]);

        $guanajuato = $state->where('slug', 'guanajuato')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Abasolo', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acámbaro', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel de Allende', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Apaseo el Alto', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Apaseo el Grande', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atarjea', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Celaya', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Manuel Doblado', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Comonfort', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coroneo', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cortazar', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuerámaro', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Doctor Mora', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Dolores Hidalgo Cuna de la Independencia Nacional', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Guanajuato', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huanímaro', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Irapuato', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jaral del Progreso', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jerécuaro', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'León', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Moroleón', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ocampo', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pénjamo', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pueblo Nuevo', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Purísima del Rincón', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Romita', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Salamanca', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Salvatierra', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Diego de la Unión', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Felipe', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco del Rincón', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San José Iturbide', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Luis de la Paz', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Catarina', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Cruz de Juventino Rosas', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Maravatío', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Silao de la Victoria', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tarandacuao', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tarimoro', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tierra Blanca', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Uriangato', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Valle de Santiago', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Victoria', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villagrán', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Xichú', 'state_id' => $guanajuato->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Yuriria', 'state_id' => $guanajuato->id]);

        $guerrero = $state->where('slug', 'guerrero')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acapulco de Juárez', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ahuacuotzingo', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ajuchitlán del Progreso', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Alcozauca de Guerrero', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Alpoyeca', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Apaxtla', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Arcelia', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atenango del Río', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atlamajalcingo del Monte', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atlixtac', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atoyac de Álvarez', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ayutla de los Libres', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Azoyú', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Benito Juárez', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Buenavista de Cuéllar', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coahuayutla de José María Izazaga', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cocula', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Copala', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Copalillo', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Copanatoyac', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coyuca de Benítez', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coyuca de Catalán', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuajinicuilapa', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cualác', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuautepec', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuetzala del Progreso', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cutzamala de Pinzón', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chilapa de Álvarez', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chilpancingo de los Bravo', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Florencio Villarreal', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'General Canuto A. Neri', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'General Heliodoro Castillo', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huamuxtitlán', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huitzuco de los Figueroa', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Iguala de la Independencia', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Igualapa', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixcateopan de Cuauhtémoc', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zihuatanejo de Azueta', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Juan R. Escudero', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Leonardo Bravo', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Malinaltepec', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mártir de Cuilapan', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Metlatónoc', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mochitlán', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Olinalá', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ometepec', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pedro Ascencio Alquisiras', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Petatlán', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pilcaya', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pungarabato', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Quechultenango', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Luis Acatlán', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Marcos', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Totolapan', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Taxco de Alarcón', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tecoanapa', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Técpan de Galeana', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Teloloapan', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepecoacuilco de Trujano', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tetipac', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tixtla de Guerrero', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlacoachistlahuaca', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlacoapa', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlalchapa', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlalixtaquilla de Maldonado', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlapa de Comonfort', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlapehuala', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Unión de Isidoro Montes de Oca', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Xalpatláhuac', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Xochihuehuetlán', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Xochistlahuaca', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zapotitlán Tablas', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zirándaro', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zitlala', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Eduardo Neri', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acatepec', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Marquelia', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cochoapa el Grande', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'José Joaquín de Herrera', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Juchitán', 'state_id' => $guerrero->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Iliatenco', 'state_id' => $guerrero->id]);

        $hidalgo = $state->where('slug', 'hidalgo')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acatlán', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acaxochitlán', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Actopan', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Agua Blanca de Iturbide', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ajacuba', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Alfajayucan', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Almoloya', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Apan', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Arenal', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atitalaquia', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atlapexco', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atotonilco el Grande', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atotonilco de Tula', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Calnali', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cardonal', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuautepec de Hinojosa', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chapantongo', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chapulhuacán', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chilcuautla', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Eloxochitlán', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Emiliano Zapata', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Epazoyucan', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Francisco I. Madero', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huasca de Ocampo', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huautla', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huazalingo', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huehuetla', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huejutla de Reyes', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huichapan', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixmiquilpan', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jacala de Ledezma', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jaltocán', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Juárez Hidalgo', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Lolotla', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Metepec', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Agustín Metzquititlán', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Metztitlán', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mineral del Chico', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mineral del Monte', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Misión', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mixquiahuala de Juárez', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Molango de Escamilla', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nicolás Flores', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nopala de Villagrán', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Omitlán de Juárez', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Felipe Orizatlán', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pacula', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pachuca de Soto', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pisaflores', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Progreso de Obregón', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mineral de la Reforma', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Agustín Tlaxiaca', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Bartolo Tutotepec', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Salvador', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago de Anaya', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Tulantepec de Lugo Guerrero', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Singuilucan', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tasquillo', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tecozautla', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tenango de Doria', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepeapulco', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepehuacán de Guerrero', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepeji del Río de Ocampo', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepetitlán', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tetepango', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa de Tezontepec', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tezontepec de Aldama', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tianguistengo', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tizayuca', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlahuelilpan', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlahuiltepa', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlanalapa', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlanchinol', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlaxcoapan', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tolcayuca', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tula de Allende', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tulancingo de Bravo', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Xochiatipan', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Xochicoatlán', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Yahualica', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zacualtipán de Ángeles', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zapotlán de Juárez', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zempoala', 'state_id' => $hidalgo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zimapán', 'state_id' => $hidalgo->id]);

        $jalisco = $state->where('slug', 'jalisco')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acatic', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acatlán de Juárez', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ahualulco de Mercado', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Amacueca', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Amatitán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ameca', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juanito de Escobedo', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Arandas', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Arenal', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atemajac de Brizuela', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atengo', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atenguillo', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atotonilco el Alto', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atoyac', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Autlán de Navarro', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ayotlán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ayutla', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Barca', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Bolaños', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cabo Corrientes', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Casimiro Castillo', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cihuatlán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zapotlán el Grande', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cocula', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Colotlán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Concepción de Buenos Aires', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuautitlán de García Barragán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuautla', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuquío', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chapala', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chimaltitán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chiquilistlán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Degollado', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ejutla', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Encarnación de Díaz', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Etzatlán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Grullo', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Guachinango', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Guadalajara', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Hostotipaquillo', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huejúcar', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huejuquilla el Alto', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Huerta', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixtlahuacán de los Membrillos', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixtlahuacán del Río', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jalostotitlán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jamay', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jesús María', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jilotlán de los Dolores', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jocotepec', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Juanacatlán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Juchitlán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Lagos de Moreno', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Limón', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Magdalena', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María del Oro', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Manzanilla de la Paz', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mascota', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mazamitla', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mexticacán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mezquitic', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mixtlán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ocotlán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ojuelos de Jalisco', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pihuamo', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Poncitlán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Puerto Vallarta', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa Purificación', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Quitupan', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Salto', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Cristóbal de la Barranca', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Diego de Alejandría', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan de los Lagos', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Julián', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Marcos', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Martín de Bolaños', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Martín Hidalgo', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel el Alto', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Gómez Farías', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Sebastián del Oeste', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María de los Ángeles', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sayula', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tala', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Talpa de Allende', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tamazula de Gordiano', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tapalpa', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tecalitlán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tecolotlán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Techaluta de Montenegro', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tenamaxtlán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Teocaltiche', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Teocuitatlán de Corona', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepatitlán de Morelos', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tequila', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Teuchitlán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tizapán el Alto', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlajomulco de Zúñiga', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Tlaquepaque', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tolimán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tomatlán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tonalá', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tonaya', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tonila', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Totatiche', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tototlán', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tuxcacuesco', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tuxcueca', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tuxpan', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Unión de San Antonio', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Unión de Tula', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Valle de Guadalupe', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Valle de Juárez', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Gabriel', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa Corona', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa Guerrero', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa Hidalgo', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cañadas de Obregón', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Yahualica de González Gallo', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zacoalco de Torres', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zapopan', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zapotiltic', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zapotitlán de Vadillo', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zapotlán del Rey', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zapotlanejo', 'state_id' => $jalisco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Ignacio Cerro Gordo', 'state_id' => $jalisco->id]);

        $mexico = $state->where('slug', 'mexico')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acambay de Ruíz Castañeda', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acolman', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Aculco', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Almoloya de Alquisiras', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Almoloya de Juárez', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Almoloya del Río', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Amanalco', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Amatepec', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Amecameca', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Apaxco', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atenco', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atizapán', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atizapán de Zaragoza', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atlacomulco', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atlautla', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Axapusco', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ayapango', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Calimaya', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Capulhuac', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coacalco de Berriozábal', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coatepec Harinas', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cocotitlán', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coyotepec', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuautitlán', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chalco', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chapa de Mota', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chapultepec', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chiautla', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chicoloapan', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chiconcuac', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chimalhuacán', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Donato Guerra', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ecatepec de Morelos', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ecatzingo', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huehuetoca', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Hueypoxtla', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huixquilucan', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Isidro Fabela', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixtapaluca', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixtapan de la Sal', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixtapan del Oro', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixtlahuaca', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Xalatlaco', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jaltenco', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jilotepec', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jilotzingo', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jiquipilco', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jocotitlán', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Joquicingo', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Juchitepec', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Lerma', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Malinalco', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Melchor Ocampo', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Metepec', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mexicaltzingo', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Morelos', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Naucalpan de Juárez', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nezahualcóyotl', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nextlalpan', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nicolás Romero', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nopaltepec', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ocoyoacac', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ocuilan', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Oro', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Otumba', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Otzoloapan', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Otzolotepec', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ozumba', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Papalotla', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Paz', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Polotitlán', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Rayón', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Antonio la Isla', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Felipe del Progreso', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Martín de las Pirámides', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Mateo Atenco', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Simón de Guerrero', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Tomás', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Soyaniquilpan de Juárez', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sultepec', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tecámac', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tejupilco', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Temamatla', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Temascalapa', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Temascalcingo', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Temascaltepec', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Temoaya', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tenancingo', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tenango del Aire', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tenango del Valle', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Teoloyucan', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Teotihuacán', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepetlaoxtoc', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepetlixpa', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepotzotlán', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tequixquiac', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Texcaltitlán', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Texcalyacac', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Texcoco', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tezoyuca', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tianguistenco', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Timilpan', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlalmanalco', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlalnepantla de Baz', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlatlaya', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Toluca', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tonatico', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tultepec', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tultitlán', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Valle de Bravo', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa de Allende', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa del Carbón', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa Guerrero', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa Victoria', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Xonacatlán', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zacazonapan', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zacualpan', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zinacantepec', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zumpahuacán', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zumpango', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuautitlán Izcalli', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Valle de Chalco Solidaridad', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Luvianos', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San José del Rincón', 'state_id' => $mexico->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tonanitla', 'state_id' => $mexico->id]);

        $michoacan = $state->where('slug', 'michoacan')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acuitzio', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Aguililla', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Álvaro Obregón', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Angamacutiro', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Angangueo', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Apatzingán', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Aporo', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Aquila', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ario', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Arteaga', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Briseñas', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Buenavista', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Carácuaro', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coahuayana', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coalcomán de Vázquez Pallares', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coeneo', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Contepec', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Copándaro', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cotija', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuitzeo', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Charapan', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Charo', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chavinda', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cherán', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chilchota', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chinicuila', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chucándiro', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Churintzio', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Churumuco', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ecuandureo', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Epitacio Huerta', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Erongarícuaro', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Gabriel Zamora', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Hidalgo', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Huacana', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huandacareo', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huaniqueo', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huetamo', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huiramba', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Indaparapeo', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Irimbo', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixtlán', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jacona', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jiménez', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jiquilpan', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Juárez', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jungapeo', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Lagunillas', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Madero', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Maravatío', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Marcos Castellanos', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Lázaro Cárdenas', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Morelia', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Morelos', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Múgica', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nahuatzen', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nocupétaro', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nuevo Parangaricutiro', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nuevo Urecho', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Numarán', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ocampo', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pajacuarán', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Panindícuaro', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Parácuaro', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Paracho', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pátzcuaro', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Penjamillo', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Peribán', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Piedad', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Purépero', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Puruándiro', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Queréndaro', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Quiroga', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cojumatlán de Régules', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Los Reyes', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sahuayo', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Lucas', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Ana Maya', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Salvador Escalante', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Senguio', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Susupuato', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tacámbaro', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tancítaro', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tangamandapio', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tangancícuaro', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tanhuato', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Taretan', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tarímbaro', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepalcatepec', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tingambato', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tingüindín', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tiquicheo de Nicolás Romero', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlalpujahua', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlazazalca', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tocumbo', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tumbiscatío', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Turicato', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tuxpan', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tuzantla', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tzintzuntzan', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tzitzio', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Uruapan', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Venustiano Carranza', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villamar', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Vista Hermosa', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Yurécuaro', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zacapu', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zamora', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zináparo', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zinapécuaro', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ziracuaretiro', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zitácuaro', 'state_id' => $michoacan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'José Sixto Verduzco', 'state_id' => $michoacan->id]);

        $morelos = $state->where('slug', 'morelos')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Amacuzac', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atlatlahucan', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Axochiapan', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ayala', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coatlán del Río', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuautla', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuernavaca', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Emiliano Zapata', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huitzilac', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jantetelco', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jiutepec', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jojutla', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jonacatepec', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mazatepec', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Miacatlán', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ocuituco', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Puente de Ixtla', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Temixco', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepalcingo', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepoztlán', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tetecala', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tetela del Volcán', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlalnepantla', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlaltizapán de Zapata', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlaquiltenango', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlayacapan', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Totolapan', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Xochitepec', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Yautepec', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Yecapixtla', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zacatepec', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zacualpan', 'state_id' => $morelos->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Temoac', 'state_id' => $morelos->id]);

        $nayarit = $state->where('slug', 'nayarit')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acaponeta', 'state_id' => $nayarit->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ahuacatlán', 'state_id' => $nayarit->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Amatlán de Cañas', 'state_id' => $nayarit->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Compostela', 'state_id' => $nayarit->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huajicori', 'state_id' => $nayarit->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixtlán del Río', 'state_id' => $nayarit->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jala', 'state_id' => $nayarit->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Xalisco', 'state_id' => $nayarit->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Del Nayar', 'state_id' => $nayarit->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Rosamorada', 'state_id' => $nayarit->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ruíz', 'state_id' => $nayarit->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Blas', 'state_id' => $nayarit->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Lagunillas', 'state_id' => $nayarit->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María del Oro', 'state_id' => $nayarit->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Ixcuintla', 'state_id' => $nayarit->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tecuala', 'state_id' => $nayarit->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepic', 'state_id' => $nayarit->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tuxpan', 'state_id' => $nayarit->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Yesca', 'state_id' => $nayarit->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Bahía de Banderas', 'state_id' => $nayarit->id]);

        $nuevoLeon = $state->where('slug', 'nuevo-leon')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Abasolo', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Agualeguas', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Los Aldamas', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Allende', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Anáhuac', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Apodaca', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Aramberri', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Bustamante', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cadereyta Jiménez', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Carmen', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cerralvo', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ciénega de Flores', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'China', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Doctor Arroyo', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Doctor Coss', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Doctor González', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Galeana', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'García', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Garza García', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'General Bravo', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'General Escobedo', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'General Terán', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'General Treviño', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'General Zaragoza', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'General Zuazua', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Guadalupe', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Los Herreras', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Higueras', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Hualahuises', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Iturbide', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Juárez', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Lampazos de Naranjo', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Linares', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Marín', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Melchor Ocampo', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mier y Noriega', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mina', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Montemorelos', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Monterrey', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Parás', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pesquería', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Los Ramones', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Rayones', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sabinas Hidalgo', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Salinas Victoria', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Nicolás de los Garza', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Hidalgo', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Catarina', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Vallecillo', 'state_id' => $nuevoLeon->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villaldama', 'state_id' => $nuevoLeon->id]);

        $oaxaca = $state->where('slug', 'oaxaca')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Abejones', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acatlán de Pérez Figueroa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Asunción Cacalotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Asunción Cuyotepeji', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Asunción Ixtaltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Asunción Nochixtlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Asunción Ocotlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Asunción Tlacolulita', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ayotzintepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Barrio de la Soledad', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Calihualá', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Candelaria Loxicha', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ciénega de Zimatlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ciudad Ixtepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coatecas Altas', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coicoyán de las Flores', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Compañía', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Concepción Buenavista', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Concepción Pápalo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Constancia del Rosario', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cosolapa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cosoltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuilápam de Guerrero', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuyamecalco Villa de Zaragoza', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chahuites', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chalcatongo de Hidalgo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chiquihuitlán de Benito Juárez', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Heroica Ciudad de Ejutla de Crespo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Eloxochitlán de Flores Magón', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Espinal', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tamazulápam del Espíritu Santo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Fresnillo de Trujano', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Guadalupe Etla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Guadalupe de Ramírez', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Guelatao de Juárez', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Guevea de Humboldt', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mesones Hidalgo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa Hidalgo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Heroica Ciudad de Huajuapan de León', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huautepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huautla de Jiménez', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixtlán de Juárez', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Heroica Ciudad de Juchitán de Zaragoza', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Loma Bonita', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Magdalena Apasco', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Magdalena Jaltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Magdalena Jicotlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Magdalena Mixtepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Magdalena Ocotlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Magdalena Peñasco', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Magdalena Teitipac', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Magdalena Tequisistlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Magdalena Tlacotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Magdalena Zahuatlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mariscala de Juárez', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mártires de Tacubaya', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Matías Romero Avendaño', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mazatlán Villa de Flores', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Miahuatlán de Porfirio Díaz', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mixistlán de la Reforma', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Monjas', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Natividad', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nazareno Etla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nejapa de Madero', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixpantepec Nieves', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Niltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Oaxaca de Juárez', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ocotlán de Morelos', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Pe', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pinotepa de Don Luis', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pluma Hidalgo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San José del Progreso', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Putla Villa de Guerrero', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Catarina Quioquitani', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Reforma de Pineda', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Reforma', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Reyes Etla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Rojas de Cuauhtémoc', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Salina Cruz', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Agustín Amatengo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Agustín Atenango', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Agustín Chayuco', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Agustín de las Juntas', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Agustín Etla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Agustín Loxicha', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Agustín Tlacotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Agustín Yatareni', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Andrés Cabecera Nueva', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Andrés Dinicuiti', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Andrés Huaxpaltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Andrés Huayápam', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Andrés Ixtlahuaca', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Andrés Lagunas', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Andrés Nuxiño', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Andrés Paxtlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Andrés Sinaxtla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Andrés Solaga', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Andrés Teotilálpam', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Andrés Tepetlapa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Andrés Yaá', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Andrés Zabache', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Andrés Zautla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Antonino Castillo Velasco', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Antonino el Alto', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Antonino Monte Verde', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Antonio Acutla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Antonio de la Cal', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Antonio Huitepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Antonio Nanahuatípam', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Antonio Sinicahua', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Antonio Tepetlapa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Baltazar Chichicápam', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Baltazar Loxicha', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Baltazar Yatzachi el Bajo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Bartolo Coyotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Bartolomé Ayautla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Bartolomé Loxicha', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Bartolomé Quialana', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Bartolomé Yucuañe', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Bartolomé Zoogocho', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Bartolo Soyaltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Bartolo Yautepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Bernardo Mixtepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Blas Atempa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Carlos Yautepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Cristóbal Amatlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Cristóbal Amoltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Cristóbal Lachirioag', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Cristóbal Suchixtlahuaca', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Dionisio del Mar', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Dionisio Ocotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Dionisio Ocotlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Esteban Atatlahuca', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Felipe Jalapa de Díaz', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Felipe Tejalápam', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Felipe Usila', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco Cahuacuá', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco Cajonos', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco Chapulapa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco Chindúa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco del Mar', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco Huehuetlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco Ixhuatán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco Jaltepetongo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco Lachigoló', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco Logueche', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco Nuxaño', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco Ozolotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco Sola', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco Telixtlahuaca', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco Teopan', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco Tlapancingo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Gabriel Mixtepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Ildefonso Amatlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Ildefonso Sola', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Ildefonso Villa Alta', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Jacinto Amilpas', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Jacinto Tlacotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Jerónimo Coatlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Jerónimo Silacayoapilla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Jerónimo Sosola', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Jerónimo Taviche', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Jerónimo Tecóatl', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Jorge Nuchita', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San José Ayuquila', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San José Chiltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San José del Peñasco', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San José Estancia Grande', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San José Independencia', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San José Lachiguiri', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San José Tenango', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Achiutla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Atepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ánimas Trujano', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Bautista Atatlahuca', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Bautista Coixtlahuaca', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Bautista Cuicatlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Bautista Guelache', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Bautista Jayacatlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Bautista Lo de Soto', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Bautista Suchitepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Bautista Tlacoatzintepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Bautista Tlachichilco', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Bautista Tuxtepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Cacahuatepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Cieneguilla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Coatzóspam', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Colorado', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Comaltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Cotzocón', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Chicomezúchil', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Chilateca', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan del Estado', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan del Río', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Diuxi', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Evangelista Analco', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Guelavía', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Guichicovi', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Ihualtepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Juquila Mixes', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Juquila Vijanos', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Lachao', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Lachigalla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Lajarcia', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Lalana', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan de los Cués', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Mazatlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Mixtepec -Dto. 08 -', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Mixtepec -Dto. 26 -', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Ñumí', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Ozolotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Petlapa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Quiahije', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Quiotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Sayultepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Tabaá', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Tamazola', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Teita', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Teitipac', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Tepeuxila', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Teposcolula', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Yaeé', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Yatzona', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Yucuita', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Lorenzo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Lorenzo Albarradas', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Lorenzo Cacaotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Lorenzo Cuaunecuiltitla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Lorenzo Texmelúcan', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Lorenzo Victoria', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Lucas Camotlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Lucas Ojitlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Lucas Quiaviní', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Lucas Zoquiápam', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Luis Amatlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Marcial Ozolotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Marcos Arteaga', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Martín de los Cansecos', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Martín Huamelúlpam', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Martín Itunyoso', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Martín Lachilá', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Martín Peras', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Martín Tilcajete', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Martín Toxpalan', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Martín Zacatepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Mateo Cajonos', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Capulálpam de Méndez', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Mateo del Mar', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Mateo Yoloxochitlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Mateo Etlatongo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Mateo Nejápam', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Mateo Peñasco', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Mateo Piñas', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Mateo Río Hondo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Mateo Sindihui', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Mateo Tlapiltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Melchor Betaza', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Achiutla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Ahuehuetitlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Aloápam', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Amatitlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Amatlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Coatlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Chicahua', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Chimalapa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel del Puerto', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel del Río', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Ejutla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel el Grande', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Huautla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Mixtepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Panixtlahuaca', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Peras', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Piedras', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Quetzaltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Santa Flor', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa Sola de Vega', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Soyaltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Suchixtepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa Talea de Castro', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Tecomatlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Tenango', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Tequixtepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Tilquiápam', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Tlacamama', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Tlacotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Tulancingo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel Yotao', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Nicolás', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Nicolás Hidalgo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pablo Coatlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pablo Cuatro Venados', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pablo Etla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pablo Huitzo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pablo Huixtepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pablo Macuiltianguis', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pablo Tijaltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pablo Villa de Mitla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pablo Yaganiza', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Amuzgos', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Apóstol', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Atoyac', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Cajonos', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Coxcaltepec Cántaros', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Comitancillo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro el Alto', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Huamelula', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Huilotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Ixcatlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Ixtlahuaca', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Jaltepetongo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Jicayán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Jocotipac', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Juchatengo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Mártir', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Mártir Quiechapa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Mártir Yucuxaco', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Mixtepec -Dto. 22 -', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Mixtepec -Dto. 26 -', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Molinos', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Nopala', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Ocopetatillo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Ocotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Pochutla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Quiatoni', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Sochiápam', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Tapanatepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Taviche', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Teozacoalco', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Teutila', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Tidaá', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Topiltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Totolápam', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa de Tututepec de Melchor Ocampo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Yaneri', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Yólox', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro y San Pablo Ayutla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa de Etla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro y San Pablo Teposcolula', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro y San Pablo Tequixtepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro Yucunama', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Raymundo Jalpan', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Sebastián Abasolo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Sebastián Coatlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Sebastián Ixcapa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Sebastián Nicananduta', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Sebastián Río Hondo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Sebastián Tecomaxtlahuaca', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Sebastián Teitipac', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Sebastián Tutla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Simón Almolongas', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Simón Zahuatlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Ana', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Ana Ateixtlahuaca', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Ana Cuauhtémoc', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Ana del Valle', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Ana Tavela', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Ana Tlapacoyan', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Ana Yareni', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Ana Zegache', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Catalina Quierí', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Catarina Cuixtla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Catarina Ixtepeji', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Catarina Juquila', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Catarina Lachatao', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Catarina Loxicha', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Catarina Mechoacán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Catarina Minas', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Catarina Quiané', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Catarina Tayata', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Catarina Ticuá', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Catarina Yosonotú', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Catarina Zapoquila', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Cruz Acatepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Cruz Amilpas', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Cruz de Bravo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Cruz Itundujia', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Cruz Mixtepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Cruz Nundaco', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Cruz Papalutla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Cruz Tacache de Mina', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Cruz Tacahua', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Cruz Tayata', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Cruz Xitla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Cruz Xoxocotlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Cruz Zenzontepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Gertrudis', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Inés del Monte', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Inés Yatzeche', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Lucía del Camino', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Lucía Miahuatlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Lucía Monteverde', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Lucía Ocotlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Alotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Apazco', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María la Asunción', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Heroica Ciudad de Tlaxiaco', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ayoquezco de Aldama', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Atzompa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Camotlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Colotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Cortijo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Coyotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Chachoápam', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa de Chilapa de Díaz', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Chilchotla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Chimalapa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María del Rosario', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María del Tule', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Ecatepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Guelacé', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Guienagati', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Huatulco', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Huazolotitlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Ipalapa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Ixcatlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Jacatepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Jalapa del Marqués', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Jaltianguis', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Lachixío', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Mixtequilla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Nativitas', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Nduayaco', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Ozolotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Pápalo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Peñoles', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Petapa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Quiegolani', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Sola', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Tataltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Tecomavaca', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Temaxcalapa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Temaxcaltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Teopoxco', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Tepantlali', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Texcatitlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Tlahuitoltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Tlalixtac', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Tonameca', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Totolapilla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Xadani', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Yalina', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Yavesía', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Yolotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Yosoyúa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Yucuhiti', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Zacatepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Zaniza', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María Zoquitlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Amoltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Apoala', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Apóstol', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Astata', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Atitlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Ayuquililla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Cacaloxtepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Camotlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Comaltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Chazumba', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Choápam', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago del Río', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Huajolotitlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Huauclilla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Ihuitlán Plumas', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Ixcuintepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Ixtayutla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Jamiltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Jocotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Juxtlahuaca', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Lachiguiri', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Lalopa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Laollaga', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Laxopa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Llano Grande', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Matatlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Miltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Minas', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Nacaltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Nejapilla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Nundiche', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Nuyoó', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Pinotepa Nacional', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Suchilquitongo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Tamazola', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Tapextla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa Tejúpam de la Unión', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Tenango', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Tepetlapa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Tetepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Texcalcingo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Textitlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Tilantongo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Tillo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Tlazoyaltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Xanica', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Xiacuí', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Yaitepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Yaveo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Yolomécatl', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Yosondúa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Yucuyachi', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Zacatepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Zoochila', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nuevo Zoquiápam', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo Ingenio', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo Albarradas', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo Armenta', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo Chihuitán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo de Morelos', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo Ixcatlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo Nuxaá', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo Ozolotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo Petapa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo Roayaga', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo Tehuantepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo Teojomulco', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo Tepuxtepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo Tlatayápam', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo Tomaltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo Tonalá', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo Tonaltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo Xagacía', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo Yanhuitlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo Yodohino', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo Zanatepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santos Reyes Nopala', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santos Reyes Pápalo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santos Reyes Tepejillo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santos Reyes Yucuná', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Tomás Jalieza', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Tomás Mazaltepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Tomás Ocotepec', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Tomás Tamazulapan', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Vicente Coatlán', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Vicente Lachixío', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Vicente Nuñú', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Silacayoápam', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sitio de Xitlapehua', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Soledad Etla', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa de Tamazulápam del Progreso', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tanetze de Zaragoza', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Taniche', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tataltepec de Valdés', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Teococuilco de Marcos Pérez', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Teotitlán de Flores Magón', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Teotitlán del Valle', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Teotongo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepelmeme Villa de Morelos', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tezoatlán de Segura y Luna', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Jerónimo Tlacochahuaya', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlacolula de Matamoros', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlacotepec Plumas', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlalixtac de Cabrera', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Totontepec Villa de Morelos', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Trinidad Zaachila', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Trinidad Vista Hermosa', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Unión Hidalgo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Valerio Trujano', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Bautista Valle Nacional', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa Díaz Ordaz', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Yaxe', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Magdalena Yodocono de Porfirio Díaz', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Yogana', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Yutanduchi de Guerrero', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa de Zaachila', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Mateo Yucutindoo', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zapotitlán Lagunas', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zapotitlán Palmas', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Inés de Zaragoza', 'state_id' => $oaxaca->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zimatlán de Álvarez', 'state_id' => $oaxaca->id]);

        $queretaro = $state->where('slug', 'queretaro')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Amealco de Bonfil', 'state_id' => $queretaro->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pinal de Amoles', 'state_id' => $queretaro->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Arroyo Seco', 'state_id' => $queretaro->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cadereyta de Montes', 'state_id' => $queretaro->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Colón', 'state_id' => $queretaro->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Corregidora', 'state_id' => $queretaro->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ezequiel Montes', 'state_id' => $queretaro->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huimilpan', 'state_id' => $queretaro->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jalpan de Serra', 'state_id' => $queretaro->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Landa de Matamoros', 'state_id' => $queretaro->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Marqués', 'state_id' => $queretaro->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pedro Escobedo', 'state_id' => $queretaro->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Peñamiller', 'state_id' => $queretaro->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Querétaro', 'state_id' => $queretaro->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Joaquín', 'state_id' => $queretaro->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan del Río', 'state_id' => $queretaro->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tequisquiapan', 'state_id' => $queretaro->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tolimán', 'state_id' => $queretaro->id]);


        $quintanaRoo = $state->where('slug', 'quintana-roo')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cozumel', 'state_id' => $quintanaRoo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Felipe Carrillo Puerto', 'state_id' => $quintanaRoo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Isla Mujeres', 'state_id' => $quintanaRoo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Othón P. Blanco', 'state_id' => $quintanaRoo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Benito Juárez', 'state_id' => $quintanaRoo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'José María Morelos', 'state_id' => $quintanaRoo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Lázaro Cárdenas', 'state_id' => $quintanaRoo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Solidaridad', 'state_id' => $quintanaRoo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tulum', 'state_id' => $quintanaRoo->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Bacalar', 'state_id' => $quintanaRoo->id]);

        $sanLuisPotosi = $state->where('slug', 'san-luis-potosi')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ahualulco', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Alaquines', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Aquismón', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Armadillo de los Infante', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cárdenas', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Catorce', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cedral', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cerritos', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cerro de San Pedro', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ciudad del Maíz', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ciudad Fernández', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tancanhuitz', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ciudad Valles', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coxcatlán', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Charcas', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ebano', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Guadalcázar', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huehuetlán', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Lagunillas', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Matehuala', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mexquitic de Carmona', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Moctezuma', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Rayón', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Rioverde', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Salinas', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Antonio', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Ciro de Acosta', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Luis Potosí', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Martín Chalchicuautla', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Nicolás Tolentino', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Catarina', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María del Río', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santo Domingo', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Vicente Tancuayalab', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Soledad de Graciano Sánchez', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tamasopo', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tamazunchale', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tampacán', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tampamolón Corona', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tamuín', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tanlajás', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tanquián de Escobedo', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tierra Nueva', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Vanegas', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Venado', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa de Arriaga', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa de Guadalupe', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa de la Paz', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa de Ramos', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa de Reyes', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa Hidalgo', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa Juárez', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Axtla de Terrazas', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Xilitla', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zaragoza', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa de Arista', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Matlapa', 'state_id' => $sanLuisPotosi->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Naranjo', 'state_id' => $sanLuisPotosi->id]);

        $sinaloa = $state->where('slug', 'sinaloa')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ahome', 'state_id' => $sinaloa->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Angostura', 'state_id' => $sinaloa->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Badiraguato', 'state_id' => $sinaloa->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Concordia', 'state_id' => $sinaloa->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cosalá', 'state_id' => $sinaloa->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Culiacán', 'state_id' => $sinaloa->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Choix', 'state_id' => $sinaloa->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Elota', 'state_id' => $sinaloa->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Escuinapa', 'state_id' => $sinaloa->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Fuerte', 'state_id' => $sinaloa->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Guasave', 'state_id' => $sinaloa->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mazatlán', 'state_id' => $sinaloa->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mocorito', 'state_id' => $sinaloa->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Rosario', 'state_id' => $sinaloa->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Salvador Alvarado', 'state_id' => $sinaloa->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Ignacio', 'state_id' => $sinaloa->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sinaloa', 'state_id' => $sinaloa->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Navolato', 'state_id' => $sinaloa->id]);

        $sonora = $state->where('slug', 'sonora')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Aconchi', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Agua Prieta', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Alamos', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Altar', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Arivechi', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Arizpe', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atil', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Bacadéhuachi', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Bacanora', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Bacerac', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Bacoachi', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Bácum', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Banámichi', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Baviácora', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Bavispe', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Benjamín Hill', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Caborca', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cajeme', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cananea', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Carbó', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Colorada', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cucurpe', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cumpas', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Divisaderos', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Empalme', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Etchojoa', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Fronteras', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Granados', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Guaymas', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Hermosillo', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huachinera', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huásabas', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huatabampo', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huépac', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Imuris', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Magdalena', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mazatán', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Moctezuma', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Naco', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nácori Chico', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nacozari de García', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Navojoa', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nogales', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Onavas', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Opodepe', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Oquitoa', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pitiquito', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Puerto Peñasco', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Quiriego', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Rayón', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Rosario', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sahuaripa', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Felipe de Jesús', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Javier', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Luis Río Colorado', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Miguel de Horcasitas', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pedro de la Cueva', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Ana', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Cruz', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sáric', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Soyopa', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Suaqui Grande', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepache', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Trincheras', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tubutama', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ures', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa Hidalgo', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa Pesqueira', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Yécora', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'General Plutarco Elías Calles', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Benito Juárez', 'state_id' => $sonora->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Ignacio Río Muerto', 'state_id' => $sonora->id]);

        $tabasco = $state->where('slug', 'tabasco')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Balancán', 'state_id' => $tabasco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cárdenas', 'state_id' => $tabasco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Centla', 'state_id' => $tabasco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Centro', 'state_id' => $tabasco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Comalcalco', 'state_id' => $tabasco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cunduacán', 'state_id' => $tabasco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Emiliano Zapata', 'state_id' => $tabasco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huimanguillo', 'state_id' => $tabasco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jalapa', 'state_id' => $tabasco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jalpa de Méndez', 'state_id' => $tabasco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jonuta', 'state_id' => $tabasco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Macuspana', 'state_id' => $tabasco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nacajuca', 'state_id' => $tabasco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Paraíso', 'state_id' => $tabasco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tacotalpa', 'state_id' => $tabasco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Teapa', 'state_id' => $tabasco->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tenosique', 'state_id' => $tabasco->id]);

        $tamaulipas = $state->where('slug', 'tamaulipas')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Abasolo', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Aldama', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Altamira', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Antiguo Morelos', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Burgos', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Bustamante', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Camargo', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Casas', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ciudad Madero', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cruillas', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Gómez Farías', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'González', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Güémez', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Guerrero', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Gustavo Díaz Ordaz', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Hidalgo', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jaumave', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jiménez', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Llera', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mainero', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Mante', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Matamoros', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Méndez', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mier', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Miguel Alemán', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Miquihuana', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nuevo Laredo', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nuevo Morelos', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ocampo', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Padilla', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Palmillas', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Reynosa', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Río Bravo', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Carlos', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Fernando', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Nicolás', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Soto la Marina', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tampico', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tula', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Valle Hermoso', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Victoria', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villagrán', 'state_id' => $tamaulipas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Xicoténcatl', 'state_id' => $tamaulipas->id]);

        $tlaxcala = $state->where('slug', 'tlaxcala')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Amaxac de Guerrero', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Apetatitlán de Antonio Carvajal', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atlangatepec', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atltzayanca', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Apizaco', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Calpulalpan', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Carmen Tequexquitla', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuapiaxtla', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuaxomulco', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chiautempan', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Muñoz de Domingo Arenas', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Españita', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huamantla', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Hueyotlipan', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixtacuixtla de Mariano Matamoros', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixtenco', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mazatecochco de José María Morelos', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Contla de Juan Cuamatzi', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepetitla de Lardizábal', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sanctórum de Lázaro Cárdenas', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nanacamilpa de Mariano Arista', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acuamanala de Miguel Hidalgo', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Natívitas', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Panotla', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Pablo del Monte', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Cruz Tlaxcala', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tenancingo', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Teolocholco', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepeyanco', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Terrenate', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tetla de la Solidaridad', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tetlatlahuca', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlaxcala', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlaxco', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tocatlán', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Totolac', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ziltlaltépec de Trinidad Sánchez Santos', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tzompantepec', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Xaloztoc', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Xaltocan', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Papalotla de Xicohténcatl', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Xicohtzinco', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Yauhquemehcan', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zacatelco', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Benito Juárez', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Emiliano Zapata', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Lázaro Cárdenas', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Magdalena Tlaltelulco', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Damián Texóloc', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Francisco Tetlanohcan', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Jerónimo Zacualpan', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San José Teacalco', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Huactzinco', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Lorenzo Axocomanitla', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Lucas Tecopilco', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Ana Nopalucan', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Apolonia Teacalco', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Catarina Ayometla', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Cruz Quilehtla', 'state_id' => $tlaxcala->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Isabel Xiloxoxtla', 'state_id' => $tlaxcala->id]);

        $veracruz = $state->where('slug', 'veracruz')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acajete', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acatlán', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acayucan', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Actopan', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acula', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acultzingo', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Camarón de Tejeda', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Alpatláhuac', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Alto Lucero de Gutiérrez Barrios', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Altotonga', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Alvarado', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Amatitlán', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Naranjos Amatlán', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Amatlán de los Reyes', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Angel R. Cabada', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Antigua', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Apazapan', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Aquila', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Astacinga', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atlahuilco', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atoyac', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atzacan', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atzalan', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlaltetela', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ayahualulco', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Banderilla', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Benito Juárez', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Boca del Río', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Calcahualco', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Camerino Z. Mendoza', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Carrillo Puerto', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Catemaco', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cazones de Herrera', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cerro Azul', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Citlaltépetl', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coacoatzintla', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coahuitlán', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coatepec', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coatzacoalcos', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coatzintla', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coetzala', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Colipa', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Comapa', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Córdoba', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cosamaloapan de Carpio', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cosautlán de Carvajal', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coscomatepec', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cosoleacaque', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cotaxtla', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coxquihui', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Coyutla', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuichapa', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuitláhuac', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chacaltianguis', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chalma', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chiconamel', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chiconquiaco', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chicontepec', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chinameca', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chinampa de Gorostiza', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Las Choapas', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chocamán', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chontla', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chumatlán', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Emiliano Zapata', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Espinal', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Filomeno Mata', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Fortín', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Gutiérrez Zamora', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Hidalgotitlán', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huatusco', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huayacocotla', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Hueyapan de Ocampo', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huiloapan de Cuauhtémoc', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ignacio de la Llave', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ilamatlán', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Isla', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixcatepec', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixhuacán de los Reyes', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixhuatlán del Café', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixhuatlancillo', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixhuatlán del Sureste', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixhuatlán de Madero', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixmatlahuacan', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixtaczoquitlán', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jalacingo', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Xalapa', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jalcomulco', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jáltipan', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jamapa', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jesús Carranza', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Xico', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jilotepec', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Juan Rodríguez Clara', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Juchique de Ferrer', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Landero y Coss', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Lerdo de Tejada', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Magdalena', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Maltrata', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Manlio Fabio Altamirano', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mariano Escobedo', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Martínez de la Torre', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mecatlán', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mecayapan', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Medellín', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Miahuatlán', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Las Minas', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Minatitlán', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Misantla', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mixtla de Altamirano', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Moloacán', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Naolinco', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Naranjal', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nautla', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nogales', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Oluta', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Omealca', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Orizaba', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Otatitlán', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Oteapan', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ozuluama de Mascareñas', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pajapan', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pánuco', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Papantla', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Paso del Macho', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Paso de Ovejas', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'La Perla', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Perote', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Platón Sánchez', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Playa Vicente', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Poza Rica de Hidalgo', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Las Vigas de Ramírez', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pueblo Viejo', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Puente Nacional', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Rafael Delgado', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Rafael Lucio', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Los Reyes', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Río Blanco', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Saltabarranca', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Andrés Tenejapan', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Andrés Tuxtla', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Juan Evangelista', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Tuxtla', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sayula de Alemán', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Soconusco', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sochiapa', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Soledad Atzompa', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Soledad de Doblado', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Soteapan', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tamalín', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tamiahua', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tampico Alto', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tancoco', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tantima', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tantoyuca', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tatatila', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Castillo de Teayo', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tecolutla', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tehuipango', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Álamo Temapache', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tempoal', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tenampa', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tenochtitlán', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Teocelo', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepatlaxco', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepetlán', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepetzintla', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tequila', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'José Azueta', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Texcatepec', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Texhuacán', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Texistepec', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tezonapa', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tierra Blanca', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tihuatlán', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlacojalpan', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlacolulan', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlacotalpan', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlacotepec de Mejía', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlachichilco', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlalixcoyan', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlalnelhuayocan', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlapacoyan', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlaquilpa', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlilapan', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tomatlán', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tonayán', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Totutla', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tuxpan', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tuxtilla', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ursulo Galván', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Vega de Alatorre', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Veracruz', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa Aldama', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Xoxocotla', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Yanga', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Yecuatla', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zacualpan', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zaragoza', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zentla', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zongolica', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zontecomatlán de López y Fuentes', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zozocolco de Hidalgo', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Agua Dulce', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Higo', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nanchital de Lázaro Cárdenas del Río', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tres Valles', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Carlos A. Carrillo', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tatahuicapan de Juárez', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Uxpanapa', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Rafael', 'state_id' => $veracruz->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santiago Sochiapan', 'state_id' => $veracruz->id]);

        $yucatan = $state->where('slug', 'yucatan')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Abalá', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Acanceh', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Akil', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Baca', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Bokobá', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Buctzotz', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cacalchén', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Calotmul', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cansahcab', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cantamayec', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Celestún', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cenotillo', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Conkal', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuncunul', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuzamá', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chacsinkín', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chankom', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chapab', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chemax', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chicxulub Pueblo', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chichimilá', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chikindzonot', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chocholá', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chumayel', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Dzán', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Dzemul', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Dzidzantún', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Dzilam de Bravo', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Dzilam González', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Dzitás', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Dzoncauich', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Espita', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Halachó', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Hocabá', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Hoctún', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Homún', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huhí', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Hunucmá', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ixil', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Izamal', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Kanasín', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Kantunil', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Kaua', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Kinchil', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Kopomá', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mama', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Maní', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Maxcanú', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mayapán', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mérida', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mocochá', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Motul', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Muna', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Muxupip', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Opichén', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Oxkutzcab', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Panabá', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Peto', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Progreso', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Quintana Roo', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Río Lagartos', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sacalum', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Samahil', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sanahcat', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'San Felipe', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa Elena', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Seyé', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sinanché', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sotuta', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sucilá', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sudzal', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Suma', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tahdziú', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tahmek', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Teabo', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tecoh', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tekal de Venegas', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tekantó', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tekax', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tekit', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tekom', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Telchac Pueblo', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Telchac Puerto', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Temax', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Temozón', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepakán', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tetiz', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Teya', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ticul', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Timucuy', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tinum', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tixcacalcupul', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tixkokob', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tixmehuac', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tixpéhual', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tizimín', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tunkás', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tzucacab', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Uayma', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ucú', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Umán', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Valladolid', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Xocchel', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Yaxcabá', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Yaxkukul', 'state_id' => $yucatan->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Yobaín', 'state_id' => $yucatan->id]);

        $zacatecas = $state->where('slug', 'zacatecas')->first();

        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Apozol', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Apulco', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Atolinga', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Benito Juárez', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Calera', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cañitas de Felipe Pescador', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Concepción del Oro', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Cuauhtémoc', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Chalchihuites', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Fresnillo', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Trinidad García de la Cadena', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Genaro Codina', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'General Enrique Estrada', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'General Francisco R. Murguía', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Plateado de Joaquín Amaro', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'General Pánfilo Natera', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Guadalupe', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Huanusco', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jalpa', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jerez', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Jiménez del Teul', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Juan Aldama', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Juchipila', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Loreto', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Luis Moya', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mazapil', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Melchor Ocampo', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Mezquital del Oro', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Miguel Auza', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Momax', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Monte Escobedo', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Morelos', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Moyahua de Estrada', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Nochistlán de Mejía', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Noria de Ángeles', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Ojocaliente', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pánuco', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Pinos', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Río Grande', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sain Alto', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'El Salvador', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Sombrerete', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Susticacán', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tabasco', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepechitlán', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tepetongo', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Teúl de González Ortega', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Tlaltenango de Sánchez Román', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Valparaíso', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Vetagrande', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa de Cos', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa García', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa González Ortega', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villa Hidalgo', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Villanueva', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Zacatecas', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Trancoso', 'state_id' => $zacatecas->id]);
        factory(App\Entities\Municipality::class)
            ->create(['name' => 'Santa María de la Paz', 'state_id' => $zacatecas->id]);
    }
}
