<?php

use Illuminate\Database\Seeder;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->delete();

        factory(\App\Entities\Group::class)->create([
            'name' => 'root'
        ]);
    }
}
