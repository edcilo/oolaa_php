<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->delete();

        /*
        for ($i = 0; $i < 30; $i++) {
            $category = \App\Entities\ServiceCategory::inRandomOrder()->first();

            factory(\App\Entities\Service::class)->create(['category_id' => $category->id]);
        }

        for ($i = 0; $i < 10; $i++) {
            $parent = \App\Entities\Service::inRandomOrder()->first();

            factory(\App\Entities\Service::class)->create(['parent_id' => $parent->id]);
        }
        */

        factory(\App\Entities\Service::class)->create([
            'name' => 'Manicure',
            'full_name' => 'Manicure',
            'average_time' => 45,
            'price' => 220,
            'price_is_mutable' => true,
            'active' => true,
            'note' => 'Si quieres manicure con gel, agrega una aplicación de gel',
            'order' => 2,
            'category_id' => 1
        ]);

        factory(\App\Entities\Service::class)->create([
            'name' => 'Pedicure',
            'full_name' => 'Pedicure',
            'average_time' => 45,
            'price' => 250,
            'price_is_mutable' => true,
            'active' => true,
            'note' => 'Si quieres pedicure con gel, agrega una aplicación de gel',
            'order' => 3,
            'category_id' => 1
        ]);

        factory(\App\Entities\Service::class)->create([
            'name' => 'Cambio de esmalte',
            'full_name' => 'Cambio de esmalte',
            'average_time' => 45,
            'price' => 100,
            'price_is_mutable' => true,
            'active' => true,
            'note' => '',
            'order' => 4,
            'category_id' => 1
        ]);

        factory(\App\Entities\Service::class)->create([
            'name' => 'Manicure y pedicure',
            'full_name' => 'Manicure y pedicure',
            'average_time' => 90,
            'price' => 450,
            'price_is_mutable' => true,
            'active' => true,
            'note' => 'Si quieres manicure o pedicure con gel, agrega las aplicaciones de gel correspondientes',
            'order' => 5,
            'category_id' => 1
        ]);

        factory(\App\Entities\Service::class)->create([
            'name' => 'Aplicación de gel en manos',
            'full_name' => 'Aplicación de gel en manos',
            'average_time' => 30,
            'price' => 200,
            'price_is_mutable' => true,
            'active' => true,
            'note' => 'Esto no incluye el manicure',
            'order' => 6,
            'category_id' => 1
        ]);

        factory(\App\Entities\Service::class)->create([
            'name' => 'Aplicación de gel en pies',
            'full_name' => 'Aplicación de gel en pies',
            'average_time' => 30,
            'price' => 200,
            'price_is_mutable' => true,
            'active' => true,
            'note' => 'Esto no incluye el pedicure',
            'order' => 7,
            'category_id' => 1
        ]);

        factory(\App\Entities\Service::class)->create([
            'name' => 'Efecto espejo',
            'full_name' => 'Efecto espejo',
            'average_time' => 45,
            'price' => 400,
            'price_is_mutable' => true,
            'active' => true,
            'note' => '',
            'order' => 8,
            'category_id' => 1
        ]);

        factory(\App\Entities\Service::class)->create([
            'name' => 'Aplicación de acrílico',
            'full_name' => 'Aplicación de acrílico',
            'average_time' => 45,
            'price' => 500,
            'price_is_mutable' => true,
            'active' => true,
            'note' => 'Este servicio no incluye manicure',
            'order' => 10,
            'category_id' => 1
        ]);

        factory(\App\Entities\Service::class)->create([
            'name' => 'Retoque de acrílico',
            'full_name' => 'Retoque de acrílico',
            'average_time' => 45,
            'price' => 420,
            'price_is_mutable' => true,
            'active' => true,
            'note' => 'Este servicio no incluye manicure',
            'order' => 11,
            'category_id' => 1
        ]);

        factory(\App\Entities\Service::class)->create([
            'name' => 'Retiro de acrílico',
            'full_name' => 'Retiro de acrílico',
            'average_time' => 45,
            'price' => 100,
            'price_is_mutable' => true,
            'active' => true,
            'note' => 'Este servicio no incluye manicure',
            'order' => 12,
            'category_id' => 1
        ]);

        factory(\App\Entities\Service::class)->create([
            'name' => 'Suelto',
            'full_name' => 'Suelto (Secado o ondulado)',
            'average_time' => 30,
            'price' => 400,
            'price_is_mutable' => true,
            'active' => true,
            'note' => '',
            'order' => 14,
            'category_id' => 2
        ]);

        factory(\App\Entities\Service::class)->create([
            'name' => 'Medio recogido (Media cola)',
            'full_name' => 'Medio recogido (Media cola)',
            'average_time' => 45,
            'price' => 500,
            'price_is_mutable' => true,
            'active' => true,
            'note' => '',
            'order' => 18,
            'category_id' => 2
        ]);

        factory(\App\Entities\Service::class)->create([
            'name' => 'Recogido (Chongo)',
            'full_name' => 'Pelo recogido (Chongo)',
            'average_time' => 45,
            'price' => 600,
            'price_is_mutable' => true,
            'active' => true,
            'note' => '',
            'order' => 19,
            'category_id' => 2
        ]);

        factory(\App\Entities\Service::class)->create([
            'name' => 'Maquillaje',
            'full_name' => 'Maquillaje',
            'average_time' => 60,
            'price' => 950,
            'price_is_mutable' => true,
            'active' => true,
            'note' => '',
            'order' => 21,
            'category_id' => 3
        ]);

        factory(\App\Entities\Service::class)->create([
            'name' => 'Facial antiedad con radiofrecuencia',
            'full_name' => 'Facial antiedad con radiofrecuencia',
            'average_time' => 120,
            'price' => 1300,
            'price_is_mutable' => true,
            'active' => true,
            'note' => 'Duración de 2 horas. Se empieza con un facial de limpieza profunda y después se aplica radiofrecuencia.',
            'order' => 2,
            'category_id' => 4
        ]);

        factory(\App\Entities\Service::class)->create([
            'name' => 'Limpieza profunda',
            'full_name' => 'Limpieza profunda',
            'average_time' => 60,
            'price' => 850,
            'price_is_mutable' => true,
            'active' => true,
            'note' => 'Consta de una limpieza profunda donde se retiran las impurezas de la piel, seguido de una mascarilla, dando como resultado una piel limpia y luminosa.',
            'order' => 31,
            'category_id' => 4
        ]);

        factory(\App\Entities\Service::class)->create([
            'name' => 'Masaje relajante',
            'full_name' => 'Masaje relajante',
            'average_time' => 60,
            'price' => 750,
            'price_is_mutable' => true,
            'active' => true,
            'note' => 'El servicio tiene duración aproximada de 1 hr',
            'order' => 38,
            'category_id' => 5
        ]);
    }
}
