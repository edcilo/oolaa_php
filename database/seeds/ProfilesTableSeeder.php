<?php

use Illuminate\Database\Seeder;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->delete();

        $users = \App\Entities\User::all();

        foreach ($users as $user) {
            factory(App\Entities\Profile::class)->create(['user_id' => $user->id]);
        }
    }
}
