<?php

use Illuminate\Database\Seeder;

class StylistZoneTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stylist_zone')->delete();

        $stylists = \App\Entities\Stylist::all();

        foreach ($stylists as $stylist) {
            $total = rand(1, 3);

            for ($i = 0; $i < $total; $i++) {
                $zone = \App\Entities\Zone::inRandomOrder()->first();

                $stylist->zones()->attach($zone->id);
            }
        }
    }
}
