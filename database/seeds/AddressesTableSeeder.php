<?php

use Illuminate\Database\Seeder;

class AddressesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('addresses')->delete();

        $users = \App\Entities\User::all();

        foreach ($users as $user) {
            $total = random_int(1, 3);

            factory(\App\Entities\Address::class, $total)->create(['user_id' => $user->id]);
        }
    }
}
