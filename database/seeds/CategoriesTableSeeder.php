<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();

        factory(App\Entities\Category::class)->create([
            'name' => 'Regular',
            'order' => '2'
        ]);

        factory(App\Entities\Category::class)->create([
            'name' => 'Medio',
            'order' => '1'
        ]);

        factory(App\Entities\Category::class)->create([
            'name' => 'Profesional',
            'order' => '0'
        ]);
    }
}
