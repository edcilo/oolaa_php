<?php

use Illuminate\Database\Seeder;

class ServiceStylistTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('service_stylist')->delete();

        for ($i = 0; $i < 60; $i++) {
            $service = \App\Entities\Service::inRandomOrder()->first();
            $stylist = \App\Entities\Stylist::inRandomOrder()->first();

            $stylist->services()->attach($service);
        }
    }
}
