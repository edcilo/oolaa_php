<?php

use Illuminate\Database\Seeder;

class WeekdaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('weekdays')->delete();

        factory(App\Entities\Weekday::class)->create(['name' => 'Domingo', 'number' => 0]);
        factory(App\Entities\Weekday::class)->create(['name' => 'Lunes', 'number' => 1]);
        factory(App\Entities\Weekday::class)->create(['name' => 'Martes', 'number' => 2]);
        factory(App\Entities\Weekday::class)->create(['name' => 'Miercoles', 'number' => 3]);
        factory(App\Entities\Weekday::class)->create(['name' => 'Jueves', 'number' => 4]);
        factory(App\Entities\Weekday::class)->create(['name' => 'Viernes', 'number' => 5]);
        factory(App\Entities\Weekday::class)->create(['name' => 'Sabado', 'number' => 6]);
    }
}
