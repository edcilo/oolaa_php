<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->delete();

        for ($i = 0; $i < 100; $i++) {
            $service = \App\Entities\Service::inRandomOrder()->first();
            $reservation = \App\Entities\Reservation::inRandomOrder()->first();
            $stylist = \App\Entities\Stylist::inRandomOrder()->first();

            factory(\App\Entities\Item::class)->create([
                'service_id' => $service->id,
                'reservation_id' => $reservation->id,
                'stylist_id' => $stylist->id,
            ]);
        }
    }
}
