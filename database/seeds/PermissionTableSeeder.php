<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->delete();

        factory(\App\Entities\Permission::class)->create([
            'name' => 'Dashboard|Access'
        ]);

        factory(\App\Entities\Permission::class)->create([
            'name' => 'User|Create'
        ]);

        factory(\App\Entities\Permission::class)->create([
            'name' => 'User|Read'
        ]);

        factory(\App\Entities\Permission::class)->create([
            'name' => 'User|Update'
        ]);

        factory(\App\Entities\Permission::class)->create([
            'name' => 'User|Delete'
        ]);
    }
}
