<?php

use Illuminate\Database\Seeder;

class PhotobookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('photo_books')->delete();

        $stylists = \App\Entities\Stylist::all();

        foreach ($stylists as $stylist) {
            $photos = random_int(1, 18);

            factory(App\Entities\PhotoBook::class, $photos)->create(['stylist_id' => $stylist->id]);
        }
    }
}
