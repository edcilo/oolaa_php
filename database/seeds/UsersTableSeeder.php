<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        factory(App\Entities\User::class, 25)->create();

        // user admin
        $root_group = \App\Entities\Group::where('name', 'root')->first();
        $u = factory(App\Entities\User::class)->create([
            'name' => 'Admin',
            'email' => 'admin@oolaa.com.mx',
            'verified' => true
        ]);
        factory(App\Entities\Profile::class)->create(['user_id' => $u->id]);
        factory(App\Entities\Customer::class)->create(['user_id' => $u->id]);

        $u->groups()->save($root_group);
    }
}
