<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->delete();

        factory(App\Entities\State::class)->create(['name' => 'Aguascalientes']);
        factory(App\Entities\State::class)->create(['name' => 'Baja California']);
        factory(App\Entities\State::class)->create(['name' => 'Baja California Sur']);
        factory(App\Entities\State::class)->create(['name' => 'Campeche']);
        factory(App\Entities\State::class)->create(['name' => 'Coahuila']);
        factory(App\Entities\State::class)->create(['name' => 'Colima']);
        factory(App\Entities\State::class)->create(['name' => 'Chiapas']);
        factory(App\Entities\State::class)->create(['name' => 'Chihuahua']);
        factory(App\Entities\State::class)->create(['name' => 'Ciudad de México']);
        factory(App\Entities\State::class)->create(['name' => 'Durango']);
        factory(App\Entities\State::class)->create(['name' => 'Guanajuato']);
        factory(App\Entities\State::class)->create(['name' => 'Guerrero']);
        factory(App\Entities\State::class)->create(['name' => 'Hidalgo']);
        factory(App\Entities\State::class)->create(['name' => 'Jalisco']);
        factory(App\Entities\State::class)->create(['name' => 'México']);
        factory(App\Entities\State::class)->create(['name' => 'Michoacan']);
        factory(App\Entities\State::class)->create(['name' => 'Morelos']);
        factory(App\Entities\State::class)->create(['name' => 'Nayarit']);
        factory(App\Entities\State::class)->create(['name' => 'Nuevo León']);
        factory(App\Entities\State::class)->create(['name' => 'Oaxaca']);
        factory(App\Entities\State::class)->create(['name' => 'Puebla']);
        factory(App\Entities\State::class)->create(['name' => 'Querétaro']);
        factory(App\Entities\State::class)->create(['name' => 'Quintana Roo']);
        factory(App\Entities\State::class)->create(['name' => 'San Luis Potosí']);
        factory(App\Entities\State::class)->create(['name' => 'Sinaloa']);
        factory(App\Entities\State::class)->create(['name' => 'Sonora']);
        factory(App\Entities\State::class)->create(['name' => 'Tabasco']);
        factory(App\Entities\State::class)->create(['name' => 'Tamaulipas']);
        factory(App\Entities\State::class)->create(['name' => 'Tlaxcala']);
        factory(App\Entities\State::class)->create(['name' => 'Veracruz']);
        factory(App\Entities\State::class)->create(['name' => 'Yucatán']);
        factory(App\Entities\State::class)->create(['name' => 'Zacatecas']);
    }
}
