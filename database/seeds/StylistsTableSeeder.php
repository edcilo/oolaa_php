<?php

use Illuminate\Database\Seeder;

class StylistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stylists')->delete();

        $u = factory(App\Entities\User::class)->create([
            'name' => 'Estilista Pendiente',
            'email' => 's.p@oolaa.com.mx'
        ]);
        factory(App\Entities\Profile::class)->create(['user_id' => $u->id]);
        factory(App\Entities\Stylist::class)->create([
            'status' => 'P',
            'user_id' => $u->id
        ]);

        $u = factory(App\Entities\User::class)->create([
            'name' => 'Estilista Aprobado',
            'email' => 's.a@oolaa.com.mx'
        ]);
        factory(App\Entities\Profile::class)->create(['user_id' => $u->id]);
        factory(App\Entities\Stylist::class)->create([
            'status' => 'A',
            'user_id' => $u->id
        ]);

        $u = factory(App\Entities\User::class)->create([
            'name' => 'Estilista Rechazado',
            'email' => 's.r@oolaa.com.mx'
        ]);
        factory(App\Entities\Profile::class)->create(['user_id' => $u->id]);
        factory(App\Entities\Stylist::class)->create([
            'status' => 'R',
            'user_id' => $u->id
        ]);

        $u = factory(App\Entities\User::class)->create([
            'name' => 'Estilista Bloqueado',
            'email' => 's.b@oolaa.com.mx'
        ]);
        factory(App\Entities\Profile::class)->create(['user_id' => $u->id]);
        factory(App\Entities\Stylist::class)->create([
            'status' => 'B',
            'user_id' => $u->id
        ]);

        /*
        factory(App\Entities\User::class, 30)->create()->each(function ($u) {
            $u->stylist()->save(factory(App\Entities\Stylist::class)->make());
        });
        */
    }
}
