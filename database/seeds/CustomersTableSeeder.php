<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->delete();

        $u = factory(App\Entities\User::class)->create([
            'name' => 'Cliente Apellido',
            'email' => 'c@oolaa.com.mx',
            'verified' => true
        ]);

        factory(App\Entities\Customer::class)->create(['user_id' => $u->id]);
        factory(App\Entities\Profile::class)->create(['user_id' => $u->id]);

        /*
        factory(App\Entities\User::class, 10)->create()->each(function ($u) {
            $u->customer()->save(factory(App\Entities\Customer::class)->make());
        });
        */
    }
}
