<?php

use Illuminate\Database\Seeder;

class StylistWeekdayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stylist_weekday')->delete();

        $stylists = \App\Entities\Stylist::all();

        foreach ($stylists as $stylist) {
            $days = rand(1, 7);

            for ($i = 0; $i < $days; $i++) {
                $day = \App\Entities\Weekday::inRandomOrder()->first();

                $stylist->weekdays()->attach($day->id);
            }
        }
    }
}
