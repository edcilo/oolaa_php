<?php

use Illuminate\Database\Seeder;

class ZonesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('zones')->delete();

        factory(App\Entities\Zone::class)->create(['name' => 'Polanco y alrededores']);
        factory(App\Entities\Zone::class)->create(['name' => 'Santa Fe']);
        factory(App\Entities\Zone::class)->create(['name' => 'Cuajimalpa']);
        factory(App\Entities\Zone::class)->create(['name' => 'Benito Juarez']);
        factory(App\Entities\Zone::class)->create(['name' => 'Cuahutémoc']);
        factory(App\Entities\Zone::class)->create(['name' => 'Coyoacán']);
        factory(App\Entities\Zone::class)->create(['name' => 'Huixquilucan']);
        factory(App\Entities\Zone::class)->create(['name' => 'Naucalpan']);

        for ($i = 0; $i < 40; $i++) {
            $colony = \App\Entities\Colony::inRandomOrder()->first();
            $zone = \App\Entities\Zone::inRandomOrder()->first();

            $zone->colonies()->attach($colony->id);
        }
    }
}
