<?php

use Illuminate\Database\Seeder;

class ServiceCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('service_categories')->delete();

        factory(\App\Entities\ServiceCategory::class)->create([
            'name' => 'Manicure y pedicure',
            'order' => 0,
            'shopping_order' => 2
        ]);

        factory(\App\Entities\ServiceCategory::class)->create([
            'name' => 'Peinado',
            'order' => 1,
            'shopping_order' => 1
        ]);

        factory(\App\Entities\ServiceCategory::class)->create([
            'name' => 'Maquillaje',
            'order' => 2,
            'shopping_order' => 0
        ]);

        factory(\App\Entities\ServiceCategory::class)->create([
            'name' => 'Facial',
            'order' => 3,
            'shopping_order' => 3
        ]);

        factory(\App\Entities\ServiceCategory::class)->create([
            'name' => 'Masaje',
            'order' => 4,
            'shopping_order' => 4
        ]);
    }
}
