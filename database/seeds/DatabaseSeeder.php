<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CmsTableSeeder::class);

        $this->call(GroupTableSeeder::class);
        $this->call(PermissionTableSeeder::class);

        $this->call(UsersTableSeeder::class);
        $this->call(ProfilesTableSeeder::class);

        $this->call(CustomersTableSeeder::class);

        $this->call(CategoriesTableSeeder::class);
        $this->call(StylistsTableSeeder::class);

        $this->call(StatesTableSeeder::class);
        $this->call(MunicipalitiesTableSeeder::class);
        $this->call(ColoniesTableSeeder::class);
        $this->call(ZonesTableSeeder::class);
        $this->call(WeekdaysTableSeeder::class);

        $this->call(StylistZoneTableSeeder::class);
        $this->call(StylistWeekdayTableSeeder::class);
        $this->call(PhotobookTableSeeder::class);
        $this->call(CurriculumsTableSeeder::class);

        $this->call(AddressesTableSeeder::class);
        $this->call(CreditCardsTableSeeder::class);

        $this->call(ServiceCategoryTableSeeder::class);
        $this->call(ServicesTableSeeder::class);
        $this->call(ServiceStylistTableSeeder::class);

        $this->call(ReservationsTableSeeder::class);
        $this->call(ItemsTableSeeder::class);
    }
}
