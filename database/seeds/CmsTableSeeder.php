<?php

use Illuminate\Database\Seeder;

class CmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cms')->delete();

        factory(App\Entities\Cms::class, 1)->create();
    }
}
