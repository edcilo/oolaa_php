let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// scripts
mix.scripts([
    'resources/assets/plugins/handlebars/handlebars-v4.0.11.js',
    'node_modules/vue/dist/vue.js',
    'node_modules/axios/dist/axios.js',
    'node_modules/moment/min/moment-with-locales.js',
    'resources/assets/plugins/jstree/dist/jstree.js',
    'resources/assets/plugins/bootstrap3-wysiwyg-master/dist/bootstrap3-wysihtml5.all.js',
    'resources/assets/plugins/bootstrap-switch-master/dist/js/bootstrap-switch.js',
    'resources/assets/plugins/summernote-0.8.9/dist/summernote.js',
    'node_modules/fullcalendar/dist/fullcalendar.js',
    'node_modules/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js',
    'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
    'node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.js',
    'resources/assets/plugins/bootstrap-datetimepicker-master/build/js/bootstrap-datetimepicker.min.js',
    'node_modules/select2/dist/js/select2.js',
    'resources/assets/js/functions.js',
], 'public/js/script.js')
    .sourceMaps();

// styles
mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css');

mix.sass('resources/assets/sass/custom.scss', 'public/css');

mix.styles([
    'resources/assets/plugins/icheck-1.x/skins/square/green.css',
    'resources/assets/plugins/jstree/dist/themes/default/style.css',
    'resources/assets/plugins/bootstrap3-wysiwyg-master/dist/bootstrap3-wysihtml5.css',
    'resources/assets/plugins/summernote-0.8.9/dist/summernote.css',
    'resources/assets/plugins/bootstrap-switch-master/dist/css/bootstrap3/bootstrap-switch.css',
    'node_modules/fullcalendar/dist/fullcalendar.css',
    'node_modules/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css',
    'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css',
    'resources/assets/plugins/bootstrap-datetimepicker-master/build/css/bootstrap-datetimepicker.css',
    // 'node_modules/select2/dist/css/select2.css',
], 'public/css/plugins.css')
    .sourceMaps();

// images
mix.copy([
    'resources/assets/plugins/icheck-1.x/skins/square/green.png'
], 'public/css/green.png');
mix.copy([
    'resources/assets/plugins/icheck-1.x/skins/square/green@2x.png'
], 'public/css/green@2x.png');

mix.copy([
    'resources/assets/plugins/jstree/dist/themes/default/32px.png'
], 'public/css/32px.png');
mix.copy([
    'resources/assets/plugins/jstree/dist/themes/default/40px.png'
], 'public/css/40px.png');
mix.copy([
    'resources/assets/plugins/jstree/dist/themes/default/throbber.gif'
], 'public/css/throbber.gif');

// fonts
mix.copy([
    'resources/assets/plugins/summernote-0.8.9/dist/font',
], 'public/css/font');
